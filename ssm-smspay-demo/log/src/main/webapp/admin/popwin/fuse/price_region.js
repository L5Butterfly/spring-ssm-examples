define(function () {

	module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new PriceRegionPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'融合产品资费信息',
	        message: $('<div></div>').load('/admin/popwin/fuse/price_region.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var e = popWin.events['ok'],$page = popWin.$page;
	                	var treeIns = popWin.treeIns, nodes = treeIns.get_checked();
	                	var data = {price: $page.find('input[name="price"]').val(), 
	                			priceId: $page.find('input[name="priceId"]').val()};
	                	data.regionIds = nodes.join(',');
	                	var regionNames = [];
	                	for(var i in nodes){
	                		var node = treeIns.get_node(nodes[i]+"");
	                		regionNames.push(node.text);
	                	}
	                	data.regionNames= regionNames.join(',');
	                	
	                	/*$.post("/Product/editProductProvince.do", data, function(data){
	                		BootstrapDialog.success("修改成功");
	                		popWin.$dialog.close();
	                	});*/
	                	
	                	for(var i in e){
                			typeof e[i] == "function" && e[i](data,'ok');
                		}
	                	popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }
	        ]
	    });
	    return popWin;
	}
};

function PriceRegionPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

PriceRegionPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

PriceRegionPopWin.prototype.init = function(cb){
	var self = this,$dialog = this.$dialog,$page = this.$page = $dialog.getModalBody(), initData = self.initialData;
	typeof cb == "function" && cb();
	//创建省份树
	var $regionTree = $page.find('#regionTree').jstree({
	    'core' : {
	    	multiple : false,
	        data : function(node,cp){
	        	$.post("/Region/queryRegionList.do?"+Math.random(), {}, function(data){
	    			for(var i in data){
	        			var d = data[i];
	        			d.id = d.regionId+"";
	        			d.text = d.regionName;
	        			d.type = d.parentRegionId ? 'default' : 'leaf';
	        			d.parent= d.parentRegionId || '#';
	    			}
	    			cp(data);
	        	});
	        }
	    },
		plugins : [ "search", "types", "checkbox" ],
	    checkbox: {
	        "keep_selected_style": false,//是否默认选中
	        "three_state": true,//父子级别级联选择
	        "tie_selection": false,
	        "whole_node" : false
	    }
	});
	
	$regionTree.on('loaded.jstree', function(e, data){
		data.instance.open_all();
		self.treeIns = data.instance;
		var regionIds = initData.regionIds;
		if(!regionIds) return;
		var ids = regionIds.split(',');
		for(var i in ids){
			$regionTree.jstree().check_node(ids[i]+"");
		}
	});

}
	return module;
});