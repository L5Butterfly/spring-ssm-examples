define(function () {

	module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new StaffListPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'用户列表',
	        message: $('<div></div>').load('/admin/popwin/staff/staff_list.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var $page = popWin.$dialog.getModalBody(), grid = popWin.grid;
	                	
	                	var rowId = grid.jqGrid('getGridParam','selrow');
	            		if(!rowId){
	            			BootstrapDialog.warning("请选择一个用户");
	            			return;
	            		}
	            	
	            		var data = grid.getRowData(rowId);
	            		data.fileId = data.id;
	            		popWin.data = data;
	            		var e = popWin.events['ok'];
	            		for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'ok');
	                	}
	            		popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }
	        ]
	    });
	    return popWin;
	}
};

function StaffListPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

StaffListPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

StaffListPopWin.prototype.init = function(cb){
	var self = this,$dialog = this.$dialog,$page = this.$page = $dialog.getModalBody();
	initData = self.initialData, hanlderObj = {}, _model = initData.model || '009';
	typeof cb == "function" && cb();
	
	var jqGrid= self.grid = $("#staff_list_dataTable").jqGrid({
		url : '/staff/queryStaffList4Jq.do',
		colModel : [
				{label : '员工账号', name : 'staffCode' },
				{label : '员工姓名', name : 'staffName' },
				{label : '创建日期', name : 'createDate'},
				{label : '状态', name : '_status'}
			],
		onSelectRow: function(rowId) {
			var data = jqGrid.griddata.res[rowId - 1];
		},
		loadComplete: function(data) {
			jqGrid.griddata = data;
		},
		pager : "#staff_list_jqGridPager"
	});
}
	return module;
});