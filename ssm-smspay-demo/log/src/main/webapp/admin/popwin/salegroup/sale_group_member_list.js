define(function () {
	
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			
			var popWin = new CompanyPopWin(initialData);
			popWin.$dialog = BootstrapDialog.show({
				title:'小组成员列表',
		        message: $('<div></div>').load('/admin/popwin/salegroup/sale_group_member_list.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
						{
						    label: '确定',
						    
						    action: function(dialog) {
//						    	var _memberType = $("#memberType").val();
						    	var e = popWin.events['ok'];
						    	var grid = popWin.grid, rowId= grid.jqGrid('getGridParam','selrow'),_memberType = $("#memberType").val();
						    	
						    	if(!rowId){
						    		BootstrapDialog.warning("请选择小组成员");
						    		return;
						    	}
						    	var rowData = grid.griddata.res[rowId - 1];
						    	rowData = {staffId:rowData.staffId,staffName:rowData.staffName,status:rowData.status,_memberType:_memberType};
			                	for(var i in e){
		                			typeof e[i] == "function" && e[i](rowData, 'ok');
		                		}
						       	popWin.$dialog.close();
						    }
						},
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function CompanyPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	CompanyPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	CompanyPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		typeof cb == "function" && cb();
		var param = self.initialData;
		var postData =$.extend(param ,{groupId:param.groupId,groupName:param.groupName,status: '00A'});
		
		var jqGrid =this.grid = $("#sale_group_member_child_dataTable").jqGrid({
			url : '/SaleGroupMember/queryStaffInfoList.do',
			postData: postData,
			colModel : [
			        {label : '小组ID',name : 'groupId',hidden : true}, 
			        {label : '成员ID',name : 'staffId',hidden : true}, 
					{label : '成员名称', name : 'staffName'},
					{label : '状态', name : '_status'},
				],
				loadComplete : function(data) {
					jqGrid.griddata = data;
				},
			pager : "#sale_group_member_child_jqGridPager"
		});
		
	}	
	return module;
});

