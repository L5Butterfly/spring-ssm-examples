define(function () {

	module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new DistributorPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'通道列表',
	        message: $('<div></div>').load('/admin/popwin/channel/channel_list.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var $page = popWin.$dialog.getModalBody(), grid = popWin.grid;
	                	
	                	var rowId = grid.jqGrid('getGridParam','selrow');
	            		if(!rowId){
	            			BootstrapDialog.warning("请选择一个通道信息");
	            			return;
	            		}
	            	
	            		var data = grid.getRowData(rowId);
	            		var e = popWin.events['ok'];
	            		for(var i in e){
	                		typeof e[i] == "function" && e[i](data,'ok');
	                	}
	            		popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }
	        ]
	    });
	    return popWin;
	}
};

function DistributorPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

DistributorPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

DistributorPopWin.prototype.init = function(cb){
	var self = this,$dialog = this.$dialog,$page = this.$page = $dialog.getModalBody();
	typeof cb == "function" && cb();
	
	var jqGrid= self.grid = $("#channel_list_dataTable").jqGrid({
		url: '/Channel/queryChannelList4Jq.do',
		colModel: [
				{label : '通道编号', name : 'channelId', hidden:true},
				{label : '通道名称', name : 'channelName' },
				{label : '合作公司', name : 'companyName' },
				{label : '运营商', name : 'operName' },
				{label : '计费类型', name : '_chargeType',hidden:true},
				{label : '创建时间', name : 'createDate'}
			],
		onSelectRow: function(rowId) {
			var data = jqGrid.griddata.res[rowId - 1];
		},
		loadComplete: function(data) {
			jqGrid.griddata = data;
		},
		pager : "#channel_list_jqGridPager"
	});
}
	return module;
});