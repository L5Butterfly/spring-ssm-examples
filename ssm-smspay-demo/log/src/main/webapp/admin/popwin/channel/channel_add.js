define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new ProductPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'通道新增',
		        message: $('<div></div>').load('/admin/popwin/channel/channel_add.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [{
		                label: '保存',
		                action: function(dialog) {
		                	var e = popWin.events['ok'];
		                	var $infoForm = popWin.$page.find('.info-form');
		                	
		                	if(!$infoForm.valid()){
		                		return;
		                	}
		                	var data = $infoForm.serializeObject();
		                	var priceIds = [];
		                	$infoForm.find('.item-selected').each(function(){
		                		priceIds.push($(this).attr('data-id'));
		                	});
		                	data.priceIds = priceIds.join(',');
		                	
		                	$.post("/Channel/addChannelInfo.do", data, function(res){
		      				  var $tree=popWin.initialData.tree;
		      				  var node= { "text": data.channelName, "id": 'T02_'+res.channelId, 
		      						  type: 'T02', businessId: res.channelId};
		      				  $tree.jstree().create_node(popWin.initialData.nodeId, node);
		      				  $tree.jstree().activate_node(node);
		      				  
		      				  popWin.$dialog.close();
		      				  BootstrapDialog.success("新增成功");
		                	});
	                	}
		            },
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function ProductPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	ProductPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	ProductPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		var $infoForm = $page.find('.info-form');
		
		typeof cb == "function" && cb();
		
		$infoForm.find('input[name="companyId"]').val(self.initialData.companyId);
		$infoForm.find('input[name="companyName"]').val(self.initialData.companyName);
		
    	$infoForm.validate();
    	
		$infoForm.on('click', '.item-select-btn', function(){
			var $parent = $(this).parent();
			if($parent.hasClass('item-selected')){
				$parent.removeClass('item-selected');
			}else{
				$parent.addClass('item-selected');
			}
		});
		
	}	
	return module;
});

