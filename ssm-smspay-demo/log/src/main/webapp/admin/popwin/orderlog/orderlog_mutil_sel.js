define(function () {

	module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new OrderLogPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'订单选择列表',
	        message: $('<div></div>').load('/admin/popwin/orderlog/orderlog_mutil_sel.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var $page = popWin.$dialog.getModalBody(), grid = popWin.grid;
	                	var orderNos = popWin.initialData.orderNos;
	            		if(!orderNos){
	            			BootstrapDialog.warning("请选择订单");
	            			return;
	            		}

            			var data = {linkOrderNos:""};
            			for(var i=0;i<orderNos.length;i++ ){
            				data.linkOrderNos += orderNos[i]+",";
            			}
            			data.linkOrderNos = data.linkOrderNos.trim(',');
            			popWin.data = data;

	            		var e = popWin.events['ok'];
	            		for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'ok');
	                	}
	            		popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }
	        ]
	    });
	    return popWin;
	}
};

function OrderLogPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

OrderLogPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

OrderLogPopWin.prototype.init = function(cb){
	var self = this,$dialog = this.$dialog,$page = this.$page = $dialog.getModalBody();
	initData = self.initialData, hanlderObj = {}, _model = initData.model || '009';
	typeof cb == "function" && cb();
	initData.orderNos = initData.linkOrderNos.length>0 ? initData.linkOrderNos.split(','):[];
 	var $searchForm = $page.find('.search-form');
 	$searchForm.find("input[name='productId']").val(initData.productId);
 	$searchForm.find("input[name='productName']").val(initData.productName);
 	$searchForm.find("input[name='mobileOrImsi']").val(initData.mobileOrImsi);
 	$searchForm.find("input[name='chargeType']").val(initData.chargeType);
	var reqUrl = initData.chargeType == 'C02'? '/OrderLogMonth/queryOrderLogMonthList.do':'/OrderLog/queryOrderLogList4Jq.do';
	
	var queryParam = $searchForm.serializeObject();
	$.extend(queryParam,{orderStatusAry:"'S07','S09','S10'"});
	var colModelLabel = [
							{label : '订单号', name : 'orderNo' },
							{label : '手机号', name : 'mobilePhone' },
							{label : 'IMSI', name : 'imsi' },
							{label : '渠道', name : 'distributorName',sortable : false},
							{label : '产品', name : 'productName',sortable : false},
							{label : '融合产品', name : 'fuseProductName',sortable : false, hidden : true},
							{label : '资费(元)', name : '_amount',sortable : false},
							{label : '省份', name : '_regionId',sortable : false,width:60},
							{label : '支付状态', name : '_orderStatus'},
							{label : 'SMT', name : '_smtFlag',sortable : false,hidden : true},
							{label : '订单时间', name : 'createDate'},
							{label : '更新时间', name : 'updateDate'}
						];
	
	if(queryParam.chargeType=="C02"){
		queryParam["phoneOrImsi"] = queryParam.mobileOrImsi;
		colModelLabel[6].name = 'regionName';
		colModelLabel[7].name = '_orderMonthStatus';
		colModelLabel[8].name = '_isSmt';
	}
	
	var jqGrid= self.grid = $("#orderlog_list_dataTable").jqGrid({
		url: reqUrl,
		postData:queryParam,
		//datatype: 'local',
		colModel: colModelLabel,
		multiselect: true,//复选框  
		loadComplete: function(data) {
			if(data.total>0){
				jqGrid.griddata = data;
				var rowIds = jqGrid.jqGrid('getDataIDs');
				for(var k=0; k<rowIds.length; k++) {
				   var curRowData = jqGrid.jqGrid('getRowData', rowIds[k]);
				   for(i=0;i<initData.orderNos.length;i++){				   
					   if(curRowData.orderNo == initData.orderNos[i]){
						   jqGrid.setSelection(rowIds[k], false); 
					   }
				   }
				}
			}
		},
		//表格行选中设置-取消
		onSelectRow: function (rowid, status) {
			var curRowData=jqGrid.jqGrid('getRowData', rowid);
			if(status){
				initData.orderNos.push(curRowData.orderNo);
			}else{
				arrayRemoveByValue(initData.orderNos,curRowData.orderNo);
			}
		},
		onSelectAll:function(rowid, status) {
			for(var i=0;i<rowid.length;i++){
				var curRowData=jqGrid.jqGrid('getRowData', rowid[i]);
				if(status){
					arrayRemoveByValue(initData.orderNos,curRowData.orderNo);
					initData.disIds.push(curRowData.distributorId);
				}else{
					arrayRemoveByValue(initData.orderNos,curRowData.orderNo);
				}
			}
		},
		pager : "#orderlog_list_jqGridPager"
	});
	
	$page.on("click",".btn-search",function(e){
		if (!$searchForm.valid()) {
	        return;
	    }
		queryParam = $searchForm.serializeObject();
		if(queryParam.chargeType=="C02")
			queryParam["phoneOrImsi"] = queryParam.mobileOrImsi;
		var postData = jqGrid.jqGrid("getGridParam", "postData") || {};
	    $.extend(postData,queryParam);
	    jqGrid.jqGrid("setGridParam", {search:true,datatype : 'json'}).trigger("reloadGrid", {page: 1}); 
	    return false;
	});
	
}
	return module;
});