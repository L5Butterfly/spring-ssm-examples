define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new ProductTicketPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'产品账单开票',
		        message: $('<div></div>').load('/admin/popwin/dialog/product_ticket.html'),
		        //打开完成时执行
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		              {
		                label: '确认',
		                action: function(dialog) {
		                   	// 执行业务处理
		                	var e = popWin.events['ok'];
		                	var $form = popWin.$page.find('.info-form');
		                	var data = $form.serializeObject();	
		                	var postData={ids:popWin.initialData.ids,status:popWin.initialData.billStatus,
		                			oldStatus:popWin.initialData.oldBillStatus,realBillTime:data.realBillTime};
		                	$.post("/Bills/approveProductBillInfo.do", postData, function(e){			    								    			    	 			    		
		                		BootstrapDialog.success("账单开票,成功"+e+"条");	    			    	
	        		    		popWin.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
	        		    		popWin.$dialog.close();	
	        		    	});	
		                }
		            },
		            {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	
	/**
	 * 创建一个弹窗对象,初始化数据
	 */
	function ProductTicketPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	/**
	 * 将传过来的事件存起来,ok点击事件
	 */
	ProductTicketPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	/**
	 * 绑定元素的监听事件
	 * @param cb
	 */
	ProductTicketPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody(),initData=self.initialData;
		var $form = $page.find('.detail_form'),validator = $form.validate();
		typeof cb == "function" && cb();
		// 定义一系列的点击监听事件，在窗口打开完成时【onshown】，完成绑定。
		$form.on('click',".btn-class-ok",function(e){});
	}	
	return module;
});

