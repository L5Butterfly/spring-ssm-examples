/**
 *  邮件弹窗选择审核模块
 */

define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new ProductTicketPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'账单审核',
		        message: $('<div></div>').load('/admin/popwin/dialog/distrbutor_email.html'),
		        //打开完成时执行
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		              {
		                label: '确认',
		                action: function(dialog) {
		                   	// 执行业务处理
		                	debugger;
		                	var $form = popWin.$page.find('.info-form');
		                	var emailToIds = [];emailCcIds = [];
		                	$form.find('.email-tos-list .email-item').each(function(){
		      				  if($(this).is(":checked")){
		      					emailToIds.push($(this).val());
		      				  }
		      			  	});
		                	$form.find('.email-ccs-list .email-item').each(function(){
			      				  if($(this).is(":checked")){
			      					emailCcIds.push($(this).val());
			      				  }
			      			});
		                	emailToIds = emailToIds.join(',');
		                	emailCcIds = emailCcIds.join(',');
		                	var e = popWin.events['ok'];
		                	var data = $form.serializeObject();	
		                	var reqData={};
		                	reqData.emailTos=emailToIds;
		                	reqData.emailCcs=emailCcIds;
		                	reqData.remark=data.remark;
		                	reqData.ids=popWin.initialData.ids;
		                	reqData.afterStatus=popWin.initialData.newStatus;
		                	reqData.beforeStatus=popWin.initialData.oldBillStatus;
		                	reqData.newStatus=popWin.initialData.newStatus;
		                	reqData.oldStatus=popWin.initialData.oldBillStatus;
		                	reqData.emailToIdArrs=emailToIds;
		                	reqData.emailCcIdArrs=emailCcIds;
		                	//jQuery.extend() 函数用于将一个或多个对象的内容合并到目标对象。
		                	//$.extend(postData,popWin.initialData,{emailToIds:emailToIds,emailCcIds:emailCcIds});
		                	console.log(reqData);
		                	$.post("/Bills/approveDistributorBill3.do", reqData, function(e){			    								    			    	 			    		
		                		BootstrapDialog.success("操作完成,成功"+e+"条");	    			    	
	        		    	    popWin.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
	        		    		popWin.$dialog.close();
	        		    	});	
		                }
		            },
		            {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	
	function ProductTicketPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	ProductTicketPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	ProductTicketPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody(),initData=self.initialData;
		var $form = $page.find('.info-form'),validator = $form.validate();
		typeof cb == "function" && cb();
		// 定义一系列的点击监听事件，在窗口打开完成时【onshown】，完成绑定。
		$form.on('click',".btn-class-ok",function(e){});
		//页面form表单数据绑定
		$page.find('h5').append(self.initialData.popMsg==""?"邮件通知：渠道负责人提交=>商务审核":self.initialData.popMsg);
		
		// 邮件接收人
		$.post('/approve/getEmail.do', {billType:initData.billType,sendType:'T01',billStatus:initData.oldBillStatus}, function(data){
			$form.find('.email-tos-list').empty();
			 if(!data) return;
			 for(var i in data){
				 $form.find('.email-tos-list').append('<label class="checkbox-inline control-label email-label">'+
				  '<input type="checkbox" class="email-item" value="'+data[i].approveId+'"> '+data[i].staffName+
				'</label>');
			 }
		 });
		
		//ajax同步请求
		/*$.ajax({
			url : '',
	        data:{},
	        async : false,
	        type : "POST",
	        success : function (data){}
	    });	*/
		
		// 邮件超送人   initData.billStatus
		$.post('/approve/getEmail.do',{billType:initData.billType,sendType:'T02',billStatus:'B02'}, function(data){
			$form.find('.email-ccs-list').empty();
			 if(!data) return;
			 for(var i in data){
				 $form.find('.email-ccs-list').append('<label class="checkbox-inline control-label email-label">'+
				  '<input type="checkbox" class="email-item" value="'+data[i].approveId+'"> '+data[i].staffName+
				'</label>');
			 }
		 });
	}	
	return module;
});