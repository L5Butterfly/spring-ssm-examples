define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new ProductMultiPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'产品信息列表',
		        message: $('<div></div>').load('/admin/popwin/product/product_multi_list.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
						{
						    label: '确定',
						    action: function(dialog) {
						    	var e = popWin.events['ok'];
						    	var selectedData = popWin.selectedData;
						    	
						    	if(isEmptyObj(selectedData)){
						    		BootstrapDialog.warning("请选择产品信息");
						    		return;
						    	}
						    	
						    	var productIds=[],productNames=[];
						    	for(var key in selectedData){
						    		productIds.push(key);
						    		productNames.push(selectedData[key]);
						    	}
						    	
			                	for(var i in e){
		                			typeof e[i] == "function" && e[i]({productIds: productIds.join(','), productNames: productNames.join(',')}, 'ok');
		                		}
						       	popWin.$dialog.close();
						    }
						},
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function ProductMultiPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		this.selectedData = {};
		var ids = initialData.productIds, names = initialData.productNames;
		if(ids){
			var idArr = ids.split(','), nameArr = names.split(',');
			for(var i=0;i<idArr.length; i++){
				this.selectedData[idArr[i]] = nameArr[i];
			}
		}
		module.win.push(this);
		return this;
	}

	ProductMultiPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	ProductMultiPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		typeof cb == "function" && cb();
		var param = self.initialData, selectedData = self.selectedData;		
		var postData =$.extend(param ,{approveStatus: 'A02'});
		
		var jqGrid =this.grid = $page.find("#product_pop_mgr_dataTable").jqGrid({
			url : '/Product/queryProductList4Jq.do',
			multiselect: true,
			postData: postData,
			colModel : [
			        {label : '产品Id',name : 'productId',hidden : true,key: true}, 
					{label : '产品名称', name : 'productName'},
					{label : '通道名称', name : 'channelName' },
					{label : '公司名称', name : 'companyName' },
					{label : '日限额(元)', name : '_dayPriceLimit'},
					{label : '月限额(元)', name : '_monthPriceLimit'},
					{label : '上量开始', name : 'startTime'},
					{label : '上量结束', name : 'endTime'}
				],
				onSelectRow : function(rowId, status) {
					if(status){
						var data = jqGrid.jqGrid('getRowData',rowId);
						selectedData[rowId] = data['productName'];
					}else{
						delete selectedData[rowId];
					}
				},
				loadComplete : function(data) {
					for(var key in selectedData)
					jqGrid.jqGrid('setSelection',key);
				},
			pager : "#product_pop_mgr_jqGridPager"
		});
		
	}	
	return module;
});

