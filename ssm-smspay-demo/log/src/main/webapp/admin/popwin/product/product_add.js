define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new ProductPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'通道新增',
		        message: $('<div></div>').load('/admin/popwin/product/product_add.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function ProductPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	ProductPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	ProductPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		var $btns = $page.find('#project_btns'), $tabs = $page.find('#projectTab'), $contents= $page.find('#project_content');
		var $infoForm = $contents.find('.info-form'), $priceContent= $contents.find('.price-content-cfg'), treeIns = null;
		
		typeof cb == "function" && cb();
		
		var validator = $infoForm.validate();
		
		$infoForm.find('input[name="channelId"]').val(self.initialData.channelId);
		$infoForm.find('input[name="channelName"]').val(self.initialData.channelName);
		
		$btns.on('click', '.prev-btn', function(){
			var index = $(this).parent().attr('data-index'), prev= parseInt(index)-1;
			//页签栏控制
			$tabs.find('li:eq('+prev+')').addClass("gray").removeClass("blue");
			$tabs.find('li:eq('+index+')').find('img').attr("src","/resources/images/gray_gray.png");
			$tabs.find('li:eq('+prev+')').find('img').attr("src","/resources/images/blue_gray.png");
			$btns.find('.next-btn').removeClass('hide');
			$btns.find('.save-btn').addClass('hide');
			
			if(prev == 1){
				$(this).addClass('hide');
			}
			
			$contents.find('.content-cfg').removeClass('active in');
			$contents.find('.content-cfg:eq('+(prev-1)+')').addClass('active in');
			$(this).parent().attr('data-index', prev);
		});
		
		$btns.on('click', '.next-btn', function(){
			var index = $(this).parent().attr('data-index'), next = parseInt(index)+1;
			
			if(!$infoForm.valid()){
				return;
			}
			
			var validFlag = true;
        	
			$priceContent.find('.price-form').each(function(){
        		if(!$(this).valid() && validFlag){
        			validFlag = false;
        		}
        	});
        	
        	if(!validFlag){
        		return ;
        	}
        	
			//页签栏控制
			$tabs.find('li:eq('+index+')').addClass("blue").removeClass("gray");
			$tabs.find('li:eq('+index+')').find('img').attr("src","/resources/images/blue_blue.png");
			$tabs.find('li:eq('+next+')').find('img').attr("src","/resources/images/blue_gray.png");
			$btns.find('.prev-btn').removeClass('hide');
			
			if(next == 3){
				$(this).addClass('hide');
				$btns.find('.save-btn').removeClass('hide');
			}
			
			$contents.find('.content-cfg').removeClass('active in');
			$contents.find('.content-cfg:eq('+index+')').addClass('active in');
			$(this).parent().attr('data-index', next);
		});
		
		$btns.on('click', '.save-btn', function(){
			  var data = $infoForm.serializeObject();
			  var priceArr= [];
			  $priceContent.find('.price-form').each(function(){
	          		var data = $(this).serializeObject();
	          		data.price = data.price*100;
	          		priceArr.push(data);
	          	});
			  
			  for(var i in data){
				  if(!data[i]){
					  delete data[i];
				  }
			  }
			  data.priceList= priceArr;
			  data.regionIds= treeIns.get_checked().join(',');
			  data.dayPriceLimit = (data.dayPriceLimit||0)*100;
	    	  data.monthPriceLimit = (data.monthPriceLimit||0)*100;
	    	  data.settlementRatio = (data.settlementRatio||0)*100;
			  $.ajax({ 
                  type: "POST",  
                  url: "/Product/addProduct.do", 
                  contentType: "application/json",
                  dataType: 'json',
                  data: JSON.stringify(data), 
                  success:function(res){ 
                	  self.$dialog.close();
    				  var $tree=self.initialData.tree;
    				  var node= { "text": data.productName, "id": 'T03_'+res.productId, 
    						  type: 'T03', businessId: res.productId, icon: 'icon-audit-pass'};
    				  $tree.jstree().create_node(self.initialData.nodeId, node);
    				  $tree.jstree().activate_node(node);
    				  
    				  BootstrapDialog.success("新增成功");      
                  } 
               }); 
		});
		
		//新增资费
		$page.on('click', '.add-price-btn', function(){
			var $form = $($page.find('.template').html());
			$priceContent.append($form);
			$form.validate();
		});
		
		$page.on('click', '.del-price-icon', function(){
			$(this).parent().remove();
		});
		
		//创建省份树
		var $provinceTree = $page.find('#provinceTree').jstree({
		    'core' : {
		    	multiple : false,
		        data : function(node,cp){
		        	$.post("/Region/queryRegionList.do?"+Math.random(), {}, function(data){
		    			for(var i in data){
		        			var d = data[i];
		        			d.id = d.regionId+"";
		        			d.text = d.regionName;
		        			d.type = d.parentRegionId ? 'default' : 'leaf';
		        			d.parent= d.parentRegionId || '#';
		    			}
		    			cp(data);
		        	});
		        }
		    },
			plugins : [ "search", "types", "checkbox" ],
		    checkbox: {
		        "keep_selected_style": false,//是否默认选中
		        "three_state": true,//父子级别级联选择
		        "tie_selection": false,
		        "whole_node" : false
		    }
		});
		
		$provinceTree.on('loaded.jstree', function(e, data){
			data.instance.open_all();
			treeIns = data.instance;
		});
	}	
	return module;
});

