define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new RegionEditPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'省份修改',
		        message: $('<div></div>').load('/admin/popwin/product/region_edit.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [{
		                label: '保存',
		                action: function(dialog) {
		                	var e = popWin.events['ok'];
		                	var treeIns = popWin.treeIns, nodes = treeIns.get_checked();
		                	var data = {productId: popWin.initialData.productId};
		                	data.regionIds = nodes.join(',');
		                	var regionNames = [];
		                	for(var i in nodes){
		                		var node = treeIns.get_node(nodes[i]+"");
		                		regionNames.push(node.text);
		                	}
		                	
		                	if('T' != popWin.initialData.isFuse){
		                		$.post("/Product/editProductProvince.do", data, function(data){
			                		BootstrapDialog.success("修改成功");
			                		popWin.$dialog.close();
			                	});
		                	}
		                	
		                	var regionData = {regionIds: data.regionIds, names: regionNames.join(',')};
		                	for(var i in e){
	                			typeof e[i] == "function" && e[i](regionData,'ok');
	                		}
		                	popWin.$dialog.close();
	                	}
		            },
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function RegionEditPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	RegionEditPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	RegionEditPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		
		typeof cb == "function" && cb();
		
		//创建省份树
		var $provinceTree = $page.find('#provinceTree').jstree({
		    'core' : {
		    	multiple : false,
		    	//check_callback: true,
		        data : function(node,cp){
		        	$.post("/Region/queryRegionList.do?"+Math.random(), {}, function(data){
		        		var region_info=Object();
		    			for(var i in data){
		        			var d = data[i];
		        			d.id = d.regionId+"";
		        			d.text = d.regionName;
		        			d.type = d.parentRegionId ? 'default' : 'leaf';
		        			d.parent= d.parentRegionId || '#';
		        			region_info[d.regionName]=d.regionId+"";
		    			}
		    			project.dictionary["DIC_REGION_INFO"]=region_info;
		    			cp(data);
		        	});
		        }
		    },
			plugins : [ "search", "types", "checkbox"],
		    checkbox: {
		        "keep_selected_style": false,//是否默认选中
		        "three_state": true,//父子级别级联选择
		        "tie_selection": false,
		        "whole_node" : false
		    }
		});
		
		$provinceTree.on('loaded.jstree', function(e, data){
			data.instance.open_all();
			self.treeIns = data.instance;
			var productId = self.initialData.productId;
			if('T' == self.initialData.isFuse){
				var regionIds = self.initialData.regionIds;
				if(!regionIds) return;
				var ids = regionIds.split(',');
				for(var i in ids){
    				$provinceTree.jstree().check_node(ids[i]+"");
    			}
				return;
			}
			$.post("/Product/queryProductProvinceList.do", {productId: productId}, function(data){
				var regionNames = [];
    			for(var i in data){
    				$provinceTree.jstree().check_node(data[i].regionId+"");
    				regionNames.push(data[i].regionName+"");
    			}
    			console.log(regionNames.join(','));
    			$page.find('textarea[name="regionNames"]').val(regionNames.join(','));
        	});
		});
		
		//点击导入省份信息，刷新省份树的省份信息
		$page.find('.btn_import_region').click(function(){
			var pro=$page.find('textarea[name="regionNames"]').val().replace(/，/ig,',').split(',');
			$('#provinceTree').jstree('check_node',100);
			$('#provinceTree').jstree('uncheck_node',100);
			for(var i in pro){
				$provinceTree.jstree().check_node(project.dictionary.DIC_REGION_INFO[pro[i]]);
			}
			BootstrapDialog.success("导入成功");
		});
	}	
	return module;
});

