define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new PriceEditPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'资费修改',
		        message: $('<div></div>').load('/admin/popwin/product/price_edit.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [{
		                label: '保存',
		                action: function(dialog) {
		                	var e = popWin.events['ok'];
		                	var dataArr = [], validFlag = true,priceNames =[];
		                	
		                	popWin.$page.find('.price-content-cfg').find('.price-form').each(function(){
		                		if(!$(this).valid() && validFlag){
		                			validFlag = false;
		                		}
		                		var data = $(this).serializeObject();
		                		data.price = data.price*100;
		                		data.productId= popWin.initialData.productId;
		                		dataArr.push(data);
		                		priceNames.push(data.priceName);
		                	});
		                	
		                	if(!validFlag){
		                		return ;
		                	}
		                	//刪除全部
		                	if(dataArr.length <1) {
		                		dataArr.push({productId: popWin.initialData.productId});
		                	}
		                	$.ajax({ 
		                        type: "POST",  
		                        url: "/Product/editProductPrice.do", 
		                        contentType: "application/json",               
		                        data: JSON.stringify(dataArr), 
		                        success: function(data){ 
		                        	popWin.$dialog.close();
		                        	BootstrapDialog.success("修改成功");           
		                        } 
		                     }); 
		                	
		                	var priceData = {names: priceNames.join(',')};
		                	for(var i in e){
	                			typeof e[i] == "function" && e[i](priceData,'ok');
	                		}
	                	}
		            },
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function PriceEditPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	PriceEditPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	PriceEditPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		
		typeof cb == "function" && cb();
		
		//新增资费
		$page.on('click', '.add-price-btn', function(){
			var $form = $($page.find('.template').html());
			$page.find('.price-content-cfg').append($form);
			$form.validate();
		});
		
		$page.on('click', '.del-price-icon', function(){
			$(this).parent().remove();
		});
		
		$.post("/Product/queryProductPriceList.do", {productId: self.initialData.productId}, function(data){
			if(!data){
				return;
			}
			
			for(var i in data){
				var $form = $($page.find('.template').html());
				$form.deSerializeObject(data[i]);
				$form.find('input[name="price"]').val(data[i].price/100);
				$page.find('.price-content-cfg').append($form);
				$form.validate();
			}
    	});
	}	
	return module;
});

