define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new CompanyPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'产品信息列表',
		        message: $('<div></div>').load('/admin/popwin/product/product_mutil_sel.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
						{
						    label: '确定',
						    action: function(dialog) {
						    	var e = popWin.events['ok'];
						    	var grid = popWin.grid; 
						    	var proIds = popWin.initialData.proIds;
			                	var proNames = popWin.initialData.proNames;
			            		if(!proIds){
			            			BootstrapDialog.warning("请选择一个渠道信息");
			            			return;
			            		}

		            			var data = {productIds:"",productNames:""};
		            			for(var i=0;i<proIds.length;i++ ){
		            				data.productIds += proIds[i]+",";
					            	data.productNames += proNames[i]+",";	
		            			}
		            			data.productIds = data.productIds.trim(',');
		            			data.productNames = data.productNames.trim(',');
		            			popWin.data = data;
						    	
			                	for(var i in e){
		                			typeof e[i] == "function" && e[i](popWin.data, 'ok');
		                		}
						       	popWin.$dialog.close();
						    }
						},
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};
	
	function CompanyPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	CompanyPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	CompanyPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		typeof cb == "function" && cb();
		var param = self.initialData;
		delete param.productId;
		delete param.productName;
		
		param.proIds = param.productIds.length>0 ? param.productIds.split(','):[];
		param.proNames = param.productNames.length>0 ? param.productNames.split(','):[];
		
		var postData =$.extend(param ,{approveStatus: 'A02',noOperId:"true", 
			productType: param.productType, businessName: param.businessName});
		
		var jqGrid =this.grid = $("#product_pop_mgr_dataTable").jqGrid({
			url : '/Product/queryProductList4Jq.do',
			postData: postData,
			colModel : [
			        {label : '产品Id',name : 'productId',hidden : true}, 
					{label : '产品名称', name : 'productName'},
					{label : '业务名称', name : 'businessName'},
					{label : '产品类型', name : '_productType'},
					{label : '接口类型', name : '_apiType'},
					{label : '通道名称', name : 'channelName' },
					{label : '公司名称', name : 'companyName' }
				],
				multiselect: true,//复选框  
				loadComplete: function(data) {
					jqGrid.griddata = data;
					var rowIds = jqGrid.jqGrid('getDataIDs');
					for(var k=0; k<rowIds.length; k++) {
					   var curRowData = jqGrid.jqGrid('getRowData', rowIds[k]);
					   for(i=0;i<param.proIds.length;i++){				   
						   if(curRowData.productId == param.proIds[i]){
							   jqGrid.setSelection(rowIds[k], false); 
						   }
					   }
					}
				},
				//表格行选中设置-取消
				onSelectRow: function (rowid, status) {
					var curRowData=jqGrid.jqGrid('getRowData', rowid);
					if(status){
						param.proIds.push(curRowData.productId);
						param.proNames.push(curRowData.productName);	
					}else{
						arrayRemoveByValue(param.proIds,curRowData.productId);
						arrayRemoveByValue(param.proNames,curRowData.productName);
					}
				},
				onSelectAll:function(rowid, status) {
					for(var i=0;i<rowid.length;i++){
						var curRowData=jqGrid.jqGrid('getRowData', rowid[i]);
						if(status){
							arrayRemoveByValue(param.proIds,curRowData.productId);
							arrayRemoveByValue(param.proNames,curRowData.productName);
							param.proIds.push(curRowData.productId);
							param.proNames.push(curRowData.productName);
						}else{
							arrayRemoveByValue(param.proIds,curRowData.productId);
							arrayRemoveByValue(param.proNames,curRowData.productName);
						}
					}
				},
			pager : "#product_pop_mgr_jqGridPager"
		});
		
	}	
	return module;
});

