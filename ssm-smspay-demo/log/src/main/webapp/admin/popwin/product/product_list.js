define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new CompanyPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'产品信息列表',
		        message: $('<div></div>').load('/admin/popwin/product/product_list.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
						{
						    label: '确定',
						    action: function(dialog) {
						    	var e = popWin.events['ok'];
						    	var grid = popWin.grid, rowId= grid.jqGrid('getGridParam','selrow');
						    	
						    	if(!rowId){
						    		BootstrapDialog.warning("请选择产品信息");
						    		return;
						    	}
						    	
						    	var rowData = grid.griddata.res[rowId - 1];
			                	for(var i in e){
		                			typeof e[i] == "function" && e[i](rowData, 'ok');
		                		}
						       	popWin.$dialog.close();
						    }
						},
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function CompanyPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	CompanyPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	CompanyPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		typeof cb == "function" && cb();
		var param = self.initialData;
		delete param.productId;
		delete param.productName;
		var postData =$.extend(param ,{approveStatus: 'A02',noOperId:"true", 
			productType: param.productType, businessName: param.businessName});
		
		var jqGrid =this.grid = $("#product_pop_mgr_dataTable").jqGrid({
			url : '/Product/queryProductList4Jq.do',
			postData: postData,
			colModel : [
			        {label : '产品Id',name : 'productId',hidden : true}, 
					{label : '产品名称', name : 'productName'},
					{label : '业务名称', name : 'businessName'},
					{label : '产品类型', name : '_productType'},
					{label : '接口类型', name : '_apiType'},
					{label : '通道名称', name : 'channelName' },
					{label : '公司名称', name : 'companyName' }
				],
				loadComplete : function(data) {
					jqGrid.griddata = data;
				},
			pager : "#product_pop_mgr_jqGridPager"
		});
		
	}	
	return module;
});

