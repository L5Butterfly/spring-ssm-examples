define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new CompanyPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'合作公司列表',
		        message: $('<div></div>').load('/admin/popwin/company/company_list.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
						{
						    label: '确定',
						    action: function(dialog) {
						    	var e = popWin.events['ok'];
						    	var grid = popWin.grid, rowId= grid.jqGrid('getGridParam','selrow');
						    	
						    	if(!rowId){
						    		BootstrapDialog.warning("请选择合作公司");
						    		return;
						    	}
						    	
						    	var rowData = grid.griddata.res[rowId - 1];
			                	for(var i in e){
		                			typeof e[i] == "function" && e[i](rowData, 'ok');
		                		}
						       	popWin.$dialog.close();
						    }
						},
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function CompanyPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	CompanyPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	CompanyPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		var $form = $page.find('.detail_form'), validator = $form.validate();
		
		typeof cb == "function" && cb();
		
		$page.find('input[name="companyType"]').val(self.initialData.companyType);
		var jqGrid =this.grid = $("#distributor_company_mgr_dataTable").jqGrid({
			url : '/Company/queryCompanyList4Jq.do',
			postData: {companyType: self.initialData.companyType},
			colModel : [
					{label : '公司名称', name : 'companyName'},
					{label : '联系人', name : 'contactName' },
					{label : '联系号码', name : 'contactPhone'},
					{label : '联系邮箱', name : 'contactEmail'},
					{label : '账号类型', name : '_accountType'},
					{label : '绑定账号', name : 'payAccount'},
					{label : '税率(%)', name : '_taxRate'},
					{label : '创建人', name : 'operName', sortable : false},
					{label : '创建时间', name : 'createDate'}
				],
			onSelectRow : function(rowId) {
				var data = jqGrid.griddata.res[rowId - 1];
				// 清除之前的校验信息
				validator.resetForm();
				
				data.taxRate = data._taxRate;
		    	$form.deSerializeObject(data).status('show');
			},
			loadComplete : function(data) {
				jqGrid.griddata = data;
				$form.status('show');
			},
			pager : "#distributor_company_mgr_jqGridPager"
		});
		
		$form.on('click',".btn-class-ok",function(e){
		    if (!$form.valid()) {
	            return;
	        }
			
		    var data = $form.serializeObject();
		    data.taxRate = parseInt(data.taxRate * 100);
		    $.post("/Company/addCompany.do", data, function(){
				jqGrid.jqGrid("setGridParam", {search:true}).trigger("reloadGrid", [{ page: 1}]); 
				BootstrapDialog.success("新增成功");
				$form.status('show');
			});
		});
		
	}	
	return module;
});

