define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new ProductPayBillPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'产品账单收款记录',
		        message: $('<div></div>').load('/admin/popwin/finance/product_paybill_list.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        //自定义按钮(关闭，取消，确定)
		        buttons : [
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	
	function ProductPayBillPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	ProductPayBillPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	ProductPayBillPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody(),initData=self.initialData;
		var $searchForm=$page.find('.search-form');
		var $form = $page.find('.detail_form'),validator = $form.validate();
		// 禁用表单搜索字段操作
		$searchForm.find('.form-control').attr('disabled','true');
		typeof cb == "function" && cb();
		//反序列化初始数据到搜索表单
		$searchForm.deSerializeObject(initData);
		$form.deSerializeObject({"billId":initData.billId,"productId":initData.productId,"realFee":initData.realFee,"productName":initData.productName,
			"otherCompany":initData.companyName,"billMonth":initData.billMonth,"saleName":initData.saleName});

		var postData  = $searchForm.serializeObject();
		
		var jqGrid =this.grid = $("#single_product_bill_mgr_dataTable").jqGrid({
			url : '/Bills/queryProductPayBillList4Jq.do',
			postData: postData,
			colModel : [
					{label : '产品名称', name : 'productName'},
					{label : '我方公司', name : 'ourCompany' },
					{label : '合作公司', name : 'otherCompany'},
					{label : '收款金额(元)', name : '_paymentFee'},
					{label : '收款日期', name : 'paymentDate'},
					{label : '创建时间', name : 'createDate'},
					{label : '更新时间', name : 'updateTime',hidden : true},
					{label : '操作人', name : 'operName', sortable : false},
					{label : '状态', name : '_status'}
				],
			onSelectRow : function(rowId) {
				var data = jqGrid.griddata.res[rowId - 1];
				// 清除之前的校验信息
				validator.resetForm();
				// 序列表单数据(金额特殊处理)
				data.realFee=postData.realFee;
				data.paymentFee=data._paymentFee;
		    	$form.deSerializeObject(data).status('show');
			},
			loadComplete: function (data) {
				jqGrid.griddata = data;
				$form.status('show');
				if(data && data.totalObj){
					$(".ui-jqgrid-sdiv").show();
					 var totalObj = data.totalObj;
	                 $(this).footerData("set", {
	                	 'productName':'<font color=\'red\'>合计<font>',
	                	 '_paymentFee':  "<font color='red'>"+totalObj._paymentFee+"<font>"});
				}else{
					$(".ui-jqgrid-sdiv").hide();
				}			
			},
			rowNum : 20,
			footerrow:true,
			pager : "#single_product_bill_mgr_jqGridPager"
		});
		
		
		//表单提交操作
		$form.on('click',".btn-class-ok",function(e){
		    if (!$form.valid()) {
	            return;
	        }
		    //表单状态，home.js封装处理的
		    var formStatus = $form.status(), 
		    method = formStatus == "new" ? 'addProductPayBill.do' : 'editProductPayBill.do',
		    msg = $form.status() == "new" ? '添加成功!' : '修改成功!';
		    reqUrl="/Bills/"+method;
		    //获取表单数据
		    var data = $form.serializeObject();
		    data.billMonth=postData.billMonth;
		    data.saleName=postData.saleName;
		    // 金额处理,数据库存分，后端字段Long型，前端传小数后端识别不了,报空异常。
		    data.paymentFee = parseInt(data.paymentFee * 1000/10);
		    console.log("post_data:"+data);
		    $.post(reqUrl, data, function(){
				jqGrid.jqGrid("setGridParam", {search:true}).trigger("reloadGrid", [{ page: 1}]); 
				BootstrapDialog.success(msg);
				$form.status('show');
			});
		});
	}	
	return module;
});

