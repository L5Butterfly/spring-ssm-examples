define(function () {
	project.addDic('DIC_MONTH_WEEK_TYPE','DIC_MONTH_WEEK_TYPE_NEW',{v:'',n:'无'});
	module = {
		win:[],
		createPopWin : function(cb,initialData){
			var popWin = new ProductBillPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				//title:initialData.operType=='edit' ? "产品账单修改" : "产品账单新增",
				title:function(){
					if(initialData.operType=='edit'){
						return '产品账单修改';
					}else{
						return '产品账单新增';
					}	
		        },
		        message: $('<div></div>').load('/admin/popwin/finance/product_bill_add.html'),
		        onshown : function(){
		        	if(popWin)popWin.init(cb);   
		        },
		        onhide:function(){
	            	popWin = null;
		        },
		        buttons : [		        			           
		        ]
		    });
		    return popWin;
		}
	};

	function ProductBillPopWin(initialData){
		this.$page = null;
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	ProductBillPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	ProductBillPopWin.prototype.init = function(cb){
		var self = this;
		var $dialog = this.$dialog;
		var $page = this.$page = $dialog.getModalBody();		
		var $infoForm = $page.find(".info-form");
		var $btns = $page.find('#project_btns');
		typeof cb == "function" && cb();		
		$infoForm.validate();
		console.log(self.initialData);
		if(self.initialData.operType=='edit'){
			self.initialData.settleRate=self.initialData.settleRate/100;
			self.initialData.usedFee=self.initialData.usedFee/100;
			self.initialData.realFee=self.initialData.realFee/100;
			$infoForm.deSerializeObject(self.initialData);
			$infoForm.find('.form-control').prop('disabled','true');			 
			$infoForm.find('.real-fee').prop('disabled','');
			$infoForm.find('.status').prop('disabled','');
		}
				
		$page.find('.product-select').click(function(){
			require(["/popwin/product/product_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $page.find('input[name="productId"]').val(data.productId);
		        	 $page.find('input[name="productName"]').val(data.productName);
		        	 $page.find('input[name="channelName"]').val(data.channelName);
		        	 $page.find('input[name="companyName"]').val(data.companyName);
		         });
			});
		});
		$btns.on('click', '.save-btn', function(){		
			if(!$infoForm.valid()){
				return;
			}
			var $form = $page.find(".info-form");
			var formStatus = $form.status();
		    var data = $form.serializeObject();		
		    //js的小数在运算时以二进制进行运算，一些特定的无法以0/1方式有限存储的值在计算时会出现误差
		    data.realFee = data._realFee*1000/10;
		    if(self.initialData.operType=='add'){
		    	$.post("/Bills/addProductBill.do", data, function(e){			    								    			    	
		    		$dialog.close();
		    		BootstrapDialog.success("操作成功");		
		    		self.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
			});
		    }
		    else if(self.initialData.operType=='edit'){
		    	data.id=self.initialData.id;
		    	data.billStatus=self.initialData.billStatus;
		    	/*if(!data.realFee){
		    		console.log("realFee:"+data.realFee);
		    		BootstrapDialog.success("实结金额不能空");
		    		return false;
		    	}*/
		    	self.initialData.realFee=data.realFee;
		    	$.post("/Bills/editProductBill.do", data, function(e){			    								    			    	
		    		$dialog.close();
		    		BootstrapDialog.error("操作成功");			    			    	
		    		self.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
		    	});		    	
		    }		    
		});	
		
		$btns.on('click', '.cancle-btn', function(){
			$dialog.close();
		});
		
	}
	return module;
});

