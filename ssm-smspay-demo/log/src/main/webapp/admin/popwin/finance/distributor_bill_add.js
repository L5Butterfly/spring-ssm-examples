define(function () {
	project.addDic('DIC_MONTH_WEEK_TYPE','DIC_MONTH_WEEK_TYPE_NEW',{v:" ",n:"选择时间"});
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new DistributorBillsPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:initialData.operType=='add' ? '新增渠道账单' : '编辑渠道账单',
		        message: $('<div></div>').load('/admin/popwin/finance/distributor_bill_add.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		                   {
		                label: '保存',
		                cssClass : "btn-primary",
		                //icon:"glyphicon glyphicon-floppy-saved",
		                action: function(dialog) {
		                	var e = popWin.events['ok'];
		                	var $form = popWin.$page.find('.info-form');
		        			var formStatus = $form.status();
		        		    var data = $form.serializeObject();	
		        		    data.billWeek=data.billWeek.replace("选择时间",null);
		        		    //js的小数在运算时以二进制进行运算，一些特定的无法以0/1方式有限存储的值在计算时会出现误差
		        		    data.realFee = data._realFee*1000/10;
		        		    if(popWin.initialData.operType=='add'){
		        		    	/*if(isEmptyObj(data){BootstrapDialog.warning('请输入必填项');return;}*/
		        		    	//form表单字段前端校验
		        		    	if(!$form.valid()){return;}
		        		    	$.post("/Bills/addDistributorBill.do", data, function(e){			    								    			    	    		
		        		    		BootstrapDialog.success("新增成功");	
		        		    		popWin.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
		        		    		popWin.$dialog.close();
		        		    	});	  
		        		    }else if(popWin.initialData.operType=='edit'){
		        		    	data.id=popWin.initialData.id;
		        		    	data.billStatus=popWin.initialData.billStatus;
		        		    	$.post("/Bills/editDistributorBill.do", data, function(e){			    								    			    	 			    		
		        		    		BootstrapDialog.success("编辑成功");
		        		    		popWin.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
		        		    		popWin.$dialog.close();
		        		    	});	
		        		    }
	                	}
		            },
		              {
		                label: '关闭',
		                //通过bootstrap的样式添加图标按钮
		                cssClass : "btn-primary",
		                //icon : "glyphicon glyphicon-remove",
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function DistributorBillsPopWin(initialData){
		this.$page = null;
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	DistributorBillsPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	DistributorBillsPopWin.prototype.init = function(cb){
		var self = this,
		$dialog = this.$dialog,
		$page = this.$page = $dialog.getModalBody();
		var $infoForm = $page.find('.info-form');
		console.log($infoForm);
		typeof cb == "function" && cb();
    	$infoForm.validate();
    	console.log(self.initialData);
    	
    	// 编辑账单
    	if(self.initialData.operType=='edit'){
			self.initialData.settleRate=self.initialData.settleRate/100;
			self.initialData.usedFee=self.initialData.usedFee/100;
			self.initialData.realFee=self.initialData.realFee/100;
			self.initialData.smtFee=self.initialData._smtFee;
			self.initialData.channelName=self.initialData.productName;
			$infoForm.deSerializeObject(self.initialData);
			var realFeeObj= $infoForm.find('input[name="realFee"]');
			$infoForm.find('.form-control').prop('disabled','true');			 
			$infoForm.find('.real-fee').prop('disabled','');
			$infoForm.find('.bill-status').prop('disabled','');
		}

    	//选择数据绑定
		$page.find('.distributor-select').click(function(){
			require(["/popwin/distributor/distrbutor_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $page.find('input[name="distributorId"]').val(data.distributorId);
		        	 $page.find('input[name="distributorName"]').val(data.distributorName);
		         });
			});
		});
			

		$page.find('.product-select').click(function(){
			require(["/popwin/product/product_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $page.find('input[name="productId"]').val(data.productId);
		        	 $page.find('input[name="productName"]').val(data.productName);
		        	 $page.find('input[name="channelName"]').val(data.channelName);
		         });
			});
		});
	
	}	
	return module;
});

