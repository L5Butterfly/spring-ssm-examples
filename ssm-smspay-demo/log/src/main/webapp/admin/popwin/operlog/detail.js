define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new OperationDetailPopWin(initialData);
			popWin.$dialog = BootstrapDialog.show({
				title:'操作明细',
		        message: $('<div></div>').load('/popwin/operlog/detail.html'),
		        onshown : function(){popWin && popWin.init(cb);},
		        onhide:function(){ popWin = null; },
		        buttons : [{label: '关闭',  action: function(dialog) { popWin.$dialog.close();}}]
		    });
		    return popWin;
		}
	};

	function OperationDetailPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	OperationDetailPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}
	OperationDetailPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		//回调
		typeof cb == "function" && cb();
		var param = self.initialData.param;
		self.initialData.param = param.replace(/ ,/g,"\n");
		$page.find('.detail_form').deSerializeObject(self.initialData);
	}
	
	return module;
});