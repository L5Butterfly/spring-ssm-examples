define(function () {

	project.addDic('DIC_DISTRIBUTOR_TYPE','DIC_DISTRIBUTOR_TYPE_NEW',{v:'',n:'选择渠道类型'});
	module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new DistributorPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'渠道选择列表',
	        message: $('<div></div>').load('/admin/popwin/distributor/distrbutor_mutil_sel.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var $page = popWin.$dialog.getModalBody(), grid = popWin.grid;
	                	var disIds = popWin.initialData.disIds;
	                	var disNames = popWin.initialData.disNames;
	            		if(!disIds){
	            			BootstrapDialog.warning("请选择一个渠道信息");
	            			return;
	            		}

            			var data = {distributorIds:"",distributorNames:""};
            			for(var i=0;i<disIds.length;i++ ){
            				data.distributorIds += disIds[i]+",";
			            	data.distributorNames += disNames[i]+",";	
            			}
            			data.distributorIds = data.distributorIds.trim(',');
            			data.distributorNames = data.distributorNames.trim(',');
            			popWin.data = data;

	            		var e = popWin.events['ok'];
	            		for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'ok');
	                	}
	            		popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }
	        ]
	    });
	    return popWin;
	}
};

function DistributorPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

DistributorPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

DistributorPopWin.prototype.init = function(cb){
	var self = this,$dialog = this.$dialog,$page = this.$page = $dialog.getModalBody();
	initData = self.initialData, hanlderObj = {}, _model = initData.model || '009';
	typeof cb == "function" && cb();
	initData.disIds = initData.distributorIds.length>0 ? initData.distributorIds.split(','):[];
	initData.disNames = initData.distributorNames.length>0 ? initData.distributorNames.split(','):[];
	
	var jqGrid= self.grid = $("#distrbutor_list_dataTable").jqGrid({
		url: '/Distributor/queryDistributorListInfo4Jq.do',
		colModel: [
				{label : '渠道编号', name : 'distributorId',hidden:true},
				{label : '渠道名称', name : 'distributorName' },
				{label : '合作公司', name : 'companyName' },
				{label : '操作人', name : 'operName' },
				{label : '创建时间', name : 'createDate'}
			],
		multiselect: true,//复选框  
		loadComplete: function(data) {
			jqGrid.griddata = data;
			var rowIds = jqGrid.jqGrid('getDataIDs');
			for(var k=0; k<rowIds.length; k++) {
			   var curRowData = jqGrid.jqGrid('getRowData', rowIds[k]);
			   for(i=0;i<initData.disIds.length;i++){				   
				   if(curRowData.distributorId == initData.disIds[i]){
					   jqGrid.setSelection(rowIds[k], false); 
				   }
			   }
			}
		},
		//表格行选中设置-取消
		onSelectRow: function (rowid, status) {
			var curRowData=jqGrid.jqGrid('getRowData', rowid);
			if(status){
				initData.disIds.push(curRowData.distributorId);
				initData.disNames.push(curRowData.distributorName);	
			}else{
				arrayRemoveByValue(initData.disIds,curRowData.distributorId);
				arrayRemoveByValue(initData.disNames,curRowData.distributorName);
			}
		},
		onSelectAll:function(rowid, status) {
			for(var i=0;i<rowid.length;i++){
				var curRowData=jqGrid.jqGrid('getRowData', rowid[i]);
				if(status){
					arrayRemoveByValue(initData.disIds,curRowData.distributorId);
					arrayRemoveByValue(initData.disNames,curRowData.distributorName);
					initData.disIds.push(curRowData.distributorId);
					initData.disNames.push(curRowData.distributorName);
				}else{
					arrayRemoveByValue(initData.disIds,curRowData.distributorId);
					arrayRemoveByValue(initData.disNames,curRowData.distributorName);
				}
			}
		},
		pager : "#distrbutor_list_jqGridPager"
	});
}
	return module;
});