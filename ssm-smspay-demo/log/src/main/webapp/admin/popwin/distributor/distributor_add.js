define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new DistributorPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:'渠道新增',
		        message: $('<div></div>').load('/admin/popwin/distributor/distributor_add.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function DistributorPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	DistributorPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	DistributorPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody();
		var $btns = $page.find('#project_btns'), $tabs = $page.find('#projectTab'), $contents= $page.find('#project_content');
		var $infoForm = $contents.find('.info-form'), $productForm = $contents.find('.product-form');
		$page.parent().parent().css('width', '60%');
		typeof cb == "function" && cb();
		
		$infoForm.validate();
		var validator = $productForm.validate();
		
		$btns.on('click', '.prev-btn', function(){
			var index = $(this).parent().attr('data-index'), prev= parseInt(index)-1;
			//页签栏控制
			$tabs.find('li:eq('+prev+')').addClass("gray").removeClass("blue");
			$tabs.find('li:eq('+index+')').find('img').attr("src","/resources/images/gray_gray.png");
			$tabs.find('li:eq('+prev+')').find('img').attr("src","/resources/images/blue_gray.png");
			$btns.find('.next-btn').removeClass('hide');
			$btns.find('.save-btn').addClass('hide');
			
			if(prev == 1){
				$(this).addClass('hide');
			}
			
			$contents.find('.content-cfg').removeClass('active in');
			$contents.find('.content-cfg:eq('+(prev-1)+')').addClass('active in');
			$(this).parent().attr('data-index', prev);
		});
		
		$btns.on('click', '.next-btn', function(){
			var index = $(this).parent().attr('data-index'), next = parseInt(index)+1;
			
			if(!$infoForm.valid()){
				return;
			}
			//页签栏控制
			$tabs.find('li:eq('+index+')').addClass("blue").removeClass("gray");
			$tabs.find('li:eq('+index+')').find('img').attr("src","/resources/images/blue_blue.png");
			$tabs.find('li:eq('+next+')').find('img').attr("src","/resources/images/blue_gray.png");
			$btns.find('.prev-btn').removeClass('hide');
			
			if(next == 2){
				$(this).addClass('hide');
				$btns.find('.save-btn').removeClass('hide');
			}
			
			$contents.find('.content-cfg').removeClass('active in');
			$contents.find('.content-cfg:eq('+index+')').addClass('active in');
			$(this).parent().attr('data-index', next);
		});
		
		$btns.on('click', '.save-btn', function(){
			  if(!$infoForm.valid() || !$productForm.valid()){
				  return;
			  }
			  
			  var infoData = $infoForm.serializeObject();
			  var productData = $productForm.serializeObject();
			  
			  var priceIds = [];
			  $productForm.find('.price-item').each(function(){
				  if($(this).is(":checked")){
					  priceIds.push($(this).val());
				  }
			  });
			  productData.priceIds = priceIds.join(',');
			  productData.billRate = productData.billRate*100;
			  $.post('/Distributor/addDistributor.do', infoData, function(res){
				  productData.distributorId = res.distributorId;
				  $.post('/Distributor/addDistributorProduct.do', productData, function(res1){
					  self.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
					  self.$dialog.close();
					  BootstrapDialog.success("新增成功");
				  });
			  });
		});
		
		
		$page.find('.company-select').click(function(){
			require(["/popwin/company/company_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {companyType: 'C02'}).on('ok', function(data){
		        	 $page.find('input[name="companyId"]').val(data.companyId);
		        	 $page.find('input[name="companyName"]').val(data.companyName);
		        	 
		        	 $page.find('.company-form').deSerializeObject(data).status('show');
		         });
			});
		});
		
		$page.find('.product-select').click(function(){
			require(["/popwin/product/product_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $page.find('input[name="productId"]').val(data.productId);
		        	 $page.find('input[name="productName"]').val(data.productName);
		        	 
		        	 $.post('Product/queryProductPriceList.do', {productId: data.productId}, function(data){
		        		 $page.find('.price-list').empty();
		        		 if(!data) return;
		        		 
		        		 for(var i in data){
		        			 $page.find('.price-list').append('<label class="checkbox-inline control-label price-label">'+
							  '<input type="checkbox" class="price-item" value="'+data[i].id+'"> '+(data[i].price/100)+'元'+
							'</label>');
		        		 }
		        	 });
		         });
			});
		});
		
		$page.find('select[name="enableSmt"]').change(function(){
			validator.resetForm();
			$page.find('.smt-cfg').val('').prop('disabled', $(this).val() == 'F');
		});
	}	
	return module;
});

