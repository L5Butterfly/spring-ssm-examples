define(function () {

	project.addDic('DIC_DISTRIBUTOR_TYPE','DIC_DISTRIBUTOR_TYPE_NEW',{v:'',n:'选择渠道类型'});
	
	module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new DistributorPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'渠道选择列表',
	        message: $('<div></div>').load('/admin/popwin/distributor/distrbutor_list.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var $page = popWin.$dialog.getModalBody(), grid = popWin.grid;
	                	
	                	var rowId = grid.jqGrid('getGridParam','selrow');
	            		if(!rowId){
	            			BootstrapDialog.warning("请选择一个渠道信息");
	            			return;
	            		}
	            	
	            		var data = grid.getRowData(rowId);
	            		data.fileId = data.id;
	            		popWin.data = data;
	            		var e = popWin.events['ok'];
	            		for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'ok');
	                	}
	            		popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }
	        ]
	    });
	    return popWin;
	}
};

function DistributorPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

DistributorPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

DistributorPopWin.prototype.init = function(cb){
	var self = this,$dialog = this.$dialog,$page = this.$page = $dialog.getModalBody();
	initData = self.initialData, hanlderObj = {}, _model = initData.model || '009';
	typeof cb == "function" && cb();
	
	var jqGrid= self.grid = $("#distrbutor_list_dataTable").jqGrid({
		url: '/Distributor/queryDistributorListInfo4Jq.do',
		colModel: [
				{label : '渠道编号', name : 'distributorId',hidden:true},
				{label : '商务编号', name : 'saleId',hidden:true},
				{label : '渠道名称', name : 'distributorName' },
				{label : '合作公司', name : 'companyName' },
				{label : '创建人', name : 'operName' },
				{label : '创建时间', name : 'createDate'}
			],
		onSelectRow: function(rowId) {
			var data = jqGrid.griddata.res[rowId - 1];
		},
		loadComplete: function(data) {
			jqGrid.griddata = data;
		},
		pager : "#distrbutor_list_jqGridPager"
	});
}
	return module;
});