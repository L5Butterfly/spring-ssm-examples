define(function () {

module = {
	win:[],
	createPopWin : function(cb,initialData){
		var popWin = new UploadPopWin(initialData);
		popWin.$dialog = BootstrapDialog.show({
			title:'文件上传',
	        message: $('<div></div>').load('/admin/popwin/upload/upload.html'),
	        onshown : function(){
	        	if(popWin)popWin.init(cb);   
	        },
	        onhide:function(){
            	popWin = null;
	        },
	        buttons : [
	        	{
	                label: '确定',
	                action: function(dialog) {
	                	var $page = popWin.$dialog.getModalBody(), grid = popWin.grid;
	                	
	                	var rowId = grid.jqGrid('getGridParam','selrow');
	            		if(!rowId){
	            			BootstrapDialog.warning("您还没有选择");
	            			return;
	            		}
	            	
	            		var data = grid.getRowData(rowId);
	            		data.fileId = data.id;
	            		popWin.data = data;
	            		var e = popWin.events['ok'];
	            		for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'ok');
	                	}
	            		popWin.$dialog.close();
                	}
	            }, {
	                label: '取消',
	                action: function(dialog) {
	                	var e = popWin.events['cancel'];
	                	for(var i in e){
	                		typeof e[i] == "function" && e[i](popWin.data,'cancel');
	                	}
	                   	popWin.$dialog.close();
	                }
	            }

	        ]
	    });
	    return popWin;
	}
};

function UploadPopWin(initialData){
	this.$page = null;
	this.$dialog = null;
	this.events = {};
	this.id = Math.random();
	this.initialData = initialData;
	module.win.push(this);
	return this;
}

UploadPopWin.prototype.on = function(event,func){
	this.events[event] = this.events[event] == undefined ? [] : this.events[event];
	this.events[event].push(func);
	return this;
}

UploadPopWin.prototype.init = function(cb){
	var self = this,  $dialog = this.$dialog,$page = this.$page = $dialog.getModalBody(), 
	initData = self.initialData, hanlderObj = {}, _model = initData.model || '009';
	
	var jqGrid = self.grid = $("#cfg_fileUpload_dataGrid").jqGrid({
		url: '/fileMgr/qryFileList.do',
		postData: {model: _model},
		colModel: [
				{label : '文件ID', name : 'id', hidden : true, key: true },
				{label : '文件名称', name : 'fileName' },
				{label : '文件类型', name : '_fileType' },
				{label : '文件大小', name : '_fileSize'},
				{label : '上传时间', name : 'createDate'},
				{label : '操作', name : '',sortable : false, formatter : function(c, o, row) {
					return "<div class='col-lg-5 col-sm-5 col-md-5'><div class='file-del' data-rowid='"+o.rowId+"' title='删除'>删除</div></div>" + 
					"<div class='col-lg-5 col-sm-5 col-md-5'><div><a href='"+row.showPath+"' target='_blank'><font class='file-show'>查看</font></a></div></div>";
				}}
			],
		onSelectRow: function(rowId) {
			
		},
		loadComplete: function(data) {
			if(jqGrid)
			jqGrid.jqGrid('setSelection', initData.fileId);
		},
		pager : "#cfg_fileUpload_jqGridPager"
	});
	
	//上传
	$page.find('.btn_upload').on('click', function(){
		if(!$page.find('.upload_file').val()){
			BootstrapDialog.warning('请选择文件');
			return;
		}
		
		upload();
	});
	
	//删除
	$page.on('click', '.file-del', function(){
		var fileId = $(this).attr('data-rowid');
		$.post('/fileMgr/delFileById.do', {fileId: fileId}, function(e){
			BootstrapDialog.success('删除成功');
			jqGrid.jqGrid("setGridParam", {search: true}).trigger("reloadGrid", [{page: 1}]);
		});
	});
	
	var upload=function(){ 
		var uploadId = "up"+(new Date()).getTime();
		var url = "/upload/fileUpload.do?uploadId=" + uploadId+"&operId="+model.user().staffId()+"&operType=P00&model="+_model;
		$page.find('#uploadForm').attr("action",url).submit();
		hanlderObj[uploadId] = (function(param){
			return window.setInterval(function(){ queryProcess(param); },1000);
		})(uploadId);
		
		(function(param){
			window.setTimeout(function(){ stopUpload(param);},10000);//10秒钟以后终止查询 
		})(uploadId);
	},
	queryProcess= function(uploadId){
		$.ajax({  
	        dataType:"json",
	        url: "/upload/qryProgress.do?uploadId="+ uploadId,
	        success:function(data){
	        	$page.find('.upload_div').show();
	        	var $tips = $page.find('.upload_text'),$progressbar=$page.find('.progress-bar');
	        	try{
	        		$tips.html(""); 
	            	var code = data.code;
	            	if(code =='0'){
	            		$progressbar.css({width: "100%",'background-color': "#5cb85c"});
	            		$tips.html(data.msg);
	            		stopUpload(uploadId);
	            		reloadGrid();
	            		return;
	            	}else if(code =='12'){
	            		$progressbar.css({width: "100%",'background-color': "#CCC"});
	            		$tips.html(data.msg);
	            	}else{
	            		stopUpload(uploadId);
	            		$progressbar.css({width: "100%",'background-color': "#CCC"});
	            		$tips.html("<font color='red'>"+data.msg+"</font>");
	            		return;
	            	}
	        	}catch(e){
	        		if(data.msg){
	        			$progressbar.css({width: "100%",'background-color': "#CCC"});
	        			$tips.html("<font color='red'>"+data.msg+"</font>");
	        		}
	        		stopUpload(uploadId);
	        	}
	        },
		    error: function(e){
		    	console.log(e);
		    }
	    }) 
	},
	stopUpload= function(uploadId){
		clearInterval(hanlderObj[uploadId]);
		delete hanlderObj[uploadId];
	},
	reloadGrid = function(){
		jqGrid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page: 1}]);
	};
}

	return module;
});