define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new billDetailPopWin(initialData);
			//console.log(initialData);
			popWin.$dialog = BootstrapDialog.show({
				title:'账单详情',
		        message: $('<div></div>').load('/admin/popwin/statistics/bill_detail_list.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [						
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function billDetailPopWin(initialData){
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	billDetailPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	billDetailPopWin.prototype.init = function(cb){
		var self = this,$dialog = this.$dialog, $page = this.$page = $dialog.getModalBody(),$searchForm=$page.find('.search-form');
		$page.find('.form-control').attr('disabled','true');
		
		typeof cb == "function" && cb();
		
		var param = self.initialData;
		$searchForm.deSerializeObject(self.initialData);
		
		var reqUrl=param.chargeType=='C02'?'/MonthOrderSummary/querySummaryList4Jq.do':'/OrderSummary/querySummaryList4Jq.do';		
		
		var postData  = $searchForm.serializeObject();
		
		var colModel = [
				        {label : '统计日期', name : 'showDate',sortable : false},
						{label : '渠道名称', name : 'distributorName',sortable : false},
						{label : '产品名称', name : 'productName' ,sortable : false},
						{label : '通道名称', name : 'channelName',sortable : false},
						{label : '商务人员', name : 'saleName',sortable : false},
						{label : '省份名称', name : 'regionName',sortable : false}
					];
		
		if(param.proOrDisBill!=null){
			$.extend(postData,{proOrDisBill:param.proOrDisBill});
			if(param.proOrDisBill=="P"){
				colModel.push({label : '通道信息费', name : '_succAmount',sortable : false});
			}else{
				colModel.push({label : '通道信息费', name : '_succAmount',sortable : false});
				colModel.push({label : '渠道信息费（元）', name : 'afterSmtAmount',sortable : true});
			};
		}else{
			colModel.push({label : '信息费', name : '_succAmount',sortable : false});
		}
		
		var jqGrid =this.grid = $("#bill_detail_pop_dataTable").jqGrid({
			url : reqUrl,
			postData: postData,
			colModel : colModel,
				loadComplete: function (data) {
					if(data && data.records > 0){
						$(".ui-jqgrid-sdiv").show();
						 var totalObj = data.totalObj;
		                 $(this).footerData("set", {'_succAmount': totalObj._succAmount});
		                 if(param.proOrDisBill=="D")
		                	 $(this).footerData("set", {'afterSmtAmount': totalObj.afterSmtAmount});
					}else{
						$(".ui-jqgrid-sdiv").hide();
					}
				},
//				loadComplete : function(data) {
//					jqGrid.griddata = data;
//				},
				footerrow:true,
				pager : "#bill_detail_pop_jqGridPager"
		});
	}	
	return module;
});

