define(function () {
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new MonthStaticticsAddPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
		        message: $('<div></div>').load('/admin/popwin/statistics/choice_export.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		                   {
		                label: '导出',
		                action: function(dialog) {
		                	var e = popWin.events['ok'];
		                	var $form = popWin.$page.find('.info-form');
		        			var formStatus = $form.status();
		        		    var data = $form.serializeObject();	
		        		    var url = '/Bills/choiceExportBills.do?1=1';
		        		    var param = $.extend(data,initialData.operType);
		        		    for(var i in param){
			                    url += "&"+i+"="+encodeURIComponent(encodeURIComponent(param[i]));
			                }
		        		    window.open(url);
	                	}
		            },
		              {
		                label: '关闭',
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function MonthStaticticsAddPopWin(initialData){
		this.$page = null;
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	MonthStaticticsAddPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	MonthStaticticsAddPopWin.prototype.init = function(cb){
		var self = this,
		$dialog = this.$dialog,
		$page = this.$page = $dialog.getModalBody();
		var $infoForm = $page.find('.info-form');
		console.log($infoForm);
		typeof cb == "function" && cb();
    	$infoForm.validate();
    	console.log(self.initialData);
	}	
	return module;
});

