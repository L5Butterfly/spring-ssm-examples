define(function () {
	project.addDic('DIC_MONTH_WEEK_TYPE','DIC_MONTH_WEEK_TYPE_NEW',{v:" ",n:"选择时间"});
	module = {
		win: [],
		createPopWin : function(cb,initialData){
			var popWin = new DistributorBillsPopWin(initialData);
			
			popWin.$dialog = BootstrapDialog.show({
				title:initialData.operType=='add' ? '新增包月统计' : '编辑包月统计',
		        message: $('<div></div>').load('/admin/popwin/statistics/month_summary_add.html'),
		        onshown : function(){
					if(popWin)popWin.init(cb);
		        },
		        onhide:function(){ popWin = null; },
		        buttons : [
		                   {
		                label: '保存',
		                cssClass : "btn-primary",
		                action: function(dialog) {
		                	var e = popWin.events['ok'];
		                	var $form = popWin.$page.find('.info-form');
		        			var formStatus = $form.status();
		        		    var data = $form.serializeObject();	
		        		   
		        		    if(popWin.initialData.operType=='add'){
		        		    	if(isEmptyObj(data.productId)|| isEmptyObj(data.distributorId)|| isEmptyObj(data.regionId) || isEmptyObj(data)){
		        		    		BootstrapDialog.warning('必填字段不能为空！');
		        		    		return;
		        		    	}
		        		    	if(!$form.valid()){return;}
		        		    	$.post("/MonthOrderSummary/addPhoneReport.do", data, function(e){			    								    			    	    		
		        		    		BootstrapDialog.success("新增成功");	
		        		    		popWin.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
		        		    		popWin.$dialog.close();
		        		    	});	  
		        		    }else if(popWin.initialData.operType=='edit'){
		        		    	data.status=popWin.initialData.status;
		        		    	data.uniqueId=popWin.initialData.uniqueId;
		        		    	$.post("MonthOrderSummary/editPhoneReport.do", data, function(e){			    								    			    	 			    		
		        		    		BootstrapDialog.success("编辑成功");
		        		    		popWin.initialData.grid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page : 1}]);
		        		    		popWin.$dialog.close();
		        		    	});	
		        		    }
	                	}
		            },
		              {
		                label: '关闭',
		                cssClass : "btn-primary",
		                action: function(dialog) {
		                   	popWin.$dialog.close();
		                }
		            }
		        ]
		    });
		    return popWin;
		}
	};

	function DistributorBillsPopWin(initialData){
		this.$page = null;
		this.$dialog = null;
		this.events = {};
		this.id = Math.random();
		this.initialData = initialData;
		module.win.push(this);
		return this;
	}

	DistributorBillsPopWin.prototype.on = function(event,func){
		this.events[event] = this.events[event] == undefined ? [] : this.events[event];
		this.events[event].push(func);
		return this;
	}

	DistributorBillsPopWin.prototype.init = function(cb){
		var self = this,
		$dialog = this.$dialog,
		$page = this.$page = $dialog.getModalBody();
		var $infoForm = $page.find('.info-form');
		console.log($infoForm);
		typeof cb == "function" && cb();
    	$infoForm.validate();
    	console.log(self.initialData);
    	
    	// 编辑账单
    	if(self.initialData.operType=='edit'){
    			self.initialData.distributorName=self.initialData.distributorName;
    			self.initialData.productName=self.initialData.productName;
    			self.initialData.univalence=self.initialData._succAmount/self.initialData.succNum;
    			self.initialData.orderDate=self.initialData.showDate;
    			$infoForm.deSerializeObject(self.initialData);
    			$infoForm.find('.distributor-select').prop('disabled','true');
    			$infoForm.find('.product-select').prop('disabled','true');
    			$infoForm.find('.regionId').prop('disabled','true');
    			$infoForm.find('.orderDate').prop('disabled','true');
    			$infoForm.find('.status').prop('disabled','');
    			
		}

		$page.find('.distributor-select').click(function(){
			require(["/popwin/distributor/distrbutor_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $page.find('input[name="distributorId"]').val(data.distributorId);
		        	 $page.find('input[name="distributorName"]').val(data.distributorName);
		        	 $page.find('input[name="saleId"]').val(data.saleId);
		         });
			});
		});
			
		$page.find('.product-select').click(function(){
			require(["/popwin/product/product_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $page.find('input[name="productId"]').val(data.productId);
		        	 $page.find('input[name="productName"]').val(data.productName);
		        	 $page.find('input[name="channelName"]').val(data.channelName);
		        	 $page.find('input[name="productSaleId"]').val(data.saleId);
		        	 
		         });
			});
		});
	
	}	
	return module;
});

