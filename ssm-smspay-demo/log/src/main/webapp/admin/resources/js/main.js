define([],function(){
    var module = {};

    module.startSysInfoRefresh = function(){
        $.post('/getSystemInfo.do', {type: '002'}, function(res){
            var s = res.split("-");
            var companyName = s[0];
            var systemStartTime = new Date(parseInt(s[1]));
            $("#admin_system_info").html(companyName);
            setInterval(function(){
                var a = (new Date()).getTime() - systemStartTime.getTime();
                var runningTime = formatTime(parseInt(a/1000));
                $("#runningtime").html(systemStartTime+"启动， 运行："+runningTime);
            },1000);
        });
    };
    
    module.checkLogin = function(){
        $.post("/getLoginInfo.do",{},function(user){
            console.log("loginok");
            module.startSysInfoRefresh();
            
            model.user(ko.mapping.fromJS(new User(user)));
            
            $(document).trigger('loginok');
            console.log("loginok");
            
            //多个角色权限
            if(user.roleList.length > 1 && !user.roleId){
            	 module.choseRole();
            	 return;
            }
            
            //如果密码失效时间小于当前时间,弹出提示密码修改pop-win
            var expDate = user.staffPo.passExpDate.toDate();
            if(expDate<(new Date()).getTime()){
            	BootstrapDialog.alert("您是首次登录或者密码已失效,需要重新修改密码",function(result){
            		var btn = $($(".pop-win-contoller")[0]);
        			btn.find("input[name='isExp']").val("T").trigger('click');
            	});
            }
        });
    };
    
    module.choseRole = function (){
        var user = ko.mapping.toJS(model.user);
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_INFO,
            title: '请选择一个角色',
            message: function(dialogRef){
                var dom = "";
                user.roleList.every(function(r){
                    dom += '<button class="btn btn-default user-role-button" style="margin-left:5px;height:100px;margin-top:5px;" data-role-id='+r.roleId+' data-role-name="'+r.roleName+'"><div class="login-user-avatar"></div><div class="text-overflow">'+r.roleName+'</div></button>';
                    return true;
                });
                var button = $(dom);
                button.on('click', {dialogRef: dialogRef}, function(e){
                    var roleId = $(e.currentTarget).data('role-id');
                    var roleName = $(e.currentTarget).data('role-name');
                    $.post("/changeRole.do",{roleId:roleId},function(d){
                        dialogRef.close();
                        location.reload();
                    });
                });
                return button ; 
            }
        });
    };

    module.logout = function(){
        $.post('/logout.do',{},function(){ location.href = "/login.do"; });
    };
    return module;
});