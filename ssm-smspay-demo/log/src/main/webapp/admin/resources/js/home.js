'use strict';
(function($){
    $.fn.reloadPage = function (obj) {
        var $self = $(this);
        var id = $self.attr('id');
        var menuId = $self.data('menu-id');
        if(!id.startsWith('tabs_modules_')){
            console.error('not a project module tab!');
            return ;
        }
        //nav_tabs_modules_cfg_product_offer
        $('#nav_'+id).find('.my-nav-tab-remove-icon').trigger('click');
        setTimeout(function(){
            $('#'+menuId).trigger('click');
        },0);
        return this;
    };
    $.fn.close = function (obj) {
        var $self = $(this);
        var id = $self.attr('id');
        var menuId = $self.data('menu-id');
        if(!id.startsWith('tabs_modules_')){ 
            console.error('not a project module tab!');
            return ;
        }
        //nav_tabs_modules_cfg_product_offer
        $('#nav_'+id).find('.my-nav-tab-remove-icon').trigger('click');
        return this;
    };
})(jQuery);



(function($){
    // form 增强
    $.fn.isDataChanged = function(){
        var backup = this.data('backup') || {};
        var now = this.serializeObject();
        for(var i in backup){
            if(backup[i]!= now[i])
                return true;
        }
        for(var i in now){
            if(backup[i]!= now[i])
                return true;
        }
        return false;
    }
    $.fn.privilege = function(data){
        var privilege = ko.mapping.toJS(model.user()).privilege;
        for(var i in data){
            var allow = privilege[i] !== undefined;
            var obj = data[i];
            if(obj instanceof jQuery){
                data[i].attr('data-privilege',allow).prop('disabled',!allow);
            }else if(typeof obj == "function"){
                obj(i,allow);
            }
            
        }
        return this;
    };

    $.fn.status = function (status) {
        if(status == null)
            return this.data('jquery-form-status');
        var privilege = this.data('privilege'), $addBtn= this.find(".btn-class-new"),
        $editBtn=this.find(".btn-class-edit"), $cancelBtn = this.find(".btn-class-cancel"),
        $okBtn = this.find(".btn-class-ok"), $approveBtn = this.find(".btn-class-approve"),
        $linkBtn = this.find(".btn-class-link"), $unLink = this.find(".del-product-btn");
        switch(status){
            case "new":
                if(privilege && !privilege.create){
                    console.error('dont have create privilege,return!');
                    break;
                }
                var inputs = this.find("input").not('.manual_handle');
                inputs.prop("disabled", true);
                inputs.filter('.creatable').prop("disabled", false);
                var textarea=this.find("textarea").not('.manual_handle');
                textarea.prop("disabled", true);
                textarea.filter('.creatable').prop('disabled',false);
                var selects = this.find("select").not('.manual_handle');
                selects.prop("disabled", true);
                selects.filter('.creatable').prop("disabled", false);
                var checkboxs=this.find("input[type='checkbox']").not('.manual_handle');
                checkboxs.prop("disabled", true);
                checkboxs.filter('.creatable').prop("disabled", false);
                var radios=this.find("input[type='radio']").not('.manual_handle');;
                radios.prop("disabled", true);
                radios.filter('.creatable').prop("disabled", false);
                hasPrivilege($editBtn) && $editBtn.hide();
                hasPrivilege($addBtn) && $addBtn.hide();
                hasPrivilege($approveBtn) && $approveBtn.hide();
                hasPrivilege($linkBtn) && $linkBtn.hide();
                hasPrivilege($unLink) && $unLink.hide();
                $okBtn.show();
                $cancelBtn.show();
                this.find(".hide-when-form-status-new").show();
                break;
            case "edit":
                if(privilege && !privilege.update){
                    console.error('dont have update privilege,return!');
                    break;
                }
                var inputs = this.find("input").not('.manual_handle');
                inputs.prop("disabled", true);
                inputs.filter('.editable').prop('disabled',false);
                var textarea=this.find("textarea").not('.manual_handle');
                textarea.prop("disabled", true);
                textarea.filter('.editable').prop('disabled',false);
                var selects = this.find("select").not('.manual_handle');;
                selects.prop("disabled", true);
                selects.filter('.editable').prop("disabled", false);
                var checkboxs=this.find("input[type='checkbox']").not('.manual_handle');
                checkboxs.prop("disabled", true);
                checkboxs.filter('.editable').prop("disabled", false);
                var radios=this.find("input[type='radio']").not('.manual_handle');
                radios.prop("disabled", true);
                radios.filter('.editable').prop("disabled", false);
                hasPrivilege($editBtn) && $editBtn.hide();
                hasPrivilege($addBtn) && $addBtn.hide();
                hasPrivilege($approveBtn) && $approveBtn.hide();
                hasPrivilege($linkBtn) && $linkBtn.hide();
                hasPrivilege($unLink) && $unLink.hide();
                $okBtn.show();
                $cancelBtn.show();
                this.find(".hide-when-form-status-edit").show();
                break;
            case "approve":
                if(privilege && !privilege.update){
                    console.error('dont have update privilege,return!');
                    break;
                }
                var inputs = this.find("input").not('.manual_handle');
                inputs.prop("disabled", true);
                inputs.filter('.approveable').prop('disabled',false);
                var textarea=this.find("textarea").not('.manual_handle');
                textarea.prop("disabled", true);
                textarea.filter('.approveable').prop('disabled',false);
                var selects = this.find("select").not('.manual_handle');;
                selects.prop("disabled", true);
                selects.filter('.approveable').prop("disabled", false);
                var checkboxs=this.find("input[type='checkbox']").not('.manual_handle');
                checkboxs.prop("disabled", true);
                checkboxs.filter('.approveable').prop("disabled", false);
                var radios=this.find("input[type='radio']").not('.manual_handle');
                radios.prop("disabled", true);
                radios.filter('.approveable').prop("disabled", false);
                hasPrivilege($editBtn) && $editBtn.hide();
                hasPrivilege($addBtn) && $addBtn.hide();
                hasPrivilege($approveBtn) && $approveBtn.hide();
                hasPrivilege($linkBtn) && $linkBtn.hide();
                hasPrivilege($unLink) && $unLink.hide();
                $okBtn.show();
                $cancelBtn.show();
                this.find(".hide-when-form-status-edit").show();
                break;
            case "link":
                if(privilege && !privilege.update){
                    console.error('dont have update privilege,return!');
                    break;
                }
                var inputs = this.find("input").not('.manual_handle');
                inputs.prop("disabled", true);
                inputs.filter('.linkable').prop('disabled',false);
                var textarea=this.find("textarea").not('.manual_handle');
                textarea.prop("disabled", true);
                textarea.filter('.linkable').prop('disabled',false);
                var selects = this.find("select").not('.manual_handle');;
                selects.prop("disabled", true);
                selects.filter('.linkable').prop("disabled", false);
                var checkboxs=this.find("input[type='checkbox']").not('.manual_handle');
                checkboxs.prop("disabled", true);
                checkboxs.filter('.linkable').prop("disabled", false);
                var radios=this.find("input[type='radio']").not('.manual_handle');
                radios.prop("disabled", true);
                radios.filter('.linkable').prop("disabled", false);
                hasPrivilege($editBtn) && $editBtn.hide();
                hasPrivilege($addBtn) && $addBtn.hide();
                hasPrivilege($approveBtn) && $approveBtn.hide();
                hasPrivilege($linkBtn) && $linkBtn.hide();
                hasPrivilege($unLink) && $unLink.hide();
                $okBtn.show();
                $cancelBtn.show();
                this.find(".hide-when-form-status-edit").show();
                break;
            case "show":
                this.find("input").not('.manual_handle').prop("disabled", true);
                this.find("select").not('.manual_handle').prop("disabled", true);
                this.find("textarea").not('.manual_handle').prop("disabled", true);
                this.find("input[type='checkbox']").not('.manual_handle').prop("disabled", true);
                hasPrivilege($editBtn) && $editBtn.show();
                hasPrivilege($addBtn) && $addBtn.show();
                hasPrivilege($approveBtn) && $approveBtn.show();
                hasPrivilege($linkBtn) && $linkBtn.show();
                hasPrivilege($unLink) && $unLink.show();
                $okBtn.hide();
                $cancelBtn.hide();
                this.find(".hide-when-form-status-show").show();
                break;
        }
        this.data('jquery-form-status',status);
        return this;
    };
})(jQuery);

function setListeners($page){
	$page.find(".btn-class-edit").on('click',function(e){
	    var $form = $($(e.target).parents("form")[0]);
	    $form.status('edit');
	    $form.data('backup',$form.serializeObject());
	})
	
	$page.find(".btn-class-new").on('click',function(e){
	    var $form = $($(e.target).parents("form")[0]);
	    var validator = $form.data('validator');
	    (validator && validator.resetForm());
	    $form.status('new');
	    $form.data('backup',$form.serializeObject());
	    if($form.data('default') != undefined){
	        $form.deSerializeObject($form.data('default'));
	    }
	})
	
	$page.find(".btn-class-approve").on('click',function(e){
	    var $form = $($(e.target).parents("form")[0]);
	    $form.status('approve');
	    $form.data('backup',$form.serializeObject());
	})
	
	$page.find(".btn-class-link").on('click',function(e){
	    var $form = $($(e.target).parents("form")[0]);
	    var validator = $form.data('validator');
	    (validator && validator.resetForm());
	    $form.status('link');
	    $form.data('backup',$form.serializeObject());
	})
	
	$page.find(".btn-class-ok").on('click',function(e){
	    var $form = $($(e.target).parents("form")[0]);
		if ($form.valid && !$form.valid()) {
			return false;
		}
	})
	$page.find(".btn-class-cancel").on('click',function(e){
	    var $form = $($(e.target).parents("form")[0]);
	    var validator = $form.data('validator');
	    (validator && validator.resetForm());
	    $form.status('show');
	    if($form.data('backup') != undefined){
	        $form.deSerializeObject($form.data('backup'));
	    }
	})

};

function setPrivilege($page){
	if(model.user().userId() =='0') return;
	
	var privilegeMap = model.user().privilege_mapping_id;
	$page.find(".privilege").each(function(i, e){
		var privilegeId = $(e).attr('data-privilege-id');
		if(!privilegeMap[privilegeId]){
			$(e).addClass('hide');
		}
	});
}

function hasPrivilege($e){
	if(model.user().userId() =='0') return true;
	var privilegeId = $e.attr('data-privilege-id');
	if(!privilegeId) return true;
	var privilegeMap = model.user().privilege_mapping_id;
	if(privilegeMap[privilegeId]) return true;
	return false;
}

function setTimePicker($page){
	// to diabled mobile auto pop keyboard
	$page.find('.input-group input.datepicker').prop("readonly",true);
	$page.find('.input-group input.pop-win-controller').prop("readonly",true);
	$page.find('.datepicker').each(function(i,e){
        var $timer = $(e), timerOpt={}, id = $timer.attr('id');
        if(!id){
        	id = 'timepicker_ID_'+i+'_'+(new Date().getTime());
        	$timer.attr('id', id);
        }
        timerOpt.elem= '#'+id; //需显示日期的元素选择器
        var type = $timer.data('type') || 'datetime';
     
        timerOpt.type= type;
        timerOpt.istoday= true; //是否显示今天
        timerOpt.issure= true; //是否显示确认
        if($timer.data("min")){
        	 timerOpt.min= parseDate($timer.data("min")).toString(); //最小日期
        }
        if($timer.data("max")){
       	 	timerOpt.max= parseDate($timer.data("max")).toString(); //最小日期
        }
       
        if($timer.data("def")){
        	var def = parseDate($timer.data("def")).toString();
        	def = type== 'date' ? def.toString('yyyy-MM-dd') : 'month' == type ? def.toString('yyyy-MM') : def;
       	 	timerOpt.start= def; //默认日期
       	 	$timer.val(def);
        }
        
        laydate.render(timerOpt);
    });
	
	$page.find('.glyphicon-calendar').click(function(){
		var $time = $(this).parent().parent().find('.datepicker');
		if($time.prop("disabled")){
			return ;
		}
		$time.trigger('click');
	});
}

var $content = $("#content"), $splitLine = $("#splitLine"), $sideMenu = $("#sideMenu");

project.resizingHandler;
$(window).resize(function(){
    clearTimeout(project.resizingHandler);
    project.resizingHandler = setTimeout(windowResize,200);
});
function windowResize(e){
    var siderbarHeight = $(".sidebar-nav").height()+80;
    var contentHeight = $("#content").height();
    var windowHeight = $(window).height();
    
    var contentTargetMinHeight = Math.max(siderbarHeight,contentHeight);
    //51 px header , 30px footer , 30 px tabs , dont know where the other 51+30+30-95=6 px go where.
    if(windowHeight > contentTargetMinHeight + 111){
        contentTargetMinHeight = windowHeight - 111;
    }
    var contentWidth = $("#content").width();
    $("#page-wrapper").css("min-height", contentTargetMinHeight + "px");
    $(".sidebar").css("min-height", contentTargetMinHeight + "px");
    var topOffset = 50;
    var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
    if (width < 768) {
        $('div.navbar-collapse').addClass('collapse');
        $(".sidebar").css("min-height","0px");
        topOffset = 100; // 2-row-menu
    } else {
        $('div.navbar-collapse').removeClass('collapse');
    }
    project.contentWidth = contentWidth - 47;  // padding-left 15,padding-right:15
    //调整页面表单大小
    $page != undefined && $page.find(".ui-jqgrid-btable").each(function(i,e){
        var $table = $(e);
        var targetWidth = $table.parents('.ui-jqgrid').parent().width();
        window.table = $table;
        //var currentWidth = $table.getGridWidth();
        $($table.setGridWidth(targetWidth).parents('.ui-jqgrid-bdiv')[0]);//.css('overflow-x','hidden');
        if(!isPC()){
            var $pageFooterMiddle = $($(".ui-pager-table").children().children().children()[1]);
            $pageFooterMiddle.width($pageFooterMiddle.width()-20);
        }
    }); 
    //调整弹出框表单大小
    $(".modal-dialog .ui-jqgrid-btable").each(function(i,e){
        var $table = $(e);
        var targetWidth = $table.parents('.ui-jqgrid').parent().width();
        $($table.setGridWidth(targetWidth).parents('.ui-jqgrid-bdiv')[0]).css('overflow-x','hidden');
    });
    var taotalWidth=0;
	var divWidth=$("#content").width();
	$(".my-nav-tabs li").each(function(index){
		taotalWidth+=$(this).width();
	});
	var tabWidth=parseInt(taotalWidth);
	if(tabWidth>=divWidth){
		//找到这类样式,触发该表单的单击事件
		$(".my-nav-tabs li:first").find('.my-nav-tab-remove-icon').trigger('click');
	}
}

//validate支持使用validate属性控制
$.metadata.setType("attr", "validate");
$(document).on("click",".pop-win > .input-group",function(e){
	var $t = $(e.target);
    var $container = $(this);
    var $input = $container.find('.pop-win-contoller');
    if($input.prop('disabled')){  //  || $input.prop('readonly') read only to avoid default mobile keyboard
        return ;
    }
    
    if($t.hasClass('exculde') || $t.parent().hasClass('exculde')){
    	return ;
    }
    var isRemove = $(e.target).children('.glyphicon').hasClass('glyphicon-remove') || $(e.target).hasClass('glyphicon-remove');
    if(isRemove){
        $container.deSerializeObject({});
        return;
    }
    $container.find("input").each(function(i,e){
        var $e = $(e);
        $e.data('backup',$e.val());
    });
    console.log($input.data('pop-win'));   
    require(["/popwin/"+$input.data('pop-win')+".js"],function(popwin) {
        var initialData = $container.serializeObject();
         var popwin = popwin.createPopWin(function(){
             // init popwin date plugins and dics;
             setListeners(popwin.$page);
             ko.applyBindings(project._dictionary,popwin.$page[0]);
             setTimePicker(popwin.$page);
         },initialData).on("ok",function(d){
             $container.find("input").each(function(i,e){
                var $e = $(e);
                if($e.hasClass('exculde') || $e.parent().hasClass('exculde') ) return;
                $e.val(d[$e.attr("name")]);
            });
             $t.trigger('focus');
             $t.trigger('blur');
         }).on("cancel",function(d){
            $container.find("input").each(function(i,e){
                var $e = $(e);
                $e.val($e.data('backup'));
            });
        });
    },requireOnError);
});
var requireOnErrorTimeoutShown = false;
function requireOnError(e){
	if(requireOnErrorTimeoutShown)
		return;
	requireOnErrorTimeoutShown = true;
	setTimeout(function(){
		requireOnErrorTimeoutShown = false;
	},1000);
	console.log(e);
	var error = e.toString().contains("404") ? e.toString() : e.toString()+"\n\n"+e.stack;
	error = error.contains("Load timeout for modules:") ? "页面加载超时,请刷新重试 !" : e;
	 BootstrapDialog.show({
	        message: error, 
	        type: BootstrapDialog.TYPE_DANGER, 
	        title: '出错啦！', 
	        buttons: [{
	            label: '关闭',
	            action: function (dialogItself) {
	                dialogItself.close();
	            }
	        }]
	    });	
}
$.jstree.openTreeNode = function(tree,nodeArray,callback){
    if(nodeArray == null || nodeArray == undefined)return;
    if(tree == null || tree == undefined)return;
    if(nodeArray.length < 1){
        callback();
        return ;
    }
    var nodeToOpen = nodeArray[0];
    nodeArray = nodeArray.slice(1);
    if(!tree.jstree('get_node',nodeToOpen)){
        $.jstree.openTreeNode(tree,nodeArray,callback);
    }else{
        tree.jstree('open_node', nodeToOpen, function(e, data) {
            $.jstree.openTreeNode(tree,nodeArray,callback);
        }, true);
    }    
}

var model = { user : ko.observable(ko.mapping.fromJS(new User())),tabs:ko.observableArray([]),showMenu:ko.observable(true),
		showMenuClick:function(){
			$("#page-wrapper").css('margin-left',this.showMenu() ? '0' : '250px');
			$('.my-nav-tabs').css('padding-left',this.showMenu() ? '25px' : '280px');
			this.showMenu(!this.showMenu());
			$(window).trigger('resize');
		}};
var $page;
var global={};
requirejs.config({shim: {'web.service': {exports: 'web.service'}}});

function User(obj){
    var a = {
        menuList: [],
        clientType: null,
        effDate: null,
        expDate: null,
        ip: null,
        lang: "",
        loginPage: "",
        mac: null,
        orgnizationId: 0,
        partyId: null,
        partyName: null,
        password: "",
        privilegePo: [],
        regionId: null,
        role: null,
        roleId: null,
        roleList: [],
        roleName: "",
        staffName:'',
        staffPo: {},
        userCode: "",
        userId: 0,
        userName: null,
        userVisitRecorded: false,
        vnoId: 0,
        vnoName: "",
    };
    for(var i in a){
        this[i] = a[i];
    }
    for(var i in obj){
        this[i]= obj[i];
    }

    if(obj != undefined && obj.staffPo != undefined){
        for(var i in obj.staffPo){
            this[i] = obj.staffPo[i] === undefined ? this[i] : obj.staffPo[i] ;
        }
        delete this.staffPo;
    }
    this.displayedName = this.roleName + ' : '+ this.staffName + ' ('+this.userCode+') ';

    //process privilege
    if(obj != undefined && obj.privilegePo != undefined){
        var privilege = {};
        var privilege_mapping_id = {};
        for(var i in obj.privilegePo){
            var item = obj.privilegePo[i];
            privilege[item.privilegeCode] = item;
            privilege_mapping_id[item.privilegeId] = item;
        }
        this.privilege = privilege;
        this.privilege_mapping_id = privilege_mapping_id;  
    }
    // process menu
    if(obj != undefined && obj.alMenuPo != undefined){
        var allMenuList = obj.alMenuPo;
        var parentMenuObjs = {};
        var childrenMenuObjs = [];
        var randomIcon = ['fa-user','fa-users','fa-archive','fa-cogs','fa-sitemap','fa-bar-chart-o','fa-table','fa-edit','fa-desktop','fa-wrench','fa-arrows-v','fa-file','fa-dashboard'];
        var count = 0;
        for(var i in allMenuList){
            var item = allMenuList[i];
            item.isShow = true;
            if(item.parentMenuId == null){
                item.subMenu = [];
                parentMenuObjs[item.menuId] = item;
            }else{
                childrenMenuObjs.push(item);
            }
        }
        for(var i in childrenMenuObjs){
            var subMenu = childrenMenuObjs[i];
            var parentMenu = parentMenuObjs[subMenu.parentMenuId];
            if(parentMenu == undefined){
                console.error(subMenu);
                console.error("could not find parent menu!");
            }else{
                parentMenu.subMenu.push(subMenu);
            }
        }

        for(var i in parentMenuObjs){
        	 count++;
             count = randomIcon[count] == undefined ? 0 : count;
             parentMenuObjs[i].icon = randomIcon[count];
            this.menuList.push(parentMenuObjs[i]);
        }
    }
    return this;
}

model.tabClick = function(tabItem){
    if(tabItem.selected()){// already actived
        var module = tabItem.module();
        var menuName = tabItem.name();
        console.log("loading module "+module+" "+menuName+"  "+$page.find(".author").html());
        return ;
    }
    // in-active previous selected.
    var allTabs =  model.tabs();
    for(var i in allTabs){
        var tab = allTabs[i];
        if(tab.selected() && tab.id() != tabItem.id()){
            tab.selected(false);
            var module = tab.module();
            var tabContentId = "tabs_"+module.replace(/\//gi,"_");
            $("#"+tabContentId).hide();
            break;
        }
    }
    // active 
    tabItem.selected(true);
    var module = tabItem.module();
    var menuName = tabItem.name();
    var tabContentId = "tabs_"+module.replace(/\//gi,"_");
    var $newPage = $("#"+tabContentId).show();
    if(!window.$page || $newPage.attr('id') !== window.$page.attr('id')){
    	window.$page = $newPage;
        window.moduleName= module.replace(/\//gi,"_").substring(8);
    }
   /* if(!window.$page || $newPage.context !== window.$page.context 
    		|| $newPage.selector !== window.$page.selector){
    	window.$page = $newPage;
        window.moduleName= module.replace(/\//gi,"_").substring(8);
    }*/
    //History.pushState('', '', '/admin/'+module.replace("modules/",""));
    console.log("loading module "+module+" "+menuName+"  "+$page.find(".author").html());
    tabPage();
    require([module],function(js) {
        try{
            (js.onActive|| function(){})($("#"+tabContentId),'');
        }catch(e){
            window.e = e;
            console.error(module);
            console.error(e.stack||e);
        }
        $(window).trigger('resize');
    },requireOnError);
    //触发一次请求，保持session在线
    $.post("/getLoginInfo.do",null,function(d){});
}

//计算菜单栏宽度
function tabPage(){
	var taotalWidth=0;
	var divWidth=$("#content").width();
	$(".my-nav-tabs li").each(function(index){
		taotalWidth+=$(this).width();
	});
	var tabWidth=parseInt(taotalWidth);
	if(tabWidth>=divWidth){
		$(".my-nav-tabs li:first").find('.my-nav-tab-remove-icon').trigger('click');
	}
	
}
//loading加载效果
function loading(){
	$("#tabs").hide();
	$("#page-img-bg").hide();
	$("#fakeloader").show();
}

model.removeTabClick = function(tabItem){
    var module = tabItem.module(),isSelected = tabItem.selected();
    var tabContentId = "#tabs_"+module.replace(/\//gi,"_");
    $(tabContentId).remove();
    model.tabs.remove(tabItem);
    if(tabItem.selected())
        $page=null;
    if(isSelected && model.tabs().length > 0){
        var tab = model.tabs()[model.tabs().length - 1];
        var tabContentId = "tabs_"+tab.module().replace(/\//gi,"_");
        $("#nav_"+tabContentId+" .my-nav-tab").trigger('click');
    }
    model.tabs().length == 0 && $('#page-img-bg').show();
}


function openTab(dom){
    var $dom = $(dom);
    $($dom.parents()[3]).find('.subMenuItem.active').removeClass('active');
    $dom.addClass('active');
    var menuId = $dom.attr('id');
    var module = "modules/"+($dom.data('page-url')||"").replace(".html","");
    var tabContentId = "tabs_"+module.replace(/\//gi,"_");
    if($page != undefined && $page.selector == "#"+tabContentId){
        return;
    }
    var menuName = $dom.data('menu-name');
    var $tabContent = $("#"+tabContentId);
    //History.pushState('', '', '/admin/'+module.replace("modules/",""));
    var relativeUrl = location.pathname.replace(module.replace("modules/",""),"");
    loading();
    require(["libs/requirejs/require-text!"+module+".html",module],function(html,js) {
            //jqgrid选择列功能，同一个页面上出现多个的话，后面的页面会选不到。无法起作用。因此，此处默认关一下。
            $(".ui-icon-closethick").trigger('click');
            // test html dom id polution
            var formatedModuleName = module.replace(/\//gi,"_").substring(8);
            var match = html.match(/ id *= *['"]{1}[a-zA-Z0-9]*['"]{1}/gi);
            for(var i in match){
                var id = match[i].replace(/ id *= *['"]{1}/gi,"").replace(/['"]{1}/gi,"");
                if(!id.startsWith(formatedModuleName)){
                    var msg =  "id请加"+formatedModuleName+"前缀防止冲突,相关id:"+id;
                    windowError(msg,module+".html",'');
                    throw msg;
                }
            }
            $(".tab").hide();
            var pageCreate = false;
            if($tabContent[0] == undefined){
                model.tabs.push(ko.mapping.fromJS({name:menuName,id:'nav_'+tabContentId,module:module,selected:false}));
                $("#tabs").append("<div class='tab' id='"+tabContentId+"' data-menu-id='"+menuId+"'>"+html+"</div>");
                $tabContent = $("#"+tabContentId);
                pageCreate = true;
            }else{
                $tabContent.show();
            }
            var devName;
            if(pageCreate){
                devName = $tabContent.find(".author").html();
                if(devName == null){
                     var msg =menuName+" 页面开头加开发人员名称，如 &lt;span class='hide author'&gt;丁明亮&lt;/span&gt;";
                     console.log(msg,module+".html",'');
                }
                try{
                    ko.applyBindings(project._dictionary,$tabContent[0]);
                }catch(e){
                    console.error(e);
                }
                setTimePicker($tabContent);
                setListeners($tabContent);
                window.$page = $tabContent;
                window.moduleName = formatedModuleName;
                try{
                    (js.onCreate||function(){})($tabContent,relativeUrl);
                    setPrivilege($tabContent);
                }catch(e){
                    window.e = e;
                    console.error(module);
                    console.error(e.stack||e);
                }
            }
            $('#page-img-bg').hide();
            $("#tabs").show();
            $("#fakeloader").hide();
            $("#nav_"+tabContentId+" .my-nav-tab").trigger('click'); 
        },requireOnError
    );
    var navButton = $(".navbar-header .navbar-toggle");
    navButton.css('display') == "block" ? navButton.trigger('click') : null;
}
requirejs.config({
    baseUrl: '/',
    urlArgs:'v='+__home.v,
    paths: {
        async: 'libs/requirejs/async'
    }
});


var hidden = "hidden";
function onchange (evt) {
    var v = "visible", h = "hidden",evtMap = { focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h };
    evt = evt || window.event;var a;
    if (evt.type in evtMap){
     a= evtMap[evt.type];
    }else{
      a = this[hidden] ? "hidden" : "visible";
    }
    if(a == "visible"){
    	$.post("/getLoginInfo.do");
	}
 }
	
function enablePageTimeout(){
  if (hidden in document){// Standards:
    	document.addEventListener("visibilitychange", onchange);
	}else if ((hidden = "mozHidden") in document){
		document.addEventListener("mozvisibilitychange", onchange);
    }else if ((hidden = "webkitHidden") in document){
    	document.addEventListener("webkitvisibilitychange", onchange);
    } else if ((hidden = "msHidden") in document){
    	document.addEventListener("msvisibilitychange", onchange);
    }else if ("onfocusin" in document){// IE 9 and lower:
    	document.onfocusin = document.onfocusout = onchange;
    }else{// All others:
	    window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;
    }
  
  if( document[hidden] !== undefined ) // set the initial state (but only if browser supports the Page Visibility API)
    onchange({type: document[hidden] ? "blur" : "focus"});
}

//__home.loadOkNumber ++ ;
require(["/resources/js/main.js"],function(login){
	//__home.loadOkNumber ++ ;
    $(".btnChangeRole").on("click",function(){ login.choseRole();});
    $(".btnLogout").on("click",function(){ login.logout();});
    $(".btnResetPassword").on("click",function(){ login.resetPassword();});
    login.checkLogin();
    $("#page-img-bg").attr("src","/resources/images/dashboard.jpg");
    $(document).on("loginok",function(){
        ko.applyBindings(model);
        $(window).trigger("resize");
        $('#side-menu').metisMenu().on('shown.metisMenu', function() {
             $(window).trigger("resize");
          }).on('hidden.metisMenu', function(event) {
              $(window).trigger("resize");
          });
        enablePageTimeout();
    })
},requireOnError);

function requestFullScreen(el) {
    // Supports most browsers and their versions.
    var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;
    if (requestMethod) { // Native full screen.
        requestMethod.call(el);
    } else if(typeof window.ActiveXObject !== "undefined") { // Older IE.
        try{
            var wscript = new ActiveXObject("WScript.Shell").SendKeys("{F11}");
        }catch(e){}
    }
    $(window).trigger('resize');
    return false
}

function toggleFull() {
    var elem = document.body; // Make the body go full screen.
    var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||  (document.mozFullScreen || document.webkitIsFullScreen);
    if (isInFullScreen) {
        cancelFullScreen(document);
    } else {
        requestFullScreen(elem);
    }
    $(window).trigger('resize');
    return false;
}

function cancelFullScreen(el) {
    var requestMethod = el.cancelFullScreen||el.webkitCancelFullScreen||el.mozCancelFullScreen||el.exitFullscreen;
    if (requestMethod) { // cancel full screen.
        requestMethod.call(el);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        try{
            var wscript = new ActiveXObject("WScript.Shell").SendKeys("{F11}");
        }catch(e){}
    }
}

function windowError(message,url,lineNumber){
    console.error(message+" @"+url+':'+lineNumber);
    BootstrapDialog.show({
        message: message , 
        type: BootstrapDialog.TYPE_DANGER, 
        title: '出错啦！', 
        buttons: [{
            label: '关闭',
            action: function (dialogItself) {
                dialogItself.close();
            }
        }]
    });
}


/**
 * 获取渠道商务信息的id
 * @param sales 渠道商务字典
 */
function getSaleIds(sales) {
    var saleIds=[];
    var saleIdStr='';
    for(var item in sales){
        saleIds.push(item);
    }
    saleIdStr=saleIds.join(',');
    return saleIdStr
}
