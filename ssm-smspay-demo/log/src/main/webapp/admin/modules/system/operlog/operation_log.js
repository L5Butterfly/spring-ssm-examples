define(function () {

function onActive($page,$relativeUrl){
}

function onCreate($page,$relativeUrl){
	//var endGetTime = new Date().getTime();     
	//var startGetTime = endGetTime - 1000*60*60*24*6;
	//var startTime = new Date(startGetTime)
	//var endTime = new Date(endGetTime)
	//$page.find("input[name='startDate']").val(startTime);
	//$page.find("input[name='endDate']").val(endTime);
	
	var startTime = $page.find("input[name='startDate']").val();
	var endTime =$page.find("input[name='endDate']").val();
	
    $page.on('click',".btn-search",function(e){
		var begin = $page.find('input[name="startDate"]').val();
		var end = $page.find('input[name="endDate"]').val();
		//if(DateDiff(begin,end)>6){
		//	BootstrapDialog.warning("已超过最大天数7天,请重新选择！");
		//	return false;
		//}
		if(begin==''||end==''){
			BootstrapDialog.warning("日期不能为空！");
			return false;
		}
		if(begin>end){
				BootstrapDialog.warning("开始时间晚于结束时间,请重新选择！");
				return false;
		}
		
		var postData = jqGrid.jqGrid("getGridParam", "postData");
		var queryParam = {
				svcName: $page.find("input[name='svcName']").val(),
				staffCode: $page.find("input[name='staffCode']").val(),
				startDate: begin,
				endDate: end
						  };
	  	$.extend(postData,queryParam);
		jqGrid.jqGrid("setGridParam", {datatype: 'json', search:true}).trigger("reloadGrid", [{ page: 1}]);
	});
    
    
    

/* 获取选中行  
**
var rowId =$("#privilege_rolemanage_dataTable").jqGrid('getGridParam','selrow');  
var rowData = $("#privilege_rolemanage_dataTable").getRowData(rowId);
*/
	var jqGrid = $("#sysmaintenance_staffoperlog_dataTable").jqGrid({
		postData : {startDate:startTime,endDate:endTime},
	    url: '/operationLog/queryStaffOperLogList4Jq.do',
	    colModel: [
	        { label: '操作工号', name: 'staffCode'},
	        { label: '员工姓名', name:'staffName'},
	        { label: '服务名', name: 'svcName',editable:true},
	        { label: '调用参数', name: 'param',hidden: true},
	        { label: '操作Ip', name: 'sourIp'},
	        { label: '异常编码', name: 'expCode'},
	        { label: '创建时间', name: 'createTime'},
	        { label: '操作时间', name: 'operTime'},
	        { label: '操作花费时间',name: 'costTime',hidden: true},
	        { label: '用户浏览器信息',name:'httpClient'},
	        { label : '操作', width: 60, sortable : false, formatter : function(c, o, row) {
	        	return "<div class='col-lg-5 col-sm-5 col-md-5'><div style='color:red;text-decoration:underline' class='operation_detail' data-rowid='"+o.rowId+"' title='详情'>详情</div></div>";
		     }}
	    ],
		//viewrecords: true,
	    height: '400px',
	    sortable: true,
	    sortname: 'createTime',
	    sortorder: 'desc',
	    //pgbuttons:true,	
	    pager: "#sysmaintenance_staffoperlog_jqGridPager"
	});
	
	$page.on('click','.operation_detail',function(){
		var a = $(this).data();
		$("#sysmaintenance_staffoperlog_dataTable").jqGrid('setSelection',a.rowid);
		var data = jqGrid.jqGrid('getRowData',jqGrid.jqGrid('getGridParam', 'selrow'));
		require(["/popwin/operlog/detail.js"],function(popwin) {
	         var popwin = popwin.createPopWin(function(){},data);
	    });
	});
}

return { onCreate : onCreate, onActive : onActive };
});


