define(function () {

function onActive($page,$relativeUrl){
	
}

function onCreate($page,$relativeUrl){
	var $form = $page.find(".detail_form");
	$page.on('click',".btn-class-new,.btn-refresh",function(e){
		//禁用复选框
		$page.find(".privilege_grid input[type='checkbox']").prop("disabled",false);
		priGrid.resetSelection();
	});

	$page.on('click',".btn-class-cancel",function(e){
		//禁用复选框
		$page.find(".privilege_grid input[type='checkbox']").prop("disabled",true);
	});
	
	$page.on('click',".btn-class-reset-passwd",function(e){
		var staffId = $form.find('input[name="staffId"]').val();
		if(!staffId){
			  BootstrapDialog.show({
	                type: BootstrapDialog.TYPE_DANGER,
	                title: '请选择员工',
	                message: '请选择员工',
	                buttons: [{
	                	label: '确定',action: function(dialogItself){
	                    dialogItself.close();
	                }}]
			  });
			return ;
		}
		$.post('/staff/resetStaffPwd.do',{staffId:staffId},function(res){
			BootstrapDialog.success('密码重置成功.重置密码为:abcd1234');
		});
		
	});

	var $btnEdit = $page.find(".btn-class-edit"), 
		$btnResetPasswd = $page.find(".btn-class-reset-passwd"),
		$btnNew = $page.find(".btn-class-new");

	$page.on('click',".btn-class-cancel",function(e){
		//全选按钮恢复禁用状态
		$page.find(".detail_form input[type='checkbox']").prop("disabled",true);
		var rowId = jqGrid.jqGrid('getGridParam','selrow');
		if(!rowId){
			priGrid.resetSelection();
			return;
		}
	
		var data = jqGrid.getRowData(rowId);
		$.post('/staff/queryStaffRoleList.do', {staffId: data.staffId}, function(res){
			if(!res) return;
    		priGrid.resetSelection();
    		$.each(res, function(i, row){
    			priGrid.jqGrid('setSelection',row.roleId);
    		});
    	});
	})

	//给表单一个默认值，在点击创建按钮时，这个默认值会被反序列化到表单中
	var vnoId = model.user().vnoId(), vnoName = model.user().vnoName();
	$page.find('input[name="vnoId"]').val(vnoId);
	$page.find('input[name="vnoName"]').val(vnoName);
	$form.data('default',{status:'00A',vnoShowFlag: 'F', vnoId: vnoId, vnoName: vnoName});
	
	var validator = $form.validate();
	
	function done(data){
		var staff = $form.serializeObject();
		var staffId = staff['staffId'];
		jqGrid.jqGrid("setGridParam", {
				search : true
			}).trigger("reloadGrid", [ {
				page : 1
			} ]);
			$form.status('show');
			
			var roleIds = priGrid.jqGrid('getGridParam','selarrrow').join(',');
			$.post('/staff/newStaffRole.do', {staffId:staffId , roleIds:roleIds}, 
				function(res){
		    		BootstrapDialog.success("更新成功");
		    	}, function(res){
		    		BootstrapDialog.error("更新失败");
		    	});
	}

	$form.on('click',".btn-class-ok",function(e){
	    if (!$form.valid()) {
            return;
        }
		var staffRole = $form.serializeObject(), roleIds = priGrid.jqGrid('getGridParam','selarrrow').join(',');
		staffRole.roleIds = roleIds;
		
		var method = $form.status() == "new" ? 'newStaffRole' : 'editStaffRoleWithoutPwd';
		var msg = $form.status() == "new" ? '添加成功！' : '更新成功！';
        
		if(method=="editStaffRoleWithoutPwd"){
			delete staffRole.password;
			$.post("/staff/"+method+".do", staffRole, function(res) {
				jqGrid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page: 1}]);
				$form.status('show');
				BootstrapDialog.success(msg);
			});
		}else{
			if(!roleIds || roleIds.length < 1){
				BootstrapDialog.confirm({btnOKLabel:'是', btnCancelLabel:'否',message:'您确定创建没有角色的用户吗?', callback: addStaff});
			}else{
				addStaff(true);
			}
			
			function addStaff(flag){
				if(!flag){
					return;
				}
				$.post("/staff/"+method+".do", staffRole, function(res) {
					jqGrid.jqGrid("setGridParam", {search: true}).trigger("reloadGrid", [{page: 1}]);
					$form.status('show');
					BootstrapDialog.success(msg);
				});
			}
		}
	});
	
	var jqGrid = $("#privilege_staffmanage_dataTable").jqGrid({
					url : '/staff/queryStaffList4Jq.do',
					colModel : [
							{label : '员工编码', name : 'staffId', hidden : true },
							{label : '员工账号', name : 'staffCode' },
							{label : '员工姓名', name : 'staffName' },
							{label : '创建日期', name : 'createDate'},
							{label : '状态', name : '_status'},
							{label : '', name : 'vnoId',hidden : true},
							{label : '组织部门名称',name : 'vnoName', sortable : false},
							{label : '密码失效时间',name : 'passExpDate',hidden : true}
						],
					onSelectRow : function(rowId) {
						var data = jqGrid.griddata.res[rowId - 1];
						// 清除之前的校验信息
						validator.resetForm();
				    	$form.deSerializeObject(data).status('show');

				    	$btnEdit.data('privilege') !== false && $btnEdit.prop('disabled', false);
						$btnResetPasswd.data('privilege') !== false && $btnResetPasswd.prop('disabled', false);
				    	$.post('/staff/queryStaffRoleList.do', {staffId: data.staffId}, function(res){
				    		priGrid.resetSelection();
				    		$.each(res, function(i, row){
				    			priGrid.jqGrid('setSelection',row.roleId);
				    		});
				    	});
					},
					loadComplete : function(data) {
						jqGrid.griddata = data;
						//默认禁用全选按钮
						$page.find(".privilege_grid input[type='checkbox']").prop("disabled",true);
						$page.find(".btn-class-edit").prop('disabled', true);
						$page.find(".btn-class-reset-passwd").prop('disabled', true);
						priGrid.resetSelection();
						$form.status('show');
					},
					pager : "#privilege_staffmanage_jqGridPager"
				});
	
    //点击编辑或者新增时触发的事件
	   $page.find(".btn-class-edit").click(function(){
		   $page.find(".privilege_grid input[type='checkbox']").prop("disabled",false);
	   });
	   	
		var  priGrid = $('#privilege_staffRoleDataTable').jqGrid({
				url: '/role/queryRoleList4Jq.do',
			    multiselect: true,
			    colModel: [
			        { label: '角色编码', name: 'roleId', key: true},
			        { label: '角色名称', name: 'roleName'}
			    ],
			    height: '378px',
			    rowNum : 200,
			    shrinkToFit:true,
			    beforeSelectRow: function (rowid, e) {
			    	var isShow = $form.status() === 'show',
			    	box = $($(e.target).closest('td')[0]).find('input[type="checkbox"]');
			    	var isSelected = priGrid.jqGrid('getGridParam','selarrrow').indexOf(rowid) != -1;
			    	box.prop('checked', isSelected || !isShow);
			        return !isShow;  
			    }
		});
	}


	return {onCreate : onCreate, onActive : onActive };
});
