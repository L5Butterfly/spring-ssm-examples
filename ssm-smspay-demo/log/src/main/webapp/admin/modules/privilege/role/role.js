define(function () {

function onActive($page,$relativeUrl){
}

function onCreate($page,$relativeUrl){
	var $privTree = null, treeIns = null, privData = null;
	var $form = $page.find(".detail_form");

	//给表单一个默认值，在点击创建按钮时，这个默认值会被反序列化到表单中
	var vnoId = model.user().vnoId(), vnoName = model.user().vnoName()
	$page.find('input[name="parentVnoId"]').val(vnoId);
	$page.find('input[name="parentVnoName"]').val(vnoName);
	$form.data('default',{status:'00A', vnoShowFlag: 'F'});
	var validator = $form.validate();
	var jqGrid = $page.find("#privilege_rolemanage_dataTable").jqGrid({
	    url: '/role/queryRoleList4Jq.do',
	    colModel: [
	        { label: '角色编码', name: 'roleId', hidden: true},
	        { label: '角色名称', name: 'roleName'},
	        { label:'创建日期', name: 'createDate'},
	        { label:'', name: 'status',hidden:true}
	    ],
	    rowNum : 10,
	    onSelectRow:function(rowId){
	    	$page.find(".btn-class-edit").prop('disabled', false);
	    	var data = jqGrid.getRowData(rowId);
	    	//清除之前的校验信息
	    	validator.resetForm();
	    	//设置表单状态，目前有new，edit，show三种状态，
	    	$form.deSerializeObject(data).status('show');
	    	
	    	loadPrivByRole(data.roleId);
	    },
		loadComplete:function(data){
			$page.find(".btn-class-edit").prop('disabled', true);
	    	$form.status('show');
	    },
	    pager: "#privilege_rolemanage_jqGridPager"
	});
	
	//创建权限树
	$privTree = $page.find('#priv_tree').jstree({
	    'core' : {
	    	multiple : false,
	        data : function(node,cp){
	        	$.post("/role/queryPrivList4jsTreeAll.do?"+Math.random(), {}, function(data){
	        		privData = data;
	    			if(!data) return;
	    			var root ={};
	    			for(var i in data){
	        			var d = data[i];
	        			d.id = d.privilegeId+"";
	        			d.text = d.privilegeName;
	        			d.type = d.leaf ? 'leaf' : 'default';
	        			if(!d.leaf) { d.children = []; }
	        			if(d.parentPrivId==undefined){ root = d; }
	    			}
	    			var treeData = toTreeData(root.privilegeId, data, root);
	    			cp(treeData);
	        		
	        	});
	        	}
	    	},
		plugins : [ "search", "types", "checkbox" ],
		types : {
			"default": { "icon" : "glyphicon glyphicon-th-list" },
			"leaf" : {"icon": "glyphicon glyphicon-book" }
	    },
	    checkbox: {
	        "keep_selected_style": false,//是否默认选中
	        "three_state": true,//父子级别级联选择
	        "tie_selection": false,
	        "whole_node" : false
	    }
	});
	
	$privTree.on('loaded.jstree', function(e, data){
		treeIns = data.instance;
		treeIns.open_all();
		//禁用checkbox
		for(var i in privData){
			treeIns.disable_checkbox(privData[i].privilegeId+"");
		}
	});
	
	function toTreeData(parentId, source, target){
		for(var i in source){
			if(source[i].parentPrivId == parentId){
				target.children.push(source[i]);
				toTreeData(source[i].privilegeId, source, source[i]);
			}
		}
		return [target];
	}
	
	function loadPrivByRole(roleId){
		//禁用checkbox
		for(var i in privData){
			treeIns.disable_checkbox(privData[i].privilegeId+"");
		}
		
		$.post("/role/queryPrivListByRoleId.do?"+Math.random(), {roleId: roleId}, function(data){
			treeIns.uncheck_all();
			if(!data) return;
			for(var i in data){
				var node = treeIns.get_node(data[i].privilegeId+"");
				if(node.children.length < 1){
					treeIns.check_node(data[i].privilegeId+"");
				}
			}
    	});
	}
	
	/*绑定事件*/
	$page.on('click',".btn-class-new",function(e){
		for(var i in privData){
			treeIns.enable_checkbox(privData[i].privilegeId+"");
			treeIns.uncheck_node(privData[i].privilegeId+"");
		}
	}).on('click',".btn-class-edit",function(e){
		for(var i in privData){
			treeIns.enable_checkbox(privData[i].privilegeId+"");
		}
	}).on('click',".btn-class-cancel,.btn-refresh",function(e){
		var row = jqGrid.jqGrid('getGridParam','selrow');
		if(!row) return;
		var data = jqGrid.getRowData(row);
		loadPrivByRole(data.roleId);
	});
	
	//提交
	$form.on('click',".btn-class-ok",function(e){
		var nodes = treeIns.get_checked(); 
		var role = $form.serializeObject();
		var method = $form.status() == "new" ? 'addRoleWithPrivilege.do' : 'modifyRoleWithPrivilege.do';
		var msg = $form.status() == "new" ?  '添加成功！' : '更新成功！';
		
		if(role.status =='00A' && (!nodes || nodes.length < 1)){
			BootstrapDialog.warning("请为角色添加相应的菜单");
			return ;
		}
		
		role.privilegeIds = nodes.join(',');
		$.post("/role/"+method+"?"+Math.random(), role, function(data){
			jqGrid.jqGrid("setGridParam", {search : true}).trigger("reloadGrid", [{page: 1}]);
			//禁用checkbox
			for(var i in privData){
				treeIns.uncheck_node(privData[i].privilegeId+"");
				treeIns.disable_checkbox(privData[i].privilegeId+"");
			}
			BootstrapDialog.success(msg);
    	});
	});
	
}

return { onCreate : onCreate, onActive : onActive };
});


