define(function () {

	function onActive($page,$relativeUrl){
		
	}
	
	function onCreate($page,$relativeUrl){
		var $searchForm = $page.find('.search-form');
		/*$searchForm.find('input[name="startDate"]').val(new Date().toString('yyyy-MM-dd 00:00:00'));
		$searchForm.find('input[name="endDate"]').val(new Date().toString('yyyy-MM-dd 23:59:59'));*/

		$searchForm.find('input[name="startDate"]').val(new Date().offsetDay(-1).toString('yyyy-MM-dd 00:00:00'));
		$searchForm.find('input[name="endDate"]').val(new Date().offsetDay(-1).toString('yyyy-MM-dd 23:59:59'));
		
		var $form = $page.find(".detail_form");
		$form.data('default',{status:'00A', operType: 'T01'});
		var validator = $form.validate();
		/*
		$form.on('click',".btn-class-ok",function(e){
		    if (!$form.valid()) {return;}
		    var formStatus = $form.status(), 
		    method = formStatus == "new" ? 'add.do' : 'edit.do',
		    msg = $form.status() == "new" ? '添加成功！' : '修改成功！';
		    var data = $form.serializeObject();
		    $.post("/operlog/"+method, data, function(re){
		    	jqGrid.jqGrid("setGridParam", {search:true}).trigger("reloadGrid", [{ page: 1}]); 
				$form.status('show');
			});
		});
		*/
		var jqGrid = $("#operLog_mgr_dataTable").jqGrid({
				url : '/operlog/queryOperLogList4Jq.do',
				//styleUI:'Bootstrap',
				datatype: 'local',
				colModel : [
						{label : '编号', name : 'logId',hidden:true},
						{label : 'sql信息', name : 'sqlInfo' },
						{label : '操作类型', name : 'operType',hidden:true},
						{label : '操作类型', name : '_operType' },
						{label : 'Ip地址', name : 'operIp'},
						{label : '操作人', name : 'operName'},
						{label : '操作时间', name : 'operTime'},
						{label : 'sql耗时', name : 'extSqlTime',hidden:true},
						{label : '结果', name : 'extResult'},
						{label : '备注', name : 'remark',hidden:true}
					],
				onSelectRow : function(rowId) {
					//var data = jqGrid.jqGrid('getRowData',rowId);
					var data = jqGrid.griddata.res[rowId - 1];
					// 清除之前的校验信息
					validator.resetForm();			
					 data.taxRate = data._taxRate;
			    	$form.deSerializeObject(data).status('show');
			    	$page.find(".btn-class-edit").prop('disabled', false);
				},
				loadComplete : function(data) {
					// griddata未定义
					if(data && data.records > 0){
						jqGrid.griddata = data;
					}else{
					}
					$page.find(".btn-class-edit").prop('disabled', true);
					$form.status('show');
				},
				pager : "#operLog_mgr_jqGridPager"
			});
		
		// 用户选择
		$searchForm.find('.staff-select').click(function(){
			require(["/popwin/staff/staff_list.js"],function(popwin) {
		 		var popwin = popwin.createPopWin(function(){
		             setListeners(popwin.$page);
		             ko.applyBindings(project._dictionary,popwin.$page[0]);
		         }, {}).on('ok', function(data){
		        	 $searchForm.find('input[name="operRealName"]').val(data.staffName);
		        	 $searchForm.find('input[name="operName"]').val(data.staffCode);
		         });
			});
		});
		
		// 清除选择
		$searchForm.find('.staff-remove').click(function(){
			$searchForm.find('input[name="operRealName"]').val("");
			$searchForm.find('input[name="operName"]').val("");
		});
		
	}

	return {onCreate : onCreate, onActive : onActive };
});
