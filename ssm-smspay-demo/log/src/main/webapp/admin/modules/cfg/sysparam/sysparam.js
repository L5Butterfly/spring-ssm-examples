
define(function () {
	
project.addDic('SYS_PARAM_TYPE','SYS_PARAM_TYPE_ALL',{v:'',n:'全部'});

function onActive($page, $relativeUrl){
}

function onCreate($page, $relativeUrl){
	var $form = $page.find(".detail_form");
	var $table = $("#cfg_sysparam_dataTable");
	var vnoId = model.user().vnoId();
	var vnoName=model.user().vnoName();
	$page.find("input[name='vnoId']").val(vnoId);
	$page.find("input[name='vnoName']").val(vnoName);
	//验证
	var validator=$form.validate();
	$form.on('click',".btn-class-ok",function(e){
		if(!$form.valid()){
			return;
		}
		var param = $form.serializeObject();
		$.post('/sysParam/updateByPrimaryKeySelective.do',param,function(res){
			BootstrapDialog.success('修改成功');
			jqGrid.jqGrid("setGridParam", {search:true}).trigger("reloadGrid", [{ page: 1}]); 
			$form.status('show');
		});
		
		
	});
	var jqGrid = $table.jqGrid({
	    url: '/sysParam/queryParamList4Jq.do',
	    colModel:[
		{label:'参数编码',name:'paramCode'},
		{label:'参数类型编码',name:'paramTypeCode',width:'150px',editable:true},
		{label:'参数名称',name:'paramName',width:'250px'},
		{label:'参数描述',name:'paramDesc',width:'280px'},
		{label:'参数值',name:'paramValue',width:'300px',formatter:function(cellvalue, options, rowObject){
			if(cellvalue=="T"){
				return '启用';
			}else if(cellvalue=="F"){
				return '禁用';
			}
			return cellvalue;
			
		}},
		{label:'状态',name:'_status',width:'150px'}
		],
	    onSelectRow:function(rowId){
	    	validator.resetForm();
	    	$form.find(".btn-class-edit").prop("disabled",false);
	    	var data = jqGrid.griddata.res[rowId-1];
	    	$form.deSerializeObject(data).status('show');
	    	$form.find("[data-value-name='paramValue']").hide().attr('name',"");
	    	var $targetElement = $form.find("[data-value-type='"+data.valueType+"']").show();
	    	var valueName = $targetElement.data('value-name');
	    	$targetElement.attr('name',valueName).val(data.paramValue);
	    	switch(data.valueType){
	    	case "A01":break;		
	    	case "B01":break;
	    	case "C01":break;
	    	case "D01":
	    	case "D02":
	    	case "D03":
	    		$targetElement.find("input").attr('name',valueName).val(data.paramValue);break;
	    		break;
	    	}
	    },
	   	loadComplete:function(data){
	   		jqGrid.griddata = data;
	    	$form.status('show');
	    	$form.find(".btn-class-edit").prop("disabled",true);
	    },
	    pager: "#cfg_sysparam_jqGridPager"
	});
}
return { onCreate : onCreate, onActive : onActive };
});


