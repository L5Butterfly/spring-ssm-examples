/**
 * 
 */
package com.xiaorui.log.service;

import java.util.ArrayList;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.PrivilegeVo;
import com.framework.web.base.vo.RolePrivilegeVo;
import com.framework.web.base.vo.RoleVo;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:03:45
 * @description: 角色信息
 * @version: v1.0
 */
public interface IRoleService {
	
	ArrayList<RoleVo> queryRoleListByPage(RoleVo role) throws BaseAppException;

    ArrayList<PrivilegeVo> queryPrivListByRoleId(Long roleId) throws BaseAppException;

    ArrayList<PrivilegeVo> queryPrivList4jsTreeAll(Long roleId) throws BaseAppException;
    
    RolePrivilegeVo modifyRoleWithPrivilege(RolePrivilegeVo vo) throws BaseAppException;

    RolePrivilegeVo addRoleWithPrivilege(RolePrivilegeVo vo) throws BaseAppException;

    RoleVo queryRoleByName(String roleName) throws BaseAppException;

}
