/**
 * 
 */
package com.xiaorui.log.controller;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;
import com.framework.core.page.Page;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.Util;
import com.framework.web.WebConstants;
import com.framework.web.base.vo.StaffLoginVo;
import com.framework.web.base.vo.StaffRoleVo;
import com.framework.web.base.vo.StaffVo;
import com.framework.web.po.StaffPo;
import com.xiaorui.log.common.BusiConstants;
import com.xiaorui.log.common.BusiErrCode;
import com.xiaorui.log.service.IStaffService;

/**
 * @author: empathy
 * @date: 2018年10月28日 下午7:10:49
 * @description: 用户信息
 * @version: v1.0
 */
@RestController
@RequestMapping(value="/staff")
public class StaffController {
	
	@Resource
	private IStaffService staffService;
	
	@ResponseBody
	@RequestMapping("/queryStaffList4Jq")
	public Page queryStaffList4Jq(Page page, StaffVo staff) throws Exception{
		page.setRes(staffService.queryStaffList4Jq(staff));
        return page;
    }
	
	@ResponseBody
	@RequestMapping("/queryStaffRoleList")
	public ArrayList<StaffRoleVo> queryStaffRoleList( StaffVo staff) throws Exception{
        return staffService.queryStaffRoleList(staff);
    }
	
	@ResponseBody
	@RequestMapping("/newStaffRole")
	public String newStaffRole(StaffVo staffVo) throws Exception{
        staffService.addStaffRole(staffVo);
        return BusiConstants.BUSINESS_SUCCESS;
    }
	
	@ResponseBody
	@RequestMapping("/editStaffRoleWithoutPwd")
	public StaffVo editStaffRoleWithoutPwd(StaffVo staff) throws Exception{
        staffService.editStaffRoleWithoutPwd(staff);
        return staff;
    }
	
	@ResponseBody
	@RequestMapping("/resetStaffPwd")
	public String resetStaffPwd(StaffVo staff) throws Exception{
        staffService.resetStaffPwd(staff);
        return BusiConstants.BUSINESS_SUCCESS;
    }
	
	@ResponseBody
	@RequestMapping("/modifyStaffPwd")
	public String modifyStaffPwd(StaffVo staff) throws Exception{
		Object obj = UserSessionInfo.threadLocal.get().getReq().getSession().getAttribute(WebConstants.STAFF_INFO);
		if(Util.isEmpty(obj)){
			throw ExceptionHandler.publish(BusiErrCode.STAFF_NOLOGIN_ERR);
		}
        staffService.editStaffPwd(staff, ((StaffLoginVo) obj).getStaffPo());
        ((StaffLoginVo) obj).getStaffPo().setPassExpDate(staff.getPassExpDate());
        UserSessionInfo.threadLocal.get().getReq().getSession().setAttribute(WebConstants.STAFF_INFO, obj);
        return BusiConstants.BUSINESS_SUCCESS;
    }
	
	@ResponseBody
	@RequestMapping("/staffCheck")
	public boolean staffCheck(StaffVo staffVo) throws Exception{
		StaffPo staff = staffService.queryStaffByCode(staffVo.getStaffCode());
        return null == staff;
    }
	
	@ResponseBody
	@RequestMapping("/querySaleStaffList")
	public ArrayList<StaffVo> querySaleStaffList() throws BaseAppException{
        return staffService.querySaleStaffList();
    }
	
	@ResponseBody
	@RequestMapping("/login")
	public StaffLoginVo login(StaffPo staff, HttpServletRequest request) throws Exception {
		StaffLoginVo staffVo = staffService.login(staff);
		request.getSession().setAttribute(WebConstants.STAFF_INFO, staffVo);
		UserSessionInfo.threadLocal.get().setManagerBean(staffVo);
		return staffVo;
	}

}
