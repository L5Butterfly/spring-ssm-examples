package com.xiaorui.log.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.framework.web.base.cache.item.DataDictionaryCache;
import com.xiaorui.log.po.OperLogPo;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午4:02:28
 * @description: 后台操作记录-扩展属性
 * @version: v1.0
 */
public class OperLogVo extends OperLogPo {
	
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date startDate;
	
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date endDate;
    
    //操作类型
    private String _operType;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String get_operType() {
		return _operType;
	}

	public void set_operType(String _operType) {
		this._operType = _operType;
	}

	@Override
	public void setOperType(String operType) {
		super.setOperType(operType);
		//this._operType = DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_OPER_TYPE", operType);
		this._operType = DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_TABLE_OPER_TYPE", operType);
	}
}
