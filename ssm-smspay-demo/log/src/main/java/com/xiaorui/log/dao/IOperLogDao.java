/*
 * @ClassName IOperLogDao
 * @Description 
 * @version 1.0
 * @Date 2018-10-30 17:06:50
 */
package com.xiaorui.log.dao;

import java.util.ArrayList;
import java.util.List;

import com.framework.core.exception.BaseAppException;
import com.xiaorui.log.po.OperLogPo;
import com.xiaorui.log.vo.OperLogVo;

public interface IOperLogDao {
    /**
     * @Title deleteByPrimaryKey
     * @param logId
     * @return int
     */
    int deleteByPrimaryKey(Long logId);

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(OperLogPo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(OperLogPo record);

    /**
     * @Title selectByPrimaryKey
     * @param logId
     * @return OperLog
     */
    OperLogPo selectByPrimaryKey(Long logId);

    /**
     * @Title updateByPrimaryKeySelective
     * @param record
     * @return int
     */
    int updateByPrimaryKeySelective(OperLogPo record);

    /**
     * @Title updateByPrimaryKey
     * @param record
     * @return int
     */
    int updateByPrimaryKey(OperLogPo record);

	/**
	 * @param operLog
	 * @return
	 */
    
    
    //自定义dao层接口
    /**
     * @param record
     * @return
     * @throws Exception
     */
    ArrayList<OperLogVo> queryOperLogInfo(OperLogVo record) throws BaseAppException;
    
    /**
     * @param record
     * @return
     * @throws Exception
     */
    ArrayList<OperLogVo> queryOperLogInfoByPage(OperLogVo record) throws BaseAppException;
}