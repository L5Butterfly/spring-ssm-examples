package com.xiaorui.log.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.core.exception.BaseAppException;
import com.framework.core.page.Page;
import com.xiaorui.log.service.IOperLogService;
import com.xiaorui.log.vo.OperLogVo;


/**
 * @author: empathy
 * @date: 2018年10月28日 下午6:02:42
 * @description: 页面操作日志
 * @version: v1.0
 */
@Controller
@CrossOrigin
@RequestMapping("/operlog")
public class OperLogController {
	
	private static final Logger logger = Logger.getLogger(OperLogController.class);
	
	@Resource
	private IOperLogService operLogService;
	
	
	@ResponseBody
	@RequestMapping("/queryOperLogList4Jq")
	public Page queryOperLogList4Jq(Page page, OperLogVo operLogVo) throws BaseAppException{
		page.setRes(operLogService.queryLogList4Jq(operLogVo));
        return page;
    }
	
	@ResponseBody
	@RequestMapping("query")
	public String queryLog(){
		logger.info("================start=================");
		logger.info("=================end================");
		return "hello log";
		
	}
	
	
	@ResponseBody
	@RequestMapping("getList")
	public List<OperLogVo> getLogInfo(OperLogVo operLog) throws Exception{
		logger.info("================start=================");
		List<OperLogVo> logList=operLogService.queryOperInfo(operLog);
		logger.info(logList);
		logger.info("=================end================");
		return logList;

	}
	
	@ResponseBody
	@RequestMapping("/queryList4Jq")
	public Page queryCompanyList4Jq(Page page, OperLogVo operLog) throws Exception{
		logger.info("================start=================");
		logger.info(page.getPageNo());
		logger.info(page.getRows());
		page.setRes(operLogService.queryLogList4Jq(operLog));
		logger.info("=================end================");
        return page;
    }

}
