/**
 * 
 */
package com.xiaorui.log.service.impl;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.framework.core.exception.BaseAppException;
import com.framework.core.utils.SeqUtil;
import com.framework.web.base.dao.ITaskTimingCfgDao;
import com.framework.web.base.vo.TaskTimingCfgVo;
import com.framework.web.po.TaskTimingCfgPo;
import com.xiaorui.log.common.SequenceNaming;
import com.xiaorui.log.service.ITaskTimingCfgService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:37:56
 * @description: 定时任务配置
 * @version: v1.0
 */
@Service
public class TaskTimingCfgServiceImpl implements ITaskTimingCfgService {

	@Resource
	private ITaskTimingCfgDao taskTimingCfgDao;

	@Override
	public ArrayList<TaskTimingCfgVo> qryTaskTimingInfo4JqByPage(TaskTimingCfgVo taskTimingCfgVo) throws BaseAppException {
		return taskTimingCfgDao.qryTaskTimingInfo4JqByPage(taskTimingCfgVo);
	}


	@Override
	public TaskTimingCfgPo insertTask(TaskTimingCfgPo taskTimingCfgPo)throws BaseAppException {
		taskTimingCfgPo.setTaskId(SeqUtil.getTableSequence(SequenceNaming.TASK_TIMING_CFG));
		taskTimingCfgDao.insertTask(taskTimingCfgPo);
		return taskTimingCfgPo;
	}


	@Override
	public int modifyTask(TaskTimingCfgPo taskTimingCfgPo)throws BaseAppException {
		return taskTimingCfgDao.modifyTask(taskTimingCfgPo);
	}


	@Override
	public TaskTimingCfgPo queryTaskByName(String taskName)throws BaseAppException {
		return taskTimingCfgDao.queryTaskByName(taskName);
	}

}
