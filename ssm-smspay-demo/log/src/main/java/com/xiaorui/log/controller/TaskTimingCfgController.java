/**
 * 
 */
package com.xiaorui.log.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.framework.core.page.Page;
import com.framework.web.base.vo.TaskTimingCfgVo;
import com.framework.web.po.TaskTimingCfgPo;
import com.xiaorui.log.service.ITaskTimingCfgService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:33:04
 * @description: 定时任务信息
 * @version: v1.0
 */

@RestController
@RequestMapping("/taskTimingCfg")
public class TaskTimingCfgController {
	
	@Resource
	private ITaskTimingCfgService taskTimingCfgService;
	
	@ResponseBody
	@RequestMapping("/qryTaskTimingInfoJq")
	public Page queryStaffList4JqByPage(Page page, TaskTimingCfgVo taskTimingCfgVo) throws Exception{
		page.setRes(taskTimingCfgService.qryTaskTimingInfo4JqByPage(taskTimingCfgVo));
        return page;
    }
	
	@ResponseBody
	@RequestMapping("/addTask")
	public TaskTimingCfgPo addTask(TaskTimingCfgPo taskTimingCfgPo) throws Exception{
        return taskTimingCfgService.insertTask(taskTimingCfgPo);
    }
	
	@ResponseBody
	@RequestMapping("/modifyTask")
	public int modifyTask(TaskTimingCfgPo taskTimingCfgPo) throws Exception{
		return taskTimingCfgService.modifyTask(taskTimingCfgPo);
        
    }
	
	@ResponseBody
	@RequestMapping("/taskCheck")
	public boolean taskCheck(TaskTimingCfgVo taskTimingCfgVo) throws Exception{
		TaskTimingCfgPo taskTimingCfgPo = taskTimingCfgService.queryTaskByName(taskTimingCfgVo.getTaskName());
        return null == taskTimingCfgPo;
    }
}
