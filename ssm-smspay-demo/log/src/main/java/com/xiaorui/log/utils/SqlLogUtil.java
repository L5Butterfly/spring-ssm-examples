package com.xiaorui.log.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.log4j.Logger;

import com.framework.core.Constants;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.DateUtil;
import com.framework.core.utils.ReflectUtil;
import com.framework.core.utils.Util;


/**
 * @author: empathy
 * @date: 2018年10月29日 下午4:00:55
 * @description: SQL日志工具类
 * @version: v1.0
 */
public class SqlLogUtil {
	
	private static final Logger logger = Logger.getLogger(SqlLogUtil.class);

	private SqlLogUtil() {
		
	}

	/**
	 * 记录SQL日志
	 * 
	 * @param originalSql  String
	 * @param amendSql  String
	 * @param costTime  long
	 */
	public static void logSql(String sql, long costTime) {
		sql = sql.replaceAll("\n", " ").replaceAll(" +", " ").replaceAll("\t", "");
		StringBuffer buff = new StringBuffer();
		UserSessionInfo userInfo = UserSessionInfo.threadLocal.get();
		if(Util.isEmpty(userInfo)){
			buff.append("******* <").append("SYSTEM").append("> | <").append("SYSTEM").append("> *******\n");
		}else{
			buff.append("******* <").append(userInfo.getAccessIp()).append("> | <").append(userInfo.getAccessCode()).append("> *******\n");
		}
		
		buff.append(sql).append("\n");
		buff.append("[SQL_EXECUTE_TIME(ms)]:").append(costTime).append("\n");
		logger.debug(buff.toString());
	}

	/**
	 * 
	 * @param boundSql
	 * @param costTime
	 */
	public static void logSql(BoundSql boundSql, long costTime) {
		String sql = boundSql.getSql();
		List<ParameterMapping> paramList = boundSql.getParameterMappings();
		
		for (int i = 0; i < paramList.size(); i++) {
			String key = paramList.get(i).getProperty();
			Class<?> keyType = paramList.get(i).getJavaType();
			sql = decorateSql(sql,key,keyType, boundSql);
		}
		
		if(sql.contains(Constants.SQL_REPLACE_KEY)){
			sql = sql.replace(Constants.SQL_REPLACE_KEY, "?");
		}
		logSql(sql, costTime);
	}
	
	/**
	 * 装饰sql语句
	 * @param sql
	 * @param index
	 * @param key
	 * @param keyType
	 * @param valObj
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private static String decorateSql(String sql, String key,Class<?> keyType, BoundSql boundSql){
		try {
			Object valObj = boundSql.getParameterObject();
			if(boundSql.hasAdditionalParameter(key)){
				valObj = boundSql.getAdditionalParameter(key);
			}
			
			String value = "null";
			
			if(valObj instanceof String){
				value = "'"+(String) valObj+"'";
			}else if(valObj instanceof Long || valObj instanceof Integer 
					||valObj instanceof Float ||valObj instanceof Double
					||valObj instanceof  Short){
				value = valObj.toString();
			}else if(valObj instanceof Date){
				value = "'"+DateUtil.date2String((Date) valObj)+"'";
			}else if(valObj instanceof HashMap){
				Object val = ((HashMap) valObj).get(key);
				if(val instanceof String){
					value = "'"+(String) val+"'";
				}else if(val instanceof Long || val instanceof Integer 
						||val instanceof Float ||val instanceof Double
						||val instanceof  Short){
					value = val.toString();
				}else if(val instanceof Date){
					value = "'"+DateUtil.date2String((Date) val)+"'";
				}else{
					value = "'"+String.valueOf(val)+"'";
				}
			}else{
				Object val = ReflectUtil.getValueByFieldName(valObj, key);
				if(null == val){
					value = "null";
				}
				else if(keyType == String.class){
					value = "'"+(String) val+"'";
				}else if(keyType == Long.class || keyType == Integer.class
						||keyType ==  Float.class ||keyType ==  Double.class
						||keyType ==  Short.class){
					value = val.toString();
				}else if(keyType == Date.class){
					value = "'"+DateUtil.date2String((Date) val)+"'";
				}else{
					value = "'"+String.valueOf(val)+"'";
				}
			}
			//防止值中包含关键字?,导致sql打印有问题
			if(value.contains("?")){
				value = value.replace("?", Constants.SQL_REPLACE_KEY);
			}
			sql = sql.replaceFirst("\\?", value);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return sql;
	}

}
