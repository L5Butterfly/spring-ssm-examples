package com.xiaorui.log.service.impl;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.framework.core.exception.BaseAppException;
import com.framework.core.utils.ParamUtil;
import com.framework.core.utils.Util;
import com.framework.web.base.dao.ISysParamDao;
import com.framework.web.base.dao.IVersionListDao;
import com.framework.web.base.vo.SysParamVo;
import com.framework.web.listener.InitDataListener;
import com.framework.web.po.VersionListPo;
import com.xiaorui.log.common.BusiConstants;
import com.xiaorui.log.service.ISysParamService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午4:00:25
 * @description: 系统参数
 * @version: v1.0
 */
@Service
public class SysParamServiceImpl implements ISysParamService {

	@Resource
	private ISysParamDao sysParamDao;
	
	@Resource
	private IVersionListDao versionListDao;
	

	/**
	 * 通过Code获取系统配置
	 */
	@Override
	public String getSysParamByCode(String code) throws BaseAppException{
		return ParamUtil.getParamString(code);
	}
	
	@Override
	public ArrayList<SysParamVo> queryParamList4Jq(SysParamVo sysparm) {
		return sysParamDao.getParamListByPage(sysparm);
	}
	
	
	@Override
	public int updateByPrimaryKeySelective(SysParamVo sysparm){
		return sysParamDao.updateByPrimaryKeySelective(sysparm);
		
	}


    @Override
    public SysParamVo getByCodeAndVnoId(SysParamVo sysparm) {
        ArrayList<SysParamVo> sysParamList = sysParamDao.getParamListByPage(sysparm);
        return Util.notEmpty(sysParamList) ? sysParamList.get(0) : null;
    }
    
    @Override
    public String getSystemInfo(String type) throws BaseAppException{
        String companyName = BusiConstants.COMPANY_NAME;
        String icpPreparedNo = BusiConstants.ICP_PREPARED_NO;
        long systemStartTime = InitDataListener.SYSTEM_START_TIME.getTime();
        
        if(Util.isEmpty(BusiConstants.VERSION_MAP)){
        	 ArrayList<VersionListPo> versionList = versionListDao.findAllOrderByTime();
        	 for(VersionListPo po : versionList){
        		 BusiConstants.VERSION_MAP.put(po.getType(), po);
        	 }
        }
        VersionListPo versionPo = BusiConstants.VERSION_MAP.get(type);
        
        return versionPo.getDescription() +" " +versionPo.getVersionCode() + "-" + systemStartTime + "-" + companyName + "-" + icpPreparedNo;
    }

}
