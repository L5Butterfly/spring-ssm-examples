/**
 * 
 */
package com.xiaorui.log.common;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.framework.core.page.Page;
import com.framework.web.base.vo.SysParamVo;
import com.xiaorui.log.service.ISysParamService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:23:09
 * @description: 系统参数配置
 * @version: v1.0
 */
@RestController
@RequestMapping(value="/sysParam")
public class SysParamController {
	
	@Resource
	private ISysParamService sysParamService;
	
	@ResponseBody
	@RequestMapping("/querySysParamByCode")
	public String querySysParamByCode(String paramCode) throws Exception{
        return sysParamService.getSysParamByCode(paramCode);
    }
	
	@ResponseBody
	@RequestMapping("/queryParamList4Jq")
	public Page queryParamList4Jq(Page page,SysParamVo sysparam)throws Exception{
		page.setRes(sysParamService.queryParamList4Jq(sysparam));
		return page;
	}
	
	@ResponseBody
	@RequestMapping("/updateByPrimaryKeySelective")
	public int updateByPrimaryKeySelective(SysParamVo SysParamVo){
		return sysParamService.updateByPrimaryKeySelective(SysParamVo);
	}

}
