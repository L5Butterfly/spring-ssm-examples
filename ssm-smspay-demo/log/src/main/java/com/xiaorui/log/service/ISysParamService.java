package com.xiaorui.log.service;

import java.util.ArrayList;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.SysParamVo;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:58:36
 * @description: 系统参数信息
 * @version: v1.0
 */
public interface ISysParamService {

	String getSysParamByCode(String code) throws BaseAppException;

	String getSystemInfo(String type) throws BaseAppException;
	
	ArrayList<SysParamVo> queryParamList4Jq(SysParamVo sysparm) ;
	
	int updateByPrimaryKeySelective(SysParamVo sysparm);

    SysParamVo getByCodeAndVnoId(SysParamVo sysparm);

}
