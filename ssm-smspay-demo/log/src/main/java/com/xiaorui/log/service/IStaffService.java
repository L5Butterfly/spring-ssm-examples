package com.xiaorui.log.service;

import java.util.ArrayList;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.StaffLoginVo;
import com.framework.web.base.vo.StaffRoleVo;
import com.framework.web.base.vo.StaffVo;
import com.framework.web.po.RolePo;
import com.framework.web.po.StaffPo;


/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:58:17
 * @description: 用户信息
 * @version: v1.0
 */
public interface IStaffService {

	StaffLoginVo login(StaffPo staff) throws BaseAppException;
	
	StaffPo queryStaffByCode(String staffCode) throws BaseAppException;
	
	ArrayList<StaffVo> queryStaffList4Jq(StaffVo staff) throws BaseAppException;
	
	ArrayList<StaffRoleVo> queryStaffRoleList(StaffVo staff) throws BaseAppException;
	
	ArrayList<StaffVo> querySaleStaffList() throws BaseAppException;
	
	int resetStaffPwd(StaffVo staff) throws BaseAppException;
	
	void addStaffRole(StaffVo staffVo) throws BaseAppException;
	
	StaffVo addStaff(StaffVo staff) throws BaseAppException;
	
	int editStaff(StaffVo staff) throws BaseAppException;
	
	int editStaffRoleWithoutPwd(StaffVo staff) throws BaseAppException;
	
	int editStaffPwd(StaffVo staff, StaffPo oldStaff) throws BaseAppException;

	StaffLoginVo changeRole(RolePo role) throws BaseAppException;
	
	StaffPo queryStaffById(int staffId) throws BaseAppException;

}
