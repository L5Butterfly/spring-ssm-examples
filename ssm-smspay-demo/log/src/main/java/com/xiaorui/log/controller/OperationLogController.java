/**
 * 
 */
package com.xiaorui.log.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.framework.core.page.Page;
import com.framework.web.base.vo.OperationLogVo;
import com.xiaorui.log.service.IOperationLogService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:17:37
 * @description: 用户登录日志
 * @version: v1.0
 */

@RestController
@RequestMapping("/operationLog")
public class OperationLogController {
	
	@Resource
	private IOperationLogService operationLogService;
	
	@ResponseBody
	@RequestMapping("/queryStaffOperLogList4Jq")
	public Page queryStaffOperLogList4JqByPage(Page page, OperationLogVo operationLogVo) throws Exception{
 		page.setRes(operationLogService.queryStaffOperLogList4JqByPage(operationLogVo));
        return page;
    }

}
