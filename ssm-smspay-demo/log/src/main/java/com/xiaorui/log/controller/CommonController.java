package com.xiaorui.log.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.framework.core.exception.BaseAppException;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.Util;
import com.framework.web.WebConstants;
import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.base.cache.item.RegionCache;
import com.framework.web.base.cache.item.RegionCacheVo;
import com.framework.web.base.cache.vo.DataDictionaryCacheVo;
import com.framework.web.base.vo.StaffLoginVo;
import com.framework.web.po.DataDictionaryValuePo;
import com.framework.web.po.RolePo;
import com.xiaorui.log.service.IStaffService;
import com.xiaorui.log.service.ISysParamService;
import com.xiaorui.log.common.BusiConstants;
import com.xiaorui.log.utils.StaffUtil;

/**
 * @author: empathy
 * @date: 2018年10月28日 下午5:53:22
 * @description: 页面资源初始化和用户权限数据加载信息
 * @version: v1.0
 */
@Controller
@RequestMapping(value="/")
public class CommonController {

	@Resource
	private IStaffService staffService;
	
	@Resource
	private ISysParamService sysParmService;
	
	@RequestMapping("/login.do")
	public String login() throws BaseAppException{
        return "admin/login";
    }
	
	@RequestMapping("/home")
    public String home() throws BaseAppException{
        return "admin/home";
    }
	
	@RequestMapping("/logout")
    public String logout(HttpServletRequest request) throws Exception{
		request.getSession().removeAttribute(WebConstants.STAFF_INFO);
		UserSessionInfo.threadLocal.get().setManagerBean(null);
        return "admin/login";
    }
	
	@RequestMapping("/m_login.do")
	public String m_login() throws BaseAppException{
        return "mobile/login";
    }
	
	@RequestMapping("/m_home")
    public String m_home() throws BaseAppException{
        return "mobile/home";
    }
	
	@RequestMapping("/m_logout")
    public String m_logout(HttpServletRequest request) throws Exception{
		request.getSession().removeAttribute(WebConstants.STAFF_INFO);
		UserSessionInfo.threadLocal.get().setManagerBean(null);
        return "mobile/login";
    }
	
	@ResponseBody
	@RequestMapping("/changeRole")
    public StaffLoginVo changeRole(RolePo role) throws Exception{
        return staffService.changeRole(role);
    }
	
	@ResponseBody
	@RequestMapping("/getLoginInfo")
	public StaffLoginVo getLoginInfo() throws BaseAppException {
		Object loginInfo = UserSessionInfo.threadLocal.get().getReq().getSession(false).getAttribute(WebConstants.STAFF_INFO);
		if(Util.isEmpty(loginInfo)){
			return null;
		}
		
		if(!(loginInfo instanceof StaffLoginVo)){
			return null;
		}
		
		StaffLoginVo loginVo = (StaffLoginVo) loginInfo;
		loginVo.setIsManager(StaffUtil.isManager(loginVo.getRoleId()) ? BusiConstants.COMMON_FLAG_TRUE : BusiConstants.COMMON_FLAG_FALSE);
		loginVo.setIsSaler(BusiConstants.SALE_ROLE_ID.equals(loginVo.getRoleId()) 
				? BusiConstants.COMMON_FLAG_TRUE : BusiConstants.COMMON_FLAG_FALSE);
		return loginVo;
	}
	
	@ResponseBody
	@RequestMapping("/getSystemInfo")
    public String getSystemInfo(String type) throws Exception{
		return sysParmService.getSystemInfo(type);
    }
	
	@ResponseBody
	@RequestMapping("/getDataDictionaryCache")
    public HashMap<String, ArrayList<HashMap<String, String>>> getDataDictionaryCache() throws BaseAppException {
        String language = "zh_CN";
		ArrayList<DataDictionaryCacheVo> allData = DataDictionaryCache.INSTANCE.getAll();
		HashMap<String,ArrayList<HashMap<String,String>>> returnData = new HashMap<String,ArrayList<HashMap<String,String>>>();
		for(DataDictionaryCacheVo po:allData){
			if(!language.equals(po.getLangType()))
				continue;
			ArrayList<HashMap<String,String>>  valueAl = new ArrayList<HashMap<String,String>>();
			for(DataDictionaryValuePo valuePo : po.getDataDictionaryValueAl()){
				if(language.equals(valuePo.getLangType())){
					HashMap<String,String> map = new HashMap<String, String>();
					map.put("v", valuePo.getDicValue()); 
					map.put("n", valuePo.getDicValueName()); 
					valueAl.add(map);
				}
			}
			returnData.put(po.getDicTypeCode(),valueAl);
		}
		
		//添加省份
		ArrayList<RegionCacheVo> regionList = RegionCache.INSTANCE.getAll();
		//按照优先级排序，数据库查询的时候会导致乱序，页面展示不正常
		Collections.sort(regionList,new Comparator<RegionCacheVo>(){
            @Override
			public int compare(RegionCacheVo arg0, RegionCacheVo arg1) {
                return arg0.getRegionCode().compareTo(arg1.getRegionCode());
            }
        });
		ArrayList<HashMap<String,String>> regionAll = new ArrayList<HashMap<String,String>>();
		for(RegionCacheVo vo : regionList){
			HashMap<String,String> map = new HashMap<String, String>();
			map.put("v", vo.getRegionId()+""); 
			map.put("n", vo.getRegionName()); 
			regionAll.add(map);
		}
		
		returnData.put("DIC_REGION_ALL",regionAll);
		return returnData;
		
	}
}
