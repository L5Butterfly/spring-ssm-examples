/**
 * 
 */
package com.xiaorui.log.listener;


import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.framework.core.Constants;
import com.framework.core.cache.IBaseCache;
import com.framework.core.exception.BaseAppException;
import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.base.cache.item.RegionCache;
import com.framework.web.base.cache.item.SysErrDescriptionCache;
import com.framework.web.base.cache.item.VnoListCache;
import com.framework.web.listener.InitDataListener;


/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:48:58
 * @description: 初始化项目加载缓存
 * @version: v1.0
 */
public class StartFinishListener extends InitDataListener {
	private static Logger logger = Logger.getLogger(StartFinishListener.class);
	
	@Override
	protected void loadData() {
		try {
			if ((!Constants.isProduction) || (Constants.IS_MAIN_PROCESS)) {
				logger.info("System load end, start load cache");
				loadCache();
			}
		} catch (BaseAppException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 项目启动缓存加载
	 */
	private void loadCache() throws BaseAppException {
		@SuppressWarnings("rawtypes")
		ArrayList<IBaseCache> cacheList = new ArrayList<IBaseCache>();
		cacheList.add(SysErrDescriptionCache.INSTANCE);
		cacheList.add(DataDictionaryCache.INSTANCE);
		cacheList.add(VnoListCache.INSTANCE);
		cacheList.add(RegionCache.INSTANCE);
		
		/* 
		IStaffDao dao = (IStaffDao) AppContextUtil.getBean("IStaffDao");
		ArrayList<StaffVo> staffList = dao.selectStaffListByRoleId(BusiConstants.SALE_ROLE_ID);
		for(StaffVo staff: staffList){
			SaleStaffCacheVo vo = new SaleStaffCacheVo();
			BeanUtil.copyProperties(staff, vo);
			SaleStaffCache.INSTANCE.setItem(vo);
		}
		*/
	
	}
}
