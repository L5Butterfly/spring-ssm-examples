/**
 * 
 */
package com.xiaorui.log.controller;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.framework.core.page.Page;
import com.framework.web.base.vo.PrivilegeVo;
import com.framework.web.base.vo.RolePrivilegeVo;
import com.framework.web.base.vo.RoleVo;
import com.xiaorui.log.service.IRoleService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:01:55
 * @description: 角色信息
 * @version: v1.0
 */

@Controller
@RequestMapping("/role")
public class RoleController {

	@Resource
	private IRoleService roleService;
	
	@RequestMapping("/index")
    public String index(){
        return "privilege/role";
    }

	@ResponseBody
	@RequestMapping("/queryRoleList4Jq")
	public Page queryRoleList4Jq(Page page, RoleVo role) throws Exception{
		page.setRes(roleService.queryRoleListByPage(role));
		return page;
    }
	
	@ResponseBody
	@RequestMapping("/queryPrivListByRoleId")
	public ArrayList<PrivilegeVo> queryPrivListByRoleId(PrivilegeVo vo) throws Exception{
	    return roleService.queryPrivListByRoleId(vo.getRoleId());
	}
	
	@ResponseBody
	@RequestMapping("/queryPrivList4jsTreeAll")
	public ArrayList<PrivilegeVo> queryPrivList4jsTreeAll(PrivilegeVo vo) throws Exception{
	    return roleService.queryPrivList4jsTreeAll(vo.getRoleId());
	}
	
	@ResponseBody
    @RequestMapping("/addRoleWithPrivilege")
    public RolePrivilegeVo addRoleWithPrivilege(RolePrivilegeVo vo) throws Exception {
        return roleService.addRoleWithPrivilege(vo);
    }
    
	@ResponseBody
    @RequestMapping("/modifyRoleWithPrivilege")
    public RolePrivilegeVo modifyRoleWithPrivilege(RolePrivilegeVo vo) throws Exception{
        return roleService.modifyRoleWithPrivilege(vo);
    }
	
	@ResponseBody
	@RequestMapping("/roleCheck")
	public boolean roleCheck(RoleVo vo) throws Exception{
		RoleVo roleVo = roleService.queryRoleByName(vo.getRoleName());
        return null == roleVo;
    }
}
