package com.xiaorui.log.service.impl;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.framework.core.Constants;
import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;
import com.framework.core.session.UserInfoBean;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.DateUtil;
import com.framework.core.utils.MD5Util;
import com.framework.core.utils.SeqUtil;
import com.framework.core.utils.Util;
import com.framework.web.WebConstants;
import com.framework.web.base.dao.IMenuDao;
import com.framework.web.base.dao.IPrivilegeDao;
import com.framework.web.base.dao.IStaffDao;
import com.framework.web.base.vo.StaffLoginVo;
import com.framework.web.base.vo.StaffRoleVo;
import com.framework.web.base.vo.StaffVo;
import com.framework.web.po.MenuPo;
import com.framework.web.po.PrivilegePo;
import com.framework.web.po.RolePo;
import com.framework.web.po.StaffPo;
import com.xiaorui.log.common.BusiConstants;
import com.xiaorui.log.common.BusiErrCode;
import com.xiaorui.log.common.SequenceNaming;
import com.xiaorui.log.service.IStaffService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午4:00:09
 * @description: 用户信息
 * @version: v1.0
 */
@Service
public class StaffServiceImpl implements IStaffService {

	@Resource
	private IStaffDao staffDao;//用户
	
	@Resource
	private IPrivilegeDao privilegeDao;//权限
	
	@Resource
	private IMenuDao menuDao;//菜单
	
	@Override
	public StaffLoginVo login(StaffPo staff) throws BaseAppException {
		ExceptionHandler.publish(Util.isEmpty(staff.getStaffCode()) || Util.isEmpty(staff.getPassword()), BusiErrCode.LOGIN_AUTH_FAIL);
		
		StaffPo qryStaff = staffDao.selectStaffByCode(staff.getStaffCode());

		if(Util.notEmpty(qryStaff)){
			UserInfoBean userBean = new UserInfoBean();
			userBean.setUserId(qryStaff.getStaffId());
			UserSessionInfo.threadLocal.get().setManagerBean(userBean);
		}
		
		ExceptionHandler.publish(Util.isEmpty(qryStaff) || !qryStaff.getPassword().equals(staff.getPassword()), BusiErrCode.LOGIN_AUTH_FAIL);
		
        ArrayList<StaffRoleVo> stafRoleList = staffDao.selectStaffRoleByStaffId(qryStaff.getStaffId());
        ExceptionHandler.publish(Util.isEmpty(stafRoleList), BusiErrCode.STAFF_NO_ROLE_ERR);
        
		StaffLoginVo staffVo = new StaffLoginVo();
		staffVo.setStaffPo(qryStaff);
		staffVo.setVnoId(qryStaff.getVnoId());
		staffVo.setRoleList(stafRoleList);
		if(stafRoleList.size() == 1){
			Long defRoleId = stafRoleList.get(0).getRoleId();
			staffVo.setRoleId(defRoleId);
			staffVo.setRoleName(stafRoleList.get(0).getRoleName());

	        ArrayList<MenuPo> menuList = staffDao.getStaffMenu(qryStaff.getStaffId(), defRoleId);
	        staffVo.setAlMenuPo(menuList);
	        
	        ArrayList<PrivilegePo> privilegeList = staffDao.getStaffPrivilege(qryStaff.getStaffId(), defRoleId);
	        staffVo.setPrivilegePo(privilegeList);
		}
		return staffVo;
	}

	@Override
	public StaffPo queryStaffByCode(String staffCode) throws BaseAppException {
		return staffDao.selectStaffByCode(staffCode);
	}

	@Override
	public ArrayList<StaffVo> queryStaffList4Jq(StaffVo staff)throws BaseAppException {
		return staffDao.getStaffListByPage(staff);
	}

	@Override
	public ArrayList<StaffRoleVo> queryStaffRoleList(StaffVo staff)throws BaseAppException {
		return staffDao.selectStaffRoleList(staff);
	}

	@Override
	public ArrayList<StaffVo> querySaleStaffList() throws BaseAppException {
		//商务标识后台配置
		Long roleId = BusiConstants.SALE_ROLE_ID;
		return staffDao.selectStaffListByRoleId(roleId);
	}

	@Override
	public int resetStaffPwd(StaffVo staff) throws BaseAppException {
		staff.setPassword(MD5Util.convert(BusiConstants.STAFF_INIT_PASSWORD));
		return staffDao.resetStaffPwd(staff);
	}

	@Override
	public void addStaffRole(StaffVo staffVo) throws BaseAppException {
		addStaff(staffVo);
		String roleIds = staffVo.getRoleIds();
        if (Util.notEmpty(roleIds)) {
            String[] ids = roleIds.split(Constants.SPLIT_COMMON);
            for (int i = 0; i < ids.length; i++) {
                StaffRoleVo staffRoleVo = new StaffRoleVo();
                staffRoleVo.setStaffId(staffVo.getStaffId());
                staffRoleVo.setRoleId(Long.valueOf(ids[i]));
                staffRoleVo.setCreateDate(DateUtil.getNowDate());
                staffRoleVo.setStatusDate(DateUtil.getNowDate());
                staffRoleVo.setStatus(BusiConstants.STATUS_EFF);
                staffRoleVo.setVnoId(BusiConstants.ROOT_VNO_ID);
                staffDao.insertStaffRole(staffRoleVo);
                
                //TODO:设置缓存
                if(BusiConstants.SALE_ROLE_ID==Long.valueOf(ids[i])){
                	/*                	
                	SaleStaffCacheVo cacheVo = new SaleStaffCacheVo();
                	BeanUtil.copyProperties(staffVo, cacheVo);
                	SaleStaffCache.INSTANCE.setItem(cacheVo);
                	*/
                }
            }
        }

	}

	@Override
	public StaffVo addStaff(StaffVo staff) throws BaseAppException {
		staff.setPassword(MD5Util.convert(staff.getPassword()));
		staff.setCreateDate(Util.getFirstNotNull(staff.getCreateDate(), DateUtil.getNowDate()));
		staff.setStatusDate(DateUtil.getNowDate());
		staff.setPassExpDate(Util.getFirstNotNull(staff.getPassExpDate(), DateUtil.getNowDate()));
		staff.setStaffId(SeqUtil.getTableSequence(SequenceNaming.STAFF_ID));
		//用户所属部分编号
		staff.setVnoId(BusiConstants.ROOT_VNO_ID);
		staffDao.insertStaff(staff);
		return staff;
	}

	@Override
	public int editStaff(StaffVo staff) throws BaseAppException {
		return staffDao.modifyStaff(staff);
	}

	@Override
	public int editStaffRoleWithoutPwd(StaffVo staff) throws BaseAppException {
		staff.setPassword(null);
		int rows = editStaff(staff);
		
		// TODO:缓存
		/*SaleStaffCacheVo cacheVo = new SaleStaffCacheVo();
    	BeanUtil.copyNotNullProperties(staff, cacheVo);
    	SaleStaffCache.INSTANCE.setItem(cacheVo);*/
    	
		String roleIds = staff.getRoleIds();
        if (Util.notEmpty(roleIds)) {
        	
    		staffDao.deleteStaffRoleByStaffId(staff.getStaffId());
            String[] ids = roleIds.split(Constants.SPLIT_COMMON);
            for (int i = 0; i < ids.length; i++) {
                StaffRoleVo staffRoleVo = new StaffRoleVo();
                staffRoleVo.setStaffId(staff.getStaffId());
                staffRoleVo.setRoleId(Long.valueOf(ids[i]));
                staffRoleVo.setCreateDate(DateUtil.getNowDate());
                staffRoleVo.setStatusDate(DateUtil.getNowDate());
                staffRoleVo.setStatus(BusiConstants.STATUS_EFF);
                staffRoleVo.setVnoId(BusiConstants.ROOT_VNO_ID);
                staffDao.insertStaffRole(staffRoleVo);
            }
        }
        
        roleIds = Constants.SPLIT_COMMON+roleIds+Constants.SPLIT_COMMON;
        String saleId = Constants.SPLIT_COMMON + BusiConstants.SALE_ROLE_ID +Constants.SPLIT_COMMON;
        if(!roleIds.contains(saleId)){
        	//SaleStaffCache.INSTANCE.delItem(staff.getStaffId());
        }    
        return rows;
	}

	@Override
	public int editStaffPwd(StaffVo staff, StaffPo oldStaff)throws BaseAppException {
		oldStaff = staffDao.selectStaffByCode(oldStaff.getStaffCode());
		
        if (!MD5Util.convert(staff.getOldPass()).equals(oldStaff.getPassword())) {
            throw ExceptionHandler.publish(BusiErrCode.STAFF_AUTH_ERR);
        }
        
        staff.setStaffId(oldStaff.getStaffId());
        staff.setPassword(MD5Util.convert(staff.getNewPass()));
        
        
        if(oldStaff.getPassExpDate().before(DateUtil.getNowDate())){
        	staff.setPassExpDate(BusiConstants.MAX_EXP_DATE);
        	oldStaff.setPassExpDate(BusiConstants.MAX_EXP_DATE);
        }
		return staffDao.updateStaffPwd(staff);
	}

	@Override
	public StaffLoginVo changeRole(RolePo role) throws BaseAppException {
		UserSessionInfo userInfo = UserSessionInfo.threadLocal.get();
        StaffLoginVo staffLoginVo = (StaffLoginVo) userInfo.getManagerBean();
        Long roleId = role.getRoleId();
        if(Util.notEmpty(staffLoginVo.getRoleId()) && staffLoginVo.getRoleId().equals(roleId)){
            return staffLoginVo;
        }
        //用户菜单权限信息
        staffLoginVo = staffSetMenuAndPrivilegeAccordingRole(staffLoginVo,roleId);
        userInfo.getReq().getSession(false).setAttribute(WebConstants.STAFF_INFO, staffLoginVo);
        userInfo.setManagerBean(staffLoginVo);
        return staffLoginVo;
    }
    
    public StaffLoginVo staffSetMenuAndPrivilegeAccordingRole(StaffLoginVo staffLoginVo, Long defaultRoleId) throws BaseAppException {
        Iterator<StaffRoleVo> iterator = staffLoginVo.getRoleList().iterator();
        boolean ok = false;
        while (iterator.hasNext()) {
            StaffRoleVo role = iterator.next();
            if (role.getRoleId().equals(defaultRoleId)) {
                staffLoginVo.setRoleId(defaultRoleId);
                staffLoginVo.setRoleName(role.getRoleName());
                ok = true;
                break;
            }
        }
        ExceptionHandler.publish(!ok, "User Don't have role id :" + defaultRoleId);

        ArrayList<MenuPo> alMenuPo = getStaffMenu(staffLoginVo);
        staffLoginVo.setAlMenuPo(alMenuPo);
       
        ArrayList<PrivilegePo> privList = privilegeDao.queryPrivilegeByStaffId(staffLoginVo.getStaffId(), staffLoginVo.getRoleId());
        staffLoginVo.setPrivilegePo(privList);
        return staffLoginVo;
	}
    
    
    /**
     * 获取用户菜单
     * @param staffLoginVo
     * @return
     * @throws BaseAppException
     */
    public ArrayList<MenuPo> getStaffMenu(StaffLoginVo staffLoginVo) throws BaseAppException {
        return menuDao.selectByStaffAndRole(staffLoginVo.getStaffId(),staffLoginVo.getRoleId());
    }

	@Override
	public StaffPo queryStaffById(int staffId) throws BaseAppException {
		return staffDao.selectStaffById(staffId);
	}

}
