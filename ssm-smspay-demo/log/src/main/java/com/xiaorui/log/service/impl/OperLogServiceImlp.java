package com.xiaorui.log.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.core.exception.BaseAppException;
import com.xiaorui.log.dao.IOperLogDao;
import com.xiaorui.log.service.IOperLogService;
import com.xiaorui.log.vo.OperLogVo;


/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:59:07
 * @description: 后台操作记录日志
 * @version: v1.0
 */
@Service
public class OperLogServiceImlp implements IOperLogService {
		
	@Autowired
	private IOperLogDao operLogDao;

	@Override
	public List<OperLogVo> getOperInfo(OperLogVo operLog) throws Exception {
		return operLogDao.queryOperLogInfo(operLog);
	}

	@Override
	public ArrayList<OperLogVo> queryOperInfo(OperLogVo operLog) throws Exception {
		return operLogDao.queryOperLogInfo(operLog);
	}

	@Override
	public ArrayList<OperLogVo> queryLogList4Jq(OperLogVo operLog) throws BaseAppException {
		return operLogDao.queryOperLogInfoByPage(operLog);
	}

}
