package com.xiaorui.log.common;

/**
 * @author: empathy
 * @date: 2018年10月28日 下午5:55:09
 * @description: 应用错误码设置
 * @version: v1.0
 */
public interface BusiErrCode {

	/** 鉴权失败，用户不存在或密码错误 */
	String LOGIN_AUTH_FAIL = "XR-01-0000";
	
	/** 鉴权失败，用户不存在角色信息 */
	String STAFF_NO_ROLE_ERR = "XR-01-0001";
	
	/** 鉴权失败，用户密码错误 */
	String STAFF_AUTH_ERR = "XR-01-0002";
	
	/** 用户没有登陆 */
	String STAFF_NOLOGIN_ERR = "XR-01-0003";
}
