/*
 * @ClassName IBlackListDao
 * @Description 
 * @version 1.0
 * @Date 2018-10-30 22:00:25
 */
package com.xiaorui.log.dao;

import com.xiaorui.log.po.BlackListPo;

public interface IBlackListDao {
    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(BlackListPo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(BlackListPo record);
}