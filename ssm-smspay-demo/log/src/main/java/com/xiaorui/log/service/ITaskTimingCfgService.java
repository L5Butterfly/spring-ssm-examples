/**
 * 
 */
package com.xiaorui.log.service;

import java.util.ArrayList;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.TaskTimingCfgVo;
import com.framework.web.po.TaskTimingCfgPo;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:35:12
 * @description: 系统任务配置
 * @version: v1.0
 */
public interface ITaskTimingCfgService {
	
	ArrayList<TaskTimingCfgVo> qryTaskTimingInfo4JqByPage(TaskTimingCfgVo taskTimingCfgVo) throws BaseAppException;
	
	TaskTimingCfgPo insertTask(TaskTimingCfgPo taskTimingCfgPo) throws BaseAppException;
	
	int modifyTask(TaskTimingCfgPo taskTimingCfgPo) throws BaseAppException;
	
	TaskTimingCfgPo queryTaskByName(String taskName) throws BaseAppException;

}
