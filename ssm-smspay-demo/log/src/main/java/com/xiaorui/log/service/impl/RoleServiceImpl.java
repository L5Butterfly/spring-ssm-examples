/**
 * 
 */
package com.xiaorui.log.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.framework.core.Constants;
import com.framework.core.exception.BaseAppException;
import com.framework.core.utils.BeanUtil;
import com.framework.core.utils.DateUtil;
import com.framework.core.utils.SeqUtil;
import com.framework.core.utils.Util;
import com.framework.web.base.dao.IRoleDao;
import com.framework.web.base.vo.PrivilegeVo;
import com.framework.web.base.vo.RolePrivilegeVo;
import com.framework.web.base.vo.RoleVo;
import com.framework.web.po.RolePo;
import com.xiaorui.log.common.BusiConstants;
import com.xiaorui.log.common.SequenceNaming;
import com.xiaorui.log.service.IRoleService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:04:55
 * @description: 角色信息
 * @version: v1.0
 */
@Service
public class RoleServiceImpl implements IRoleService{

	@Resource
	private IRoleDao roleDao;
	
	@Override
	public ArrayList<RoleVo> queryRoleListByPage(RoleVo role) throws BaseAppException {
		return roleDao.selectRoleListByPage(role);
	} 
	
    @Override
    public ArrayList<PrivilegeVo> queryPrivListByRoleId(Long roleId) throws BaseAppException {
        return roleDao.selectPrivListByRoleId(roleId);
    }

    @Override
    public RolePrivilegeVo modifyRoleWithPrivilege(RolePrivilegeVo vo) throws BaseAppException {
    	RolePo role = new RolePo();
        BeanUtil.copyProperties(vo, role);
        if(BusiConstants.STATUS_EXP.equals(vo.getStatus())){
        	role.setStatusDate(DateUtil.getNowDate());
        }
        roleDao.modifyRole(role);
        addRolePrivileges(vo);
        return vo;
    }

    @Override
    public RolePrivilegeVo addRoleWithPrivilege(RolePrivilegeVo vo) throws BaseAppException {
    	RolePo role = new RolePo();
        BeanUtil.copyProperties(vo, role);
        role.setRoleId(SeqUtil.getTableSequence(SequenceNaming.ROLE_ID));
        Date currentDate = DateUtil.getNowDate();
        role.setCreateDate(currentDate);
        role.setStatusDate(currentDate);
        roleDao.addRole(role);
        vo.setRoleId(role.getRoleId());
        addRolePrivileges(vo);
        return vo;
    }

    private void addRolePrivileges(RolePrivilegeVo vo) throws BaseAppException {
        Long roleId = vo.getRoleId();
        String privilegeId = vo.getPrivilegeIds();
        //先清空原来的权限
        roleDao.deleteRolePrivilege(roleId);
        //查询选中的所有权限，根据idPath将未选中的父节点也选中
        if(!Util.isEmpty(privilegeId)){
            Set<String> idSet = new HashSet<String>();
            String[] privIds = privilegeId.split(",");
            Long[] qryIds = new Long[privIds.length];
            for(int i=0; i< qryIds.length; i++){
            	qryIds[i] = Long.valueOf(privIds[i]);
            }
            ArrayList<PrivilegeVo> privList =  roleDao.selectPrivListByIds(qryIds);
            String[] ids = null;
            for(PrivilegeVo privVo : privList){
            	ids = privVo.getPrivIdPath().substring(3).split(Constants.SPLIT_STRIKE);
            	for(String id : ids){
            		if(Util.notEmpty(id)){
            			idSet.add(id);
            		}
            	}
            }
            
            for(String id : idSet){
            	 RolePrivilegeVo rolePrivilegeVo = new RolePrivilegeVo();
                 rolePrivilegeVo.setRoleId(roleId);
                 rolePrivilegeVo.setPrivilegeId(Long.parseLong(id));
                 rolePrivilegeVo.setCreateDate(DateUtil.getNowDate());
                 rolePrivilegeVo.setStatusDate(DateUtil.getNowDate());
                 rolePrivilegeVo.setStatus(BusiConstants.STATUS_EFF);
                 roleDao.addRolePrivilege(rolePrivilegeVo);
            }
        }
    }
    
	@Override
	public RoleVo queryRoleByName(String roleName) throws BaseAppException {
		return roleDao.selectRoleByName(roleName);
	}

	@Override
	public ArrayList<PrivilegeVo> queryPrivList4jsTreeAll(Long roleId)
			throws BaseAppException {
		return roleDao.queryPrivList4jsTreeAll(roleId);
	}

}
