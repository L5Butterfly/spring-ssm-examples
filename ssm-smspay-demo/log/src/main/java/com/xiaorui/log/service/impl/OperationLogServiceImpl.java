/**
 * 
 */
package com.xiaorui.log.service.impl;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.dao.IOperationLogDao;
import com.framework.web.base.vo.OperationLogVo;
import com.xiaorui.log.service.IOperationLogService;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:19:48
 * @description: 用户登录日志
 * @version: v1.0
 */
@Service
public class OperationLogServiceImpl implements IOperationLogService {


	@Resource
	private IOperationLogDao operationLogDao;
	
	@Override
	public ArrayList<OperationLogVo> queryStaffOperLogList4JqByPage(OperationLogVo operationLogVo) throws BaseAppException {
		return operationLogDao.queryStaffOperLogList4JqByPage(operationLogVo);
	}

}
