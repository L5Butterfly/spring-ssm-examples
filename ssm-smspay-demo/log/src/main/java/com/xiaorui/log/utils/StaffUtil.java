package com.xiaorui.log.utils;


import org.apache.log4j.Logger;

import com.framework.core.Constants;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.ReflectUtil;
import com.framework.core.utils.Util;
import com.framework.web.base.vo.StaffLoginVo;
import com.xiaorui.log.common.BusiConstants;


/**
 * @author: empathy
 * @date: 2018年10月29日 下午4:01:22
 * @description: 用户信息工具类
 * @version: v1.0
 */
public final class StaffUtil {
	private static final Logger logger = Logger.getLogger(StaffUtil.class);
	
	private StaffUtil(){
		
	}
	
	/**
	 * 是否为管理级别的角色登录
	 * @return
	 */
	public static void managerCheck(Object obj){
		StaffLoginVo staffVo =  (StaffLoginVo) UserSessionInfo.threadLocal.get().getManagerBean();
		Long roleId = staffVo.getRoleId();
		if(Util.isEmpty(BusiConstants.MANAGER_ROLE_MAP)){
			String[] ids = BusiConstants.MANAGER_ROLE_IDS.split(Constants.SPLIT_COMMON);
			for(String id : ids){
				BusiConstants.MANAGER_ROLE_MAP.put(Long.valueOf(id), Long.valueOf(id));
			}
		}
		
		if(!BusiConstants.MANAGER_ROLE_MAP.containsKey(roleId) && ReflectUtil.hasField(obj, "operId")){
			try {
				ReflectUtil.setValueByFieldName(obj, "operId", staffVo.getStaffId());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
		} 
	}
	
	/**
	 * 是否管理员判断
	 * @param obj
	 */
	public static boolean isManager(Long roleId){
		if(Util.isEmpty(BusiConstants.MANAGER_ROLE_MAP)){
			String[] ids = BusiConstants.MANAGER_ROLE_IDS.split(Constants.SPLIT_COMMON);
			for(String id : ids){
				BusiConstants.MANAGER_ROLE_MAP.put(Long.valueOf(id), Long.valueOf(id));
			}
		}
		
		if(BusiConstants.MANAGER_ROLE_MAP.containsKey(roleId)){
			return true;
		}
		
		return false;
	}
	
	/**
	 * 获取登陆的操作员ID
	 * @return
	 */
	public static Long getStaffId(){
		StaffLoginVo staffVo =  (StaffLoginVo) UserSessionInfo.threadLocal.get().getManagerBean();
		return Util.notEmpty(staffVo) ? staffVo.getStaffId() : null;
	}
}