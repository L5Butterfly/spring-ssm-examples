/**
 * 
 */
package com.xiaorui.log.service;

import java.util.ArrayList;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.OperationLogVo;

/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:18:41
 * @description: 用户登录日志
 * @version: v1.0
 */
public interface IOperationLogService {
	
	ArrayList<OperationLogVo> queryStaffOperLogList4JqByPage(OperationLogVo operationLogVo) throws BaseAppException;

}
