package com.xiaorui.log.common;

import java.util.Date;
import java.util.HashMap;

import com.framework.core.Constants;
import com.framework.core.utils.DateUtil;
import com.framework.core.utils.ParamUtil;
import com.framework.core.utils.Util;
import com.framework.web.po.VersionListPo;

/**
 * @author: empathy
 * @date: 2018年10月28日 下午5:54:34
 * @description: 应用变量信息
 * @version: v1.0
 */
public interface BusiConstants {
	// utf8编码
	String ENCODING_UTF8 = "UTF-8";

	// iso-8859-1
	String ENCODING_ISO_8859 = "iso-8859-1";

	// 操作员原始密码
	String STAFF_INIT_PASSWORD = "abcd1234";

	// 平台解密秘钥
	String DES_KEY = "12345678";

	// 平台验证码长度
	int CODE_LENGTH = 6;

	// 有效状态
	String STATUS_EFF = "00A";

	// 失效状态
	String STATUS_EXP = "00X";

	// 根组织部门ID
	Long ROOT_VNO_ID = 0l;

	// 业务处理成功
	String BUSINESS_SUCCESS = "success";

	// 最迟的失效时间
	Date MAX_EXP_DATE = DateUtil.string2Date("2050-12-31 00:00:00");

	// 超级管理员id
	Long SUPER_ADMIN_ROLE_ID = 1l;

	// 商务人员所属角色ID
	Long SALE_ROLE_ID = ParamUtil.getParamLong("POR_SERV_0003", 1002l);

	// 管理级别的角色ID
	String MANAGER_ROLE_IDS = ParamUtil.getParamString("POR_SERV_0004", "1");
	
	//李博文商务运营ID
	String LIBOWEN_STAFF_IDS = ParamUtil.getParamString("POR_SERV_0005", "1005");
	
	//颜城菲商务运营ID
	String  YANCHENGFEI_STAFF_IDS = ParamUtil.getParamString("POR_SERV_0006", "1260");
	
	//特殊计费点产品
	String  SPECIAL_PRODUCT_JSON = ParamUtil.getParamString("POR_SERV_0007", "{}");
	
	
	//精确指令的产品配置
	String  PRODUCT_ACCURATE_CMD_JSON = ParamUtil.getParamString("POR_SERV_0008", "{}");

	// 管理级别的角色ID
	HashMap<Long, Long> MANAGER_ROLE_MAP = new HashMap<Long, Long>();

	// 文件上传的最大大小
	int FILE_MAX_SIZE = ParamUtil.getParamInt("POR_FTP_SERVER_0003", 5120000);

	/** 文件存储目录 , 在imgservlet中有判断win linx 进行反斜杠转义 */
	String REPOSITORY = Util.parseFTPStorePath(
			ParamUtil.getParamString("POR_FTP_SERVER_0005", Constants.APP_HOME
					+ "/ad_img/"), Constants.APP_HOME + "/ad_img/");

	/** 图片上传后，用于portal访问的域名端口前�? */
	String FTP_DOMAIN_PREFIX = ParamUtil.getParamString("POR_FTP_SERVER_0006",
			"");

	/** 图片访问的前�? */
	String IMG_ACCESS_URL = BusiConstants.FTP_DOMAIN_PREFIX
			+ (Constants.isProduction ? "/img.servlet/" : "/ad_img/");

	// 系统版本信息MAP
	HashMap<String, VersionListPo> VERSION_MAP = new HashMap<String, VersionListPo>();

	// 公司名称
	String COMPANY_NAME = ParamUtil.getParamString("COMPANY_NAME", "小锐科技");

	// 备案号
	String ICP_PREPARED_NO = ParamUtil.getParamString("ICP_PREPARED_NO",
			"ICP1111111");

	// 审核状态：待审核
	String APPROVE_STATUS_INIT = "A01";

	// 审核状态：通过
	String APPROVE_STATUS_PASS = "A02";

	// 审核状态：拒绝
	String APPROVE_STATUS_REJECT = "A03";

	// 订单状态：请求创建，
	String ORDER_STATUS_REQ_CREATE = "S01";

	// 订单状态：请求成功，
	String ORDER_STATUS_REQ_SUCC = "S02";

	// 订单状态：请求失败
	String ORDER_STATUS_REQ_FAILED = "S03";

	// 订单状态：提交创建，
	String ORDER_STATUS_SUB_CREATE = "S04";
	// 订单状态：提交成功，
	String ORDER_STATUS_SUB_SUCC = "S05";
	// 订单状态：提交失败
	String ORDER_STATUS_SUB_FAILED = "S06";
	// 订单状态：计费成功，
	String ORDER_STATUS_BILL_SUCC = "S07";
	// 订单状态：计费失败
	String ORDER_STATUS_BILL_FAILED = "S08";

	// 订单状态：续订成功
	String ORDER_STATUS_REPAY_SUCC = "S09";
	
	// 订单状态：退订成功
	String ORDER_STATUS_UNSUB_SUCC = "S10";
	
	// 订单状态：续订失败
	String ORDER_STATUS_REPAY_FAILED = "S11";
	
	// 订单状态：生成订购关系，
	String ORDER_STATUS_GENERATE = "S12";
		
	// 订单状态：其他失败
	String ORDER_STATUS_OTHER_FAILED = "S99";
	
	//已经点播处理完的订单状态集合
	String RDO_ORDER_DETAILS_STATUS = "-S07-S08-S09-S10-S11-S99-";
	
	//已经包月处理完的订单状态集合
	String MON_ORDER_DETAILS_STATUS = "-S08-S09-S10-S11-S99-";
		
	// 合同状态：创建
	String CONTRACT_STATUS_CREATE = "P01";

	// 计费类型：包月
	String CHARGE_TYPE_MONTH = "C02";

	// 通用判斷：是
	String COMMON_FLAG_TRUE = "T";

	// 通用判斷：否
	String COMMON_FLAG_FALSE = "F";

	// 参数类型：接口参数
	String PARAM_TYPE_INTERFACE = "T02";

	// 周期类型：N+1
	String CYCLE_TYPE_N1 = "B01";
	// 周期类型：N+2
	String CYCLE_TYPE_N2 = "B02";
	// 周期类型：N+3
	String CYCLE_TYPE_N3 = "B03";
	// 周期类型：N+4
	String CYCLE_TYPE_N4 = "B04";
	
	// 周期类型：N+5
	String CYCLE_TYPE_N5 = "B12";
		
	// 周期类型：周一
	String CYCLE_TYPE_W1 = "B05";
	// 周期类型：周二
	String CYCLE_TYPE_W2 = "B06";
	// 周期类型：周三
	String CYCLE_TYPE_W3 = "B07";
	// 周期类型：周四
	String CYCLE_TYPE_W4 = "B08";
	// 周期类型：周五
	String CYCLE_TYPE_W5 = "B09";
	// 周期类型：周六
	String CYCLE_TYPE_W6 = "B10";
	// 周期类型：周日
	String CYCLE_TYPE_W7 = "B11";
	// 周期类型：其他
	String CYCLE_TYPE_OTHER = "B99";
	
	//账单状态：初始化
	String PRODUCT_BILL_STATUS_INIT = "B01";
	//账单状态：渠道负责人提交
	String PRODUCT_BILL_STATUS_SUBMIT = "B02";
	//账单状态：通过
	String PRODUCT_BILL_STATUS_AGREE = "B03";
	//账单状态：拒绝
	String PRODUCT_BILL_STATUS_REFUSE = "B04";
	//账单状态：已开票
	String PRODUCT_BILL_STATUS_BILL= "B05";
	//账单状态：已结款
	String PRODUCT_BILL_STATUS_SETTLE= "B06";
	
	/*渠道账单*/
	//账单状态：初始化
	String DISTRIBUTOR_BILL_STATUS_INIT = "B01";
	//账单状态：渠道负责人提交
	String DISTRIBUTOR_BILL_STATUS_SUBMIT = "B02";
	//账单状态：商务通过
	String DISTRIBUTOR_BILL_STATUS_SALE_AGREE = "B03";
	//账单状态：商务拒绝
	String DISTRIBUTOR_BILL_STATUS_SALE_REFUSE = "B04";
	//账单状态：经理通过
	String DISTRIBUTOR_BILL_STATUS_MGR_AGREE = "B05";
	//账单状态：经理拒绝
	String DISTRIBUTOR_BILL_STATUS_MGR_REFUSE = "B06";
	//账单状态：总经理通过
	String DISTRIBUTOR_BILL_STATUS_MAINMGR_AGREE = "B07";
	//账单状态：总经理拒绝
	String DISTRIBUTOR_BILL_STATUS_MAINMGR_REFUSE = "B08";
	//账单状态：已开票
	String DISTRIBUTOR_BILL_STATUS_BILL= "B09";
	//账单状态：已结款
	String DISTRIBUTOR_BILL_STATUS_SETTLE= "B10";
	
		
	
	
	
	//渠道类型：卡商
	String DISTRIBUTOR_TYPE_CARD = "C03";
	
	//全局单用户日限
	Long DAY_USER_LIMIT_AMOUNT = ParamUtil.getParamLong("POR_SERV_004", -1l);
	
	//全局单用户月限
	Long MON_USER_LIMIT_AMOUNT = ParamUtil.getParamLong("POR_SERV_005", -1l);
	
	//全局单用户刷卡月限
	Long MON_USER_CARD_LIMIT_AMOUNT = ParamUtil.getParamLong("POR_SERV_006", -1l);
	
	//计费类型：点播
	String CHARGE_TYPE_RDO = "C01";
	//计费类型：包月
	String CHARGE_TYPE_MON = "C02";
	//计费类型：包月买断
	String CHARGE_TYPE_MON_RDO = "C03";
	
	//成本类型：通道破解费
	String PAY_RECORD_PRO_CRACK = "T01";
	//成本类型：通道刷卡费
	String PAY_RECORD_PRO_CARD = "T02";
	//成本类型：通道产品费
	String PAY_RECORD_PRO_FEE= "T03";
	//成本类型：渠道退费
	String PAY_RECORD_DIS_RETURN = "T04";
	//成本类型：商务费
	String PAY_RECORD_BUSI_FEE= "T05";
	
	// 接口访问地址
	String API_ACCESS_URL = ParamUtil.getParamString("POR_SERV_0010", "http://access.jiebasdk.com");
	
	//排除生成账单的渠道ID
	String EXCULDE_DISTRIBUTOR_BILL_IDS = "-1454-";
	
	//同步处理，新增content字段
	String EXCULDE_DISTRIBUTOR_SYNC_IDS = "-TD20180501802-TD70077-TD80088-TD2018082003-TD2018082201-TD2018090602-";
	
	//同步处理，新增content字段
	String EXCULDE_DISTRIBUTOR_IVRSYNC_IDS = "-TD2018082801-";
	
	//同步处理，去除code添加status字段
	String EXCULDE_DISTRIBUTOR_IDS = "-1309-";
	
	//产品标记
	String PRODUCT_FLAG="P";
	
	//渠道标记
	String DISTRIBUTOR_FLAG="D";
	
	//模拟订单前缀
	String SIMULATE_PREFIX="TTD";
	
	//模拟手机号
	String SIMULATE_MOBILE="15757111111";

	//产品接口类型：短信模式
	String PRODUCT_API_TYPE_SMS="T01";
	//产品接口类型：验证码模式
	String PRODUCT_API_TYPE_VALID="T02";
	//产品接口类型：离线模式
	String PRODUCT_API_TYPE_OFF="T03";
	
	//产品类型：普通产品
	String PRODUCT_TYPE_SINGLE="T01";
	//产品类型：融合产品
	String PRODUCT_TYPE_FUSE="T02";

	//产品省份到量警告：待处理
	String PRODUCT_ALARM_INIT="S01";
	
	//产品省份到量警告：已处理	
	String PRODUCT_ALARM_HANDLED="S02";
	
	//账单类型：渠道账单
		String BILL_TYPE_DIS="T01";
		
	//账单类型：产品账单
	String BILL_TYPE_PRO="T02";
}