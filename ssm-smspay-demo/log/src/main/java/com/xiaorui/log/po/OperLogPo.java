/*
 * @ClassName OperLog
 * @Description 
 * @version 1.0
 * @Date 2018-10-30 17:06:50
 */
package com.xiaorui.log.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class OperLogPo {
    /**
     * @Fields logId null
     */
    private Long logId;
    /**
     * @Fields sqlInfo null
     */
    private String sqlInfo;
    /**
     * @Fields operType null
     */
    private String operType;
    /**
     * @Fields operTime null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date operTime;
    /**
     * @Fields createTime null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * @Fields operName null
     */
    private String operName;
    /**
     * @Fields extSqlTime null
     */
    private Long extSqlTime;
    /**
     * @Fields extResult null
     */
    private String extResult;
    /**
     * @Fields remark null
     */
    private String remark;
    /**
     * @Fields operId null
     */
    private Long operId;
    
    /**
     * @Fields operIp null
     */
    private String operIp;

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getSqlInfo() {
        return sqlInfo;
    }

    public void setSqlInfo(String sqlInfo) {
        this.sqlInfo = sqlInfo;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public Long getExtSqlTime() {
        return extSqlTime;
    }

    public void setExtSqlTime(Long extSqlTime) {
        this.extSqlTime = extSqlTime;
    }

    public String getExtResult() {
        return extResult;
    }

    public void setExtResult(String extResult) {
        this.extResult = extResult;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getOperId() {
        return operId;
    }

    public void setOperId(Long operId) {
        this.operId = operId;
    }

	public String getOperIp() {
		return operIp;
	}

	public void setOperIp(String operIp) {
		this.operIp = operIp;
	}
}