/*
 * @ClassName BlackList
 * @Description 
 * @version 1.0
 * @Date 2018-10-30 22:00:25
 */
package com.xiaorui.log.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class BlackListPo {
    /**
     * @Fields blackListId null
     */
    private Long blackListId;
    /**
     * @Fields phone null
     */
    private String phone;
    /**
     * @Fields remark null
     */
    private String remark;
    /**
     * @Fields status 00A：有效，00X：失效
     */
    private String status;
    /**
     * @Fields operId null
     */
    private Long operId;
    /**
     * @Fields createDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
     * @Fields updateDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateDate;
    /**
     * @Fields productId null
     */
    private Long productId;
    /**
     * @Fields linkOrderNos null
     */
    private String linkOrderNos;

    public Long getBlackListId() {
        return blackListId;
    }

    public void setBlackListId(Long blackListId) {
        this.blackListId = blackListId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getOperId() {
        return operId;
    }

    public void setOperId(Long operId) {
        this.operId = operId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getLinkOrderNos() {
        return linkOrderNos;
    }

    public void setLinkOrderNos(String linkOrderNos) {
        this.linkOrderNos = linkOrderNos;
    }
}