package com.xiaorui.log.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 
 * @author: empathy
 * @date: 2018年10月28日 下午6:02:17
 * @description: 测试控制器
 * @version: v1.0
 */
@Controller
@RequestMapping
public class HelloController {

}
