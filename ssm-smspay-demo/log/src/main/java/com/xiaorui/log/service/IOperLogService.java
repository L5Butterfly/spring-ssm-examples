package com.xiaorui.log.service;

import java.util.ArrayList;
import java.util.List;

import com.framework.core.exception.BaseAppException;
import com.xiaorui.log.vo.OperLogVo;


/**
 * @author: empathy
 * @date: 2018年10月29日 下午3:57:52
 * @description: 后台操作日志
 * @version: v1.0
 */
public interface IOperLogService {
	
	List<OperLogVo> getOperInfo(OperLogVo operLog) throws Exception;
	
	ArrayList<OperLogVo>  queryOperInfo(OperLogVo operLog) throws Exception;

	/**
	 * 分页查询
	 * @param operLog
	 * @return
	 */
	ArrayList<OperLogVo> queryLogList4Jq(OperLogVo operLog) throws BaseAppException;

}
