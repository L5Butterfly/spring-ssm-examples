package org.mybatis.generator.util;

import java.sql.Types;
import java.util.HashMap;

public class DomainGenerateUtil {

    // public final static String SQL_STR =
    // "SELECT a.attname,pg_catalog.format_type(a.atttypid, a.atttypmod) AS data_type  FROM pg_catalog.pg_attribute a,  (SELECT  c.oid FROM pg_catalog.pg_class c  LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE (c.relname) = lower ( ? ) AND (n.nspname) = lower('public')  ) b WHERE a.attrelid = b.oid "
    // + " AND a.attnum > 0 AND NOT a.attisdropped ORDER BY a.attnum ";
    public final static String SQL_STR = "SELECT DISTINCT a.attname,format_type(a.atttypid, a.atttypmod) as data_type, coalesce(i.indisprimary,false) as primary_key,com.description as comment ,def.adsrc as default , a.attnotnull as notnull, a.* FROM pg_attribute a JOIN pg_class pgc ON pgc.oid = a.attrelid LEFT JOIN pg_index i ON (pgc.oid = i.indrelid AND i.indkey[0] = a.attnum)"
            + "LEFT JOIN pg_description com on (pgc.oid = com.objoid AND a.attnum = com.objsubid) LEFT JOIN pg_attrdef def ON (a.attrelid = def.adrelid AND a.attnum = def.adnum) LEFT JOIN pg_catalog.pg_namespace n ON n.oid = pgc.relnamespace WHERE a.attnum > 0 AND pgc.oid = a.attrelid AND pg_table_is_visible(pgc.oid) AND NOT a.attisdropped AND pgc.relname = lower ( ? )  AND (n.nspname) = lower('public') ORDER BY a.attnum;";

    public static HashMap<String, Integer> DATA_TYPE_HM = new HashMap<String, Integer>();

    public static HashMap<String, Integer> DATA_LEN_HM = new HashMap<String, Integer>();
    
    static {
        DomainGenerateUtil.DATA_TYPE_HM.put("id_1", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("id_2", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("type", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("name", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("status", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("code", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("description", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("date_time", Types.TIMESTAMP);
        DomainGenerateUtil.DATA_TYPE_HM.put("phone_nbr", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("money_type", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("short_num", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("long_num", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("mac", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("short_memo", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("long_memo", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("password", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("short_number", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("flag", Types.VARCHAR);
        DomainGenerateUtil.DATA_TYPE_HM.put("traffic", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("duration", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("package_count", Types.NUMERIC);
        DomainGenerateUtil.DATA_TYPE_HM.put("seqnbr", Types.NUMERIC);
        
        DomainGenerateUtil.DATA_LEN_HM.put("id_1", 12);
        DomainGenerateUtil.DATA_LEN_HM.put("id_2", 7);
        DomainGenerateUtil.DATA_LEN_HM.put("date_time", 6);
        DomainGenerateUtil.DATA_LEN_HM.put("money_type", 16);
        DomainGenerateUtil.DATA_LEN_HM.put("short_num", 5);
        DomainGenerateUtil.DATA_LEN_HM.put("long_num", 12);
        DomainGenerateUtil.DATA_LEN_HM.put("traffic", 18);
        DomainGenerateUtil.DATA_LEN_HM.put("duration", 18);
        DomainGenerateUtil.DATA_LEN_HM.put("package_count", 12);
        DomainGenerateUtil.DATA_LEN_HM.put("seqnbr", 4);
    }

    /**
     * 表字段名 to 类字段名
     * 
     * @param columnName
     * @return
     */
    public static String columnName2fieldName(String columnName) {
        StringBuffer fieldName = new StringBuffer();
        for (int i = 0; i < columnName.length(); i++) {
            if (columnName.charAt(i) == '_' || columnName.charAt(i) == '-') {
                i++;
                fieldName.append(columnName.substring(i, i + 1).toUpperCase());
            } else {
                fieldName.append(columnName.substring(i, i + 1).toLowerCase());
            }
        }
        return fieldName.toString();
    }
    
    public static int getJdbcType(String columnName) {
       /* if (DATA_TYPE_HM.containsKey(columnName)) {
            return DATA_TYPE_HM.get(columnName);
        } else {
            return Types.VARCHAR; // 缺省使用String
        }*/
    	
    	if(columnName.contains("numeric")){
    		return Types.NUMERIC;
    	}else if(columnName.contains("timestamp")){
    		return Types.TIMESTAMP;
    	}else{
    		return Types.VARCHAR;
    	}
    }

    public static int getColLen(String columnName) {
        if (DATA_LEN_HM.containsKey(columnName)) {
            return DATA_TYPE_HM.get(columnName);
        } else {
            return -1; 
        }
    }
}
