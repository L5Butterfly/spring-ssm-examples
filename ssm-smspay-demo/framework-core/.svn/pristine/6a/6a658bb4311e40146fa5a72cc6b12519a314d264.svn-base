/**
 * 
 */
package com.framework.web.interceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.PropertyException;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.executor.statement.BaseStatementHandler;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeException;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.apache.log4j.Logger;

import com.framework.core.page.Page;
import com.framework.core.utils.ReflectUtil;
import com.framework.core.utils.SqlLogUtil;
import com.framework.core.utils.Util;


/**
 * @author Administrator
 *
 */
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class,Integer.class })})
	//@Signature(type = StatementHandler.class, method = "query", args = {Statement.class, ResultHandler.class}),
	//@Signature(type = StatementHandler.class, method = "update", args = {Statement.class})})
public class SqlInterceptor implements Interceptor {
	private static final Logger logger = Logger.getLogger(SqlInterceptor.class);
	
	private String dialect = "";
	private String pageSqlId = "";

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		String sql = null;
		BoundSql boundSql = null;
		if (invocation.getTarget() instanceof RoutingStatementHandler) {
			BaseStatementHandler delegate = (BaseStatementHandler) ReflectUtil.getValueByFieldName((RoutingStatementHandler) invocation.getTarget(), "delegate");
			MappedStatement mappedStatement = (MappedStatement) ReflectUtil.getValueByFieldName(delegate, "mappedStatement");
			Page page = Page.threadLocal.get();
			boundSql = delegate.getBoundSql();
			sql = boundSql.getSql();
			//是否需要进行分页查询
			if (isPageSql(mappedStatement, page)) {
				//获取总数
				int totalCount = getTotalCount(invocation, delegate, mappedStatement);
				page.setRecords(totalCount);
				
				//替换查询sql为分页语句
				sql = generatePageSql(boundSql.getSql(), page);
				ReflectUtil.setValueByFieldName(boundSql, "sql", sql);
			}
		}
		long start = System.currentTimeMillis();  
		Object res  = invocation.proceed();  
        long end = System.currentTimeMillis();  
        SqlLogUtil.logSql(boundSql, end-start);
		return res;
	}

	/**
	 * 对SQL参数(?)设值,参考org.apache.ibatis.executor.parameter.
	 * DefaultParameterHandler
	 * 
	 * @param ps
	 * @param mappedStatement
	 * @param boundSql
	 * @param parameterObject
	 * @throws SQLException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setParameters(PreparedStatement ps, MappedStatement mappedStatement, BoundSql boundSql, Object parameterObject) throws SQLException {
		ErrorContext.instance().activity("setting parameters").object(mappedStatement.getParameterMap().getId());
		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
		if (parameterMappings != null) {
			Configuration configuration = mappedStatement.getConfiguration();
			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
			for (int i = 0; i < parameterMappings.size(); i++) {
		        ParameterMapping parameterMapping = parameterMappings.get(i);
		        if (parameterMapping.getMode() != ParameterMode.OUT) {
		          Object value;
		          String propertyName = parameterMapping.getProperty();
		          if (boundSql.hasAdditionalParameter(propertyName)) { // issue #448 ask first for additional params
		            value = boundSql.getAdditionalParameter(propertyName);
		          } else if (parameterObject == null) {
		            value = null;
		          } else if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
		            value = parameterObject;
		          } else {
		            MetaObject metaObject = configuration.newMetaObject(parameterObject);
		            value = metaObject.getValue(propertyName);
		          }
		          TypeHandler typeHandler = parameterMapping.getTypeHandler();
		          JdbcType jdbcType = parameterMapping.getJdbcType();
		          if (value == null && jdbcType == null) {
		            jdbcType = configuration.getJdbcTypeForNull();
		          }
		          try {
		            typeHandler.setParameter(ps, i + 1, value, jdbcType);
		          } catch (TypeException e) {
		            throw new TypeException("Could not set parameters for mapping: " + parameterMapping + ". Cause: " + e, e);
		          } catch (SQLException e) {
		            throw new TypeException("Could not set parameters for mapping: " + parameterMapping + ". Cause: " + e, e);
		          }
		        }
		      }
		}
	}

	/**
	 * 获取查询数据的总数量
	 * @param invocation
	 * @param delegate
	 * @param mappedStatement
	 * @return
	 * @throws Exception
	 */
	private int getTotalCount(Invocation invocation, BaseStatementHandler delegate, MappedStatement mappedStatement) throws Exception{
		BoundSql boundSql = delegate.getBoundSql();
		Object parameterObject = boundSql.getParameterObject();
		String sql = boundSql.getSql();
		
		String countSqlId = mappedStatement.getId().replaceAll(pageSqlId, "Count");
		MappedStatement countMappedStatement = null;
		if (mappedStatement.getConfiguration().hasStatement(countSqlId)) {
			countMappedStatement = mappedStatement.getConfiguration().getMappedStatement(countSqlId);
		}
		String countSql = null;
		if (countMappedStatement != null) {
			countSql = countMappedStatement.getBoundSql(parameterObject).getSql();
		} else {
			countSql = "SELECT COUNT(1) FROM (" + sql + ") T_COUNT";
		}

		int totalCount = 0;
		PreparedStatement countStmt = null;
		ResultSet resultSet = null;
		try {
			Connection connection = null;
			Object obj =  invocation.getArgs()[0];
			if(obj instanceof Connection){
				connection = (Connection)obj;
			}else if(obj instanceof PreparedStatement){
				connection = ((PreparedStatement) obj).getConnection();
			}
			
			countStmt = connection.prepareStatement(countSql);
			BoundSql countBoundSql = new BoundSql(mappedStatement.getConfiguration(), countSql,
					boundSql.getParameterMappings(), parameterObject);
			
			List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
			if (parameterMappings != null) {
				for (int i = 0; i < parameterMappings.size(); i++) {
			        ParameterMapping parameterMapping = parameterMappings.get(i);
			        if (parameterMapping.getMode() != ParameterMode.OUT) {
			          String propertyName = parameterMapping.getProperty();
			          if (boundSql.hasAdditionalParameter(propertyName)) { 
			        	  countBoundSql.setAdditionalParameter(propertyName, boundSql.getAdditionalParameter(propertyName));
			          }
			        }
				}
			}
			
			setParameters(countStmt, mappedStatement, countBoundSql, parameterObject);
			long start = System.currentTimeMillis();
			resultSet = countStmt.executeQuery();
			long end = System.currentTimeMillis();
			if (resultSet.next()) {
				totalCount = resultSet.getInt(1);
			}
			SqlLogUtil.logSql(countBoundSql, end-start);
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
			} finally {
				if (countStmt != null) {
					countStmt.close();
				}
			}
		}
		
		return totalCount;
	}
	
	/**
	 * 是否需要进行分页查询
	 * @return
	 */
	private boolean isPageSql(MappedStatement statement, Page page){
		if(Util.isEmpty(page)){
			return false;
		}
		
		if(page.getRows() <= 0){
			return false;
		}
		
		if(!statement.getId().matches(".*(" + this.pageSqlId + ")$")){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 根据数据库方言，生成特定的分页sql
	 * 
	 * @param sql
	 * @param page
	 * @return
	 */
	private String generatePageSql(String sql, Page page) {
		//添加排序操作
		if(Util.notEmpty(page.getSidx()) && Util.notEmpty(page.getSord())){
			String upperSql = sql.replaceAll("\n", "").replaceAll(" +", " ").replaceAll("\t", "").toUpperCase();
			
			sql += upperSql.contains(" ORDER BY ") ? " , " : " ORDER BY ";
			String colName = Util.filed2ColName(page.getSidx());
			colName = colName.charAt(0) == '_' ? colName.substring(1) : colName;
			String sortName = colName;
			
			//字段是否包含前缀
			if(upperSql.indexOf("."+colName.toUpperCase()) != -1){
				int index = upperSql.indexOf(colName.toUpperCase());
				upperSql =upperSql.substring(0, index);
				String prefix = upperSql.substring(upperSql.lastIndexOf(",")+1);
				if(prefix.contains(" ")){
					prefix = upperSql.substring(upperSql.lastIndexOf(" ")+1);
				}
				sortName = prefix + colName;
			}
			
			sql += sortName + " " + page.getSord();
		}
		
		if (page != null && StringUtils.isNotBlank(dialect)) {
			//查询数目小于0,查询全部
			if(page.getPageNo() < 0){
				return sql;
			}
			StringBuffer pageSql = new StringBuffer();
			if ("mysql".equals(dialect)) {
				pageSql.append(sql);
				pageSql.append(" LIMIT " + ((page.getPageNo()-1)* page.getRows()) + ","+ page.getRows());
			}else if("postgres".equals(dialect)){
				pageSql.append(sql);
				pageSql.append(" LIMIT " + page.getRows() + " offset " +((page.getPageNo()-1)* page.getRows()) );
			} else if ("oracle".equals(dialect)) {
				pageSql.append("SELECT * FROM (SELECT TMP_TB.*,ROWNUM ROW_ID FROM (");
				pageSql.append(sql);
				pageSql.append(") AS TMP_TB WHERE ROWNUM <= ");
				pageSql.append(page.getCurrentResult() + page.getRows());
				pageSql.append(") WHERE ROW_ID > ");
				pageSql.append(page.getCurrentResult());
			}
		
			return pageSql.toString();
		} else {
			return sql;
		}
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		try {
			if (StringUtils.isEmpty(this.dialect = properties.getProperty("dialect"))) {
				throw new PropertyException("dialect property is not found!");
			}
			if (StringUtils.isEmpty(this.pageSqlId = properties.getProperty("pageSqlId"))) {
				throw new PropertyException("pageSqlId property is not found!");
			}
		} catch (PropertyException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
