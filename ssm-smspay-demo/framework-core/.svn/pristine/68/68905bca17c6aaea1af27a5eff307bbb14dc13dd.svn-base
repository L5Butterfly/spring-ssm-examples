package com.framework.core.exception;

import org.apache.log4j.Logger;

import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.Util;
import com.framework.web.base.cache.item.SysErrDescriptionCache;
import com.framework.web.base.cache.vo.SysErrDescriptionCacheVo;

/**
 * 
 * @author xgf
 * 
 */
public final class ExceptionHandler {
	
    private static Logger logger = Logger.getLogger(ExceptionHandler.class);

    /**
     * 构造器.
     */
    private ExceptionHandler() {}

    /**
     * 发布异常,默认错误类型为业务错误.
     * 
     * @param errorCode 错误编码
     * @throws BaseAppException
     */
    public static BaseAppException publish(String errorCode) throws BaseAppException {
        return publish(errorCode, "");
    }
    
    /**
     * 发步异常,默认错误类型为业务错误.
     * 
     * @param errorCode 编码
     * @param t Throwable
     * @throws BaseAppException
     */
    public static BaseAppException publish(String errorCode, Throwable t) throws BaseAppException {
        return publish(true, errorCode, "", t);
    }

    /**
     * 发布异常,默认错误类型为业务错误.
     * 
     * @param errorCode 错误编码
     * @param msg 错误消息
     * @throws BaseAppException
     */
    public static BaseAppException publish(String errorCode, String msg) throws BaseAppException {
        return publish(true,errorCode, msg, null);
    }
    
    public static BaseAppException publish(String errorCode, String msg, Throwable t) throws BaseAppException {
        return publish(true,errorCode, msg, t);
    }
    
    
    /**
     * 发布异常,默认错误类型为业务错误.
     * @param isThrow 是否将异常抛出
     * @param errorCode 错误编码
     * @throws BaseAppException
     */
    public static BaseAppException publish(boolean isThrow,String errorCode) throws BaseAppException {
        return publish(isThrow,errorCode, "");
    }

    /**
     * 发布异常,默认错误类型为业务错误.
     * @param isThrow  是否将异常抛出
     * @param errorCode 错误编码
     * @param msg 错误消息
     * @throws BaseAppException
     */
    public static BaseAppException publish(boolean isThrow,String errorCode, String msg) throws BaseAppException {
        return publish(isThrow,errorCode, msg, null);
    }
    
    /**
     * 发布异常,默认错误类型为业务错误.
     * @param isThrow 是否将异常抛出
     * @param errorCode  错误编码
     * @param msg 错误消息
     * @throws BaseAppException
     */
    public static BaseAppException publishMsg(boolean isThrow,String msg) throws BaseAppException {
        return publish(isThrow,SysErrCode.UNKNOW_EXPCEPTION, msg, null);
    }
    
    public static BaseAppException publishMsg(String errorMsg) throws BaseAppException {
        return publish(SysErrCode.UNKNOW_EXPCEPTION,errorMsg);
    }

    /**
     * 发布异常.
     * @param isThrow  是否将异常抛出
     * @param errorCode  错误编码
     * @param msg 错误消息
     * @param t Throwable
     * @throws BaseAppException
     */
    public static BaseAppException publish(boolean isThrow,String errorCode, String msg, Throwable t) throws BaseAppException {
        if(!isThrow) {
        	return null;
        }
        
        String errDesc = msg;
        if (Util.isEmpty(msg)) { // 异常发布如果有自定义传入则不取系统定义的
        	errDesc = getDesc(errorCode);
        }
        BaseAppException bae = t instanceof BaseAppException ? (BaseAppException)t : new BaseAppException(errorCode, errDesc, t);
        logger.error(getUserInfoLog(errorCode, errDesc), bae);
        throw bae;
    }
    
    /**
     * 获取异常
     * 
     * @param errCode
     * @return
     */
    public static String getDesc(String errCode) {
        return getDesc(errCode, "zh_CN");
    }

    /**
     * 获取用户信息日志
     * @param errorCode 错误码
     * @param errDesc 错误描述
     * @return 信息日志
     */
    public static String getUserInfoLog(String errorCode, String errDesc) {
    	 // 获取session客户端信息，记录日志
        UserSessionInfo sessionInfo = UserSessionInfo.threadLocal.get();
        StringBuffer userInfoBuff = new StringBuffer();
        if(Util.notEmpty(sessionInfo)){
        	 userInfoBuff.append("<").append(sessionInfo.getAccessIp()).append("> ");
             userInfoBuff.append("<").append(sessionInfo.getStaffId()).append("> ");
             userInfoBuff.append("<").append(sessionInfo.getAccessCode()).append("> ");
        }
       
        userInfoBuff.append("<").append(errorCode).append("> ");
        userInfoBuff.append(errDesc).append("\n");
        
        return userInfoBuff.toString();
    }
    
    /**
     * 根据异常编码获取异常描述
     * 
     * @param errCode
     * @param langType
     * @return
     */
	public static String getDesc(String errCode, String langType) {
        // 缓存获取
        try {// 捕获异常防止死循环
            String key = new StringBuilder().append(langType).append("_").append(errCode).toString();
            SysErrDescriptionCacheVo sysErrDescriptionVo = SysErrDescriptionCache.INSTANCE.getItem(key);
            return sysErrDescriptionVo.getErrDescription();
        	
        } catch (Exception e) {
            logger.error("GET ERR_CODE ERROR, KENEL EXCEPTION", e);
        }
        return errCode + " GET ERR_CODE ERROR, KENEL EXCEPTION";
    }
}
