/**
 * 
 */
package com.framework.core.cache.redis.jedo;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

/**
 * @author Administrator
 *
 */
public final class JedisUtil {
	
	private static final Logger logger = Logger.getLogger(JedisUtil.class);
	
	private RedisTemplate<Serializable, Object> redisTemplate;

	/**
	 * 
	 * @param prefix
	 * @param key
	 */
	public void hdel(String prefix, String key) {
		redisTemplate.opsForHash().delete(prefix, key);
	}
	
	/**
	 * 
	 * @param prefix
	 */
	public List<Object> hvals(String prefix) {
		return redisTemplate.opsForHash().values(prefix);
	}
	
	/**
	 * 
	 * @param prefix
	 * @param key
	 */
	public Object hget(String prefix, String key) {
		return redisTemplate.opsForHash().get(prefix, key);
	}
	
	/**
	 * 
	 * @param prefix
	 * @param key
	 */
	public Object get(String key) {
		ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
		return operations.get(key);
	}
	
	/**
	 * 新增缓存，1表示新增，0表示修改
	 * @param prefix
	 * @param key
	 * @param value
	 */
	public int hset(String prefix, String key, String value) {
		int res = 0;
		if(!redisTemplate.opsForHash().hasKey(prefix, key)){
			res = 1;
		}
		redisTemplate.opsForHash().put(prefix, key, value);
		
		return res;
	}

	/**
	 * 删除对应的value
	 * 
	 * @param key
	 */
	public void del(String key) {
		if (exists(key)) {
			redisTemplate.delete(key);
		}
	}

	/**
	 * 判断缓存中是否有对应的value
	 * 
	 * @param key
	 * @return
	 */
	public boolean exists(final String key) {
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置有效期，单位秒
	 * @param key
	 * @param expire
	 * @return
	 */
	public boolean setExpire(String key, Long expire) {
		return redisTemplate.expire(key, expire, TimeUnit.SECONDS);
	}
	
	/**
	 * 写入缓存
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean set(final String key, Object value, Long expireTime) {
		boolean result = false;
		try {
			ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
			operations.set(key, value);
			redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
			result = true;
		} catch (Exception e) {
			logger.error("Set redis failed. key is:" + key+", value:"+ value+", expTime:" + expireTime);
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	public void setRedisTemplate(RedisTemplate<Serializable, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}
}
