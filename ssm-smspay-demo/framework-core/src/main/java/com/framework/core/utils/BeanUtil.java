package com.framework.core.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.cglib.beans.BeanCopier;
import net.sf.cglib.core.Converter;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.log4j.Logger;


/**
 * bean操作辅助类，如对象拷贝等
 * 
 * @author xgf
 * 
 */
public class BeanUtil {

	public static final Logger logger = Logger.getLogger(BeanUtil.class.getName());
	// 拷贝实例map
	private static Map<String, BeanCopier> beanCopierMap = new ConcurrentHashMap<String, BeanCopier>();

	private static CopyFromNotNullBeanUtilsBean copyBean = new CopyFromNotNullBeanUtilsBean();
	
	/**
	 * 对象拷贝
	 * 
	 * @param source
	 * @param target
	 * @param overFlag <br />true : Source中�?�覆盖Target，不管Source和Target的�?�是否为空；<br />false ：Source 只填 Target 中的空�?�， Target 有�?�时，不覆盖
	 */
	public static void copyProperties(Object source, Object target, boolean overFlag) {
		if(source == null || target == null)
			return;
		// 组合主键
		String compKey = source.getClass().getName() + target.getClass().getName()+overFlag;
		BeanCopier copier = beanCopierMap.get(compKey);
		if (copier == null) {
			copier = BeanCopier.create(source.getClass(), target.getClass(), !overFlag);
			synchronized (beanCopierMap) {
				beanCopierMap.put(compKey, copier);
			}
		}
		Converter converter = null;
		if (!overFlag) {
			//target 有�?�时，不覆盖
			converter = new CustomConverter(target);
		}
		copier.copy(source, target, converter);
	}

	/**
	 * 默认全量拷贝
	 * 
	 * @param source
	 * @param target
	 */
	public static void copyProperties(Object source, Object target) {
		copyProperties(source, target, true);
	}
	
	/**
	 * 将source中非空的属性覆盖target中的属性，为空的不覆盖
	 * 
	 * @param source
	 * @param target
	 */
	public static void copyNotNullProperties(Object source, Object target) {
		try {
			copyBean.copyProperties(target, source);
		} catch (Exception e) {
			logger.error("exception !",e);
		}
	}
	
	/**
     * 浅拷�?
     * */
    @SuppressWarnings("unchecked")
    public static <T> T getPOJO(Object sorce, Class<T> clazz) {
    	if(sorce == null || clazz == null)
    		return null;
        if (clazz.isPrimitive() || clazz.equals(Long.class) || clazz.equals(String.class) || clazz.equals(Integer.class) || clazz.equals(Double.class) || clazz.equals(Date.class))
            return (T) sorce;
		try {
			T t = clazz.newInstance();
			BeanUtil.copyProperties(sorce, t);
			return t;
		} catch (Exception e) {
			logger.error("exception !",e);
			throw new RuntimeException(e.getMessage());
		}
	}
    /**
     * 浅拷�?
     * */
    @SuppressWarnings("unchecked")
	public static <T> T getClone(Object sorce) {
    	if(sorce == null)
    		return null;
    	return (T) getPOJO(sorce,sorce.getClass());
	}
    
    /**
     * 深拷�?
     * */
    @SuppressWarnings("unchecked")
	public static <T> T getDeepClone(Object sorce) {
    	if(sorce == null)
    		return null;
    	Class<?> clazz = sorce.getClass();
    	if (clazz.isPrimitive() || clazz.equals(Long.class) || clazz.equals(String.class) || clazz.equals(Integer.class) || clazz.equals(Double.class) || clazz.equals(Date.class))
              return (T) sorce;
    	String json = GsonFactory.getGson().toJson(sorce);
    	return (T) GsonFactory.getGson().fromJson(json,clazz);
	}
}


class CustomConverter implements Converter {
    public static final Logger logger = Logger.getLogger(CustomConverter.class.getName());
    public Object target;

    public CustomConverter(Object target) {
        this.target = target;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Object convert(Object sourceValue, Class target, Object context) {
        if (context.toString().startsWith("set")) {
            String methodName = context.toString().replace(context.toString().substring(0, 3), "get");
            Map<String, PropertyDescriptor> getterMap = getPropertiesHelper(this.target.getClass());
            if (getterMap.containsKey(methodName)) {
                try {
                    Object targetValue = getterMap.get(methodName).getReadMethod().invoke(this.target);
                    if ((targetValue instanceof String) && (Util.isEmpty((String) targetValue))) {// 是空字符串，才取源对象�?�覆�?
                        return sourceValue;
                    } else if (Util.isEmpty(targetValue)) {// 是空对象，才取源对象值覆�?
                        return sourceValue;
                    } else {
                        return targetValue;
                    }
                } catch (Exception e) {
                    logger.error("reflect get value error:", e);
                }
            }
        }
        return sourceValue;
    }

    /**
     * 获取类的�?有get方法
     * 
     * @param type
     * @return
     */
    private Map<String, PropertyDescriptor> getPropertiesHelper(Class<?> type) {
        Map<String, PropertyDescriptor> propertyMap = new HashMap<String, PropertyDescriptor>();
        try {
            BeanInfo info = Introspector.getBeanInfo(type, Object.class);
            PropertyDescriptor[] all = info.getPropertyDescriptors();
            for (int i = 0; i < all.length; i++) {
                PropertyDescriptor pd = all[i];
                if (pd.getReadMethod() != null) {
                    propertyMap.put(pd.getReadMethod().getName(), pd);
                }
            }
        } catch (Exception e) {
            logger.error("reflect get value error:", e);
        }
        return propertyMap;
    }
}


/**
 * 如果source中某字段没值（为null），则该字段不复制，也就是不要把null复制到target当中。 
 * @author Administrator
 *
 */
class CopyFromNotNullBeanUtilsBean extends BeanUtilsBean {  
	  
    @Override  
    public void copyProperty(Object bean, String name, Object value) throws IllegalAccessException, InvocationTargetException {  
        if (value == null) {
            return;  
        }  
        super.copyProperty(bean, name, value);  
    }  
}
