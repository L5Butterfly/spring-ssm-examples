package com.framework.core.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.framework.core.Constants;

/**
 * 2018
 * 
 * @author scq
 * 
 */
public final class HMACSHA1Util {
	private static Logger logger = Logger.getLogger(HMACSHA1Util.class);

	private HMACSHA1Util() {

	}

	/**
	 * data：加密内容二进制,key:密钥二进制
	 * 
	 * @param data
	 * @param key
	 * @return
	 */
	public static byte[] convert(byte[] data, byte[] key) {
		try {
			SecretKeySpec signingKey = new SecretKeySpec(key, "HmacSHA1");
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);
			return mac.doFinal(data);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public static String convert2Str(byte[] data, byte[] key, String retType) {
		byte[] byteSign = convert(data, key);
		if (retType.equals(Constants.Base64_ENCODING))
			return Base64.encodeBase64String(byteSign);
		else if (retType.equals(Constants.HexStr_ENCODING))
			return HexStringUtil.bytes2strHex(byteSign);
		else
			return Base64.encodeBase64String(byteSign);
	}

	public static String hmacSha1(String data, String key) {
		byte[] res = convert(data.getBytes(), key.getBytes());
		return new String(res);
	}

}
