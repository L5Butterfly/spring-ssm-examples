/**
 * @author 
 * @version 
 * ��˵��
 */
package com.framework.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import org.apache.log4j.Logger;

import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;
import com.framework.core.exception.SysErrCode;

/** 
 * ���� : @author 
 * @version ����ʱ�䣺2016��8��1�� ����2:51:29 
 * ��˵�� 
 */
/**
 * @author Administrator
 *
 */
public final class DateUtil {

	private static final Logger logger = Logger.getLogger(DateUtil.class);

	/** Ĭ�����ڸ�ʽ:yyyy-MM-dd HH:mm:ss. */
	public static final String DATE_FORMAT_COMMON = "yyyy-MM-dd HH:mm:ss";

	/** �����ļ����������ڸ�ʽ:yyyyMMdd_HHmmss. */
	public static final String DATE_FORMAT_FILE = "yyyyMMdd_HHmmss";

	/** ���ڸ�ʽ: yyyy */
	public static final String DATE_FORMAT_YEAR = "yyyy";

	/** ���ڸ�ʽ: yyyy-MM */
	public static final String DATE_FORMAT_MONTH = "yyyy-MM";

	/** ���ڸ�ʽ:yyyy-MM-dd */
	public static final String DATE_FORMAT_DAY = "yyyy-MM-dd";

	/** �������ڸ�ʽ:yyyy-MM-dd HH:mm */
	public static final String DATE_FORMAT_MINUTE = "yyyy-MM-dd HH:mm";

	/** ���ڸ�ʽ: yyyyMMddHHmmss */
	public static final String DATE_FORMAT_NOSPLIT = "yyyyMMddHHmmss";

	/** ���ڸ�ʽ:yyyyMMdd */
	public static final String DATE_FORMAT_DAY_EN = "yyyyMMdd";
	
	/** ���ڸ�ʽ:yyyyMMdd */
	public static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss'Z'";

	public static final int TYPE_DAY= 0;
	public static final int TYPE_HOUR = 3;
	public static final int TYPE_MINITE = 4;
	public static final int TYPE_SECOND = 5;
	public static final int TYPE_MONTH = 1;
	public static final int TYPE_YEAR = 2;
	public static Date DATE_UNLIMIT;

	static {
		try {
			DATE_UNLIMIT = new SimpleDateFormat("yyyyMMddHHmmss").parse("20301230235959");
		} catch (ParseException e) {
			logger.error("DateHelper init DATE_UNLIMIT error", e);
		}
	}
	
    /**
     * 返回系统当前时间(精确到毫秒),作为一个唯一的订单编号
     * @return
     *      以yyyyMMddHHmmss为格式的当前系统时间
     */
	public  static String getOrderNum(){
		Date date=new Date();
		DateFormat df=new SimpleDateFormat(DATE_FORMAT_NOSPLIT);
		return df.format(date);
	}
	
	private DateUtil() {
	}
	/**
	 * 将秒数改为xx天xx小时xx分xx秒<br />
	 * 如 formatTime（60） --> 1分<br />
	 * 如 formatTime（70） --> 1分10秒<br />
	 * 类推
	 * */
	public static String formatTime(Long t){
	    return Util.isEmpty(t) ? "" : formatTime(t.intValue(),false);
	}
	
	/**
	 * 将秒数改为xx天xx小时xx分xx秒<br />
	 * 如 formatTime（60） --> 1分<br />
	 * 如 formatTime（70） --> 1分10秒<br />
	 * 类推
	 * */
	public static String formatTime(int t){
	    return Util.isEmpty(t) ? "" :  formatTime(t,false);
	}
	/**
	 * 将秒数改为xx天xx小时xx分xx秒<br />
	 * 如 formatTime（60） --> 1分<br />
	 * 如 formatTime（70） --> 1分10秒<br />
	 * 类推
	 * */
	public static String formatTime(int t, boolean isShowEmpty){
	    int[] s = {60,60,24,365,1000000};
	    String[] ss = {"秒","分","小时","天","年"};
	    String r = "";
	    for(int i = 0;i<s.length;i++){
	       r = (t%s[i] == 0 && !isShowEmpty) ? r : t%s[i] + ss[i]+ r;
	       t = (t/s[i]);
	       if(t==0)break;
	    }
	    return r;
	}
	
	/**
	 * 将Date转换成字符串
	 *
	 * @param date
	 *            Date 要转换的Date实例
	 * @param format
	 *            String 日期格式字符串
	 *
	 * @return String
	 */
	public static String date2String(Date date, String format) {
		if (date == null) {
			date = new Date();
		}
		SimpleDateFormat sdf = null;
		try {
			sdf = new SimpleDateFormat(format);
		} catch (Exception e) {
			sdf = new SimpleDateFormat(DATE_FORMAT_COMMON);
			logger.debug("====com.wboss.bsn.common.DateUtils.date2String(" + date + "," + format);
		}
		return sdf.format(date);
	}

	/**
	 * 将Date转换成字符串(天)
	 *
	 * @param date
	 * @return
	 */
	public static String date2StringDay(Date date) {
		return date2String(date, DATE_FORMAT_DAY);
	}

	/**
	 * 将Date转换成字符串(月)
	 *
	 * @param date
	 * @return
	 */
	public static String date2StringMonth(Date date) {
		return date2String(date, DATE_FORMAT_MONTH);
	}

	public static String date2StringYear(Date date) {
		return date2String(date, DATE_FORMAT_YEAR);
	}
	
	/**
	 * 得到合适的格式化时间
	 *
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2SuitString(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int sec = calendar.get(Calendar.SECOND);
		int minute = calendar.get(Calendar.MINUTE);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		if (sec == 0 && minute != 0) {// 只取分钟
			return date2String(date, DATE_FORMAT_MINUTE);
		}
		if (minute == 0 && hour == 0) {// 取日
			return date2String(date, DATE_FORMAT_DAY);
		}
		return date2String(date, DATE_FORMAT_MINUTE);
	}

	/**
	 * 将Date类转换成字符串形式,使用默认的格式做转换. yyyy年MM月DD日 HH:MM:SS
	 *
	 * @param date
	 *            Date
	 * @return String
	 */
	public static String date2String(Date date) {
		return date2String(date, DATE_FORMAT_COMMON);
	}

	/**
	 * 得到字符串形式的当前时间,日期格式采用默认的格式.
	 *
	 * @return String
	 */
	public static String getCurrentDate() {
		Date date = new Date();
		return date2String(date, DATE_FORMAT_COMMON);
	}
	
	public static String getCurrentDate(String date_format) {
		Date date = new Date();
		return date2String(date,date_format );
	}

    /**
     * 日期格式. yyyyMMddHHmmss
     * 
     * */
    public static String getCurrentDate2() {
        return date2String(new Date(), DATE_FORMAT_NOSPLIT);
    }

    /**
     * 日期格式. yyyyMMdd
     * 
     * */
    public static String getCurrentDate3() {
        return date2String(new Date(), DATE_FORMAT_DAY_EN);
    }

	/**
	 * 将字符串格式的日期转换成SQL日期,格式yyyy-MM-DD
	 *
	 * @param date
	 *            String
	 * @return Date
	 */
    public static java.util.Date string2Date(String date) {
       return string2Date(date, DATE_FORMAT_COMMON);
    }

    public static java.util.Date string2Date(String date, String dateFormat) {
        if (Util.isEmpty(date))
            return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            long time = sdf.parse(date).getTime();
            return new java.util.Date(time);
        } catch (Exception e) {
            logger.error("unsupported date format : " + date);
            throw new RuntimeException("时间格式不正确：" + date+"，正确格式："+dateFormat);
        }
    }

	/**
	 * 得到当前日期
	 *
	 * @return java.sql.Date 当前服务器时间
	 */
	public static java.util.Date getNowDate() {
		return new java.util.Date();
	}

	/**
	 * 偏移时间
	 *
	 * @param date
	 *            Date 初始时间
	 * @param second
	 *            long 偏移秒数
	 * @return Date
	 */
	public static Date offsetSecond(Date date, long seconds) {
		long time = date.getTime();
		time = time + (seconds * 1000);
		return new Date(time);
	}

	/**
	 * 偏移时间
	 *
	 * @param date
	 *            Date 初始时间
	 * @param minute
	 *            long 偏移分钟数
	 *
	 * @return Date
	 */
	public static Date offsetMinute(Date date, long minutes) {
		return offsetSecond(date, 60 * minutes);
	}

	/**
	 * 偏移时间
	 *
	 * @param date
	 *            Date 初始时间
	 * @param hour
	 *            long 偏移小时数
	 *
	 * @return Date
	 */
	public static Date offsetHour(Date date, long hours) {
		return offsetMinute(date, 60 * hours);
	}

	/**
	 * 偏移时间
	 *
	 * @param date
	 *            Date 初始时间
	 * @param day
	 *            long 偏移天数
	 * @return Date
	 */
	public static Date offsetDay(Date date, int days) {
		if(date == null)
			return null;
		return offsetHour(date, 24 * days);
	}
	/**
	 * @param baseDate 偏移基准
	 * @param unit 偏移数量
	 * @param timeOffsetType 偏移类型：DAY,HOU,MIN
	 * */
	public static Date offset(Date baseDate,int unit,String timeOffsetType) throws BaseAppException{
		switch (timeOffsetType) {
		case "DAY":
			return offsetDay(baseDate, unit);
		case "HOU":
			return offsetHour(baseDate, unit);
		case "MIN":
			return offsetMinute(baseDate, unit);
		default:
			ExceptionHandler.publish(SysErrCode.UNKNOW_EXPCEPTION,"Unknow time offset type : "+timeOffsetType);
			break;
		}
		return null;
	}

	/**
	 * 偏移时间
	 *
	 * @param date
	 *            Date 初始时间
	 * @param week
	 *            long 偏移周数
	 * @return Date
	 */
	public static java.util.Date offsetWeek(java.util.Date date, int weeks) {
		return offsetDay(date, 7 * weeks);
	}

	/**
	 * 得到本月的最后时间
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @return Date
	 */
	public static java.util.Date getMonthLastday(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, maxDay);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		newDate.setTime(calendar.getTimeInMillis());
		return newDate;
	}

	/**
	 * 得到传入当月的天数（即最后一天的日子）
	 *
	 * @param date
	 * @return
	 */
	public static int getMonthMaxDay(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return maxDay;
	}

	/**
	 * 得到传入时间的当月最后一天
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @return Date
	 */
	public static java.util.Date getMonthLastDay(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, maxDay);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		newDate.setTime(calendar.getTimeInMillis());
		return newDate;
	}

	/**
	 * 得到传入时间的前一天
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @return Date
	 */
	public static java.util.Date getYesterday(java.util.Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		date = calendar.getTime();
		return date;
	}

	/**
	 * 得到分钟的最开始时间
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @return Date
	 */
	public static java.util.Date getMinuteFisrt(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		calendar.set(Calendar.SECOND, 00);
		newDate.setTime(calendar.getTimeInMillis());
		return newDate;
	}

	/**
	 * 得到分钟的最开始时间
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @return Date
	 */
	public static java.util.Date getMinuteLast(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		calendar.set(Calendar.SECOND, 59);
		newDate.setTime(calendar.getTimeInMillis());
		return newDate;
	}

	/**
	 * 得到本月的开始时间
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @return Date
	 */
	public static java.util.Date getMonthBeginday(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		newDate.setTime(calendar.getTimeInMillis());
		return newDate;
	}

	/**
	 * 得到时间天
	 *
	 * @param date
	 * @return
	 */
	public static int getDateDay(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 得到时间月
	 *
	 * @param date
	 * @return
	 */
	public static int getDateMonth(java.util.Date date) {
		Date newDate = new Date(date.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		return calendar.get(Calendar.MONTH);
	}

	/**
	 * 偏移时间(按月) 规则: 1. 如果要偏移的时间是月末, 偏移后也是月末
	 *
	 * 2. 要偏移的时间的当前天大于偏移后的月份的最大天数也调整为月末, 比如12月30号(非月末)偏移两个月
	 *
	 * 应变为2月28(29)号
	 *
	 * @param date
	 *            Date 要偏移的时间
	 * @param months
	 *            int 要偏移的月份
	 * @return Date
	 */
	public static java.sql.Date offsetMonth(java.sql.Date date, int months) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int curDay = calendar.get(Calendar.DAY_OF_MONTH);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		// 将当前天设置为1号, 然后增加月份数 (先加月份, 再加天)
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.MONTH, months);
		// 加过月份以后的日期当月的最大天数
		int newMaxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		// 如果当前天为月底, 加过以后也调整为月底
		if (curDay == maxDay) {
			calendar.set(Calendar.DAY_OF_MONTH, newMaxDay);
		} else {
			// 如果要加的初始日期的天大于新的月份的最大天数, 则调整为月底, 比如12月30号加两个月
			// 不是2 * 30天 到 3月2号, 而是到2月底
			if (curDay > newMaxDay) {
				calendar.set(Calendar.DAY_OF_MONTH, newMaxDay);
			} else {
				calendar.set(Calendar.DAY_OF_MONTH, curDay);
			}
		}
		date.setTime(calendar.getTimeInMillis());
		return date;
	}

	// 加一个Util.date 类型的
	/**
	 * 偏移时间(按月) 规则: 1. 如果要偏移的时间是月末, 偏移后也是月末
	 *
	 * 2. 要偏移的时间的当前天大于偏移后的月份的最大天数也调整为月末, 比如12月30号(非月末)偏移两个月
	 *
	 * 应变为2月28(29)号
	 *
	 * @param date
	 *            java.util.Date 要偏移的时间
	 * @param months
	 *            int 要偏移的月份
	 * @return Date
	 */
	public static java.util.Date offsetMonth(java.util.Date date, int months) {
		Calendar calendar = Calendar.getInstance();
		Date reDate = new Date();
		calendar.setTime(date);
		int curDay = calendar.get(Calendar.DAY_OF_MONTH);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		// 将当前天设置为1号, 然后增加月份数 (先加月份, 再加天)
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.MONTH, months);
		// 加过月份以后的日期当月的最大天数
		int newMaxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		// 如果当前天为月底, 加过以后也调整为月底
		if (curDay == maxDay) {
			calendar.set(Calendar.DAY_OF_MONTH, newMaxDay);
		} else {
			// 如果要加的初始日期的天大于新的月份的最大天数, 则调整为月底, 比如12月30号加两个月
			// 不是2 * 30天 到 3月2号, 而是到2月底
			if (curDay > newMaxDay) {
				calendar.set(Calendar.DAY_OF_MONTH, newMaxDay);
			} else {
				calendar.set(Calendar.DAY_OF_MONTH, curDay);
			}
		}
		reDate.setTime(calendar.getTimeInMillis());
		return reDate;
	}

	// add by fzz
	/**
	 * 检查指定时间是否在某个时间范围内(闭区间), 时间格式必须一致, 长度一致
	 *
	 * @param date
	 *            String 指定时间
	 * @param beginDate
	 *            String 范围开始时间
	 *
	 * @param endDate
	 *            String 范围结束时间
	 * @return boolean true-在范围内, false-不在范围内
	 *
	 * @throws BaseAppException
	 */
	public static boolean isInRange(String date, String beginDate, String endDate) throws BaseAppException {
		if (Util.isEmpty(date) || Util.isEmpty(beginDate) || Util.isEmpty(endDate)) {
			ExceptionHandler.publish("");
			/** @todo */
		}
		int dateLen = date.length();
		int beginDateLen = date.length();
		if (beginDateLen != dateLen) {
			ExceptionHandler.publish("");
			/** @todo */
		}
		boolean asc = isAsc(beginDate, endDate);
		if (asc) {
			if (date.compareTo(beginDate) >= 0 && date.compareTo(endDate) <= 0) {
				return true;
			}
		} else {
			if (date.compareTo(beginDate) >= 0 || date.compareTo(endDate) <= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 检查指定时间是否在某个时间范围内(闭区间)
	 *
	 * @param date
	 *            Date 指定时间
	 * @param beginDate
	 *            Date 范围开始时间
	 *
	 * @param endDate
	 *            Date 范围结束时间
	 * @return boolean true-在范围内, false-不在范围内
	 */
	public static boolean isInRange(Date date, Date beginDate, Date endDate) {
		// long time = date.getTime();
		// long beginTime = beginDate.getTime();
		// long endTime = endDate.getTime();
		// if (time >= beginTime && time <= endTime) {
		// return true;
		// } else {
		// return false;
		// }
		if (Util.isEmpty(date)) {
			logger.fatal("null date !");
			return false;
		}
		if (Util.isEmpty(beginDate)) {
			logger.fatal("null beginDate !");
			return false;
		}
		if (Util.isEmpty(endDate)) {
			logger.fatal("null endDate !");
			return false;
		}
		if (date.compareTo(beginDate) >= 0 && date.compareTo(endDate) <= 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * test is date1 later than date2 or not<br />
	 * date1 > date2 = true
	 *
	 * @param date1
	 * @param date2
	 * @return boolean
	 * */
	public static boolean isLaterThan(Date date1, Date date2) {
		if (date1 == null) {
			logger.fatal("date1 is null");
		}
		if (date2 == null) {
			logger.fatal("date2 is null");
		}
		return date1.getTime() > date2.getTime();
	}

	/**
	 * test date1 is early than date2 or not<br />
	 * date1 < date2 = true
	 *
	 * @param date1
	 * @param date2
	 * @return boolean
	 * */
	public static boolean isEarlyThan(Date date1, Date date2) {
		if (date1 == null) {
			logger.fatal("date1 is null");
		}
		if (date2 == null) {
			logger.fatal("date2 is null");
		}
		return date1.getTime() < date2.getTime();
	}

	public static boolean isEqual(Date date1, Date date2) {
		if (date1 == null) {
			logger.fatal("date1 is null");
		}
		if (date2 == null) {
			logger.fatal("date2 is null");
		}
		return date1.getTime() == date2.getTime();
	}

	/**
	 * test date1 not early than date2 or not<br />
	 * date1 >= date2 = true
	 *
	 * @param date1
	 * @param date2
	 * @return boolean
	 * */
	public static boolean notEarlyThan(Date date1, Date date2) {
		return !isEarlyThan(date1, date2);
	}

	/**
	 * test date1 not early than date2 or not<br />
	 * date1 <= date2 = true
	 *
	 * @param date1
	 * @param date2
	 * @return boolean
	 * */
	public static boolean notLaterThan(Date date1, Date date2) {
		return !isLaterThan(date1, date2);
	}

	/**
	 * 检查指定时间比较大小
	 *
	 * * @param beginDate Date 范围开始时间
	 *
	 * @param endDate
	 * @deprecated 使用 isEarlyThan , isLaterThan Date, notEarlyThan,notLateThan
	 *             范围结束时间（非空检测，返回false）<br />
	 *             请使用Date 对象的before,after方法（没有非空检测）
	 * @return boolean 0-小于, 1-等于，2-大于
	 */
	@Deprecated
	public static int isCompare(Date beginDate, Date endDate) {
		// int ret=1;
		// long beginTime = beginDate.getTime();
		// long endTime = endDate.getTime();
		//
		// if (beginTime >endTime) {
		// ret=2;
		// }
		// if(beginTime==endTime){
		// ret=1;
		// }
		// if(beginTime<endTime){
		// ret=0;
		// }
		int ret = 1;
		if (beginDate.after(endDate)) {
			ret = 2;
		}
		if (beginDate.equals(endDate)) {
			ret = 1;
		}
		if (beginDate.before(endDate)) {
			ret = 0;
		}
		return ret;
	}

	/**
	 * 判断字符串是否升序的, 第一个字符串小于第二个字符串
	 *
	 * @param firstStr
	 *            String
	 * @param secondStr
	 *            String
	 * @return boolean true-升序, false-降序
	 */
	private static boolean isAsc(String firstStr, String secondStr) {
		return (firstStr.compareTo(secondStr) < 0);
	}

	/** PostgreSQL 取数据库sql */
	public static String POSTGRESQL_GETTIME = " SELECT CURRENT_TIMESTAMP ";
	/** Oracle 取数据库sql */
	public static String ORACLE_GETTIME = " SELECT SYSDATE FROM DUAL ";

	/**
	 * 得到数据库当前时间
	 *
	 * @return Date
	 * @throws BaseAppException
	 */
	public static java.util.Date getDBCurrentTime() {
		// 取session 数据库链接
		// DBConnSession.getDBConnSession().regSvcFunc();
		// Connection oneConn =
		// DBConnSession.getDBConnSession().getMainConnection();
		// PreparedStatement ps = null;
		// ResultSet rs = null;
		// java.sql.Date date = null;
		// try {
		// switch (Constants.DB_DIALECT_INT) {
		// case Constants.ORACLE_DIALECT_INT:
		// ps = oneConn.prepareStatement(ORACLE_GETTIME);
		// case Constants.POSTGRESQL_DIALECT_INT:
		// ps = oneConn.prepareStatement(POSTGRESQL_GETTIME);
		// }
		// // ----------------
		// rs = ps.executeQuery();
		// Timestamp dateTs = null;
		// if (rs.next()) {
		// dateTs = rs.getTimestamp(1);
		// }
		// date = new java.sql.Date(dateTs.getTime());
		// } catch (SQLException ex) {
		// throw ExceptionHandler.publish(SysExceptionCode.DB_OPER_EXPCEPTION,
		// ex);
		// } finally {
		// Dustman.cleanUp(ps, rs);
		// DBConnSession.getDBConnSession().unRegSvcFunc();
		// }
		return getNowDate();// 同一台pc_server，就先取机器时间
	}

	/**
	 * 获得日期的一天的开始时间 00：00：00
	 *
	 * @param date
	 * @return
	 */
	public static Date getDayStartTime(Date start) {
		Calendar todayStart = Calendar.getInstance();
		todayStart.setTime(start);
		todayStart.set(Calendar.HOUR_OF_DAY, 0);
		todayStart.set(Calendar.MINUTE, 0);
		todayStart.set(Calendar.SECOND, 0);
		todayStart.set(Calendar.MILLISECOND, 0);
		return todayStart.getTime();
	}

	/**
	 * 获得日期的一天的结束时间 23：59：59
	 *
	 * @param date
	 * @return
	 */
	public static Date getDayEndTime(Date start) {
		Calendar todayEnd = Calendar.getInstance();
		todayEnd.setTime(start);
		todayEnd.set(Calendar.HOUR_OF_DAY, 23);
		todayEnd.set(Calendar.MINUTE, 59);
		todayEnd.set(Calendar.SECOND, 59);
		todayEnd.set(Calendar.MILLISECOND, 999);
		return todayEnd.getTime();
	}

	/**
	 * 不带毫秒
	 *
	 * @param start
	 * @return
	 */
	public static Date getDayEndTimeNotMillisecond(Date start) {
		Calendar todayEnd = Calendar.getInstance();
		todayEnd.setTime(start);
		todayEnd.set(Calendar.HOUR_OF_DAY, 23);
		todayEnd.set(Calendar.MINUTE, 59);
		todayEnd.set(Calendar.SECOND, 59);
		todayEnd.set(Calendar.MILLISECOND, 0);
		return todayEnd.getTime();
	}

	/**
	 * 根据开始时间、结束时间得到两个时间段内所有的日期 要求配置每天时段时，不能跨天，结束时间最大配置成23:59:59
	 *
	 * @param start
	 *            开始日期
	 * @param end
	 *            结束日期
	 * @param calendarType
	 *            类型
	 * @return 两个日期之间的日期,例如如果 开始时间是2012-12-30 20:59:17， 结束时间是2013-1-1 10:45:57
	 *         结果是如下数组信息 2012-12-30 20:59:17 2012-12-30 23:59:59 2012-12-31
	 *         0:00:00 2012-12-31 23:59:59 2013-1-1 10:45:57
	 */
	public static Date[] getDateArrays(Date start, Date end, int calendarType) {
		ArrayList<Date> ret = new ArrayList<Date>();
		Calendar calendar = Calendar.getInstance();
		Date tmpEndDate = getDayEndTime(start);
		calendar.setTime(tmpEndDate);
		// Date tmpDate = calendar.getTime();
		long endTime = end.getTime();
		// System.out.println("tmpEndDate: " + tmpEndDate.toLocaleString());
		ret.add(start);
		int i = 0;
		while (tmpEndDate.before(end) || tmpEndDate.getTime() == endTime) {
			if (i != 0) {
				ret.add(getDayStartTime(calendar.getTime()));
			}
			ret.add(calendar.getTime());
			calendar.add(calendarType, 1);
			tmpEndDate = calendar.getTime();
			// System.out.println("tmpEndDate: " + tmpEndDate.toLocaleString());
			i++;
		}
		ret.add(end);
		Date[] dates = new Date[ret.size()];
		return ret.toArray(dates);
	}

	/**
	 * 根据开始时间、结束时间得到两个时间段内所有的日期 要求配置每天时段时，不能跨天，结束时间最大配置成23:59:59
	 *
	 * @param start
	 *            开始日期
	 * @param end
	 *            结束日期
	 * @param calendarType
	 *            类型
	 * @return 两个日期之间的日期,例如如果 开始时间是2012-12-30 20:59:17， 结束时间是2013-1-1 10:45:57
	 *         结果是如下数组信息 2012-12-30 20:59:17 2012-12-31 0:00:00 2013-1-1 0:00:00
	 *         // 注意这个是循环后添加的 2013-1-1 10:45:57
	 */
	public static ArrayList<Date> getDateArraysWithoutEndTime(Date start, Date end, int calendarType) {
		ArrayList<Date> ret = new ArrayList<Date>();
		Calendar calendar = Calendar.getInstance();
		Date tmpEndDate = getDayEndTime(start);
		calendar.setTime(tmpEndDate);
		long endTime = end.getTime();
		// System.out.println("tmpEndDate: " + tmpEndDate.toLocaleString());
		ret.add(start);
		int i = 0;
		while (tmpEndDate.before(end) || tmpEndDate.getTime() == endTime) {
			if (i != 0) {
				ret.add(getDayStartTime(calendar.getTime()));
			}
			ret.add(calendar.getTime());
			calendar.add(calendarType, 1);
			tmpEndDate = calendar.getTime();
			// System.out.println("tmpEndDate: " + tmpEndDate.toLocaleString());
			i++;
		}
		if (getDayStartTime(tmpEndDate).before(end) && getDayStartTime(tmpEndDate).after(start)) {
			ret.add(getDayStartTime(end));
		}
		ret.add(end);
		// Date[] dates = new Date[ret.size()];
		return ret;
	}

	/**
	 * 拆分两周
	 *
	 * @param start
	 *            开始日期
	 * @param end
	 *            结束日期
	 * @return 两个日期之间的第一个时间周结束时间划分,例如如果 开始时间是2006-9-13 17:38:27， 结束时间是2013-1-14
	 *         13:11:47 2006-9-13 17:38:27 2006-9-17 23:59:59 2013-1-14 13:11:47
	 */
	public static ArrayList<Date> getDateArraysByWeek(Date start, Date end) {
		ArrayList<Date> ret = new ArrayList<Date>();
		Date tmpDate = start;
		ret.add(start);
		Calendar c = new GregorianCalendar();
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6);
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setTime(tmpDate);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6); // Sunday
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		tmpDate = c.getTime();
		// System.out.println("tmpDate" + tmpDate.toLocaleString());
		if (DateUtil.isCompare(tmpDate, end) == 0) {
			// 拆分
			ret.add(tmpDate);
		}
		ret.add(end);
		return ret;
	}

	// 获取和timeSpanDate相同周几，并且和date在同一周的时间
	// 比如timeSpanDate 2013-1-14 13:31:31 周一
	// date 2006-9-13 17:58:11 周三
	// 则返回2006-9-11 17:58:11 周一
	public static Date getWeekDate(Date date, Date timeSpanDate) {
		Calendar c = new GregorianCalendar();
		Date tmpDate = new Date();
		c.setTime(timeSpanDate);
		int week = c.get(Calendar.DAY_OF_WEEK);
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + (week - 1)); // Sunday
		tmpDate = c.getTime();
		return tmpDate;
	}

	// 获取和timeSpanDate相同周周几，
	// 比如timeSpanDate 2006-9-13 18:07:43 周三
	// week 2 ， 本周第几天，周日是 1 周一为二
	// 则返回2006-9-11 17:58:11 周一
	public static Date getWeekDate(Date timeSpanDate, int week) {
		Calendar c = new GregorianCalendar();
		Date tmpDate = new Date();
		c.setTime(timeSpanDate);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + (week - 1)); // Sunday
		tmpDate = c.getTime();
		return tmpDate;
	}

	// 获取start 和 timespanDate相同周几，时分秒 的日期
	public static Date setWeekDateHHMMSS(Date start, Date timeSpanDate) {
		Calendar c = new GregorianCalendar();
		Calendar c1 = new GregorianCalendar();
		Date tmpDate = new Date();
		c1.setTime(timeSpanDate);
		c.setTime(start);
		c.set(Calendar.DAY_OF_WEEK, c1.get(Calendar.DAY_OF_WEEK)); // Sunday
		c.set(Calendar.HOUR_OF_DAY, c1.get(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c1.get(Calendar.MINUTE));
		c.set(Calendar.SECOND, c1.get(Calendar.SECOND));
		tmpDate = c.getTime();
		return tmpDate;
	}

	/**
	 * 获取两个时间间隔的天
	 *
	 * @param lastExsitDate
	 * @param lastday
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Date> getIntervalDay(Date lastExsitDate, Date lastday) throws Exception {
		if (lastExsitDate == null || lastday == null) {// 没有时间，返回null
			return null;
		}
		int beginDay = DateUtil.getDateDay(lastExsitDate);
		int lastDay = DateUtil.getDateDay(lastday);
		String str = DateUtil.date2String(lastExsitDate, "yyyy-MM");
		ArrayList<Date> al = new ArrayList<Date>();
		for (int i = beginDay; i <= lastDay; i++) {
			al.add(DateUtil.string2Date(str + "-" + i, "yyyy-MM-dd"));
		}
		return al;
	}
	/**
	 * 获取两个时间的间隔
	 *
	 * @param lastExsitDate
	 * @param lastday
	 * @Unit 单位：年，月，日，时，分，秒
	 * @return 根据单位返回两个时间间隔
	 * 如果两个日期都为null，就返回0
	 * 如果起始时间为null，结束时间不为null，就查结束时间之前，负无穷
	 * 如果结束时间为null，起始时间不为null，就查起始时间之后，正无穷
	 * @throws Exception
	 */
	public static Long getIntervalDay(Date baseDate, Date lastday , int Unit) throws Exception {
		if (baseDate == null && lastday == null) {// 没有时间，返回null
			return 0L;
		}else if(baseDate == null){
			return -999999L;
		}else if(lastday ==null){
			return 999999L;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(baseDate);
		int basemonth =  cal.get(Calendar.MONTH)+1;
		int baseyear =  cal.get(Calendar.YEAR);
		cal.setTime(lastday);
		int lastmonth =  cal.get(Calendar.MONTH)+1;
		int lastyear =  cal.get(Calendar.YEAR);
		Long timespan=lastday.getTime()-baseDate.getTime();
		int time=1000;
		switch (Unit) {
		case TYPE_SECOND:
		    return timespan/time;
		case TYPE_MINITE:
			time=60*time;
			return timespan/time;
		case TYPE_HOUR:
			time=60*60*time;
			return timespan/time;
		case TYPE_DAY:
			time=24*60*60*time;
			return timespan/time;
		case TYPE_MONTH:
			if(baseyear==lastyear){
				return (long) (lastmonth-basemonth);
			}else{
				return (long) (12*(lastyear-baseyear)+lastmonth-basemonth);
			}
		case TYPE_YEAR:
			time=12*30*24*60*60*time;
			return timespan/time;
		}

		return 1l;
	}

	// 毫秒转换为日期
	public static Date long2Date(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		return c.getTime();
	}
	/**
	 * 在某个日期时间加个N个月后的日期时间
	 */
	public static String getAddMonthForTime(String formatStr, String dateTime, int month) {
		
		SimpleDateFormat sdf=new SimpleDateFormat(formatStr);
		Date date=null;
		try {
		date = sdf.parse(dateTime.replace("/", "-"));
		} catch (ParseException e) {
		// TODO 自动生成 catch 块
		e.printStackTrace();
		}
		Calendar ca=Calendar.getInstance();
		ca.setTime(date);
		ca.add(Calendar.MONTH, month);
		return sdf.format(ca.getTime());
	}
	
	/**
	 * 按指定的时间格式转换
	 */
	public static String getTimeFormat(String formactStr, String dateTime) {
		
		SimpleDateFormat sdf=new SimpleDateFormat(formactStr);
		Date date=null;
		try {
		date = sdf.parse(dateTime.replace("/", "-"));
		} catch (ParseException e) {
		// TODO 自动生成 catch 块
		e.printStackTrace();
		}
		Calendar ca=Calendar.getInstance();
		ca.setTime(date);
		return sdf.format(ca.getTime());
	}
	
	/**
	 * 产生随机的三位数
	 * @return
	 */
	public static String getThree(){
		Random rad=new Random();
		return rad.nextInt(1000)+"";
	}
	
	
	/** 
	 * 获取精确到秒的时间戳 
	 * @param date 
	 * @return 
	 */  
	public static int getSecondTimestamp(Date date){  
	    if (null == date) {  
	        return 0;  
	    }  
	    String timestamp = String.valueOf(date.getTime());  
	    return Integer.valueOf(timestamp);  
	} 
	
	
	/** 
	 * 获取精确到秒的时间戳 
	 * @return 
	 */  
	public static String getSecondTime3stamp(Date date){  
	    if (null == date) {  
	        return "";  
	    }  
	    String timestamp = String.valueOf(date.getTime());  
	    return timestamp;
	}
	
	
	/** 
     *  获取两个日期相差的月数 
     * @param d1    较大的日期 
     * @param d2    较小的日期 
     * @return  如果d1>d2返回 月数差 否则返回0 
     */  
    public static int getMonthDiff(Date maxDate, Date minDate) {  
        Calendar c1 = Calendar.getInstance();  
        Calendar c2 = Calendar.getInstance();  
        c1.setTime(maxDate);  
        c2.setTime(minDate);  
        if(c1.getTimeInMillis() < c2.getTimeInMillis()) return 0;  
        int year1 = c1.get(Calendar.YEAR);  
        int year2 = c2.get(Calendar.YEAR);  
        int month1 = c1.get(Calendar.MONTH);  
        int month2 = c2.get(Calendar.MONTH);  
        int day1 = c1.get(Calendar.DAY_OF_MONTH);  
        int day2 = c2.get(Calendar.DAY_OF_MONTH);  
        // 获取年的差值 假设 d1 = 2015-8-16  d2 = 2011-9-30  
        int yearInterval = year1 - year2;  
        // 如果 d1的 月-日 小于 d2的 月-日 那么 yearInterval-- 这样就得到了相差的年数  
        if(month1 < month2 || month1 == month2 && day1 < day2) yearInterval --;  
        // 获取月数差值  
        int monthInterval =  (month1 + 12) - month2  ;  
        if(day1 < day2) monthInterval --;  
        monthInterval %= 12;  
        return yearInterval * 12 + monthInterval;  
    }
    

    /**
     * 获取精确到秒的时间戳 10
     * @param date
     * @return
     */
    public static String getSecondTimestampTen(Date date){  
      if (null == date) {  
          return "0";  
      	}  
      return String.valueOf(date.getTime()/1000);   
    } 
    
    
    /** 
     * 获取精确到秒的时间戳 13
     * @return 
     */  
    public static String getSecondTimestampThir(Date date){  
        if (null == date) {  
            return "0";  
        }  
        String timestamp = String.valueOf(date.getTime());  
        int length = timestamp.length();  
        if (length > 3) {  
            return timestamp.substring(0,length-3);  
        } else {  
            return "0";  
        }  
    }
    
    /**
     * 获取2个日期相差天数(只在乎天数)
     * @param fDate
     * @param oDate
     * @return
     */
    public static int daysOfTwo(Date fDate, Date oDate) {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.setTime(fDate);
        int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);
        aCalendar.setTime(oDate);
        int day2 = aCalendar.get(Calendar.DAY_OF_YEAR);
        return day2 - day1;
     }
    
    
    /**
     *  获取2个日期相差天数(涉及到具体时间)
     * @param fDate
     * @param oDate
     * @return
     */
    public static int getIntervalDays(Date fDate, Date oDate) {
        if (null == fDate || null == oDate) {
            return -1;
        }
        long intervalMilli = oDate.getTime() - fDate.getTime();
        return (int) (intervalMilli / (24 * 60 * 60 * 1000));
     }
    
    
    

    /**
     * 计算time2减去time1的差值 差值只设置 几天 几个小时 或 几分钟,根据差值返回多长之间前或多长时间后,这里设置返回分钟
     * @param time1 
     * @param time2
     * @return
     */
    public static int getDistanceTime(long time1, long time2) {
        double min = 0;
        long diff;

        if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
        try {
            min = diff*1.0/ 60;
            // 不采取四舍五入返回分钟，不足一分钟算一分钟处理
            return (int)Math.ceil(min);
		} catch (Exception e) {
			return 0;
		}   
    }   
}












