package com.framework.core.cache.local;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.util.ReflectionUtils;

import com.framework.core.cache.BaseCacheProvider;
import com.framework.core.cache.ICacheItem;
import com.framework.core.utils.AppContextUtil;
import com.framework.core.utils.BeanUtil;
import com.framework.core.utils.Util;

/**
 * 抽象缓存数据集合类，本地缓存的实现模�?
 * 
 * @author 
 * 
 */
public class LocalCacheProvider<T extends ICacheItem> extends BaseCacheProvider<T> {
	private static final Logger logger = Logger.getLogger(LocalCacheProvider.class);
	
	/** 缓存数据map,这个是本地存取缓存用�? */
	private HashMap<String, T> cacheMap = new HashMap<String, T>();

	/** 全量数据列表 */
	private ArrayList<T> cacheAL = new ArrayList<T>();

	private String daoName;
	
	private Class<T> clazz;
	    
	/**
	 * 构�?�函�?
	 * 
	 * @param cacheName
	 * @param condition
	 * @param clazzCD
	 */
	public LocalCacheProvider(String cacheName, String daoName, Class<T> clazzCD) {
		this.clazz = clazzCD;
		super.setCacheName(cacheName);
		this.daoName = daoName;
		loadData();
	}
		
	@SuppressWarnings("unchecked")
	public ArrayList<T> getAll() {
    	ArrayList<T> res = new ArrayList<>();
    	for(T t:cacheAL) {
    		//防止用户取出后直接修�?
        	T newone = (T) BeanUtil.getPOJO(t, t.getClass());
    		res.add(newone);
    	}
		return res;
	}

	@SuppressWarnings("unchecked")
	public T getItem(String key) {
    	T t = this.cacheMap.get(key);
    	if(t== null) return null;
    	//防止用户取出后直接修�?
		T newone = (T) BeanUtil.getPOJO(t, t.getClass());
		return newone;
	}

	public <E extends T> void setItem(E iCacheItem) {
		this.nullDataObject(iCacheItem, iCacheItem.getClass());// 约定空数值处�?
		int index = 0;
		String key = null;
		synchronized (this.cacheMap) {
			if(this.cacheMap.get(iCacheItem.key())!=null){
				for(int i=0;i<this.cacheAL.size();i++){
					key = this.cacheAL.get(i).key();
					if (Util.isEmpty(key)) {
						continue;
					}
					if (key.equals(iCacheItem.key())) {
						this.cacheAL.remove(i);
						index = i;
						break;
					}
				}
			}
			this.cacheMap.put(iCacheItem.key(), iCacheItem);
			this.cacheAL.add(index, iCacheItem);
		}
	}

	@Override
	public void delItem(String key) {
		synchronized (this.cacheMap) {
			this.cacheMap.remove(key);
			if(key!=null){
				for(int i=0;i<this.cacheAL.size();i++){
					if(this.cacheAL.get(i).key().equals(key)){
						this.cacheAL.remove(this.cacheAL.get(i));
						break;
					}
				}
			}
		}
	}

	@Override
	public void cleanData() {
		cacheAL.clear();
		cacheMap.clear();
	}

	@Override
	public T getItem(Long key) {
		return getItem(key == null ? null : key+"");
	}

	@Override
	public void delItem(Long key) {
		delItem(key == null ? null : key+"");
	}

	@Override
	public void delItem(T item) {
		delItem(item == null ? null : item.key());
	}

	@Override
	public void reload() {
		cleanData();
		loadData();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void loadData() {
		try {
			
			//Dao为空表示不需要从数据库加载
			if(Util.isEmpty(this.daoName)){
				return;
			}
			Object obj = AppContextUtil.getBean(this.daoName);
			 Class<?> clazz = obj.getClass();
			 Method method = clazz.getMethod("getCacheList");
			
			 ArrayList<T> objAL = (ArrayList<T>) ReflectionUtils.invokeMethod(method, obj);
			 for (int i = 0; i < objAL.size(); i++) {
					T iCacheDataItem = this.clazz.newInstance();
					BeanUtil.copyProperties(objAL.get(i), iCacheDataItem);
					this.setItem(iCacheDataItem);
				}
		} catch (Exception e) {
			logger.error("load cache "+ this.getCacheName()+" failed.");
			logger.error(e.getMessage(), e);
		} 
	}
}
