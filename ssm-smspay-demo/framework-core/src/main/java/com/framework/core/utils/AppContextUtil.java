/**
 * 
 */
package com.framework.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author Administrator
 *
 */
public class AppContextUtil implements ApplicationContextAware {

	private static ApplicationContext appCtx; 
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		appCtx = arg0;
	}

	/**
	 *  
	 * @return
	 */
    public static ApplicationContext getApplicationContext(){  
        return appCtx;
    }  
      
    /**  
     * 这是一个便利的方法，帮助我们快速得到一个BEAN  
     * @param beanName bean的名字  
     * @return 返回一个bean对象  
     * @author xgf 
     */    
    public static Object getBean( String beanName ) {    
        return appCtx.getBean( beanName );    
    }    
    
    /**
     * 通过类型获得BEAN
     * @param cls
     * @return
     */
    public static <T> T getBean(Class<T> cls){
        return appCtx.getBean(cls); 
    }
}
