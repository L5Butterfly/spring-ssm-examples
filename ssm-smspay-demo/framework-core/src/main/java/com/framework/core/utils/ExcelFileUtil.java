package com.framework.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;
import com.framework.core.exception.SysErrCode;
import com.framework.web.base.vo.FileFormatVo;


/**
 * Excel操作助手
 * 
 * @author xgf
 * 
 */
public final class ExcelFileUtil {
	/** logger */
	public static final Logger logger = Logger.getLogger(ExcelFileUtil.class);
	public final static SimpleDateFormat WEB_DATA_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public final static SimpleDateFormat WEB_Simple_DATA_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private ExcelFileUtil() {
	}

	/**
	 * 从excel读取记录构造成指定的类
	 * 
	 * @param fileFormat
	 * @param fileName
	 * @param clazz
	 * @return
	 * @throws BaseAppException
	 */
	public static <T> ArrayList<T> readExcelFile(FileFormatVo fileFormatVo, String fileName, Class<T> clazz) throws BaseAppException {
		String[] fieldName = fileFormatVo.getFieldName().split(",");// 字段名称
		String[] fieldType = fileFormatVo.getFieldType().split(",");// 字段类型
		int cellNum = fieldName.length;
		ArrayList<T> alVo = new ArrayList<T>();
		try {
			Workbook wb = WorkbookFactory.create(new FileInputStream(fileName));
			// 获得第0个sheet
			Sheet sheet = wb.getSheetAt(0);
			// 获得行
			int rowNum = sheet.getPhysicalNumberOfRows();
			
			int firstRowNum = sheet.getFirstRowNum() + 1;// 第二行开始，有头
			for (int i = firstRowNum; i < rowNum; i++) {
				T objVo = clazz.newInstance();// new vo
				Row row = sheet.getRow(i);
				// 获得行中的列
				for (int j = row.getFirstCellNum(); j < cellNum; j++) {
					Cell cell = row.getCell(j);
					String value = "";
					if(cell!=null){
					if(fieldType[j].equalsIgnoreCase("date")){
						try{
						value = WEB_DATA_FORMAT.format(row.getCell(j).getDateCellValue());
						}catch(Exception ex){
							logger.debug(ex.getMessage());
							value=null;
							try{
								if(row.getCell(j).getStringCellValue()!=null && row.getCell(j).getStringCellValue().trim().length()>0){
									value= WEB_DATA_FORMAT.format(WEB_DATA_FORMAT.parse(row.getCell(j).getStringCellValue()));
								}
							}catch(Exception ex2){
								value=null;
								try{
									if(row.getCell(j).getStringCellValue()!=null && row.getCell(j).getStringCellValue().trim().length()>0){
										value= WEB_Simple_DATA_FORMAT.format(WEB_Simple_DATA_FORMAT.parse(row.getCell(j).getStringCellValue()))+" 00:00:00";
									}
								}catch(Exception ex3){
									value=null;
								}
							}
						}
						} else if (fieldType[j].equalsIgnoreCase("String")) {// 数字型特殊处理
							try {
								row.getCell(j).setCellType(Cell.CELL_TYPE_STRING);
								value = row.getCell(j).getStringCellValue().trim();
							} catch (Exception ex) {
								logger.debug(ex.getMessage());
								value = null;
							}
					}else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {// 数字型特殊处理
						try{
						double d = cell.getNumericCellValue();
						DecimalFormat df = new DecimalFormat("#");// 转换成整型
						value = df.format(d);
						}catch(Exception ex){
							value=null;
						}
					} else  {
						try{
						value = row.getCell(j).toString().trim();
						}catch(Exception ex){
							logger.debug(ex.getMessage());
							value=null;
						}
					}
					}else{
						value=null;
					}
					if(value!=null)
					setClassProperty(fieldName[j], fieldType[j], clazz, objVo, value);
				}
				if(objVo!=null)
				alVo.add(objVo);
			}
		} catch (Exception e) {
			throw ExceptionHandler.publish(SysErrCode.FILE_ACCESS_ERROR, e);
		}
		return alVo;
	}

	/**
	 * set属性
	 * 
	 * @param <T>
	 * 
	 * @param columnName
	 * @param clazz
	 * @param objVo
	 * @param objProp
	 */
	private static <T> void setClassProperty(String fieldName, String fileType, Class<T> clazz, Object objVo, String fieldValue) {
		if (Util.isEmpty(fieldValue)) {// 取值为空，直接返回
			return;
		}
		// 属性set
		try {
			String methodName = field2MethodName(fieldName);
			if (fileType.equals("String")) { // String
				Method method = clazz.getMethod(methodName, String.class);
				method.invoke(objVo, fieldValue);
			} else if (fileType.equals("Long")&& fieldValue!=null) {// Long
				Method method = clazz.getMethod(methodName, Long.class);
				method.invoke(objVo, Long.parseLong(fieldValue.split("\\.")[0]));// 防止数字当字符串处理
			} else if ((fileType.equals("Date")&& fieldValue!=null)||fileType.equals("date")&& fieldValue!=null) {// Date
				
				Method method = clazz.getMethod(methodName, Date.class);
				method.invoke(objVo, DateUtil.string2Date(fieldValue));
			} else {
				logger.error("setClassProperty Exception no this Property " + fieldName);
			}
		} catch (Exception e) {
			throw new RuntimeException("Excel setClassProperty Exception", e);
		}
	}

	/**
	 * 字段名 to 类方法名
	 * 
	 * @param columnName
	 * @return
	 */
	private static String field2MethodName(String fieldName) {
		StringBuilder methodName = new StringBuilder();
		methodName.append("set");
		methodName.append(fieldName.substring(0, 1).toUpperCase());
		methodName.append(fieldName.substring(1));
		return methodName.toString();
	}

	/**
	 * 获取模板文件
	 * 
	 * @param path
	 * @param fieldName
	 * @return
	 * @throws Exception
	 */
	public static String getExcelTempleFile(String path, String fieldName) throws Exception {
		File newRepositoryPath = new File(path);
		if (!newRepositoryPath.exists()) {
			newRepositoryPath.mkdir();
		}
		String[] field = fieldName.split(",");
		FileOutputStream fileOutputStream = null;
		String fileName = System.currentTimeMillis() + ".xlsx";
		try {
			Workbook wb = new XSSFWorkbook();
			CreationHelper createHelper = wb.getCreationHelper();
			Sheet sheet = wb.createSheet("Sheet1");
			// 创建行
			Row row = sheet.createRow(0);
			for (int i = 0; i < field.length; i++) {
				row.createCell(i).setCellType(HSSFCell.CELL_TYPE_STRING);
				row.createCell(i).setCellValue(createHelper.createRichTextString(field[i]));
			}
			// 写入文件
			fileOutputStream = new FileOutputStream(path + fileName);
			wb.write(fileOutputStream);
		} catch (Exception e) {
			throw ExceptionHandler.publish(SysErrCode.FILE_ACCESS_ERROR, e);
		} finally {
			fileOutputStream.close();
		}
		return fileName;
	}

}
