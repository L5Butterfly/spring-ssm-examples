package com.framework.core.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *  Map操作工具类
 * @author Administrator
 *
 */
public class MapUtil {
	
	
    /**
     * Map 按 key 进行排序
     * 
     * @param map
     * @param isAsc
     * @return
     */
    public static Map<String,String> sortMapByKey(Map<String, String> map,Boolean isAsc) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        Map<String,String> sortMap = new TreeMap<String,String>(new MapKeyComparator(isAsc));
        sortMap.putAll(map);
        return sortMap;
    }

   /**
    * Map 按 value 进行排序
    * 
    * @param oriMap
    * @param isAsc
    * @return
    */
    public static Map<String,String> sortMapByValue(Map<String, String> oriMap,Boolean isAsc) {
        if (oriMap == null || oriMap.isEmpty()) {
            return null;
        }
        Map<String,String> sortedMap = new LinkedHashMap<String,String>();
        List<Map.Entry<String,String>> entryList = new ArrayList<Map.Entry<String,String>>(oriMap.entrySet());
        Collections.sort(entryList, new MapValueComparator(isAsc));

        Iterator<Map.Entry<String,String>> iter = entryList.iterator();
        Map.Entry<String,String> tmpEntry = null;
        while (iter.hasNext()) {
            tmpEntry = iter.next();
            sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());
        }
        return sortedMap;
    }

   /**
    * 将 UrlParams String 转为 Map
    * 
    * @param param
    * @return
    */
    public static Map<String,Object> urlParams2Map(String param) {
    	Map<String,Object> map = new HashMap<String,Object>();
        if ("".equals(param) || null == param) {
            return map;
        }
        String[] params = param.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] p = params[i].split("=");
            if (p.length == 2) {
                map.put(p[0], p[1]);
            }
        }
        return map;
    }

    /**
     * 将 map 转为 UrlParams String
     * 
     * @param map
     * @return
     */
    public static String map2UrlParams(Map<String, String> map, boolean isSort) {
        if (map == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        List<String> keys = new ArrayList<String>(map.keySet());
        if (isSort) {
            Collections.sort(keys);
        }
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = map.get(key).toString();
            sb.append(key + "=" + value);
            sb.append("&");
        }
        String s = sb.toString();
        if (s.endsWith("&")) {
            s = s.substring(0, s.lastIndexOf("&"));
        }
        return s;
    }
}


class MapKeyComparator implements Comparator<String> {
	
	Boolean flag=true;
	
	public MapKeyComparator(Boolean isAsc){
		this.flag=isAsc;
	}
	
    @Override
    public int compare(String str1, String str2) {
    	if(flag){
    		 return str1.compareTo(str2);
    	}else{
    		 return str2.compareTo(str1);
    	}
    }
}

class MapValueComparator implements Comparator<Map.Entry<String, String>> {
	
	Boolean flag=true;
	
	public MapValueComparator(Boolean isAsc){
		this.flag=isAsc;
	}
	
    @Override
    public int compare(Entry<String, String> me1, Entry<String, String> me2) {
    	if(flag){
    		return me1.getValue().compareTo(me2.getValue());
    	}else{
    		return me2.getValue().compareTo(me1.getValue());
    	}
    }
}