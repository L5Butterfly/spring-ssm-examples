package com.framework.core.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import com.framework.core.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class GsonFactory {
    private static final GsonFactory gf = new GsonFactory();
    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().serializeNulls().registerTypeAdapter(int.class, gf.new IntegerTypeAdapter())
            .registerTypeAdapter(long.class, gf.new LongTypeAdapter()).registerTypeAdapter(double.class, gf.new DoubleTypeAdapter())
            .registerTypeAdapter(Integer.class, gf.new IntegerTypeAdapter()).registerTypeAdapter(Long.class, gf.new LongTypeAdapter()).registerTypeAdapter(Date.class, gf.new DateTypeAdapter())
            .registerTypeAdapter(Double.class, gf.new DoubleTypeAdapter()).registerTypeAdapter(String.class, gf.new StringTypeApAdapter()).create();
    private static final Gson gsonNull = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().serializeNulls().registerTypeAdapter(int.class, gf.new IntegerNullTypeAdapter())
            .registerTypeAdapter(long.class, gf.new LongNullTypeAdapter()).registerTypeAdapter(double.class, gf.new DoubleNullTypeAdapter())
            .registerTypeAdapter(Integer.class, gf.new IntegerNullTypeAdapter()).registerTypeAdapter(Long.class, gf.new LongNullTypeAdapter())
            .registerTypeAdapter(Double.class, gf.new DoubleNullTypeAdapter()).registerTypeAdapter(String.class, gf.new StringNullTypeAdapter()).registerTypeAdapter(Date.class, gf.new DateTypeAdapter()).create();


    /**
     * @param isParseNull true的话，会将null,"",undefined 等转为Constants.NULL_LONG 中的对应空类型�??
     * */
    public static Gson getGson(boolean isParseNull) {
        return isParseNull ? gsonNull : gson;
    }
    private static HashSet<String> NULL_VALUES = new HashSet<String>() {
		private static final long serialVersionUID = 1L;
		{
    		add("");
    		add("null");
    		add("undefined");
    	}
    };
    
    public static Gson getGson() {
        return gson;
    }

    private GsonFactory() {}

    class StringTypeApAdapter extends TypeAdapter<String>{

		@Override
		public String read(JsonReader arg0) throws IOException {
			   if (arg0.peek() == JsonToken.NULL) {
	                arg0.nextNull();
	                return null;
	            }
			   return arg0.nextString();
		}

		@Override
		public void write(JsonWriter arg0, String arg1) throws IOException {
			if(arg1 != null) {
				arg0.value(arg1.toString());
			}else {
				arg0.value(arg1);
			}
		}
    	
    }
    class DateTypeAdapter extends TypeAdapter<Date>{

        @Override
        public Date read(JsonReader arg0) throws IOException {
             if (arg0.peek() == JsonToken.NULL) {
                    arg0.nextNull();
                    return null;
                }
                String result = arg0.nextString().trim();
                if (NULL_VALUES.contains(result)) {
                    return null;
                }
                String[] format = {"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd'T'HH:mm:ss.SSS", "yyyy-MM-dd HH:mm" , "yyyy-MM-dd HH","yyyy-MM-dd","yyyy-MM","yyyy"};
                for(String f:format) {
                    try {
                        return new java.util.Date((new SimpleDateFormat(f)).parse(result).getTime());
                    } catch (ParseException e) { }
                }
                throw new JsonSyntaxException(result);
        }

        @Override
        public void write(JsonWriter arg0, Date arg1) throws IOException {
             String string = null;
             if(arg1 != null) {
                 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                 string = sdf.format(arg1);
             }
            arg0.value(string);
        }
    }
    
    class DoubleTypeAdapter extends TypeAdapter<Number> {
        @Override
        public Number read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return null;
            }
            try {
                String result = getNumberFromString(arg0.nextString());
                return NULL_VALUES.contains(result) ? null : Double.parseDouble(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        @Override
        public void write(JsonWriter arg0, Number arg1) throws IOException {
            arg0.value(arg1);
        }
    }

    class LongTypeAdapter extends TypeAdapter<Number> {
        @Override
        public Number read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return null;
            }
            try {
                String result = getNumberFromString(arg0.nextString());
                return NULL_VALUES.contains(result) ? null : Long.parseLong(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        @Override
        public void write(JsonWriter arg0, Number arg1) throws IOException {
            arg0.value(arg1);
        }
    }

    class IntegerNullTypeAdapter extends TypeAdapter<Number> {
        @Override
        public Number read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return Constants.NULL_INT;
            }
            try {
                String result = getNumberFromString(arg0.nextString());
                return NULL_VALUES.contains(result) ? Constants.NULL_INT : Integer.parseInt(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        @Override
        public void write(JsonWriter arg0, Number arg1) throws IOException {
            arg0.value(arg1);
        }
    }

    class StringNullTypeAdapter extends TypeAdapter<String> {
        @Override
        public String read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return Constants.NULL_STRING;
            }
            String result = arg0.nextString().trim();
            if ("".equals(result) || "undefined".equals(result) || "null".equals(result)) {
                return Constants.NULL_STRING;
            }
            return result;
        }

        @Override
        public void write(JsonWriter arg0, String arg1) throws IOException {
			String res = arg1 == null ? "" : arg1.toString();
			res = res.replace("\"", "\\\"");
	        arg0.value(res);
        }
    }

    class LongNullTypeAdapter extends TypeAdapter<Number> {
        @Override
        public Number read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return Constants.NULL_LONG;
            }
            try {
                String result = getNumberFromString(arg0.nextString());
                return NULL_VALUES.contains(result) ? Constants.NULL_LONG : Long.parseLong(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        @Override
        public void write(JsonWriter arg0, Number arg1) throws IOException {
            arg0.value(arg1);
        }
    }

    class IntegerTypeAdapter extends TypeAdapter<Number> {
        @Override
        public Number read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return null;
            }
            try {
                String result = getNumberFromString(arg0.nextString());
                return NULL_VALUES.contains(result) ? null : Integer.parseInt(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        @Override
        public void write(JsonWriter arg0, Number arg1) throws IOException {
            arg0.value(arg1);
        }
    }

    class DoubleNullTypeAdapter extends TypeAdapter<Number> {
        @Override
        public Number read(JsonReader arg0) throws IOException {
            if (arg0.peek() == JsonToken.NULL) {
                arg0.nextNull();
                return Constants.NULL_DOUBLE;
            }
            try {
                String result = getNumberFromString(arg0.nextString());
                return NULL_VALUES.contains(result) ? Constants.NULL_DOUBLE : Double.parseDouble(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        @Override
        public void write(JsonWriter arg0, Number arg1) throws IOException {
            arg0.value(arg1);
        }
    }

    private static String getNumberFromString(String s) {
    	if(s == null)return null;
    	String res = "";
    	char begin = '0';
    	char end = '9';
        char minus = '-';
    	for(int i = 0;i<s.length();i++) {
    		char c = s.charAt(i);
    		if((c>= begin && c<= end) || c == minus) {
    			res+=c;
    		}
    	}
    	return res;
    }
}
