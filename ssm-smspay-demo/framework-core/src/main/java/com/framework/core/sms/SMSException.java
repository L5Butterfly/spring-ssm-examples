package com.framework.core.sms;


public class SMSException extends Exception {

	private static final long serialVersionUID = -6283225512607218674L;

	public final static int UNKONW_EXCEPTION = 9999;

	private String msg;
	private String phoneNum;
	private int code;

	public SMSException(int code, String msg, String phoneNum) {
		super(msg);
		this.code = code;
		this.phoneNum = phoneNum;
		this.msg = msg;
	}

	public SMSException(String msg, String phoneNum) {
		this(-1, msg, phoneNum);
	}

	public SMSException(String phoneNum, Throwable e) {
		this(UNKONW_EXCEPTION, e.getMessage(), phoneNum);
	}

	public String getMsg() {
		return msg;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public int getCode() {
		return code;
	}
}
