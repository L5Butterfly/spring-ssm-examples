/**
 * 
 */
package com.framework.core.utils;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


/**
 * @author Administrator
 *
 */
public final class JobUtil {

	/**
	 * 系统内部处理任务时使用此线程
	 */
	private static ThreadPoolTaskExecutor systemPool;
	
	/**
	 * 业务处理任务时使用此线程
	 */
	private static ThreadPoolTaskExecutor busiPool;
	
	private JobUtil(){}
	
	/**
	 * 添加到任务队列:系统内部任务
	 * 
	 * @param job
	 */
	public static void addSystemWorkJob(Runnable job) {
		systemPool.execute(job);
	}

	/**
	 * 添加到任务队列:业务类任务
	 * 
	 * @param job
	 */
	public static void addBusiWorkJob(Runnable job) {
		busiPool.execute(job);
	}
	
	public void setSystemPool(ThreadPoolTaskExecutor systemPool) {
		JobUtil.systemPool = systemPool;
	}
	public void setBusiPool(ThreadPoolTaskExecutor busiPool) {
		JobUtil.busiPool = busiPool;
	}
	
}
