package com.framework.core.cache;

import java.util.ArrayList;

public interface IBaseCache <T extends ICacheItem>{

	public T getItem(String key);

	public T getItem(Long key);

	public void delItem(String key);

	public void delItem(Long key);
	
	public void delItem(T item);

	public <E extends T> void setItem(E iCacheDateItem);

	public ArrayList<T> getAll();

	public void reload();
	
	public void cleanData();
}
