package com.framework.core.cache;

import java.util.Set;
import java.util.TreeMap;

import com.framework.core.Constants;
import com.framework.core.cache.local.LocalCacheProvider;
import com.framework.core.cache.redis.GsonRedisCacheProvider;
import com.framework.core.utils.AppContextUtil;

/**
 * 缓存工厂，此工厂提供不同来源或�?�获取方式的缓存提供�?
 * 
 * @author ying.rui
 * 
 */
public class CacheFactory {

	private static final TreeMap<String, ICacheProvider<?>> cacheMap = new TreeMap<>();

	public static Set<String> getCacheNames() {
		return cacheMap.keySet();
	}

	public static ICacheProvider<?> getCache(String name) {
		return cacheMap.get(name);
	}

	/**
	 * 根据缓存和过来条件获取缓存数据集�?
	 * 
	 * @param <E>
	 * 
	 * @param cacheName
	 * @param condition
	 * @return
	 */
	public static <T extends ICacheItem, E> ICacheProvider<T> getCacheProvider(String cacheName, String daoName,Class<T> clazzCD, Class<E> cacheClass) {
		ICacheProvider<T> cacheProvider = null;
		 CacheBean cacheBean =  (CacheBean) AppContextUtil.getBean("cacheBean");
		switch (cacheBean.getCacheModel()) {
		// 全局配了redis缓存
		case Constants.CACHE_MODE_REDIS:
			cacheProvider = new GsonRedisCacheProvider<T>(cacheName, daoName, clazzCD, cacheBean.getJedisUtil());
			break;
		// 全局配了local缓存
		case Constants.CACHE_MODE_LOCAL:
			cacheProvider = new LocalCacheProvider<T>(cacheName, daoName, clazzCD);
			break;
		default:
			break;
		}
		cacheMap.put(cacheName + "_" + clazzCD.getSimpleName(), cacheProvider);
		return cacheProvider;
	}

}
