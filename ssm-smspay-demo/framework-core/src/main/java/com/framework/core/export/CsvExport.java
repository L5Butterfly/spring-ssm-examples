/**
 * 
 */
package com.framework.core.export;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.framework.core.page.Page;
import com.framework.core.utils.DateUtil;

/**
 * @author Administrator
 *
 */
public class CsvExport {

	 private static final Logger logger = Logger.getLogger(CsvExport.class);
	 
	  public void export4Jq(HttpServletRequest request, HttpServletResponse response, Page page)  {
	        String data = null;
			try {
				data = URLDecoder.decode(request.getParameter("colnames"), "UTF-8") + "\r\n";
			} catch (UnsupportedEncodingException e1) {
				logger.error("create csv file faild:", e1);
			}
	        String[] fieldNames = request.getParameter("colnamesen").split(",");
	        Object[] nullArgs = new Object[0];
	        List<?> list = page.getRes();
	        for (Object object : list) {
	            Class<?> clazz = object.getClass();
	            for (String fieldName : fieldNames) {
	                Object fieldValue = null;
	                Method method;
	                try {
	                    method = clazz.getMethod("get_" + fieldName);
	                    fieldValue = method.invoke(object, nullArgs);
	                } catch (Exception e) {
	                    try {
	                        method = clazz.getMethod("get" + String.valueOf(fieldName.charAt(0)).toUpperCase() + fieldName.substring(1));
	                        fieldValue = method.invoke(object, nullArgs);
	                    } catch (Exception e2) {
	                        // do nothing;
	                    }
	                }
	                if (fieldValue == null) { 
	                    // do nothing;
	                } else if (fieldValue instanceof Date) {
	                	data += DateUtil.date2String((Date) fieldValue);
	                } else {
	                	data += fieldValue.toString();
	                }
	                data += ",";
	            }
	            data += "\r\n";
	        }
	        response.setContentType("text/csv");
	        response.setHeader("Content-disposition", "attachment;filename=" + DateUtil.getNowDate().getTime() + ".csv");
	        byte[] bom = { (byte) 239, (byte) 187, (byte) 191 };
	        OutputStream os = null;  
	        try {
	        	os = response.getOutputStream();
				os.write(bom);
				os.write(data.getBytes("UTF-8"));
				//此处关闭，否则会导致将流中的返回数据也写到文件中
				os.close();
			} catch (Exception e) {
				page= null;
				 logger.error("create csv file faild:", e);
			}finally{
	        	IOUtils.closeQuietly(os);
	        }
	    }
	  
}
