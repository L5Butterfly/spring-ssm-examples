package com.framework.core.utils;

import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.framework.core.Constants;
import com.sun.mail.util.MailSSLSocketFactory;


/**
 * 
 * @author Administrator
 *
 */
public final class EmailUtil {

	private static final Logger logger = Logger.getLogger(EmailUtil.class);

	private EmailUtil() {
	}

	// 邮件服务器地址
	private static final String HOST_ADDR = ParamUtil.getParamString("POR_MAL_0001", "smtp.jiebasdk.com");//"smtp.jiebasdk.com";;

	// 邮件服务器登录名
	private static final String USER_ANME = ParamUtil.getParamString("POR_MAL_0003", "services@jiebasdk.com");//"services@jiebasdk.com";//
	
	// 邮件服务器授权码
	private static final String AUTH_CODE = ParamUtil.getParamString("POR_MAL_0004", "Doinfo2016");//"Doinfo2016";//
	
	// 发送人邮箱地址
	private static final String FROM_ADDRESS = ParamUtil.getParamString("POR_MAL_0002", "services@jiebasdk.com");//"services@jiebasdk.com";//


	// 发送人名称
	private static final String FROM_USERNAME = "接吧平台";
		
	/**
	 * 
	 * @param tos
	 * @param subject
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public static boolean sendMail(String tos, String subject, String content) throws Exception {
		return sendMail(tos, subject, content, null);
	}

	/**
	 * 
	 * @param tos
	 * @param subject
	 * @param content
	 * @param fileList
	 * @return
	 * @throws Exception
	 */
	public static boolean sendMail(String tos, String subject, String content, ArrayList<String> fileList) throws Exception {
		try {
			
			logger.debug("发送邮件至:"+tos);
			Properties props = System.getProperties();
			props.put("mail.smtp.host", HOST_ADDR); // 设置SMTP的主机
			props.put("mail.smtp.auth", "true"); // 需要经过验证
			 //端口
	        props.setProperty("mail.smtp.port", "465");
	        //使用smtp身份验证
	        props.setProperty("mail.smtp.auth", "true");
			MailSSLSocketFactory sf = new MailSSLSocketFactory();
			sf.setTrustAllHosts(true);
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.ssl.socketFactory", sf);
			Session session = Session.getInstance(props, new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(USER_ANME, AUTH_CODE);
				}
			});

			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(MimeUtility.encodeText(FROM_USERNAME)+"<"+FROM_ADDRESS+">"));
			
			//处理接收人列表，按照,分割
			String[] toArr = tos.split(Constants.SPLIT_COMMON);
			InternetAddress[] address = new InternetAddress[toArr.length];

			for (int i = 0; i < toArr.length; i++) {
				address[i] = new InternetAddress(toArr[i]);
			}

			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(subject);

			Multipart mp = new MimeMultipart();
			MimeBodyPart mbpContent = new MimeBodyPart();
			mbpContent.setText(content);
			mp.addBodyPart(mbpContent);

			/* 往邮件中添加附件 */
			if (Util.notEmpty(fileList)) {
				for (String fileName : fileList) {
					MimeBodyPart mbpFile = new MimeBodyPart();
					FileDataSource fds = new FileDataSource(fileName);
					mbpFile.setDataHandler(new DataHandler(fds));
					mbpFile.setFileName(MimeUtility.encodeText(fds.getName()));
					mp.addBodyPart(mbpFile);
				}
			}

			msg.setContent(mp);
			msg.setSentDate(DateUtil.getNowDate());
			Transport.send(msg);
		} catch (Exception e) {
			logger.error("send email failed.", e);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param tos
	 * @param subject
	 * @param content
	 * @param fileList
	 * @return
	 * @throws Exception
	 */
	public static boolean sendHtmlMail(String tos,String ccs, String subject, String content, ArrayList<String> fileList) throws Exception {
		try {
			logger.debug("发送邮件至:"+tos);
			// spring提供的邮件实现类  
	        JavaMailSenderImpl send = new JavaMailSenderImpl();  
	        Properties prop = new Properties();  
	        prop.setProperty("mail.transport.protocol", "smtp"); // 设置邮件发送协议  
	        prop.setProperty("mail.host", HOST_ADDR); // 邮件服务器地址  
	        prop.setProperty("mail.smtps.ssl.enable", "true"); // 邮件ssl验证  
	        prop.setProperty("mail.smtp.auth", "true"); // 邮件服务身份验证
	        prop.setProperty("mail.smtp.port", "465");
	        prop.setProperty("mail.smtp.socketFactory.port", "465");//设置ssl端口
	        prop.setProperty("mail.smtp.socketFactory.fallback", "false");
	        prop.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        
	        send.setUsername(USER_ANME); // 设置用户名  
	        send.setPassword(AUTH_CODE); // 设置密码  
	        send.setJavaMailProperties(prop);  
	  
	        MimeMessage msg = send.createMimeMessage();  
	        // 指定HTML编码，参数true表示为multipart  
	        MimeMessageHelper helper = new MimeMessageHelper(msg, true, Constants.UTF_8_ENCODING);  
	        helper.setFrom(new InternetAddress(MimeUtility.encodeText(FROM_USERNAME)+"<"+FROM_ADDRESS+">")); // 发送者邮箱 
	        
	        //处理接收人列表，按照,分割
			String[] toArr = tos.split(Constants.SPLIT_COMMON);
			
			InternetAddress[] address = new InternetAddress[toArr.length];

			for (int i = 0; i < toArr.length; i++) {
				address[i] = new InternetAddress(toArr[i]);
			}
			
			
			//处理抄送收人列表，按照,分割
			String[] ccArr = ccs.split(Constants.SPLIT_COMMON);
			InternetAddress[] ar = new InternetAddress[ccArr.length];
			
			for (int i = 0; i < ar.length; i++) {
				ar[i] = new InternetAddress(ccArr[i]);
			}
			
			
			
	        helper.setTo(address); // 接收者邮箱  
	        helper.setCc(ar);//抄送邮箱
	        helper.setReplyTo(FROM_ADDRESS); // 回复邮箱  
	        helper.setSentDate(DateUtil.getNowDate()); // 发送日期  
	        helper.setSubject(subject);  
	        helper.setText(content, true); // 邮件内容，参数true表示是html代码 
	        
	        /* 往邮件中添加附件 */
			if (Util.notEmpty(fileList)) {
				for (String fileName : fileList) {
					FileDataSource fds = new FileDataSource(fileName);
					helper.addAttachment(MimeUtility.encodeWord(fds.getName()), fds); // 如果文件是中文名，需要转码。  
				}
			}
	       
	        send.send(msg); // 发送邮件  
	        logger.debug("Successfully send mail to the user");  
		} catch (Exception e) {
			logger.error("send html email failed.", e);
			return false;
		}
		return true;
	}
	
//	public static void main(String[] args) throws Exception {
//		ArrayList<String> fileList = new ArrayList<String>();
//		EmailUtil.sendHtmlMail("2104902498@qq.com,2661621376@qq.com", "报表2017-11-11至21", "<font size='5' color='red'>附件测试成功！</font>", fileList);
//	}
}
