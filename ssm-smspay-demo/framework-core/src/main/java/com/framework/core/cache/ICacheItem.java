package com.framework.core.cache;

public interface ICacheItem {

	/**
	 * 唯一key
	 * 
	 * @return
	 */
	public String key();
	
}
