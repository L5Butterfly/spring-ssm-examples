/**
 * 
 */
package com.framework.core.utils;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.MobileInfoVo;


/**
 * @author DS
 *
 */
public final class MobileUtil {
	
	private static Logger logger = Logger.getLogger(HttpUtil.class);
	
	private static final String TAOBAO_API_URL = "https://tcc.taobao.com/cc/json/mobile_tel_segment.htm";
	
	private  MobileUtil(){
		
	}
	
    /**
     * 
     * @param mobile
     * @return
     * @throws UnsupportedEncodingException 
     * @throws BaseAppException 
     */
	public static MobileInfoVo getMobileInfo(String mobile) {
		MobileInfoVo info = new MobileInfoVo();
		info.setMobile(mobile);
		String res = null;
		try {
			res = HttpUtil.httpGet(TAOBAO_API_URL+"?tel="+mobile, 2000, "GB2312");
			res = res.substring(res.indexOf("{"));
			
			JSONObject resJson = JSONObject.parseObject(res);
			info.setOperatorName(resJson.getString("catName"));
			info.setProvinceName(resJson.getString("province"));
			
		} catch (BaseAppException e) {
			logger.error(e.getMessage(), e);
		}
		return info;
	}
	
	public static void main(String[] args) {
		MobileInfoVo info = getMobileInfo("18705169542");
		
		System.out.println(info.getOperatorName());
	}
}
