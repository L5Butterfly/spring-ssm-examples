package com.framework.core.utils;

import org.apache.log4j.PropertyConfigurator;

import com.framework.core.Constants;

/**
 * ConfigHelper
 * 
 * @author xgf
 * 
 */
public class Log4jUtil {
    /**
     * 初始化日志配置
     */
    public static void initLogConfig() {
    	
    	initLogConfig(Constants.isProduction ? Constants.PROCESS_NAME+"-"+Constants.LISTENNING_PORT+(Constants.IS_CONSOLE_PROCESS?"-Main":"") :"weblog");
    }

    /**
     * 初始化日志配置
     */
    public static void initLogConfig(String local) {
    	
        // log4j设置
        System.setProperty("WORK_DIR", Constants.APP_HOME);// 供log4j使用
        System.setProperty("JETTY", Constants.PROCESS_NAME+"-"+Constants.LISTENNING_PORT);// 供log4j使用
        if (!Util.isEmpty(local)) {
            System.setProperty("LOCAL_DIR", local);// 供自定义使用
        } else {
            System.setProperty("LOCAL_DIR", ".");// 供自定义使用
        }
        PropertyConfigurator.configure(Constants.CONFIG_DIR + "log4j.properties");
    }
}
