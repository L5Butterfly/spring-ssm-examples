/**
 * 
 */
package com.framework.core.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.framework.core.Constants;
import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;
import com.framework.core.exception.SysErrCode;

/**
 * @author Administrator
 *
 */
public final class HttpUtil {
	private static Logger logger = Logger.getLogger(HttpUtil.class);
	
	private HttpUtil(){}
	
	/**
     * @param url, get 请求的url, 默认超时3秒返回空.不抛任何异常.
	 * @throws BaseAppException 
     * */
    public static String httpGet(String url) throws BaseAppException {
        return httpGet(url, 60000, "UTF-8");
    }

    
    /**
     * @param url get 请求的url
     * @param timeout 超时时间 单位毫秒  超时返回空.不抛任何异常.
     * @throws BaseAppException 
     * */
    public static String httpGet(String url, int timeout, String charset) throws BaseAppException {
        String result = "";
        BufferedReader in = null;
        try {
            URLConnection connection = new URL(url).openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
            connection.setRequestProperty("user-agent",  "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            connection.connect();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            logger.error("http get error.url:"+url, e);
            throw new BaseAppException(SysErrCode.UNKNOW_EXPCEPTION, e.getMessage(), e);
        } finally {
        	IOUtils.closeQuietly(in);
        }
        return result;
    }
    
    /**
     * 
     * @param url
     * @param data
     * @return
     * @throws BaseAppException
     */
    public static String httpPost4Json(String url, String data) throws BaseAppException {
    	return httpPost(url, data, 20000, "application/json");
    }
    
    /**
     * 
     * @param surl
     * @param data
     * @param timeout
     * @param contentType
     * @return
     * @throws BaseAppException
     */
	public static String httpPost(String surl, String data, int timeout, String contentType) throws BaseAppException {
		HttpURLConnection httpconn = null;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		OutputStream out = null;
		InputStreamReader isr = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL url = new URL(surl);
			httpconn = (HttpURLConnection) url.openConnection();
			bout.write(data.getBytes(Constants.UTF_8_ENCODING));
			byte[] b = bout.toByteArray();
			httpconn.setRequestProperty("Content-Type", contentType);//(application/x-www-form-urlencoded   application/json)
			httpconn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			httpconn.setRequestProperty("Cache-Control", "no-cache");
			httpconn.setRequestProperty("Pragma", "no-cache");
			httpconn.setRequestProperty("Content-Length", String.valueOf(b.length));
			httpconn.setRequestMethod("POST");
			httpconn.setConnectTimeout(timeout);
			httpconn.setDoInput(true);
			httpconn.setDoOutput(true);
			out = httpconn.getOutputStream();
			out.write(b);
			isr = new InputStreamReader(httpconn.getInputStream());
			in = new BufferedReader(isr);
			String inputLine;
			while (null != (inputLine = in.readLine())) {
				result += inputLine;
			}
			result = new String(result.getBytes(), Constants.UTF_8_ENCODING);
		} catch (Exception e) {
			throw new BaseAppException(SysErrCode.UNKNOW_EXPCEPTION, e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(isr);
			IOUtils.closeQuietly(bout);
		}
		return result;
	}
	
	public static String httpPost(String url, String data, Map<String, String> headers) throws BaseAppException {
		HttpURLConnection httpconn = null;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		OutputStream out = null;
		InputStreamReader isr = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL url1 = new URL(url);
			httpconn = (HttpURLConnection) url1.openConnection();
			bout.write(data.getBytes(Constants.UTF_8_ENCODING));
			byte[] b = bout.toByteArray();
			httpconn.setRequestProperty("Content-Type", "application/json");
			httpconn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			httpconn.setRequestProperty("Cache-Control", "no-cache");
			httpconn.setRequestProperty("Pragma", "no-cache");
			httpconn.setRequestProperty("Content-Length", String.valueOf(b.length));
			
			if(Util.notEmpty(headers)){
				for(String key : headers.keySet()){
					httpconn.setRequestProperty(key, headers.get(key));
				}
			}
			
			httpconn.setRequestMethod("POST");
			httpconn.setConnectTimeout(20000);
			httpconn.setDoInput(true);
			httpconn.setDoOutput(true);
			out = httpconn.getOutputStream();
			out.write(b);
			isr = new InputStreamReader(httpconn.getInputStream());
			in = new BufferedReader(isr);
			String inputLine;
			while (null != (inputLine = in.readLine())) {
				result += inputLine;
			}
			result = new String(result.getBytes(), Constants.UTF_8_ENCODING);
		} catch (Exception e) {
			throw new BaseAppException(SysErrCode.UNKNOW_EXPCEPTION, e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(isr);
			IOUtils.closeQuietly(bout);
		}
		return result;
	}
	
	/**
	 * 获取post的内容
	 * 
	 * @param request
	 * @return
	 * @throws BaseAppException 
	 */
	public static String getPostBody(HttpServletRequest request) throws BaseAppException {
		StringBuffer json = new StringBuffer();
		String line = null;
		InputStream ins = null;
		InputStreamReader insReader  = null;
		BufferedReader bReader = null;
		try {
			ins = request.getInputStream();
			insReader = new InputStreamReader(ins, "UTF-8");
			bReader = new BufferedReader(insReader);
			while ((line = bReader.readLine()) != null) {
				json.append(line);
			}
		} catch (Exception e) {
			logger.error("read error ",e);
			ExceptionHandler.publish("data recieved error !");
		}finally {
			if(ins != null) try { ins.close(); } catch (Exception e2) {}
			if(insReader != null) try { insReader.close(); } catch (Exception e2) {}
			if(bReader != null) try { bReader.close(); } catch (Exception e2) {}
		}
		String retJson = "";
		try {
			retJson = URLDecoder.decode(json.toString(),"UTF-8");
			if (retJson.indexOf("=") == 0) {
				retJson = retJson.substring(1);
			}
		} catch (Exception e) {
			retJson = json.toString();
			logger.error("read parse erro : "+json.toString());
		}
		return retJson;
	}
	
	/**
	 * 获取post的内容
	 * 
	 * @param request
	 * @return
	 * @throws BaseAppException 
	 */
	public static Map<String, String> getPostBody4Url(HttpServletRequest request) throws Exception {
		Map<String, String> reqMap = new HashMap<String, String>();
		Map<String, String[]> paramMap = request.getParameterMap();
		for(String key : paramMap.keySet()){
			reqMap.put(key, URLDecoder.decode(paramMap.get(key)[0], "utf-8"));
		}
		
		return reqMap;
	}
	
	/**
	 * 
	 * @param url
	 * @param method
	 * @param paramMap
	 * @return
	 * @throws BaseAppException
	 */
	public static String send(String url, String method, HashMap<String, String> paramMap) throws BaseAppException{
		String res = null;
		if(Util.isEmpty(url)){
			return res;
		}
		Set<String> keySet = paramMap.keySet();
		if(Constants.HTTP_METHOD_GET.equals(method)){
			String params = url.contains("?") ? "&" : "?";
			for(String key: keySet){
				params += key+"=" + paramMap.get(key)+"&";
			}
			
			String send = url + params;
			logger.debug("Send request msg:" + send);
			res = httpGet(send);
		}else{
			String data = JSONObject.toJSONString(paramMap);
			logger.debug("Send request msg: url is:" + url +", data is:"+ data);
			res = httpPost(url, data, 20000, Constants.HTTP_CONTENT_TYPE_JSON);
		}
		
		logger.debug("Receive response msg:" + res);
		
		return res;
	}
	/**
	 * 
	 * @param url
	 * @param method
	 * @param paramMap
	 * @param timeOut
	 * @param charset
	 * @return
	 * @throws BaseAppException
	 */
	public static String send(String url, String method, HashMap<String, String> paramMap,int timeout,String charset) throws BaseAppException{
		String res = null;
		if(Util.isEmpty(url)){
			return res;
		}
		Set<String> keySet = paramMap.keySet();
		if(Constants.HTTP_METHOD_GET.equals(method)){
			String params = url.contains("?") ? "&" : "?";
			for(String key: keySet){
				params += key+"=" + paramMap.get(key)+"&";
			}
			
			String send = url + params;
			logger.debug("Send request msg:" + send);
			res = httpGet(send,timeout,charset);
		}else{
			String data = JSONObject.toJSONString(paramMap);
			logger.debug("Send request msg: url is:" + url +", data is:"+ data);
			res = httpPost(url, data, timeout, Constants.HTTP_CONTENT_TYPE_JSON);
		}
		
		logger.debug("Receive response msg:" + res);
		
		return res;
	}
	
	
	
	/**
     * @param url参数
	 * @throws BaseAppException 
     * */
    public static String UrlEncode(String url) throws BaseAppException {
    	String ret="";
        try {
        	ret=URLEncoder.encode(url, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return ret;
    }
    
    
    /**
     * @param url参数
	 * @throws BaseAppException 
     * */
    public static String UrlDecode(String url) throws BaseAppException {
    	String ret="";
        try {
        	ret=URLDecoder.decode(url, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return ret;
    }
   
}
