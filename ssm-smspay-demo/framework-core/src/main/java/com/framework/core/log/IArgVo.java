/**
 * 
 */
package com.framework.core.log;

/**
 * @author Administrator
 *
 */
public interface IArgVo {
	/**
	 * 返回该vo中需要记录进log的数据
	 */
	public String getOperateArgs();
}
