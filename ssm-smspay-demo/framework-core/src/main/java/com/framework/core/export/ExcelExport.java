/**
 * 
 */
package com.framework.core.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.framework.core.Constants;
import com.framework.core.page.Page;
import com.framework.core.utils.DateUtil;
import com.framework.core.utils.ReflectUtil;
import com.framework.core.utils.Util;

/**
 * @author Administrator
 *
 */
public final class ExcelExport {

	 private static final Logger logger = Logger.getLogger(ExcelExport.class);
	 
	 
    /**
     * 报表数据生成excel
     * 
     * @param report
     * @return
     */
    public void export4Jq(HttpServletRequest request, HttpServletResponse response, Page page) {
        String titleName = "数据";
        // 文件成员变量
        CellStyle cellStyle = null;// 单元格样式
        SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory, exceeding rows will be flushed to disk
        Sheet  sheet = wb.createSheet(titleName);
        
        // 当前行、单元格
        int rowIndex = 0;
        int cellIndex = 1;

        // 列数据展示
        String[] metaDate = request.getParameter("colnamesen").split(",");
        String[] metaDateChinese = null;
		try {
			metaDateChinese = URLDecoder.decode(request.getParameter("colnames"),"UTF-8").split(",");
		} catch (UnsupportedEncodingException e1) {
			logger.error("create excel file faild:", e1);
		}

        Row row = sheet.createRow(rowIndex++);
        Cell cell;
        cellIndex = 0;
        Font headerfont = wb.createFont();
        headerfont.setFontName("宋体");
        headerfont.setFontHeightInPoints((short) 10);
        headerfont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        cellStyle = wb.createCellStyle();
        // excel里写列名
        for (String title : metaDateChinese) {
            cell = row.createCell(cellIndex++);
            cellStyle.setFont(headerfont);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(title);
        }
        // 写列数据
        Object[] nullArgs = new Object[0];
        
        Font fontStyle = wb.createFont();
        fontStyle.setFontName("宋体");
        fontStyle.setFontHeightInPoints((short) 10);
        fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        
        List<?> list = page.getRes();
        for (Object object : list) {
            Class<?> clazz = object.getClass();
            row = sheet.createRow(rowIndex++);
            cellIndex = 0;
            for (String fieldName : metaDate) {
                cell = row.createCell(cellIndex++);
                cellStyle.setFont(fontStyle);
                cell.setCellStyle(cellStyle);
                Object fieldValue = null;
                Method method = null;
                Type retType = null;
                try {
                    method = clazz.getMethod("get_" + fieldName);
                    fieldValue = method.invoke(object, nullArgs);
                } catch (Exception e) {
                    try {
                        method = clazz.getMethod("get" + String.valueOf(fieldName.charAt(0)).toUpperCase() + fieldName.substring(1));
                        fieldValue = method.invoke(object, nullArgs);
                    } catch (Exception e2) {
                        // do nothing;
                    }
                }
                if (Util.isEmpty(fieldValue)) {
                    continue;
                }

                retType = method.getGenericReturnType();

                if (fieldValue instanceof Date) {
                    cell.setCellValue(DateUtil.date2String((Date) fieldValue));
                }else if (fieldName.endsWith("Fee") || fieldName.endsWith("Amount")) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(Double.valueOf(fieldValue.toString()));
                    CellStyle style = getFeeCellStyle(wb);
                    cell.setCellStyle(style);
                } else if (fieldName.endsWith("Rate") || fieldName.endsWith("Ratio")) {
                    cell.setCellValue(fieldValue+"%");
                }
                else if (Util.isNumber(retType)) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((long) fieldValue);
                }
                else if (Util.isDouble(retType)) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((double) fieldValue);
                } else {
                    cell.setCellValue(fieldValue.toString());
                }
            }
        }
        OutputStream os = null;  
        try {
        	os = response.getOutputStream();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + DateUtil.getNowDate().getTime() + ".xlsx");
            wb.write(os);
        } catch (Exception e) {
        	page= null;
            logger.error("create excel file faild:", e);
        }finally{
        	IOUtils.closeQuietly(os);
        }
    }
    
    /**
     * 
     * @param request
     * @param response
     * @param dataList
     */
    public void exportBillReport(HttpServletRequest request, HttpServletResponse response, ArrayList<?> dataList) {
    	SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory, exceeding rows will be flushed to disk
        // 列数据展示
        String[] fields = request.getParameter("colnamesen").split(",");
        String[] names = null;
		try {
			names = URLDecoder.decode(request.getParameter("colnames"),"UTF-8").split(",");
		} catch (UnsupportedEncodingException e1) {
			logger.error("create excel file faild:", e1);
			return;
		}
		Map<String, ArrayList<Object>> disMap = createSheet("汇总数据", wb, dataList, fields, names, true);
		
		if(Util.notEmpty(disMap)){
			Set<String> keySet = disMap.keySet();
			for(String key: keySet){
				createSheet(Util.getFirstNotNull(key, "空"), wb, disMap.get(key), fields, names, false);
			}
		}
		
    	OutputStream os = null;  
         try {
         	os = response.getOutputStream();
             response.setContentType("application/vnd.ms-excel");
             response.setHeader("Content-disposition", "attachment;filename=" + DateUtil.getNowDate().getTime() + ".xlsx");
             wb.write(os);
         } catch (Exception e) {
             logger.error("create excel file faild:", e);
         }finally{
         	IOUtils.closeQuietly(os);
         }
    }
    
    public Map<String, ArrayList<Object>> createSheet(String sheetName, SXSSFWorkbook wb, ArrayList<?> dataList, String[] fields, String[] names, boolean isSummary){
    	Map<String, ArrayList<Object>> disMap = new HashMap<String, ArrayList<Object>>();
    	 // 当前行、单元格
        int rowIndex = 0;
        int cellIndex = 0;
        Sheet sheet = wb.createSheet(sheetName);
        Row row = sheet.createRow(rowIndex++);
        Cell cell = null;
       
        CellStyle cellStyle = null;// 单元格样式
        cellStyle = wb.createCellStyle();
        Font headFont = getHeadFont(wb);
        // excel里写列名
        for (String title : names) {
            cell = row.createCell(cellIndex++);
            cellStyle.setFont(headFont);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(title);
        }
        
        Font cellFont = getCellFont(wb);
        for (Object object : dataList) {
            row = sheet.createRow(rowIndex++);
            cellIndex = 0;
            if(isSummary){
            	try {
            		Object distributorId = ReflectUtil.getValueByFieldName(object, "disName");
                	ArrayList<Object> disList = disMap.get((String)distributorId);
                	
                	if(Util.isEmpty(disList)){
                		disList = new ArrayList<Object>();
                		disMap.put((String)distributorId, disList);
                	}
                	disList.add(object);
				} catch (Exception e) {
					continue;
				}
            }
            for (String fieldName : fields) {
                cell = row.createCell(cellIndex++);
                cellStyle.setFont(cellFont);
                cell.setCellStyle(cellStyle);
                Object fieldValue = null;
				try {
					fieldValue = ReflectUtil.getValueByFieldName(object, fieldName);
				} catch (Exception e) {
					 // do nothing;
				}
              
                if (Util.isEmpty(fieldValue)) {
                    continue;
                }

                if (fieldValue instanceof Date) {
                    cell.setCellValue(DateUtil.date2String((Date) fieldValue));
                }else if (fieldName.endsWith("Fee") || fieldName.endsWith("Amount")) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(Double.valueOf(fieldValue.toString()));
                    CellStyle style = getFeeCellStyle(wb);
                    cell.setCellStyle(style);
                } else if (fieldName.endsWith("Rate") || fieldName.endsWith("Ratio")) {
                    cell.setCellValue(fieldValue+"%");
                }
                else if (fieldValue instanceof Integer || fieldValue instanceof Long) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((long) fieldValue);
                }
                else if (fieldValue instanceof Float || fieldValue instanceof Double) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((double) fieldValue);
                }  else {
                    cell.setCellValue(fieldValue.toString());
                }
            }
        }
        
        return disMap;
    }
    
    /**
     * 
     * @param wb
     * @return
     */
    private static Font getHeadFont(SXSSFWorkbook wb){
    	 Font headerfont = wb.createFont();
         headerfont.setFontName("宋体");
         headerfont.setFontHeightInPoints((short) 10);
         headerfont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
         
         return headerfont;
    }
    
    /**
     * 
     * @param wb
     * @return
     */
    private static Font getCellFont(SXSSFWorkbook wb){
    	 Font fontStyle = wb.createFont();
         fontStyle.setFontName("宋体");
         fontStyle.setFontHeightInPoints((short) 10);
         fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        
        return fontStyle;
   }
    
    private static CellStyle getFeeCellStyle(SXSSFWorkbook wb){
    	 CellStyle cellStyle = wb.createCellStyle();
	     DataFormat df = wb.createDataFormat();  //此处设置数据格式  
	     cellStyle.setDataFormat(df.getFormat("#,#0.00")); 
       
       return cellStyle;
  }
   
    public void exportBills(HttpServletRequest request, HttpServletResponse response, ArrayList<?> dataList) {
    	SXSSFWorkbook wb = new SXSSFWorkbook(100);
        // 列数据展示
        String[] fields = request.getParameter("colnamesen").split(",");
        String[] names = null;
		try {
			names = URLDecoder.decode(request.getParameter("colnames"),"UTF-8").split(",");
		} catch (UnsupportedEncodingException e1) {
			logger.error("create excel file faild:", e1);
			return;
		}
		createBillsSheet("账单数据", wb, dataList, fields, names);
		
    	OutputStream os = null;  
         try {
         	os = response.getOutputStream();
             response.setContentType("application/vnd.ms-excel");
             response.setHeader("Content-disposition", "attachment;filename=" + DateUtil.getNowDate().getTime() + ".xlsx");
             wb.write(os);
         } catch (Exception e) {
             logger.error("create excel file faild:", e);
         }finally{
         	IOUtils.closeQuietly(os);
         }
    }
    
    private void createBillsSheet(String sheetName, SXSSFWorkbook wb, ArrayList<?> dataList, String[] fields, String[] names){
    	 // 当前行、单元格
        int rowIndex = 0;
        int cellIndex = 0;
        Sheet sheet = wb.createSheet(sheetName);
        Row row = sheet.createRow(rowIndex++);
        Cell cell = null;
       
        CellStyle defStyle = wb.createCellStyle();
        CellStyle selfStyle = wb.createCellStyle();
        selfStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        selfStyle.setFillBackgroundColor(HSSFColor.ROYAL_BLUE.index);//设置背景色      
        selfStyle.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);//前景填充色 
        Font headFont = getHeadFont(wb);
        // excel里写列名
        for (String title : names) {
            cell = row.createCell(cellIndex++);
            defStyle.setFont(headFont);
            cell.setCellStyle(defStyle);
            cell.setCellValue(title);
        }
        
        Font cellFont = getCellFont(wb);
        for (Object object : dataList) {
            row = sheet.createRow(rowIndex++);
            cellIndex = 0;
            boolean isOperAdd = false;
            try {
				Object operId = ReflectUtil.getValueByFieldName(object, "operId");
				isOperAdd = Util.isEmpty(operId) ? false : true;
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
            for (String fieldName : fields) {
                cell = row.createCell(cellIndex++);
                if(isOperAdd){
                	selfStyle.setFont(cellFont);
                	cell.setCellStyle(selfStyle);
                }else{
                	defStyle.setFont(cellFont);
                	cell.setCellStyle(defStyle);
                }
                
                Object fieldValue = null;
				try {
					fieldValue = ReflectUtil.getValueByFieldName(object, fieldName);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
              
                if (Util.isEmpty(fieldValue)) {
                    continue;
                }

                if (fieldValue instanceof Date) {
                    cell.setCellValue(DateUtil.date2String((Date) fieldValue));
                }else if (fieldName.endsWith("Fee") || fieldName.endsWith("Amount")) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(Double.valueOf(fieldValue.toString()));
                    CellStyle style = getFeeCellStyle(wb);
                    if(isOperAdd){
                    	style.setFont(cellFont);
                    	style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    	style.setFillBackgroundColor(HSSFColor.ROYAL_BLUE.index);//设置背景色      
                    	style.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);//前景填充色 
                    }
                    cell.setCellStyle(style);
                } else if (fieldName.endsWith("Rate") || fieldName.endsWith("Ratio")) {
                    cell.setCellValue(fieldValue+"%");
                }
                else if (fieldValue instanceof Integer || fieldValue instanceof Long) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((long) fieldValue);
                }
                else if (fieldValue instanceof Float || fieldValue instanceof Double) {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((double) fieldValue);
                } else {
                    cell.setCellValue(fieldValue.toString());
                }
                
            }
        }
        
    }
    
    
    public void choiceExportBills(HttpServletRequest request, HttpServletResponse response, ArrayList<?> dataList,String fileName) throws Exception{
        // 得到文件绝对路径
//        String realPath = ExcelExport.class.getResource("/").getPath()+"template/"+fileName;
        
        WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
        ServletContext servletContext = webApplicationContext.getServletContext();
        // 得到文件绝对路径fi
        String realPath = servletContext.getRealPath("/")+"/admin/resources/template/"+fileName;
        logger.error("当前项目地址："+realPath);
        FileInputStream fileInputStream=new FileInputStream(new File(realPath));
        // 获取模板
        XSSFWorkbook tempWorkBook = new XSSFWorkbook(fileInputStream);
        // 获取模板sheet页
        Sheet tempSheet = tempWorkBook.getSheetAt(0);
        
        
        
        // 列数据展示
    	String[] fields = new String[]{"companyName","billMonth","productName","smtAfterFee","_settleRate","","","","_realFee"};
		createBillsSheet1(tempSheet, tempWorkBook, dataList, fields);
		
		OutputStream out =null;
//				new FileOutputStream("D://tes.xlsx");
//		tempWorkBook.write(out);
		
//		OutputStream os = null;  
        try {
        	out = response.getOutputStream();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + DateUtil.getNowDate().getTime() + ".xlsx");
            tempWorkBook.write(out);
        } catch (Exception e) {
            logger.error("create excel file faild:", e);
        }finally{
        	IOUtils.closeQuietly(out);
        }
		
		
    }
    
    private void createBillsSheet1(Sheet sheet, XSSFWorkbook wb, ArrayList<?> dataList, String[] fields) throws Exception{
    	
    	//title标题
    	Object data = dataList.get(0);
    	String companyName = String.valueOf(ReflectUtil.getValueByFieldName(data, fields[0]));
    	String billMonth =String.valueOf(ReflectUtil.getValueByFieldName(data, fields[1]));
    	String replace = billMonth.replace("-", "年");
    	String time = replace+"月";
    	if(companyName.equals("null")){
    		companyName="";
    	}
    	Row row1 = sheet.getRow(1);
        Cell cell1 = row1.getCell(0);
        cell1.setCellValue(companyName+time+"结算单");
        // 当前行、单元格
       int rowIndex = 3;
       int cellIndex = 0;
       Row headRow = sheet.getRow(2);
       
       Row row = null;
       Cell cell = null;
      
       CellStyle numStyle = wb.createCellStyle();
       numStyle.cloneStyleFrom(headRow.getCell(0).getCellStyle());
       numStyle.setFont(getCellFont(wb));
       
       CellStyle totalStyle = wb.createCellStyle();
       totalStyle.cloneStyleFrom(headRow.getCell(8).getCellStyle());
       totalStyle.setFont(getCellFont(wb));
//       Long settleFeeTotal = 0L;
//       Long realFeeTotal=0L;
       for (Object object : dataList) {
           row = createRow(sheet, rowIndex++);
           cellIndex = 0;
           
           
//           settleFeeTotal +=(Long) ReflectUtil.getValueByFieldName(object, "settleFee");
//           realFeeTotal += (Long) ReflectUtil.getValueByFieldName(object, "realFee");
//           
//           
           for (String fieldName : fields) {
               cell = row.createCell(cellIndex++);
               cell.setCellStyle(cellIndex == 9 ? totalStyle : numStyle);
               Object fieldValue = null;
				try {
					fieldValue = ReflectUtil.getValueByFieldName(object, fieldName);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
               if (Util.isEmpty(fieldValue)) {
                   continue;
               }

               if (fieldValue instanceof Date) {
                   cell.setCellValue(DateUtil.date2String((Date) fieldValue));
               }
               else if (fieldValue instanceof Integer || fieldValue instanceof Long) {
                   cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                   cell.setCellValue((long) fieldValue);
               }
               else if (fieldValue instanceof Float || fieldValue instanceof Double) {
                   cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                   cell.setCellValue((double) fieldValue);
               } else {
                   cell.setCellValue(fieldValue.toString());
               }
           }
           
       }
       
   }
    
    /** 
     * 找到需要插入的行数，并新建一个POI的row对象 
     * @param sheet 
     * @param rowIndex 
     * @return 
     */  
     private static Row createRow(Sheet sheet, Integer rowIndex) {  
         if (sheet.getRow(rowIndex) != null) {  
             int lastRowNo = sheet.getLastRowNum();  
             sheet.shiftRows(rowIndex, lastRowNo, 1);  
         }  
         return sheet.createRow(rowIndex);  
     } 
/**
 * 
 * @param wb
 * @return
 */
private static Font getCellFont(XSSFWorkbook wb){
	 Font fontStyle = wb.createFont();
     fontStyle.setFontName("宋体");
     fontStyle.setFontHeightInPoints((short) 10);
     fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
    
    return fontStyle;
}
    
    
    
    
}
