package com.framework.core.exception;

/**
 * 系统异常编码
 */
public class SysErrCode {

	/** 未知异常 */
	public final static String UNKNOW_EXPCEPTION = "XJL-00-0000";
	/** 数据库操作异常 */
	public final static String DB_OPER_EXPCEPTION = "XJL-00-0001";

	/** 调用服务异常 */
	public final static String REFLECTION_EXPCEPTION = "XJL-00-0002";

	/** 没有查询到数据 */
	public final static String NO_FIND_DATA = "XJL-00-0003";

	/** 表映射文件参数配置异常 */
	public final static String TABLE_MAPPING_EXPCEPTION = "XJL-00-0004";

	/** 服务调用失败, 请稍侯再试 */
	public static final String CALL_SERVICE_ERROR = "XJL-00-0005";

	/** 获取不到数据库资源 */
	public static final String DB_POOL_BUSY = "XJL-00-0006";

	/** 实例化异常 */
	public static final String INSTANCE_EXCEPTION = "XJL-00-0007";
	
	/** 数据源查找异常 */
	public static final String LOOKUP_DATASOURCE_ERROR = "XJL-00-0008";

	/** 配置文件异常. */
	public static final String CONFIG_PROPERTIES_NULL = "XJL-00-0009";

	/** 文件访问错误 */
	public static final String FILE_ACCESS_ERROR = "XJL-00-0010";

}
