package com.framework.core.utils;


import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomUtils;
import org.apache.log4j.Logger;

import com.framework.core.Constants;

/**
 * 
 * @author
 * 
 */
public final class Util {
	private static final Logger logger = Logger.getLogger(Util.class);
	private Util() {
	}

	public static boolean isEmpty(Object[] objs) {
		if (objs == null || objs.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 判断字符串是否为�?
	 * 
	 * @param str
	 *            String
	 * @return boolean
	 */
	public static boolean notEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * 判断对象数组是否为空
	 * 
	 * @param objs
	 *            Object[]
	 * @return boolean
	 */
	public static boolean notEmpty(Object[] objs) {
		return !isEmpty(objs);
	}

	public static boolean isEmpty(String str) {
		if (str == null || str.length() == 0) {
			return true;
		}
		return false;
	}

	/** 至少有一个为�? */
	public static boolean isAnyEmpty(Object... args) {
		for (Object object : args) {
			if (isEmpty(object))
				return true;
		}
		return false;
	}

	/** 全部为空 */
	public static boolean isAllEmpty(Object... args) {
		for (Object object : args) {
			if (notEmpty(object))
				return false;
		}
		return true;
	}

	/** 全部不为�? */
	public static boolean allNotEmpty(Object... args) {
		for (Object object : args) {
			if (isEmpty(object))
				return false;
		}
		return true;
	}

	/** 至少有一个不为空 */
	public static boolean notAllEmpty(Object... args) {
		for (Object object : args) {
			if (notEmpty(object))
				return true;
		}
		return false;
	}

	/**
	 * 判断集合]是否为空
	 * 
	 * @param coll
	 *            Collection
	 * @return boolean
	 */
	public static boolean notEmpty(Collection<?> coll) {
		return !isEmpty(coll);
	}

	public static boolean isEmpty(Collection<?> coll) {
		if (coll == null || coll.size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 判断int数组是否为空
	 * 
	 * @param intArr
	 *            int[]
	 * @return boolean
	 */
	public static boolean notEmpty(int[] arg) {
		return !isEmpty(arg);
	}

	public static boolean isEmpty(int[] intArr) {
		if (intArr == null || intArr.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 判断long数组是否为空
	 * 
	 * @param longArr
	 *            long[]
	 * @return boolean
	 */
	public static boolean notEmpty(long[] arg) {
		return !isEmpty(arg);
	}

	public static boolean isEmpty(long[] longArr) {
		if (longArr == null || longArr.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 判断Map是否为空
	 * 
	 * @param map
	 *            Map
	 * @return boolean
	 */
	public static boolean notEmpty(Map<?, ?> arg) {
		return !isEmpty(arg);
	}

	public static boolean isEmpty(Map<?, ?> map) {
		if (map == null || map.size() == 0) {
			return true;
		}
		return false;
	}

	public static boolean notEmpty(Object arg) {
		return !isEmpty(arg);
	}

	public static boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof String) {
			return obj.equals("");
		}
		return false;
	}

	public static boolean isEmpty(Object obj, Object nullValue) {
		if (obj == null) {
			return true;
		} else if (obj.equals(nullValue)) {
			return true;
		}
		return false;
	}

	public static boolean isAllEmpty(String... strings) {
		for (String s : strings) {
			if (Util.notEmpty(s))
				return false;
		}
		return true;
	}

	public static String trim(String arg, String trim) {
		if (arg == null)
			return null;
		if (trim == null)
			trim = "";
		arg = arg.trim();
		while (arg.length() > trim.length() && arg.startsWith(trim))
			arg = arg.substring(trim.length());
		while (arg.length() > trim.length() && arg.endsWith(arg))
			arg = arg.substring(0, arg.length() - trim.length());
		return arg.trim();
	}

	public static String trimLeft(String arg, String trim) {
		if (arg == null)
			return null;
		if (trim == null)
			trim = "";
		arg = arg.trim();
		while (arg.length() > trim.length() && arg.startsWith(trim))
			arg = arg.substring(trim.length());
		return arg.trim();
	}

	public static String trimRight(String arg, String trim) {
		if (arg == null)
			return null;
		if (trim == null)
			trim = "";
		arg = arg.trim();
		while (arg.length() > trim.length() && arg.endsWith(arg))
			arg = arg.substring(0, arg.length() - trim.length());
		return arg.trim();
	}

	public static String trim(String arg, char trim) {
		if (arg == null)
			return null;
		arg = arg.trim();
		while (arg.length() > 0 && arg.charAt(0) == trim) {
			arg = arg.substring(1, arg.length());
		}
		while (arg.length() > 0 && arg.charAt(arg.length() - 1) == trim) {
			arg = arg.substring(0, arg.length() - 1);
		}
		return arg.trim();
	}

	/**
	 * 去掉字符串的空格
	 * 
	 * @param str
	 *            String
	 * @return String
	 */
	public static String trim(String str) {
		return (str == null) ? str : str.trim();
	}

	/**
	 * 获取第一个非空数据
	 * 
	 * @param args
	 * @return
	 */
	public static String getFirstNotNull(String... args) {
		for (String string : args) {
			if (notEmpty(string)) {
				return string;
			}
		}
		return null;
	}

	@SafeVarargs
	public static <G extends Object> G getFirstNotNull(G... args) {
		for (G o : args) {
			if (notEmpty(o)) {
				return o;
			}
		}
		return null;
	}

	/**
	 * 判断方法返回值类型
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isNumber(Type type) {
		return type == Integer.class || type == Long.class;
	}

	/**
	 * 判断方法返回值类型
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isDouble(Type type) {
		return type == Double.class || type == Float.class;
	}

	/**
	 * 将key=value的链接转换成map
	 * 
	 * @param link
	 * @return
	 */
	public static HashMap<String, String> link2Map(String link) {
		return link2Map(link, false);
	}

	/**
	 * 将key=value的链接转换成map
	 * 
	 * @param link
	 * @param key2Lower
	 * @return
	 */
	public static HashMap<String, String> link2Map(String link, boolean key2Lower) {
		HashMap<String, String> res = new HashMap<String, String>();

		if (isEmpty(link)) {
			return res;
		}

		// 去除首个问号
		if (link.substring(0, 1).equals("?")) {
			link = link.substring(1);
		}

		String key = null;
		String value = null;

		String[] params = link.split("&");
		for (String s : params) {
			String[] p = s.split("=");
			if (p.length != 2) {
				continue;
			}
			key = p[0];
			value = p[1];
			if (key2Lower) {
				res.put(key.toLowerCase(), value);
			} else {
				res.put(key, value);
			}
		}

		return res;
	}

	/**
	 * 对象属性转换成数据库字段
	 * 
	 * @param filed
	 * @return
	 */
	public static String filed2ColName(String filed) {
		if (filed == null || filed.trim().equals(""))
			return filed;
		int stringLength = filed.length();
		StringBuilder sBuilder = new StringBuilder();
		for (int y = 0; y < stringLength; y++) {
			Character c = filed.charAt(y);
			if (Character.isUpperCase(c)) {
				sBuilder.append("_");
				sBuilder.append(c);
			} else {
				sBuilder.append(c);
			}
		}
		return sBuilder.toString();
	}

	/**
	 * 从url读取图片流
	 * */
	public static final int CLIENT_PC = 100;
	public static final int CLIENT_PC_WINDOWS = 102;
	public static final int CLIENT_PC_LINUX = 103;
	public static final int CLIENT_PC_MAC = 104;
	public static final int CLIENT_PC_IE = 113;
	public static final int CLIENT_PC_IE6 = 114;
	public static final int CLIENT_PC_IE7 = 115;
	public static final int CLIENT_PC_IE8 = 116;
	public static final int CLIENT_PC_IE9 = 17;
	public static final int CLIENT_PC_IE10 = 118;
	public static final int CLIENT_PC_IE11 = 119;
	public static final int CLIENT_PC_CHROME = 120;
	public static final int CLIENT_PC_360 = 121;
	public static final int CLIENT_PC_SOUGOU = 122;
	public static final int CLIENT_PC_FIREFOX = 123;
	public static final int CLIENT_PC_LIEBAO = 124;
	public static final int CLIENT_PHONE = 200;
	public static final int CLIENT_PHONE_IPHONE = 201;
	public static final int CLIENT_PHONE_ANDROID = 202;
	public static final int CLIENT_PHONE_WINPHONE = 203;
	public static final int CLIENT_PHONE_UC = 224;
	public static final int CLIENT_PHONE_QQ = 225;
	public static final int CLIENT_PHONE_360 = 226;
	public static final int CLIENT_PHONE_OPERA = 227;
	public static final int CLIENT_PHONE_CHROME = 228;
	public static final int CLIENT_PHONE_HUAWEI = 251;
	public static final int CLIENT_PHONE_ZTE = 252;
	public static final int CLIENT_PHONE_LENOVE = 253;
	public static final int CLIENT_PHONE_NUBIA = 254;
	public static final int CLIENT_PHONE_XIAOMI = 255;
	public static final int CLIENT_PHONE_COOLPAD = 256;
	public static final int CLIENT_PHONE_TIANYU = 257;
	public static final int CLIENT_PHONE_JINLI = 258;
	public static final int CLIENT_PHONE_TCL = 259;
	public static final int CLIENT_PHONE_SAMSUNG = 259;
	public static final int CLIENT_PHONE_NOKIA = 259;
	public static final int CLIENT_PHONE_SNOY = 259;
	public static final int CLIENT_PHONE_MEIZU = 259;
	public static final int CLIENT_PHONE_HAIXIN = 259;
	public static final int CLIENT_PAD = 300;
	public static final int CLIENT_PAD_ANDROID = 302;
	public static final int CLIENT_PAD_IPAD = 303;
	public static final int CLIENT_PAD_IPAD_MINI = 304;
	public static final int CLIENT_PAD_KINDEL = 305;

	public static boolean isPhone(String userAgent) {
		int type = checkClientType(userAgent);
		return type >= 200 && type < 300;
	}

	public static boolean isPad(String userAgent) {
		int type = checkClientType(userAgent);
		return type >= 300 && type < 400;
	}

	public static boolean isPC(String userAgent) {
		int type = checkClientType(userAgent);
		return type >= 100 && type < 200;
	}

	public static boolean isPhone(int type) {
		return type >= 200 && type < 300;
	}

	public static boolean isPad(int type) {
		return type >= 300 && type < 400;
	}

	public static boolean isPC(int type) {
		return type >= 100 && type < 200;
	}

	public static String getDeviceType(int type) {
		if (isPC(type)) {
			return "PC";
		}
		if (isPad(type)) {
			return "PAD";
		}
		return "PHONE";
	}

	public static int checkClientType(String userAgent) {
		String hd = (userAgent == null ? "" : userAgent).toLowerCase();
		if (hd.contains("android")) {
			return CLIENT_PHONE_ANDROID;
		}
		if (hd.contains("iphone")) {
			return CLIENT_PHONE_IPHONE;
		}
		if (hd.contains("mac")) {
			return CLIENT_PC_MAC;
		}
		if (hd.contains("chromium")) {
			return CLIENT_PC_LINUX;
		}
		if (hd.contains("coolpad")) {
			return CLIENT_PHONE_COOLPAD;
		}
		if (hd.contains("firefox")) {
			return CLIENT_PC_FIREFOX;
		}
		if (hd.contains("windows phone")) {
			return CLIENT_PHONE_WINPHONE;
		}
		if (hd.contains("arm")) {
			return CLIENT_PHONE;
		}
		if (hd.contains("windows") || hd.contains("msie")) {
			return CLIENT_PC_IE;
		}
		if (hd.contains("chom")) {
			return CLIENT_PC_CHROME;
		}
		if (hd.contains("linux")) {
			return CLIENT_PC_LINUX;
		}
		if (hd.contains("pad")) {
			return CLIENT_PAD;
		}
		return CLIENT_PHONE;
	}

	/**
	 * 处理文件上传的目录
	 * 
	 * @param path
	 * @param rootPath
	 * @return
	 */
	public static String parseFTPStorePath(String path, String rootPath) {
		// 统一按照/处理，win下linx下都支持/ 但，linux不支持\
		path = path.replace("\\", "/");
		rootPath = rootPath.replace("\\", "/");
		// linux 和 win下都 非 绝对路径
		if (!path.startsWith("/") && !path.contains(":")) {
			while (rootPath.endsWith("/")) {
				rootPath = rootPath.substring(0, rootPath.length() - 1);
			}
			while (path.startsWith("../")) {
				path = path.substring(3, path.length());
				rootPath = rootPath.substring(0, rootPath.lastIndexOf("/"));
			}
			while (path.startsWith("./")) {
				path = path.substring(2, path.length());
				rootPath = rootPath.substring(0, rootPath.lastIndexOf("/"));
			}
			path = rootPath + "/" + path;
		}
		// 结尾加斜杠或者反斜杠.
		if (!path.endsWith("/")) {
			path = path + "/";
		}
		// 防止手误写出双//
		return path.replace("//", "/");
	}

	/**
	 * 防html标签转义
	 * @param input
	 * @param encoding
	 * @return
	 */
	public static String htmlEscape(String input) {
		if (input == null)
			return null;
		StringBuilder escaped = new StringBuilder(input.length() * 2);
		for (int i = 0; i < input.length(); i++) {
			char character = input.charAt(i);
			String reference = null;
			switch (character) {
				case 60: // '<'
					reference= "&lt;";
					break;
				case 62: // '>'
					reference= "&gt;";
					break;
			}
			
			escaped.append(null == reference ?  character : reference);
		}

		return escaped.toString();
	}
	
	/**
	 * 防html标签转义
	 * @param input
	 * @param encoding
	 * @return
	 */
	public static String decode(String input) {
		if (input == null)
			return null;
		String res ="";
		try {
			res = URLDecoder.decode(input, Constants.UTF_8_ENCODING);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}

		return res;
	}
	
	 /**
	 * 获取ip
	 * @param request
	 * @return
	 */
	public static String getIp(HttpServletRequest request) {
		if(Util.isEmpty(request)){
			return "";
		}
		
		//从nginx配置中获取
		if(Util.notEmpty(request.getHeader("X-Real-IP"))){
			return request.getHeader("X-Real-IP");
		}
		
		return request.getRemoteHost();
	}

	/**
	 * 文件大小换算
	 * @param size
	 * @return
	 */
	public static String convertFileSize(long size) {
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;
 
        if (size >= gb) {
            return String.format("%.1f GB", (float) size / gb);
        } else if (size >= mb) {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        } else if (size >= kb) {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        } else
            return String.format("%d B", size);
    }
	
	/**
     * 取的系统配置参数，从环境变量中取，如果取不到就从JVM参数中取，仍然没有取到，返回默认值
     * 
     * @param paramName
     * @param defValue
     * @return
     */
    public static String getSystemPararm(String paramName, String defValue) {
        String sysProValue = SysPropUtil.getPropertyAsString(paramName, "");
        if (!Util.isEmpty(sysProValue)) {
            return sysProValue;
        }
        String jvmValue = System.getProperty(paramName);
        if (!Util.isEmpty(jvmValue)) {
            return jvmValue;
        }
        return defValue;
    }
    
    /**
     * 
     * @param prefix
     * @return
     */
    public static String generateOrderNo(String prefix) {
    	RandomUtils.nextInt(10, 100);
        return prefix+System.currentTimeMillis()+ RandomUtils.nextInt(100,1000);
    }
    
    
    /**
     * 
     * @param paramMap
     * @return
     */
    public static String map2Link(Map<String, String> paramMap) {
    	Set<String> keySet = paramMap.keySet();
    	String link = "";
    	String val = "";
    	for(String key : keySet){
    		val = isEmpty(paramMap.get(key)) ? "" : paramMap.get(key);
    		link += key+"="+val+"&";
    	}
    	
    	if(notEmpty(link)){
    		link = link.substring(0, link.length()-1);
    	}
    	
    	return link;
    }
    
    /**
     * 
     * @param String[]
     * @return
     */
    public static Long[] StrArr2LongArr(String[] strArr) {
    	Long[] longArr=new Long[strArr.length];
    	for(int i=0;i<strArr.length;i++)
    	{
    		longArr[i]=Long.valueOf(strArr[i]); 
    	}
    	return longArr;
    }
    
    private final static String[] AGENT = { "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser" };  
    
    public static boolean isMobile(String ua) {  
        boolean flag = false;  
        if (!ua.contains("Windows NT") || (ua.contains("Windows NT") && ua.contains("compatible; MSIE 9.0;"))) {  
            // 排除 苹果桌面系统  
            if (!ua.contains("Windows NT") && !ua.contains("Macintosh")) {  
                for (String item : AGENT) {  
                    if (ua.contains(item)) {  
                        flag = true;  
                        break;  
                    }  
                }  
            }  
        }  
        return flag;  
    }
    
    
    /**
     * 获取通道状态码信息,统一规范处理  
     * @param code
     * @param msg
     * @return
     */
    public static String getCodeMsg(String code,String msg){
		String desc="";
		if(Util.notEmpty(code)){
			desc+="retcode:"+code+";";
		}
		if(Util.notEmpty(msg)){
			desc+="retmsg:"+msg+";";
		}
		return desc;
	}
    
    /**
     * String数组转Long数组
     * @param strAry
     * @return
     */
    public static Long[] stringAry2LongAry(String[] strAry) {
    	Long[] longAry= new Long[strAry.length];
        for (int idx = 0; idx < strAry.length; idx++) {
        	longAry[idx] = Long.parseLong(strAry[idx]);
        }
    	return longAry;
	}
    /**
     * 
     * @param strAry
     * @return
     */
    public static ArrayList<Long> stringAry2LongAry2(String[] strAry) {
    	ArrayList<Long> longAry = new ArrayList<Long>();
        for (int idx = 0; idx < strAry.length; idx++) {
        	longAry.add(Long.parseLong(strAry[idx]));
        }
    	return longAry;
	}
    
    public static List<Long> stringAry2LongAry3(String[] strAry) {
    	List<Long> longAry = new ArrayList<Long>();
        for (int idx = 0; idx < strAry.length; idx++) {
        	longAry.add(Long.parseLong(strAry[idx]));
        }
    	return longAry;
	}
}