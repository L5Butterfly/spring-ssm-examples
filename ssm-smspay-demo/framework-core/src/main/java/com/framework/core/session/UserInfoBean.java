package com.framework.core.session;

import java.io.Serializable;

public class UserInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 访问的用户ID，可能是管理员，也可能是访问用户的
	 */
	private Long userId;

	private String userCode;
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
}
