/**
* @author 
* @version 
* ��˵��
*/ 
package com.framework.core; 

import com.framework.core.utils.Util;





/**
 * 
 * @author Administrator
 *
 */
public interface Constants {

	/** int类型的NULL表示. */
	int NULL_INT = -2147483648;

	/** float类型的NULL表示. */
	float NULL_FLOAT = 1.4E-45F;

	/** double类型的NULL表示. */
	double NULL_DOUBLE = 4.9E-324D;

	/** long类型的NULL表示. */
	long NULL_LONG = -9999999999999998L; // -9223372036854775808L; 前台JAVASCRIPT没有这么大的值

	/** String类型的NULL标识 */
	String NULL_STRING = "-nullnull-";
	
	/** 缓存模式_本地缓存 */
	String CACHE_MODE_LOCAL = "LOCAL";

	/** 缓存模式_REDIS */
	String CACHE_MODE_REDIS = "REDIS";
	
	/** UTF-8编码集. */
	String UTF_8_ENCODING = "UTF-8";
	
	/** Base64编码集. */
	String Base64_ENCODING = "Base64";
	
	/** HexStr编码集. */
	String HexStr_ENCODING = "HexStr";
	
	 //通用分隔符:,
	String SPLIT_COMMON = ",";
	
	//分隔符:-
    String SPLIT_STRIKE = "-";
    
    //分隔符::
    String SPLIT_COLON= ":";

    //分隔符: _
    String SPLIT_UNDERLINE = "_";
    
	//数据库类型：postgres
	String DB_TYPE_POSTGRES = "postgres";
	
	 // 数据库类型：mysql
	String DB_TYPE_MYSQL = "mysql";
	
	// 数据库类型：oracle
	String DB_TYPE_ORACLE = "oracle";
	
	/** 运行在windows系统 */
	//boolean isRunningOnWindows = (System.getenv("OS") != null && System.getenv("OS").toLowerCase().contains("win"));
	boolean isRunningOnWindows = (System.getProperty("os.name") != null && System.getProperty("os.name").toLowerCase().contains("win"));

	/** 运行在mac系统 */
	boolean isRunningOnMac = (System.getProperty("os.name") != null && System.getProperty("os.name").toLowerCase().contains("mac os x"));

	/** 是否为主进程 */
	boolean IS_MAIN_PROCESS = Boolean.valueOf(Util.getFirstNotNull(System.getProperty("MainProcess"), "false"));

	//是否是生产环境
	boolean isProduction = !isRunningOnMac && !isRunningOnWindows;
	
	/** jetty.port jetty监听的端口 */
    int LISTENNING_PORT = Integer.valueOf(Util.getFirstNotNull(System.getProperty("jetty.http.port"),"-1"));
    
    /** 进程名 */
    String PROCESS_NAME = Util.getFirstNotNull(System.getProperty("processName"), "unkown");
    
    /** 进程名 */
    String HOST_NAME = Util.getFirstNotNull(System.getenv("HOSTNAME"), "unkown");
   
    /** 是否是console进程，默认为false，为Portal进程 */
    boolean IS_CONSOLE_PROCESS = isProduction ? Boolean.valueOf(Util.getFirstNotNull(System.getProperty("MainProcess"),"false")) : true;
    
    /** 应用目录 */
    String APP_HOME = Util.getSystemPararm("PROJECT_HOME", System.getProperty("user.dir"));
    
    /** 配置目录 */
    String CONFIG_DIR = APP_HOME + "/config/";
    
    //http报文内容类型：json
    String CONTENT_TYPE_JSON = "application/json";
    
  //http报文内容类型：键值对
    String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
    
    //http报文内容类型：键值对
    String CONTENT_TYPE_XML = "text/xml";
    
    //根工厂
    Long ROOT_VNO = 0l;
    
    //http请求类型
    String HTTP_METHOD_GET = "M01";
    
    //http请求类型
    String HTTP_METHOD_POST = "M02";
    
    //http报文类型
    String HTTP_CONTENT_TYPE_JSON = "application/json";
    
    /** sql语句中?替换标识 */
	String SQL_REPLACE_KEY = "-QUESTION-MARK-";
	
	
}
