/**
 * 
 */
package com.framework.core.utils;

import java.security.MessageDigest;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.framework.core.Constants;
import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;

/**
 * @author Administrator
 *
 */
public final class WxUtil {

	private static Logger logger = Logger.getLogger(WxUtil.class);
	
	//获取token地址
	private static final String TOKEN_URL="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appId}&secret={appSecret}";
	
	//获取发布的菜单
	private static final String QUERY_MENU_URL="https://api.weixin.qq.com/cgi-bin/menu/get?access_token={access_token}";
		
	//发布菜单的地址
	private static final String PUBLISH_MENU_URL=" https://api.weixin.qq.com/cgi-bin/menu/create?access_token={access_token}";
	
	//网页授权token获取；用用根据code获取openid
	private static final String AUTH_TOKEN_URL ="https://api.weixin.qq.com/sns/oauth2/access_token?appid={appId}&secret={appSecret}&code={code}&grant_type=authorization_code";
	
	//微信分享需要的ticket
	private static final String JS_TICKET_URL ="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={access_token}&type=jsapi";
	
	//微信分享需要的ticket
	private static final String TO_WX_AUTH_PAGE ="https://open.weixin.qq.com/connect/oauth2/authorize?appid={appid}&redirect_uri={url}&response_type=code&scope=snsapi_base&state=jychongzhi#wechat_redirect";
		
	//微信接口成功返回结果码
	private static final String WECHAT_SUCCESS_RES_CODE = "0";
	
	/**
	 * 微信获取accessToken接口
	 * @param appId
	 * @param appSecret
	 * @return
	 * @throws BaseAppException
	 */
	public static String getAccessToken(String appId, String appSecret) throws BaseAppException{
		String reqUrl = TOKEN_URL.replace("{appId}", appId).replace("{appSecret}", appSecret);
		logger.debug("Send wechat token url:"+reqUrl);
		String resJson = HttpUtil.httpGet(reqUrl);
		logger.debug("Get wechat res:" + resJson);
		
		JSONObject json = JSONObject.parseObject(resJson);
		ExceptionHandler.publishMsg(Util.notEmpty(json.getString("errcode")), json.getString("errmsg"));
		
		return json.getString("access_token");
	}
	
	/**
	 * 查询当前发布的菜单信息
	 * @param access_token
	 * @return
	 * @throws BaseAppException
	 */
	public static JSONObject getPublishMenu(String access_token) throws BaseAppException{
		String reqUrl = QUERY_MENU_URL.replace("{access_token}", access_token);
		logger.debug("Send wechat query menu url:"+reqUrl);
		String resJson = HttpUtil.httpGet(reqUrl);
		logger.debug("Get query menu res:" + resJson);
		
		JSONObject json = JSONObject.parseObject(resJson);
		
		ExceptionHandler.publishMsg(Util.notEmpty(json.getString("errcode")), json.getString("errmsg"));
		
		return json;
	}
	
	/**
	 * 发布菜单
	 * @param access_token
	 * @param jsonData
	 * @return
	 * @throws BaseAppException
	 */
	public static JSONObject publishMenu(String access_token, String jsonData) throws BaseAppException{
		String reqUrl = PUBLISH_MENU_URL.replace("{access_token}", access_token);
		logger.debug("Send wechat publish menu url:"+reqUrl);
		String resJson = HttpUtil.httpPost(reqUrl, jsonData, 2000, Constants.CONTENT_TYPE_FORM);
		System.out.println("Get publish menu res:" + resJson);
		
		JSONObject json = JSONObject.parseObject(resJson);
		
		String resCode = json.getString("errcode");
		ExceptionHandler.publishMsg(!WECHAT_SUCCESS_RES_CODE.equals(resCode) , json.getString("errmsg"));
		
		return json;
	}
	
	/**
	 * 根据网页携带的code参数获取访问用户的openId
	 * @param appId
	 * @param appSecret
	 * @param code
	 * @return
	 * @throws BaseAppException
	 */
	public static String getOpenIdByCode(String appId, String appSecret, String code) throws BaseAppException{
		String reqUrl = AUTH_TOKEN_URL.replace("{appId}", appId).replace("{appSecret}", appSecret).replace("{code}", code);
		logger.debug("Send auth token url:"+reqUrl);
		String resJson = HttpUtil.httpGet(reqUrl);
		logger.debug("Get auth token res:" + resJson);
		
		JSONObject json = JSONObject.parseObject(resJson);
		
		ExceptionHandler.publishMsg(Util.notEmpty(json.getString("errcode")), json.getString("errmsg"));
		
		return json.getString("openid");
	}
	
	/**
	 * 微信分享需要的ticket
	 * @param access_token
	 * @return
	 * @throws BaseAppException 
	 */
	public static String getTicket(String access_token) throws BaseAppException{
		String reqUrl = JS_TICKET_URL.replace("{access_token}", access_token);
		logger.debug("Send js ticket url:"+reqUrl);
		String resJson = HttpUtil.httpGet(reqUrl);
		logger.debug("Get js ticket res:" + resJson);
		
		JSONObject json = JSONObject.parseObject(resJson);
		String resCode = json.getString("errcode");
		ExceptionHandler.publishMsg(!WECHAT_SUCCESS_RES_CODE.equals(resCode) , json.getString("errmsg"));
		
		return json.getString("ticket");
	}
	
	/**
	 * 生成分享需要的参数
	 * @param jsapi_ticket
	 * @param url
	 * @param appid
	 * @return
	 * @throws BaseAppException 
	 */
	public static Map<String, String> createWxShareParam(String appId, String appSecret, String url) throws BaseAppException {  
	    Map<String, String> ret = new HashMap<String, String>();  
	    String nonce_str = UUID.randomUUID().toString();  
	    String timestamp = Long.toString(System.currentTimeMillis() / 1000);  
	  
	    String token = getAccessToken(appId, appSecret);
	    String jsapi_ticket = getTicket(token);
	    //注意这里参数名必须全部小写，且必须有序  
	    String str = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + nonce_str + "&timestamp=" + timestamp + "&url=" + url;  
	    String signature = getSign(str);
	  
	    ret.put("url", url);  
	    ret.put("jsapi_ticket", jsapi_ticket);  
	    ret.put("nonceStr", nonce_str);  
	    ret.put("timestamp", timestamp);  
	    ret.put("signature", signature);  
	    ret.put("appid", appId);  
	    return ret;  
	}  
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	private static String getSign(String source){
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");  
			crypt.reset();  
			crypt.update(source.getBytes("UTF-8"));  
			byte[] hash = crypt.digest();
			Formatter formatter = new Formatter();  
			for (byte b : hash) {  
			    formatter.format("%02x", b);  
			}  
			String result = formatter.toString();  
			formatter.close();  
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return source; 
	}
	
	/**
	 * 将页面跳转到鉴权页面
	 * @param access_token
	 * @return
	 * @throws BaseAppException 
	 */
	public static String toWxauthPage(String url, String appid) throws BaseAppException{
		String reqUrl = TO_WX_AUTH_PAGE.replace("{appid}", appid).replace("{url}", url);
		return reqUrl;
	}
	
	public static void main(String[] args) throws BaseAppException {
		String appId= "wx928b8513c9c2c1e9";
		String appSecret = "8c75bbca71256b89c02b487b3ab1c9d6";
		String newDevUrl = "http://www.wowifi.com/traffic/index.html?enc=FEADCF6E5BE49F0902139027F288FABCF7EC26A883478248&to=dev";
		String newTestUrl = "http://www.wowifi.com/traffic/index.html?enc=CCC91A5098224D98B77AD5CA4F6D763C88C34240FF220EB8&to=test";
		String token = getAccessToken(appId, appSecret);
		JSONObject menu = getPublishMenu(token);
		
		//JSONObject testBtn = JSONObject.parseObject("{\"name\":\"流量充值测试\",\"sub_button\":[],\"type\":\"view\",\"url\":\"http://www.wowifi.com/traffic/index.html?enc=FEADCF6E5BE49F0902139027F288FABCF7EC26A883478248&to=test\"}");
		
		JSONArray btnArr = menu.getJSONObject("menu").getJSONArray("button");
		
		for(Object btnObj : btnArr){
			JSONObject btn = (JSONObject) btnObj;
			JSONArray subBtnArr = btn.getJSONArray("sub_button");
			for(Object subBtnObj : subBtnArr){
				JSONObject subBtn = (JSONObject) subBtnObj;
				if("流量充值开发".equals(subBtn.getString("name"))){
					System.out.println(subBtn.getString("name")+"----"+subBtn.getString("url"));
					subBtn.replace("url", newDevUrl);
				}
				if("流量充值测试".equals(subBtn.getString("name"))){
					System.out.println(subBtn.getString("name")+"----"+subBtn.getString("url"));
					subBtn.replace("url", newTestUrl);
				}
			}
		}
		
		System.out.println("publish data:" + menu.getJSONObject("menu").toString());
		
		publishMenu(token, menu.getJSONObject("menu").toString());
		menu = getPublishMenu(token);
		System.out.println(menu.toString());
		
		/*String token = getAccessToken(appId, appSecret);
		String ticket = getTicket(token);
		System.out.println(ticket);*/
		
	}
}
