package com.framework.core.cache;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import com.framework.core.Constants;
import com.framework.core.utils.Util;

/**
 * 抽象缓存数据集合类，用于本地缓存
 * 
 * @author ying.rui
 * 
 */
public abstract class BaseCacheProvider<T extends ICacheItem> implements ICacheProvider<T> {
	
    private static final Logger logger = Logger.getLogger(BaseCacheProvider.class);

    /* cacheName */
    private String cacheName;
    
    /**
     * 将数据对象里面的属�?�为约定NULL的置�?
     * 
     * @param obj
     */
    protected void nullDataObject(Object obj, Class<?> clazz) {
        try {
            Field[] fields = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                String fieldName = fields[i].getName();
                if(fieldName.equalsIgnoreCase("serialVersionUID")) {
                	continue;
                }
                Method getMethod = null;
                try {
                    getMethod = obj.getClass().getMethod(columnName2MethodName("get", fieldName));// get方法
                } catch (NoSuchMethodException e) {
                    getMethod = obj.getClass().getMethod(columnName2MethodName("is", fieldName));// boolean类型的get方法
                }
                Class<?> fieldType = fields[i].getType();
                Object[] arg = { null };// 空参�?
                if (fieldType == Long.class) {
                    Long l = (Long) getMethod.invoke(obj);
                    // 是NULL_LONG，set null
                    if (l != null && l == Constants.NULL_LONG) {
                        Method setMethod = obj.getClass().getMethod(columnName2MethodName("set", fieldName), Long.class);// set方法
                        setMethod.invoke(obj, arg);
                    }
                }
                if (fieldType == String.class) {
                    String str = (String) getMethod.invoke(obj);
                    // 是NULL_STRING，set null
                    if (!Util.isEmpty(str) && str.equals(Constants.NULL_STRING)) {
                        Method setMethod = obj.getClass().getMethod(columnName2MethodName("set", fieldName), String.class);// set方法
                        setMethod.invoke(obj, arg);
                    }
                }
            }
            Class<?> supClazz = clazz.getSuperclass();// 父类递归
            if (supClazz != null && supClazz != Object.class) {
                nullDataObject(clazz.cast(obj),supClazz);
            }
        } catch (Exception e) {
            logger.error("nullDateObject Exception ", e);
        }
    }

    /**
     * 表字段名 to 类方法名
     * 
     * @param columnName
     * @return
     */
    private static String columnName2MethodName(String type, String columnName) {
        StringBuilder methodName = new StringBuilder();
        methodName.append(type);
        methodName.append(columnName.substring(0, 1).toUpperCase()).append(columnName.substring(1));
        return methodName.toString();
    }
    
    /**
     * 获取前缀
     * @param clazz
     * @return
     */
    protected String getPrefix(Class<T> clazz) {
		return clazz.getSimpleName()+"_";
	}

	public String getCacheName() {
		return cacheName;
	}

	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
}
