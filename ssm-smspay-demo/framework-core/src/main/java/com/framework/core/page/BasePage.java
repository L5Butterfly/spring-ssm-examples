/**
 * 
 */
package com.framework.core.page;

import java.io.Serializable;

/**
 * @author Administrator
 *
 */
public abstract class BasePage implements IPage, Serializable {
	private static final long serialVersionUID = 1L;
	public static int DEFAULT_PAGE_SIZE = 20;
	private int rows = DEFAULT_PAGE_SIZE;
	private int currentResult;
	private int total;
	private int page = 1;
	private int records = -1;

	//排序列
	private String sidx;
	
	//排序，升序还是降序
	private String sord;
	
	
	public BasePage(int page, int rows, int records) {
		this.page = page;
		this.rows = rows;
		this.records = records;
	}

	public int getRecords() {
		return this.records;
	}

	public void setRecords(int records) {
		if (records < 0) {
			this.records = 0;
			return;
		}
		this.records = records;
	}

	public BasePage() {
	}

	public int getFirstResult() {
		return (this.page - 1) * this.rows;
	}

	public void setRows(int rows) {
		if (rows < 0) {
			this.rows = DEFAULT_PAGE_SIZE;
			return;
		}
		this.rows = rows;
	}

	public int getTotal() {
		if (this.total <= 0) {
			this.total = (this.records / this.rows);
			if ((this.total == 0) || (this.records % this.rows != 0)) {
				this.total += 1;
			}
		}
		return this.total;
	}

	public int getRows() {
		return this.rows;
	}

	public void setPageNo(int page) {
		this.page = page;
	}

	public int getPageNo() {
		return this.page;
	}

	public boolean isFirstPage() {
		return this.page <= 1;
	}

	public boolean isLastPage() {
		return this.page >= getTotal();
	}

	public int getNextPage() {
		if (isLastPage()) {
			return this.page;
		}
		return this.page + 1;
	}

	public int getCurrentResult() {
		this.currentResult = ((getPageNo() - 1) * getRows());
		if (this.currentResult < 0) {
			this.currentResult = 0;
		}
		return this.currentResult;
	}

	public int getPrePage() {
		if (isFirstPage()) {
			return this.page;
		}
		return this.page - 1;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}
}
