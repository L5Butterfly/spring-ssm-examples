package com.framework.core.sms.sender;

public interface ISMSSender {
	
	/**
	 * 短信发送接口
	 * @param phoneNum 手机号
	 * @param content  发送内容
	 * @return SP接收成功返回字符串，接口实现
	 * @throws Exception
	 */
	public String sendMessage(String phoneNum, String content) throws Exception;
	
}
