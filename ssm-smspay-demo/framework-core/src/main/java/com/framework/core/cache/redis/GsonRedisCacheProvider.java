package com.framework.core.cache.redis;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.ReflectionUtils;

import com.framework.core.Constants;
import com.framework.core.cache.BaseCacheProvider;
import com.framework.core.cache.ICacheItem;
import com.framework.core.cache.redis.jedo.JedisUtil;
import com.framework.core.utils.AppContextUtil;
import com.framework.core.utils.BeanUtil;
import com.framework.core.utils.GsonFactory;
import com.framework.core.utils.Util;

public class GsonRedisCacheProvider<T extends ICacheItem> extends BaseCacheProvider<T> {
	private static final Logger logger = Logger.getLogger(GsonRedisCacheProvider.class);
	
	public static long totalItemSize;
	
	private String prefix;
	
	private String daoName;
	
	private Class<T> clazz;

	private JedisUtil jedisUtil;
	
	public GsonRedisCacheProvider(String cacheName, String daoName, Class<T> clazzCD, JedisUtil jedisUtil) {
		this.prefix = clazzCD.getSimpleName() + "_";
		this.clazz = clazzCD;
		super.setCacheName(cacheName);
		this.jedisUtil = jedisUtil;
		this.daoName = daoName;
		// 反射调用dao 放到cachemap arraylist
		loadData();
	}

	@Override
	public T getItem(String key) {
		if (null == key)
			key = "null";
		// 使用hashmap
		String value = (String) jedisUtil.hget(super.getPrefix(clazz), key);
		T res = GsonFactory.getGson().fromJson(value, clazz);
		return res;
	}

	@Override
	public T getItem(Long key) {
		return getItem(key+"");
	}

	@Override
	public void delItem(String key) {
		if (null == key)
			key = "null";
		jedisUtil.hdel(this.prefix, key);
	}

	@Override
	public void delItem(Long key) {
		delItem(key+"");
	}

	@Override
	public void delItem(T item) {
		delItem(item == null ? "null" : item.key());
	}

	@Override
	public <E extends T> void setItem(E iCacheItem) {
		this.nullDataObject(iCacheItem, iCacheItem.getClass());// 约定空数值处�?
		String sValue = GsonFactory.getGson().toJson(iCacheItem);
		// 使用hashmap
		totalItemSize += jedisUtil.hset(this.prefix, iCacheItem.key(), sValue);
	}

	@Override
	public ArrayList<T> getAll() {
		ArrayList<T> list = new ArrayList<T>();
		// 使用hashmap
		List<Object> values = jedisUtil.hvals(prefix);
		for (Object s : values) {
			T t = GsonFactory.getGson().fromJson((String)s, clazz);
			list.add(t);
		}
		return list;
	}

	@Override
	public void reload() {
		cleanData();
		loadData();
	}

	@Override
	public void cleanData() {
		jedisUtil.del(prefix);
	}
	
	@SuppressWarnings("unchecked")
	public synchronized void loadData() {
		try {
			if ((!Constants.isProduction) || (Constants.IS_MAIN_PROCESS)) {
				cleanData();
			}
			
			long start = System.currentTimeMillis();
			logger.fatal("Load cache start, cache name [" + this.getCacheName() + "] , dao class is ["+ this.daoName + "] ");
			
			//Dao为空表示不需要从数据库加载
			if(Util.isEmpty(this.daoName)){
				return;
			}
			Object obj = AppContextUtil.getBean(this.daoName);
			 Class<?> clazz = obj.getClass();
			 Method method = clazz.getMethod("getCacheList");
			
			 ArrayList<T> objAL = (ArrayList<T>) ReflectionUtils.invokeMethod(method, obj);
			 
			 long queryEnd = System.currentTimeMillis();
			 for (int i = 0; i < objAL.size(); i++) {
					T iCacheDataItem = this.clazz.newInstance();
					BeanUtil.copyProperties(objAL.get(i), iCacheDataItem);
					this.setItem(iCacheDataItem);
				}
			 long loadEnd = System.currentTimeMillis();
			 logger.fatal("Load cache end, cache name [" + this.getCacheName() + "] , size is ["+ objAL.size() + "], totlaSize ["+totalItemSize+"],"
			 		+ " query used [ "+(queryEnd-start) +"] millisecond, load used ["+(loadEnd -queryEnd)+"], total used ["+(loadEnd -start)+"] millisecond.");
			 
		} catch (Exception e) {
			logger.error("load cache "+ this.getCacheName()+" failed.");
			logger.error(e.getMessage(), e);
		} 
	}
}
