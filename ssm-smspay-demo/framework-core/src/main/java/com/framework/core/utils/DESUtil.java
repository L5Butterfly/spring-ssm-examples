package com.framework.core.utils;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.log4j.Logger;
import org.springframework.util.Base64Utils;

import com.framework.core.exception.BaseAppException;

public class DESUtil {
	
	private static final Logger logger = Logger.getLogger(DESUtil.class);
	
	/**
	 * 加密，输出16进制字符串
	 * 
	 * @param key
	 * @param str
	 * @return
	 * @throws BaseAppException 
	 */
	public static String encrypt(String key, String source) throws BaseAppException {
		try {
			if (Util.isEmpty(source)) {
				return null;
			}
			// DES算法要求有一个可信任的随机数源
			SecureRandom sr = new SecureRandom();
			DESKeySpec dks = new DESKeySpec(key.getBytes());
			// 创建一个密匙工厂，然后用它把DESKeySpec转换成一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);
			// Cipher对象实际完成加密操作
			Cipher cipher = Cipher.getInstance("DES");
			// 用密匙初始化Cipher对象
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, sr);
			byte[] encryptedData = cipher.doFinal(source.getBytes());
			return HexStringUtil.bytes2strHex(encryptedData);// 16进制
		} catch (Exception e) {
			logger.error("Encrypt failed! key="+key+", source = " +source);
			throw new BaseAppException("DESHelper Encrypt Exception", e);
		}
	}

	/**
	 * 解密，输入16进制字符串
	 * 
	 * @param key
	 * @param str
	 * @return
	 * @throws BaseAppException 
	 */
	public static String decrypt(String key, String source) throws BaseAppException {
		try {
			if (Util.isEmpty(source)) {
				return null;
			}
			// 转为大写字符
			source = source.toUpperCase();
			// DES算法要求有一个可信任的随机数源
			SecureRandom sr = new SecureRandom();
			DESKeySpec dks = new DESKeySpec(key.getBytes());
			// 创建一个密匙工厂，然后用它把DESKeySpec对象转换成一个SecretKey对象
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);
			// Cipher对象实际完成解密操作 
			Cipher cipher = Cipher.getInstance("DES");	
			// 用密匙初始化Cipher对象
			cipher.init(Cipher.DECRYPT_MODE, secretKey, sr);
			byte[] decryptedData = cipher.doFinal(HexStringUtil.strHex2Bytes(source));
			return new String(decryptedData);
		} catch (Exception e) {
			logger.error("Decrypt failed! key="+key+", source = " +source);
			throw new BaseAppException("DESHelper Decrypt Exception", e);
		}
	}

	/** 
     * 3DES加密 
     * @param key 密钥，24位 
     * @param srcStr 将加密的字符串 
     * @return 
	 * @throws BaseAppException 
     * 
     */  
	public static String encode3Des(String key, String source) throws BaseAppException {
		try {
			Key deskey = null;  
	        DESedeKeySpec spec = new DESedeKeySpec(key.getBytes());  
	        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");  
	        deskey = keyfactory.generateSecret(spec);  
	        Cipher cipher = Cipher.getInstance("desede/ECB/PKCS5Padding");  
	        cipher.init(Cipher.ENCRYPT_MODE, deskey);  
	        byte[] bOut = cipher.doFinal(source.getBytes());  
	        return Base64Utils.encodeToString(bOut);  
		} catch (Exception e) {
			logger.error("encrypt failed! key=" + key + ", source = " + source);
			throw new BaseAppException("encode3Des Exception", e);
		}
	}
	
	/**
	 * 3DES解密
	 * 
	 * @param key
	 *            加密密钥，长度为24字节
	 * @param desStr
	 *            解密后的字符串
	 * @return
	 * @throws BaseAppException 
	 */
	public static String decode3Des(String key, String source) throws BaseAppException {
		try {
			Key deskey = null;
			DESedeKeySpec spec = new DESedeKeySpec(key.getBytes());
			SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
			deskey = keyfactory.generateSecret(spec);
			Cipher cipher = Cipher.getInstance("desede/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, deskey);
			byte[] bOut = cipher.doFinal(Base64Utils.decodeFromString(source));
			return new String(bOut);
		} catch (Exception e) {
			logger.error("Decrypt failed! key=" + key + ", source = " + source);
			throw new BaseAppException("decode3Des Exception", e);
		}
	}
	
}
