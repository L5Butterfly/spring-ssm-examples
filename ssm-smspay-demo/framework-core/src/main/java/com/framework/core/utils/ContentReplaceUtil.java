package com.framework.core.utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Set;

public class ContentReplaceUtil {
	private ContentReplaceUtil() {
	}

	/** BRACKET_BEG */
	private final static String BRACKET_BEG = "{";
	/** BRACKET_END */
	private final static String BRACKET_END = "}";

	/**
	 * 得到替换的信息
	 * 
	 * @param content
	 * @return
	 */
	public static String getParamContent(String content, Object... args) {
		String strRet = content;
		for (int i = 0; i < args.length; i++) {
			if (args[i] == null) {
				continue;
			}
			StringBuilder sbOld = new StringBuilder().append(BRACKET_BEG).append(i).append(BRACKET_END);
			StringBuilder sbNew = new StringBuilder().append(args[i].toString());
			strRet = strRet.replace(sbOld.toString(), sbNew.toString());
		}
		return strRet;
	}

	/**
	 * 得到替换的信息，根据hsah键值获取
	 * 
	 * @param hashMap
	 * @return
	 */
	public static String getParamContent(String content, HashMap<String, String> hashMap) {
		Set<String> keys = hashMap.keySet();
		for (String key : keys) {
			String askey = "\\" + BRACKET_BEG + key + "\\" + BRACKET_END;
			content = content.replaceAll(askey, hashMap.get(key));
		}
		return content;
	}

	/**
	 * 对字符串进行参数替换/填充,如:<br/>
	 * <code>
	 *    http://www.baidu.com?uname={username}&pswd={password}
	 *     http://www.baidu.com?uname=[username]&pswd=[password]
	 * </code>
	 * <br />
	 * 调用sourceObject 中对应get方法值进行替换<br />
	 * 如果对应方法不存在或调用出错,则替换为null
	 * 
	 * @param str
	 *            需要进行参数填充的str
	 * @param sourceObject
	 *            用于取值的custAccessInfoVo
	 * @param leftCut
	 *            左边界,在上面的例子中左边界为"{"
	 * @param rightCut
	 *            右边界,在上面的例子中右边界为"}"
	 * @param ignoreCases
	 *            是否忽略大小写
	 * @return url 参数填充过后的url
	 * */
	public static String praseStringVaribles(String str, Object sourceObject, String leftCut, String rightCut,
			boolean ignoreCases) {
		if (str.indexOf("{") == -1) {
			return str;
		}
		Method[] ms = sourceObject.getClass().getMethods();
		HashMap<String, Method> methods = new HashMap<>();
		for (Method m : ms) {
			methods.put(ignoreCases ? m.getName().toLowerCase() : m.getName(), m);
		}
		int start, end;
		while ((start = str.indexOf(leftCut)) != -1 && (end = str.indexOf(rightCut)) != -1 && start < end) {
			String name = str.substring(start + leftCut.length(), end);
			String keyName = ignoreCases ? "get" + name.toLowerCase() : "get" + name.substring(0, 1).toUpperCase()
					+ name.substring(1);
			Object value = "";
			if (methods.containsKey(keyName)) {
				try {
					value = methods.get(keyName).invoke(sourceObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			str = str.replace(leftCut + name + rightCut, value == null ? "" : value.toString());
		}
		return str;
	}
}
