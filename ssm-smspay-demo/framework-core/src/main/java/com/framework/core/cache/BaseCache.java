package com.framework.core.cache;

import java.util.ArrayList;

import com.framework.core.utils.BeanUtil;
import com.framework.core.utils.Util;

/**
 *
 * @author ying.rui
 *
 */
public abstract class BaseCache<T extends ICacheItem> implements IBaseCache<T> {

	 /** iBaseCache */
	protected ICacheProvider<T> iBaseCache;

    private void init(String cacheName, String daoName, Class<T> clazzCD) {
    	iBaseCache = CacheFactory.getCacheProvider(cacheName, daoName, clazzCD, this.getClass());
    }

    public BaseCache(String cacheName, String daoName, Class<T> clazzCD) {
        init(cacheName, daoName, clazzCD);
    }

    public void delItem(T item) {
    	iBaseCache.delItem(item == null ? null : item.key());
    }

    public T getItem(String key) {
        return iBaseCache.getItem(key);
    }

    public T getItem(Long key) {
        if (null == key) {
            return null;
        }
        return iBaseCache.getItem(Long.toString(key));
    }

    public void delItem(String key) {
    	iBaseCache.delItem(key);
    }

    public void delItem(Long key) {
    	iBaseCache.delItem(Long.toString(key));
    }

    public <E extends T> void setItem(E iCacheDateItem) {
    	T oldCache = getItem(iCacheDateItem.key());
    	if(Util.isEmpty(oldCache)){
    		iBaseCache.setItem(iCacheDateItem);
    	}else{
    		BeanUtil.copyNotNullProperties(iCacheDateItem, oldCache);
    		iBaseCache.setItem(oldCache);
    	}
    }

    public ArrayList<T> getAll() {
        return iBaseCache.getAll();
    }

    public void reload() {
    	iBaseCache.reload();
    }

    public void cleanData() {
    	iBaseCache.cleanData();
    }
}
