package com.framework.core.utils;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.framework.core.Constants;
import com.framework.web.base.cache.item.SysParamCache;
import com.framework.web.po.SysParamPo;

/**
 * 2017
 * 
 * @author xgf
 * 
 */
public class ParamUtil {

	/** logger */
	private static Logger logger = Logger.getLogger(ParamUtil.class);
    // 记录哪些类调用了ParamHelper
    private static final HashMap<String, Class<?>> classMap = new HashMap<String, Class<?>>();
    
	private ParamUtil() {

	}


	/**
	 * 取参数
	 * 
	 * @param param
	 * @return
	 */
	public static String getParamString(String paramCode) {
		String value = getParamObject(paramCode);
		if (value != null) {
			return value;
		} else {
			throw new RuntimeException("Get String Param is null : " + paramCode);
		}
	}

	/**
	 * 取不到，取缺省的
	 * 
	 * @param paramCode
	 * @param defaultValue
	 * @return
	 */
	public static String getParamString(String paramCode, String defaultValue) {
		String value = getParamObject(paramCode);
		if (value == null) {
			return defaultValue;
		} else {
			return value;
		}
	}

	/**
	 * 取参数
	 * 
	 * @param param
	 * @return
	 */
	public static int getParamInt(String paramCode) {
		String value = getParamObject(paramCode);
		if (value != null) {
			return Integer.parseInt(value);
		} else {
			throw new RuntimeException("Get Int Param is null : " + paramCode);
		}
	}

	/**
	 * 取不到，取缺省的
	 * 
	 * @param paramCode
	 * @param defaultValue
	 * @return
	 */
	public static int getParamInt(String paramCode, int defaultValue) {
		String value = getParamObject(paramCode);
		if (value != null) {
			return Integer.parseInt(value);
		} else {
			return defaultValue;
		}
	}

	/**
	 * 取参数
	 * 
	 * @param param
	 * @return
	 */
	public static Long getParamLong(String paramCode) {
		String value = getParamObject(paramCode);
		if (value != null) {
			return Long.parseLong(value);
		} else {
			throw new RuntimeException("Get Long Param is null : " + paramCode);
		}
	}

	/**
	 * 取不到，取缺省的
	 * 
	 * @param paramCode
	 * @param defaultValue
	 * @return
	 */
	public static Long getParamLong(String paramCode, Long defaultValue) {
		String value = getParamObject(paramCode);
		if (value == null) {
			return defaultValue;
		} else {
			return Long.parseLong(value);
		}
	}

	/**
	 * 去boolean参数
	 * 
	 * @param paramCode
	 * @return
	 */
	public static boolean getParamBoolean(String paramCode) {
		String value = getParamObject(paramCode);
		if (value == null) {
			throw new RuntimeException("Get Boolean Param is null : " + paramCode);
		}
		if ("F".equals(value)) {
			return false;
		} else if ("T".equals(value)) {
			return true;
		} else {
			throw new RuntimeException("Get Boolean Param value is error : " + paramCode);
		}
	}

	/**
	 * 取不到，取缺省的
	 * 
	 * @param paramCode
	 * @param b
	 * @return
	 */
	public static boolean getParamBoolean(String paramCode, boolean defaultValue) {
		String value = getParamObject(paramCode);
		if (value == null) {
			return defaultValue;
		} else {
			return getParamBoolean(paramCode);
		}
	}

	/**
	 * 获取参数
	 * 
	 * @param paramCode
	 * @return
	 */
	public static String getParamObject(String paramCode) {
		return getParamObject(paramCode, Constants.ROOT_VNO);
	}

	/**
	 * 获取参数
	 * 
	 * @param paramCode
	 * @param vnoId 
	 * @return
	 */
	public static String getParamObject(String paramCode, Long vnoId) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String thisClassName = ParamUtil.class.getName();
        String threadClassName = Thread.class.getName();
        String callerClassName = null;
        for (StackTraceElement e : stackTraceElements) {
            if (e.getClassName().equals(thisClassName) || e.getClassName().equals(threadClassName))
                continue;
            callerClassName = e.getClassName();
            System.out.println(e.getFileName());
            System.out.println(e.getLineNumber());
            System.out.println(e.getMethodName());
            logger.debug("callerClassName:" + callerClassName);
            try {
                classMap.put(callerClassName, Class.forName(callerClassName));
            } catch (ClassNotFoundException e1) {
                logger.error(callerClassName);
                logger.error(e);
            }
            break;
        }
		SysParamPo sysParamPo = SysParamCache.INSTANCE.getItem(paramCode+ Constants.SPLIT_UNDERLINE + vnoId);
		if (sysParamPo != null && sysParamPo.getParamValue() != null) {
			return sysParamPo.getParamValue();
		} else {
			logger.fatal("Get ParamObject is null : " + paramCode);
			return null;
		}	
	}
	   
	/**
	 * 根据vnopath递归查找参数（可调用VnoListCache.getVnoPathById获取）
	 * @param paramCode
	 * @param vnoPath 
	 * @return
	 */
	public static String getParamObjectByVnoPath(String paramCode, String vnoPath) {
		if(null == vnoPath){
			logger.fatal("Wrong vnoPath !");
			return null;
		}
		
		String[] vnoIds = vnoPath.split(Constants.SPLIT_STRIKE);
		SysParamPo sysParamPo = null;
		
		for(int i = vnoIds.length -1 ;i >=0; i--){
			if(Util.isEmpty(vnoIds[i])){
				continue;
			}
			sysParamPo = SysParamCache.INSTANCE.getItem(paramCode+ Constants.SPLIT_UNDERLINE + vnoIds[i]);
			if(null != sysParamPo){
				break;
			}
		}
		
		if (sysParamPo != null && sysParamPo.getParamValue() != null) {
			return sysParamPo.getParamValue();
		} else {
			logger.fatal("Get ParamObject is null : " + paramCode);
			return null;
		}
	}
}
