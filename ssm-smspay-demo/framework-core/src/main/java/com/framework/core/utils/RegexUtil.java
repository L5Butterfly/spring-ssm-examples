package com.framework.core.utils;

import java.util.Arrays;
import java.util.regex.Matcher;  
import java.util.regex.Pattern;  
import java.util.regex.PatternSyntaxException;

public final class RegexUtil {
	private RegexUtil() {
	}

	/*
	 * 手机号验证
	 * */
	public static boolean isPhoneLegal(String str)throws PatternSyntaxException {  
		return isChinaPhoneLegal(str) || isHKPhoneLegal(str);  
	}  
	
	  /** 
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数 
     * 此方法中前三位格式有： 
     * 13+任意数 
     * 15+除4的任意数 
     * 18+除1和4的任意数 
     * 17+除9的任意数 
     * 147 
     */  
	
	//18756972495‬
    public static boolean isChinaPhoneLegal(String str) throws PatternSyntaxException {  
        String regExp = "^((13[0-9])|(15[^4])|(18[0-9])|(17[0-9])|(16[0-9])|(19[0-9])|(147))\\d{8}$";  
        Pattern p = Pattern.compile(regExp);  
        Matcher m = p.matcher(str);  
        return m.matches();  
    }
  
    /** 
     * 香港手机号码8位数，5|6|8|9开头+7位任意数 
     */  
    public static boolean isHKPhoneLegal(String str)throws PatternSyntaxException {  
        String regExp = "^(5|6|8|9)\\d{7}$";  
        Pattern p = Pattern.compile(regExp);  
        Matcher m = p.matcher(str);  
        return m.matches();  
    }
    
    /** 
     * IMSI验证 
     */  
    public static boolean isIMSILegal(String str)throws PatternSyntaxException {  
        String regExp = "(\\d{2})(\\w{1})(\\w{1})(\\d{3})(\\d{6})(\\w{2})";  
        Pattern p = Pattern.compile(regExp);  
        Matcher m = p.matcher(str);  
        return m.matches();
    }
    
    /** 
     * 手机号码靓号判断 
     */  
	public static boolean isGoodMobile(String mobile) throws PatternSyntaxException {
		String last4Num = mobile.substring(mobile.length() - 4);
		int a = last4Num.charAt(0);
		int b = last4Num.charAt(1);
		int c = last4Num.charAt(2);
		int d = last4Num.charAt(3);

		// aaa
		if (b == c && c == d) {
			return true;
		}
		// 66 或88
		else if (c == d && (c == 6 || c == 8)) {
			return true;
		}
		// aabb
		else if (a == b && c == d) {
			return true;
		}
		// abab
		else if (a == c && b == d) {
			return true;
		}
		// abba
		else if (a == d && b == c) {
			return true;
		}
		// abc
		else if (b + 1 == c && c + 1 == d) {
			return true;
		} else {
			return false;
		}

	}
	
	public static void main(String[] args) {
		String phon1="18756972495";
		String phone="18756972495‬";
		System.out.println(Arrays.toString(phon1.getBytes()));
		System.out.println(Arrays.toString(phone.getBytes()));
		String phon2="13633736855";
		System.out.println(isChinaPhoneLegal(phon1));
		System.out.println(isChinaPhoneLegal(phone));
		System.out.println(isChinaPhoneLegal(phon2));
	}
}
