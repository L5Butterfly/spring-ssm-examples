/**
 * 
 */
package com.framework.core.session;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.framework.core.utils.Util;

/**
 * @author Administrator
 *
 */
public class UserSessionInfo implements Serializable{

	private static final long serialVersionUID = 1L;

	public static ThreadLocal<UserSessionInfo> threadLocal = new ThreadLocal<UserSessionInfo>();
	
	private HttpServletRequest req;
	private HttpServletResponse res;
	
	/**
	 * portal访问时保存的用户信息
	 */
	private UserInfoBean userBean;
	
	/**
	 * 管理员登陆时保存的用户信息
	 */
	private UserInfoBean managerBean;
	
	public UserSessionInfo(HttpServletRequest req, HttpServletResponse res){
		this.req = req;
		this.res = res;
	}
	
	public HttpServletRequest getReq() {
		return req;
	}

	public void setReq(HttpServletRequest req) {
		this.req = req;
	}

	public HttpServletResponse getRes() {
		return res;
	}

	public void setRes(HttpServletResponse res) {
		this.res = res;
	}

	public UserInfoBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserInfoBean userBean) {
		this.userBean = userBean;
	}

	public UserInfoBean getManagerBean() {
		return managerBean;
	}

	public void setManagerBean(UserInfoBean managerBean) {
		this.managerBean = managerBean;
	}

	public String getAccessIp() {
		if(Util.isEmpty(req)){
			return "";
		}
		
		//从nginx配置中获取
		if(Util.notEmpty(req.getHeader("X-Real-IP"))){
			return req.getHeader("X-Real-IP");
		}
		
		return req.getRemoteHost();
	}

	public String getClientInfo() {
		if(Util.isEmpty(req)){
			return "";
		}
		return Util.getFirstNotNull(req.getParameter("useragent"), req.getHeader("User-Agent"));
	}
	
	public Long getStaffId() {
		if(Util.isEmpty(managerBean)){
			return null;
		}
		return managerBean.getUserId();
	}
	
	public String getAccessCode() {
		if(Util.isEmpty(managerBean) && Util.isEmpty(userBean)){
			return "anonym";
		}
		
		if(Util.notEmpty(managerBean)){
			return managerBean.getUserCode();
		}
		return userBean.getUserCode();
	}
}
