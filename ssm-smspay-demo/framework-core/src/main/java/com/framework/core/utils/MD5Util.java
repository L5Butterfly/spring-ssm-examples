package com.framework.core.utils;

import java.security.MessageDigest;

/**
 * 2012
 * 
 * @author xgf
 * 
 */
public final class MD5Util {

	private MD5Util(){
		
	}
	/**
	 * MD5后转16进制大写输出
	 * @param s
	 * @return
	 */
	public static String convert(String s) {
		try {
			byte[] bytes = s.getBytes();
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(bytes);
			bytes = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				int val = ((int) bytes[i]) & 0xff;
				if (val < 16)
					sb.append("0");
				sb.append(Integer.toHexString(val).toUpperCase());
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * 返回原始MD5加密后字符
	 * @param s
	 * @return
	 */
	public static String convertOriginal(String s) {
		try {
			byte[] bytes = s.getBytes();
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(bytes);
			bytes = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				int val = ((int) bytes[i]) & 0xff;
				if (val < 16)
					sb.append("0");
				sb.append(Integer.toHexString(val));
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}
}
