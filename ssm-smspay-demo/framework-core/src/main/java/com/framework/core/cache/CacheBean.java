/**
 * 
 */
package com.framework.core.cache;

import com.framework.core.cache.redis.jedo.JedisUtil;

/**
 * @author Administrator
 *
 */
public class CacheBean {

	private String cacheModel;
	
	private JedisUtil jedisUtil;

	public String getCacheModel() {
		return cacheModel;
	}

	public void setCacheModel(String cacheModel) {
		this.cacheModel = cacheModel;
	}

	public JedisUtil getJedisUtil() {
		return jedisUtil;
	}

	public void setJedisUtil(JedisUtil jedisUtil) {
		this.jedisUtil = jedisUtil;
	}
	
}
