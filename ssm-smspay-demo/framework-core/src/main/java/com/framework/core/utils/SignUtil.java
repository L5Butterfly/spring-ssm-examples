/**
 * 
 */
package com.framework.core.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author DS
 *
 */
public final class SignUtil {
	
	private  SignUtil(){
		
	}
	
	/**
	 * 
	 * @param paramMap
	 * @param secret
	 * @return
	 */
	public static String sign(Map<String, String> paramMap , String secret){
		String prestr = createLinkString(paramMap);
		
		return MD5Util.convert(prestr +secret);
	}
	
	/**
	 * 把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
	 * 
	 * @param params
	 *  需要排序并参与字符拼接的参数组
	 * @return 拼接后字符串
	 */
	public static String createLinkString(Map<String, String> params) {

		List<String> keys = new ArrayList<String>(params.keySet());
		Collections.sort(keys);

		String prestr = "";

		for (int i = 0; i < keys.size(); i++) {
			String key = keys.get(i);
			String value = params.get(key);

			if (i == keys.size() - 1) {// 拼接时，不包括最后一个&字符
				prestr = prestr + key + "=" + value;
			} else {
				prestr = prestr + key + "=" + value + "&";
			}
		}

		return prestr;
	}
	/**
	 * 把数组所有元素排序，并按照“Key1Value1Key2Value2SecretKey”的模式拼接成字符串
	 * @param params
	 * @return
	 */
	public static String createLinkString2(Map<String, String> params) {

		List<String> keys = new ArrayList<String>(params.keySet());
		//Collections.sort(keys);

		String prestr = "";

		for (int i = 0; i < keys.size(); i++) {
			String key = keys.get(i);
			String value = params.get(key);
			prestr = prestr + key + value;
		}

		return prestr;
	}
	
	
	
	/**
	 * md5按Ma的Key值升序签名
	 * @param map
	 * @param mchntKey
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String doEncrypt(Map<String, String> map,String mchntKey) throws UnsupportedEncodingException {
		
		StringBuilder originStr = new StringBuilder();
		map=MapUtil.sortMapByKey(map,true);
		Iterator<Map.Entry<String, String>> entries = map.entrySet().iterator(); 
		while (entries.hasNext()) { 
		  Map.Entry<String, String> entry = entries.next();
		  if(entry.getValue()!=null && !"".equals(entry.getValue()) && map.containsKey(entry.getKey()) && !"sign".equals(entry.getKey())){
			  originStr.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		  }
		}
		String signStr = originStr.substring(0, originStr.length()-1).toString();
		signStr = signStr+mchntKey;
		System.out.println("signStr:"+signStr);
		String sign = DigestUtils.md5Hex(signStr.getBytes("utf-8"));
		//String sign3 = MD5Util.convert(signStr).toLowerCase();
		System.out.println("sign:"+sign);
		return sign;
	}	
	
	
		/**
		 * 签名Key值升序
		 * @param map
		 * @param mchntKey
		 * @return
		 * @throws UnsupportedEncodingException
		 */
		public static String doEncryptAsc(Map<String, Object> map,String mchntKey) throws UnsupportedEncodingException {
			Object[] keys =  map.keySet().toArray();
			Arrays.sort(keys);
			StringBuilder originStr = new StringBuilder();
			for(Object key:keys){
				if(map.get(key)!=null&&map.containsKey(key)&&!"sign".equals(key))
					originStr.append(key).append("=").append(map.get(key)).append("&");
			}
			String signStr = originStr.substring(0, originStr.length()-1).toString();
			signStr = signStr+mchntKey;
			String sign = DigestUtils.md5Hex(signStr.getBytes("utf-8"));
			return sign;
		}
}
