package com.framework.core.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.framework.core.Constants;
import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.ExceptionHandler;
import com.framework.core.exception.SysErrCode;
import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * 
 * @author xgf
 * 
 */
public class SeqUtil {

	public static String POSTGRESQL_SEQ = "select nextval( ? )";

	public static String ORACLE_SEQ = "select seq_name.nextval from dual";

	public static String dbType;
	
	/**
	 * 得到
	 * 
	 * @param seqName
	 * @return
	 * @throws BaseAppException
	 */
	public static Long getTableSequence(String seqName) throws BaseAppException {
		ComboPooledDataSource dataSource = (ComboPooledDataSource)AppContextUtil.getBean("dataSource");  
		Connection conn = DataSourceUtils.getConnection(dataSource); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		long start = 0l;
		long end = 0l;
		String sql = "";
		try {
			Long seq = null;
			switch (dbType) {
			case Constants.DB_TYPE_ORACLE:
				sql = ORACLE_SEQ.replace("seq_name", seqName);
				ps = conn.prepareStatement(sql);
				break;
			case Constants.DB_TYPE_POSTGRES:
				sql = POSTGRESQL_SEQ;
				ps = conn.prepareStatement(sql);
				ps.setString(1, seqName);
				break;
			default:
				sql = POSTGRESQL_SEQ;
				ps = conn.prepareStatement(sql);
				ps.setString(1, seqName);
				break;
			}
			start = System.currentTimeMillis();
			rs = ps.executeQuery();
			end = System.currentTimeMillis();
			if (rs.next()) {
				seq = rs.getLong(1);
			}
			
			return seq;
		} catch (SQLException ex) {
			throw ExceptionHandler.publish(SysErrCode.DB_OPER_EXPCEPTION, ex);
		} finally {
			if(null != rs){
				try {rs.close();} catch (SQLException e) {}
			}
			if(null != ps){
				try {ps.close();} catch (SQLException e) {}
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
			SqlLogUtil.logSql(sql.replace("?", seqName), end- start);
		}
	}
}
