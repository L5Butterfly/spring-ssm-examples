/*
 * @ClassName IRegionDao
 * @Description 
 * @version 1.0
 * @Date 2017-03-16 09:56:27
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.base.cache.item.RegionCacheVo;
import com.framework.web.base.vo.RegionVo;
import com.framework.web.po.RegionPo;

public interface IRegionDao {
    /**
     * @Title deleteByPrimaryKey
     * @param regionId
     * @return int
     */
    int deleteByPrimaryKey(Long regionId);

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(RegionPo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(RegionPo record);

    /**
     * @Title selectByPrimaryKey
     * @param regionId
     * @return Region
     */
    RegionPo selectByPrimaryKey(Long regionId);

    /**
     * @Title updateByPrimaryKeySelective
     * @param record
     * @return int
     */
    int updateByPrimaryKeySelective(RegionPo record);

    /**
     * @Title updateByPrimaryKey
     * @param record
     * @return int
     */
    int updateByPrimaryKey(RegionPo record);
    
    
    
    /**
     * selectRegionList
     * @param regionVo
     * @return
     */
    ArrayList<RegionVo> selectRegionList (RegionVo selectRegionList);
    
    ArrayList<RegionPo> selectSubRegionList(Long parentId);
    
    /**
     * queryRegionByRegionId
     */
    ArrayList<RegionVo> queryRegionByRegionId(Long regionId);
    
    /**
     * queryRegionListByPathName
     * @param regionVo
     * @return
     */
    ArrayList<RegionVo> queryRegionListByPathName(RegionPo regionPo);
    
    
    /**
   	 * 查询缓存使用，方法名固定为getCacheList
   	 * @return
   	 */
   	ArrayList<RegionCacheVo> getCacheList();
    
}