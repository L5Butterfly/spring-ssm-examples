package com.framework.web.base.cache.item;


import java.util.ArrayList;

import com.framework.core.cache.BaseCache;
import com.framework.core.utils.Util;
import com.framework.web.po.RegionPo;
import com.google.inject.Singleton;

@Singleton
public class RegionCache extends BaseCache<RegionCacheVo> {

	public RegionCache(String cacheName,String condtion, Class<RegionCacheVo> clazzCD) {
		super(cacheName, condtion, clazzCD);
	}

	public static RegionCache INSTANCE;

	static {
		INSTANCE = new RegionCache("Region", "IRegionDao", RegionCacheVo.class);
	}


	public String getRegionName(Long regionId){
		RegionPo regionPo = this.getItem(regionId);
		if(Util.isEmpty(regionPo)){
			return null;
		}
		return regionPo.getRegionName();
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public Long getRegionIdByName(String name){
		ArrayList<RegionCacheVo> list = this.getAll();
		
		for(RegionCacheVo cacheVo : list){
			if(cacheVo.getRegionName().equals(name)){
				return cacheVo.getRegionId();
			}
		}
		
		return null;
	}
}
