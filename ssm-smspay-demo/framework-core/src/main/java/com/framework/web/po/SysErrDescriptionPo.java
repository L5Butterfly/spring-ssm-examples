package com.framework.web.po;
import java.io.Serializable;
public class SysErrDescriptionPo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    private String errDescription;
    /**
    编码形式<br />W3A-00-0000<br />系统名-模块（子系统编码）-错误码<br /><br /><br />00--系统级异常
    */
    private String errCode;
    /**
    */
    private String langType;
    public String getErrDescription(){
        return this.errDescription;
    }
    public void setErrDescription(String errDescription){
        this.errDescription=errDescription;
    }
    public String getErrCode(){
        return this.errCode;
    }
    public void setErrCode(String errCode){
        this.errCode=errCode;
    }
    public String getLangType(){
        return this.langType;
    }
    public void setLangType(String langType){
        this.langType=langType;
    }
}
