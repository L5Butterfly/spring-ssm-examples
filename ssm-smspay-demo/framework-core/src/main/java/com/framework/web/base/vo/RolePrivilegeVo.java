package com.framework.web.base.vo;

import com.framework.web.po.RolePrivilegePo;


public class RolePrivilegeVo extends RolePrivilegePo {

    private static final long serialVersionUID = 1L;

    private String roleName;
    
    private String status;
    
    private String privilegeIds;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrivilegeIds() {
		return privilegeIds;
	}

	public void setPrivilegeIds(String privilegeIds) {
		this.privilegeIds = privilegeIds;
	}
    
}
