/**
 * 
 */
package com.framework.web.base.vo;

import com.framework.web.base.cache.item.RegionCache;

/**
 * @author DS
 *
 */
public class MobileInfoVo {

	private String mobile;
	
	private String provinceName;
	
	private Long regionId;
	
	private String operatorName;
	
	private String operator;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
		this.regionId = RegionCache.INSTANCE.getRegionIdByName(provinceName);
	}

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	
}
