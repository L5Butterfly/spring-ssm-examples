/*
 * @ClassName ISysParamDao
 * @Description 
 * @version 1.0
 * @Date 2017-03-08 10:05:15
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.base.cache.vo.SysParamCacheVo;
import com.framework.web.base.vo.SysParamVo;
import com.framework.web.po.SysParamPo;

public interface ISysParamDao {

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(SysParamPo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(SysParamPo record);

    /**
     * @Title updateByPrimaryKeySelective
     * @param record
     * @return int
     */
    int updateByPrimaryKeySelective(SysParamPo record);

    /**
     * @Title updateByPrimaryKey
     * @param record
     * @return int
     */
    int updateByPrimaryKey(SysParamPo record);
    
    
    /**
     * 分页查询
     * @param sysParamVo
     * @return
     */
    ArrayList<SysParamVo>getParamListByPage(SysParamVo sysParamVo);

    /**
     * 根据paramCode和vnoId进行查询
     * @param paramCode
     * @param vnoId
     * @return SysParamPo
     */
    SysParamPo findByCodeAndVnoId(String paramCode, Long vnoId);
    
    /**
	 * 查询缓存使用，方法名固定为getCacheList
	 * @return
	 */
	ArrayList<SysParamCacheVo> getCacheList();
}