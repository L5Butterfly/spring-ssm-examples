package com.framework.web.base.cache.vo;

import com.framework.core.Constants;
import com.framework.core.cache.ICacheItem;
import com.framework.web.po.DataDictionaryValuePo;

public class DataDictionaryValueCacheVo extends DataDictionaryValuePo implements ICacheItem{

	private static final long serialVersionUID = 1L;
    @Override
    public String key() {
        return super.getDicValueCode() + Constants.SPLIT_UNDERLINE  + super.getLangType();
    }

}
