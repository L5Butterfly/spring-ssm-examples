package com.framework.web.base.dao;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.framework.core.exception.BaseAppException;
import com.framework.web.po.MenuPo;


public interface IMenuDao {
    ArrayList<MenuPo> selectAllEff() throws BaseAppException;

    ArrayList<MenuPo> selectByStaffAndRole(@Param("staffId") Long staffId,@Param("roleId") Long roleId) throws BaseAppException;
}
