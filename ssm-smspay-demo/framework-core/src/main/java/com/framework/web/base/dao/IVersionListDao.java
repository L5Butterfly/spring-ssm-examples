/*
 * @ClassName IVersionListDao
 * @Description 
 * @version 1.0
 * @Date 2017-03-07 10:05:37
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.po.VersionListPo;

public interface IVersionListDao {
    /**
     * @Title deleteByPrimaryKey
     * @param versionCode
     * @return int
     */
    int deleteByPrimaryKey(String versionCode);

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(VersionListPo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(VersionListPo record);

    /**
     * @Title selectByPrimaryKey
     * @param versionCode
     * @return VersionList
     */
    VersionListPo selectByPrimaryKey(String versionCode);

    /**
     * @Title updateByPrimaryKeySelective
     * @param record
     * @return int
     */
    int updateByPrimaryKeySelective(VersionListPo record);

    /**
     * @Title updateByPrimaryKey
     * @param record
     * @return int
     */
    int updateByPrimaryKey(VersionListPo record);

    /**
     * @return ArrayList
     */
    ArrayList<VersionListPo> findAllOrderByTime();
    
    /**
     * @return ArrayList
     */
    VersionListPo findAllOrderByTimeAndType(String type);
    
}