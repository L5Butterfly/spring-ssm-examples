package com.framework.web.base.cache.vo;

import java.io.Serializable;
import java.util.ArrayList;

import com.framework.core.Constants;
import com.framework.core.cache.ICacheItem;

public class DataDictionaryCacheVo implements Serializable, ICacheItem {
	private static final long serialVersionUID = 1L;
	private ArrayList<DataDictionaryValueCacheVo> DataDictionaryValueAl = new ArrayList<DataDictionaryValueCacheVo>();
    private String dicTypeCode;
    private String langType;
    private String dicDesc;

    public String getDicTypeCode() {
		return dicTypeCode;
	}

	public void setDicTypeCode(String dicTypeCode) {
		this.dicTypeCode = dicTypeCode;
	}

	public String getLangType() {
        return this.langType;
    }

    public void setLangType(String langType) {
        this.langType = langType;
    }

    public String getDicDesc() {
        return this.dicDesc;
    }

    public void setDicDesc(String dicDesc) {
        this.dicDesc = dicDesc;
    }

    @Override
    public String key() {
        return dicTypeCode + Constants.SPLIT_UNDERLINE  + langType;
    }

    public ArrayList<DataDictionaryValueCacheVo> getDataDictionaryValueAl() {
        return DataDictionaryValueAl;
    }

    public void setDataDictionaryValueAl(ArrayList<DataDictionaryValueCacheVo> dataDictionaryValueAl) {
        DataDictionaryValueAl = dataDictionaryValueAl;
    }
}
