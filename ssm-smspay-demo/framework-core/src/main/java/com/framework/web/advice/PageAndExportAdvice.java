/**
 * 
 */
package com.framework.web.advice;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.framework.core.export.CsvExport;
import com.framework.core.export.ExcelExport;
import com.framework.core.page.Page;
import com.framework.core.session.UserSessionInfo;

/**
 * @author Administrator
 *
 */
@ControllerAdvice
public class PageAndExportAdvice implements  ResponseBodyAdvice<Object>{

	@Override
	public Object beforeBodyWrite(Object obj, MethodParameter arg1,
			MediaType arg2, Class<? extends HttpMessageConverter<?>> arg3,
			ServerHttpRequest arg4, ServerHttpResponse arg5) {
		
		if(obj instanceof Page){
			//获取页面传入参数封装的信息
			Page inPage = Page.threadLocal.get();
			Page page = (Page) obj;
			//已经设置过结果，保留结果
			if(page.getRecords() < 0){
				page.setRecords(inPage.getRecords());	
			}
				
			String url = arg4.getURI().toString();
			//导出
			if(url.indexOf("export=1") != -1){
				if(url.indexOf("filetype=excel") != -1){
					new ExcelExport().export4Jq(UserSessionInfo.threadLocal.get().getReq(), UserSessionInfo.threadLocal.get().getRes(), page);
				}else{
					new CsvExport().export4Jq(UserSessionInfo.threadLocal.get().getReq(), UserSessionInfo.threadLocal.get().getRes(), page);
				}
			}
		}
		
		return obj;
	}

	@Override
	public boolean supports(MethodParameter arg0,
			Class<? extends HttpMessageConverter<?>> arg1) {
		return true;
	}

}
