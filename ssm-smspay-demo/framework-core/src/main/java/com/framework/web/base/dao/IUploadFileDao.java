/*
 * @ClassName IUploadFileDao
 * @Description 
 * @version 1.0
 * @Date 2017-07-24 11:50:02
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.po.UploadFilePo;

public interface IUploadFileDao {
    /**
     * @Title deleteByPrimaryKey
     * @param id
     * @return int
     */
    int deleteByPrimaryKey(Long id);

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(UploadFilePo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(UploadFilePo record);

    /**
     * @Title selectByPrimaryKey
     * @param id
     * @return UploadFile
     */
    UploadFilePo selectByPrimaryKey(Long id);
    
    /**
     * selectColsByPrimaryKey
     * @param id
     * @return
     */
    UploadFilePo selectColsByPrimaryKey(Long id);
    
    ArrayList<UploadFilePo> selectColsinPrimaryKey(UploadFilePo record);

    /**
     * @Title updateByPrimaryKeySelective
     * @param record
     * @return int
     */
    int updateUploadFile(UploadFilePo record);

    ArrayList<UploadFilePo> queryFileListByPage(UploadFilePo filePo);
    
}