/**
 * 
 */
package com.framework.web.filter;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import com.framework.core.Constants;
import com.framework.core.utils.Util;
import com.framework.web.WebConstants;


/**
 * @author Administrator
 *
 */
public class SessionFilter extends OncePerRequestFilter{
	private static final Logger logger = Logger.getLogger(SessionFilter.class);

	private static Set<String> uncheckUrlSet = new HashSet<String>();
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filter)
			throws ServletException, IOException {
		String uri = request.getRequestURI();
		if(!isPassUrl(uri)){
			//从session中获取登录者实体
            Object staff = request.getSession().getAttribute(WebConstants.STAFF_INFO);
            Object user = request.getSession().getAttribute(WebConstants.USER_INFO_BEAN);
            if(Util.isEmpty(user) && Util.isEmpty(staff)){
            	boolean isMobile = Util.isMobile(request.getHeader("User-Agent"));
            	if(isMobile){
            		response.sendRedirect("/m_login.do");
            	}else{
            		response.sendRedirect("/login.do");
            	}
            }else{
            	filter.doFilter(request, response);
            }
		}else{
			filter.doFilter(request, response);
		}
	}
	
	/**
	 * 判断是否是非过滤的URL
	 * @param url
	 * @return
	 */
	private boolean isPassUrl(String url){
		if(Util.isEmpty(uncheckUrlSet)){
			return true;
		}
		
		for(String u : uncheckUrlSet){
			if(url.indexOf(u) != -1){
				return true;
			}
		}
		
		return false;
	}

	@Override
	protected void initFilterBean() throws ServletException {
		super.initFilterBean();
		FilterConfig cfg = getFilterConfig();
		String filePath = Constants.CONFIG_DIR + cfg.getInitParameter("uncheck-file");
		
		try {
			Properties prop = new Properties();
			prop.load(new BufferedInputStream(new FileInputStream(filePath)));
			
			Set<Object> keySet = prop.keySet();
			for(Object key : keySet){
				uncheckUrlSet.add(StringUtils.trim((String) key));
			}
		} catch (Exception e) {
			logger.error("sessionfilter loaf resouce failed."+e.getMessage(),  e);
		}
	}
}
