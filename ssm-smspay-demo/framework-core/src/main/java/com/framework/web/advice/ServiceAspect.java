/**
 * 
 */
package com.framework.web.advice;

import java.util.HashMap;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.SysErrCode;
import com.framework.core.log.IArgVo;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.DateUtil;
import com.framework.core.utils.JobUtil;
import com.framework.core.utils.Util;
import com.framework.web.base.vo.OperationLogVo;
import com.framework.web.operatelog.OperationLogJob;

/**
 * @author Administrator
 *
 */
@Aspect
public class ServiceAspect {

	private HashMap<String, String> svcMap;

	private boolean enableLog;

	private OperationLogVo serviceLog;

	@Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void pointcut() {
	}

	@Before("pointcut()")
	public void before(JoinPoint point) {
	}

	@Around("pointcut()")
	public Object proceed(ProceedingJoinPoint point) throws Throwable {
		if (isRecordLog(point)) {
			serviceLog = new OperationLogVo();
			UserSessionInfo userSessionInfo = UserSessionInfo.threadLocal.get();
			serviceLog.setSourIp(userSessionInfo.getAccessIp());
			serviceLog.setHttpClient(userSessionInfo.getClientInfo());
			serviceLog.setUserId(userSessionInfo.getStaffId());
		}

		// 方法返回值
		Object retVal = null;
		long begin = System.currentTimeMillis();
		long end = 0l;
		String expCode = null;
		String operResult = "T";
		try {
			retVal = point.proceed();
			end = System.currentTimeMillis();
		} catch (Exception e) {
			end = System.currentTimeMillis();
			operResult = "F";
			expCode = e instanceof BaseAppException ? ((BaseAppException) e).getCode() : SysErrCode.UNKNOW_EXPCEPTION;
			throw e;
		} finally {
			if (isRecordLog(point)) {
				serviceLog.setCostTime(end - begin);
				serviceLog.setExpCode(expCode);
				saveOperationLog(point, operResult);
			}
		}
		return retVal;
	}

	@AfterReturning(pointcut = "pointcut()", returning = "res")
	public void afterReturning(JoinPoint point, Object res) {
	} 

	@AfterThrowing(pointcut = "pointcut()", throwing = "ex")
	public void afterThrowing(JoinPoint point, Exception ex) {
	}

	public void setSvcMap(HashMap<String, String> svcMap) {
		this.svcMap = svcMap;
		if (Util.notEmpty(svcMap)) {
			enableLog = true;
		}
	}

	/**
	 * 记录操作日志
	 * 
	 * @param methodName
	 * @param params
	 * @param operResult
	 * @param expCode
	 */
	private void saveOperationLog(JoinPoint point, String operResult) {
		if (Util.isEmpty(serviceLog)) {
			return;
		}
		Object[] params = point.getArgs();
		String methodName = point.getSignature().getDeclaringTypeName() + "."
				+ point.getSignature().getName();
		StringBuilder param = new StringBuilder();
		for (Object obj : params) {
			if (obj instanceof IArgVo) {// 只记录次类型对象
				if (param.length() > 0) {
					param.append("||");
				}
				param.append(((IArgVo) obj).getOperateArgs());
			}
		}

		serviceLog.setOperResult(operResult);
		serviceLog.setParam(param.toString());
		serviceLog.setSvcName(svcMap.get(methodName));
		serviceLog.setOperTime(DateUtil.getNowDate());

		if(Util.isEmpty(serviceLog.getUserId())){
			serviceLog.setUserId(UserSessionInfo.threadLocal.get().getStaffId());
		}
		JobUtil.addSystemWorkJob(new OperationLogJob(serviceLog));
	}
	
	/**
	 * 判断是否需要记录操作日志
	 * 
	 * @param point
	 * @return
	 */
	private boolean isRecordLog(JoinPoint point) {
		String methodName = point.getSignature().getDeclaringTypeName() + "."
				+ point.getSignature().getName();
		if (enableLog && svcMap.containsKey(methodName)) {
			return true;
		}

		return false;
	}
}
