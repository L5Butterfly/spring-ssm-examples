package com.framework.web.base.cache.item;

import com.framework.core.cache.ICacheItem;
import com.framework.web.po.RegionPo;
import com.google.inject.Singleton;

@Singleton
public class RegionCacheVo extends RegionPo implements ICacheItem {

	@Override
	public String key() {
		return Long.toString(super.getRegionId());
	}
}
