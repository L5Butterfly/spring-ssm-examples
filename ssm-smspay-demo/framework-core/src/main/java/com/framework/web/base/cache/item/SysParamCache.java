package com.framework.web.base.cache.item;

import com.framework.core.cache.BaseCache;
import com.framework.web.base.cache.vo.SysParamCacheVo;
import com.google.inject.Singleton;

/**
 * 系统参数缓存
 *
 * @author xgf
 *
 */
@Singleton
public class SysParamCache extends BaseCache<SysParamCacheVo> {

	/** INSTANCE */
	public static SysParamCache INSTANCE;

	static {
		INSTANCE = new SysParamCache("SysParam", "ISysParamDao",SysParamCacheVo.class);
	}

	private SysParamCache(String cacheName, String daoName, Class<SysParamCacheVo> clazzCD) {
		super(cacheName, daoName, clazzCD);
	}

}
