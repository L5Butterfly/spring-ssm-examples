/**
 * 
 */
package com.framework.web.base.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.po.SmsSendLogPo;

/**
 * @author Administrator
 *
 */
public class SmsSendVo extends SmsSendLogPo {

	@DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date qryDate;
	
	private Date beginDate;

	private Date endDate;

	private String gdate;

	private Long cnt;
	private String _sucFlag;

	@Override
	public void setSucFlag(String sucFlag) {
		super.setSucFlag(sucFlag);
		this._sucFlag = DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_SMS_SEND_SUCFLAG", sucFlag);
	}

	public String get_sucFlag() {
		return _sucFlag;
	}

	public void set_sucFlag(String _sucFlag) {
		this._sucFlag = _sucFlag;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getGdate() {
		return gdate;
	}

	public void setGdate(String gdate) {
		this.gdate = gdate;
	}

	public Long getCnt() {
		return cnt;
	}

	public void setCnt(Long cnt) {
		this.cnt = cnt;
	}

	public Date getQryDate() {
		return qryDate;
	}

	public void setQryDate(Date qryDate) {
		this.qryDate = qryDate;
	}

}
