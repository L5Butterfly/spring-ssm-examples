package com.framework.web.po;
import java.io.Serializable;
public class MenuPo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    private Long menuId;
    /**
    M00 专业版管理员菜单<br />M01 大众版菜单
    */
    private String menuType;
    /**
    = parent_menu_id + menu_id
    */
    private String menuCode;
    /**
    00A 有效<br />00X 失效<br />00U 归档
    */
    private String status;
    /**
    */
    private Long showOrder;
    /**
    */
    private String menuDesc;
    /**
    */
    private String menuName;
    /**
    用于扩展菜单页面详细的业务控制<br />如只读权限等
    */
    private String property;
    /**
    */
    private Long parentMenuId;
    /**
    */
    private String url;
    /**
    权限标识为空，则表示不作权限控制
    */
    private Long privilegeId;
    public Long getMenuId(){
        return this.menuId;
    }
    public void setMenuId(Long menuId){
        this.menuId=menuId;
    }
    public String getMenuType(){
        return this.menuType;
    }
    public void setMenuType(String menuType){
        this.menuType=menuType;
    }
    public String getMenuCode(){
        return this.menuCode;
    }
    public void setMenuCode(String menuCode){
        this.menuCode=menuCode;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status=status;
    }
    public Long getShowOrder(){
        return this.showOrder;
    }
    public void setShowOrder(Long showOrder){
        this.showOrder=showOrder;
    }
    public String getMenuDesc(){
        return this.menuDesc;
    }
    public void setMenuDesc(String menuDesc){
        this.menuDesc=menuDesc;
    }
    public String getMenuName(){
        return this.menuName;
    }
    public void setMenuName(String menuName){
        this.menuName=menuName;
    }
    public String getProperty(){
        return this.property;
    }
    public void setProperty(String property){
        this.property=property;
    }
    public Long getParentMenuId(){
        return this.parentMenuId;
    }
    public void setParentMenuId(Long parentMenuId){
        this.parentMenuId=parentMenuId;
    }
    public String getUrl(){
        return this.url;
    }
    public void setUrl(String url){
        this.url=url;
    }
    public Long getPrivilegeId(){
        return this.privilegeId;
    }
    public void setPrivilegeId(Long privilegeId){
        this.privilegeId=privilegeId;
    }
}
