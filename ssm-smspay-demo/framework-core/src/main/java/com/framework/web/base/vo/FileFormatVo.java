package com.framework.web.base.vo;

/**
 * 文件格式
 * 
 * @author xgf
 * 
 */
public class FileFormatVo {
	/** 文件中字段的名称，逗号分割 */
	private String fieldName;
	/** 文件中字段的数据类型，逗号分割，现在支持三种类型String,Long,Date */
	private String fieldType;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

}
