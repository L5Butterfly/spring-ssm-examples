/*
 * @ClassName SysParamPo
 * @Description 
 * @version 1.0
 * @Date 2017-03-08 10:05:15
 */
package com.framework.web.po;

public class SysParamPo{
    /**
     * @Fields paramCode 参数的编码
建议
PARAM_TYPE_CODE + xxx
     */
    private String paramCode;
    /**
     * @Fields vnoId null
     */
    private Long vnoId;
    /**
     * @Fields dicTypeCdoe 类型分组
如
性别类型
省份类型等
     */
    private String dicTypeCode;
    /**
     * @Fields paramTypeCode 按模块子系统分组
     */
    private String paramTypeCode;
    /**
     * @Fields valueType A01 数值型
B01 字符串类型
C01 布尔型（T，F）
D01 时间型（日期）
D02 时间型（时间）
D03 时间型（时间+日期）
     */
    private String valueType;
    /**
     * @Fields paramValue 参数的数据类型
     */
    private String paramValue;
    /**
     * @Fields paramName null
     */
    private String paramName;
    /**
     * @Fields paramDesc null
     */
    private String paramDesc;
    /**
     * @Fields status 00A 有效
00X 失效
00U 归档
     */
    private String status;

    public String getDicTypeCode() {
		return dicTypeCode;
	}

	public void setDicTypeCode(String dicTypeCode) {
		this.dicTypeCode = dicTypeCode;
	}

	public String getParamTypeCode() {
        return paramTypeCode;
    }

    public void setParamTypeCode(String paramTypeCode) {
        this.paramTypeCode = paramTypeCode == null ? null : paramTypeCode.trim();
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType == null ? null : valueType.trim();
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue == null ? null : paramValue.trim();
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName == null ? null : paramName.trim();
    }

    public String getParamDesc() {
        return paramDesc;
    }

    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc == null ? null : paramDesc.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode == null ? null : paramCode.trim();
    }

    public Long getVnoId() {
        return vnoId;
    }

    public void setVnoId(Long vnoId) {
        this.vnoId = vnoId;
    }
}