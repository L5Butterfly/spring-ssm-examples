package com.framework.web.operatelog;

import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.framework.core.utils.DateUtil;
import com.framework.core.utils.Util;
import com.framework.web.base.dao.IOperationLogDao;
import com.framework.web.base.vo.OperationLogVo;

public class OperationLogJob implements Runnable {

	private static Logger logger = Logger.getLogger(OperationLogJob.class);

	private OperationLogVo logVo;

	public OperationLogJob(OperationLogVo logVo) {
		this.logVo = logVo;
	}

	@Override
	/**
	 * 将log写表
	 */
	public void run() {
		if (logVo != null) {
			String svcName = logVo.getSvcName();
			if (Util.isEmpty(svcName)) {
				return;
			}
			try {
				logVo.setCreateTime(DateUtil.getNowDate());
				WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();  
				IOperationLogDao logDao = (IOperationLogDao) wac.getBean("IOperationLogDao");
				logDao.saveOperationLog(logVo);
			} catch (Exception e) {
				logger.error("OperationLogDo record log err");
				logger.error(e.getMessage(), e);
			}
		}
	}


}
