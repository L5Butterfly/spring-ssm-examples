package com.framework.web.po;
import java.io.Serializable;
public class DataDictionaryPo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    类型分组<br />如<br />性别类型<br />省份类型等
    */
    private String dicTypeCdoe;
    /**
    */
    private String dicDesc;
    /**
    */
    private String langType;
    public String getDicTypeCdoe(){
        return this.dicTypeCdoe;
    }
    public void setDicTypeCdoe(String dicTypeCdoe){
        this.dicTypeCdoe=dicTypeCdoe;
    }
    public String getDicDesc(){
        return this.dicDesc;
    }
    public void setDicDesc(String dicDesc){
        this.dicDesc=dicDesc;
    }
    public String getLangType(){
        return this.langType;
    }
    public void setLangType(String langType){
        this.langType=langType;
    }
}
