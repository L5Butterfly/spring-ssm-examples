package com.framework.web.base.cache.item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.framework.core.cache.BaseCache;
import com.framework.core.utils.BeanUtil;
import com.framework.core.utils.Util;
import com.framework.web.base.cache.vo.VnoListCacheVo;
import com.framework.web.po.VnoListPo;
import com.google.inject.Singleton;

/**
 * SysErrDescriptionCache
 *
 * @author xgf
 *
 */
@Singleton
public class VnoListCache extends BaseCache <VnoListCacheVo>{

	/** INSTANCE */
	public static VnoListCache INSTANCE;
	
    public final static String VNO_PATH_SPLIT = "-";

	static {
		INSTANCE = new VnoListCache("VnoList", "IVnoListDao", VnoListCacheVo.class);
	}

    public String getVnoNameById(Long vnoId) {
        VnoListPo vnoListPo = INSTANCE.getItem(vnoId);
        return Util.notEmpty(vnoListPo) ? vnoListPo.getVnoName() : null;
    }
    
    public Set<Long> getChildrenVnoIds(Long parentVnoId){
    	//最多应该不会超过10层
    	Set<Long> resList = new HashSet<Long>();
    	String vString = "-"+parentVnoId+"-";
    	for(VnoListPo vo : INSTANCE.getAll()){
    		if(vo.getVnoIdPath().contains(vString)){
     			resList.add(vo.getVnoId().longValue());
    		}
    	}
    	return resList;
    }
	public String getVnoCodeById(Long vnoID) {
		VnoListPo vnoListPo = INSTANCE.getItem(vnoID);
		return Util.notEmpty(vnoListPo) ? vnoListPo.getVnoCode() : null;
	}
    public String getVnoPathById(Long vnoID) {
        VnoListPo vnoListPo = INSTANCE.getItem(vnoID);
        return Util.notEmpty(vnoListPo) ? vnoListPo.getVnoIdPath() : null;
    }
    
    /**
     * 例如: vnoPath  _0_12_123_1233_34_232_<br />
     * 起始vnoId 12, 结束vnoId 34 返回结果 12,123,1233,34<br />
     * beginVnoId 和 endVnoId 可以颠倒
     * @param beginVnoId 起始vnoId
     * @param endVnoId 结束vnoId
     * */
    public ArrayList<VnoListPo> getVnoList(Long beginVnoId,Long endVnoId) {
        String beginVnoPath = INSTANCE.getItem(beginVnoId).getVnoIdPath();
        String endVnoPath = INSTANCE.getItem(endVnoId).getVnoIdPath();
        String vnoPath;
        if(endVnoPath.length() > beginVnoPath.length()) {
        	vnoPath = endVnoPath.replace(beginVnoPath, "");
        }else {
        	vnoPath = beginVnoPath.replace(endVnoPath, "");
        }
        HashSet<Long> vnoids = new HashSet<>();
        vnoids.add(beginVnoId);
        vnoids.add(endVnoId);
        String[] strings = vnoPath.split(VNO_PATH_SPLIT);
        for(String s : strings) {
        	try { vnoids.add(Long.valueOf(s)); }catch(Exception e) {}
        }
        ArrayList<VnoListPo> res = new ArrayList<VnoListPo>();
        for(Long ss:vnoids) {
        	VnoListPo vnoListPo = INSTANCE.getItem(ss);
        	if(vnoListPo != null) {
        		res.add(vnoListPo);
        	}
        }
        return res;
    }
    
    public ArrayList<VnoListPo> getParentVno(Long vnoID) {
        VnoListPo vnoListPo = INSTANCE.getItem(vnoID);
        ArrayList<VnoListPo> res = new ArrayList<VnoListPo>();
        if(vnoListPo == null) {
        	return res;
        }
        String  vnoPath = vnoListPo.getVnoIdPath();
        for(String string : vnoPath.split(VNO_PATH_SPLIT)) {
        	VnoListPo po = INSTANCE.getItem(string);
        	if(po != null) {
        		res.add(po);
        	}
        }
        return res;
    }
    

	@Override
	public void delItem(String key) {
		// 先是默认按表主键更新的
		super.delItem(key);
	}

	@Override
	public <E extends VnoListCacheVo> void setItem(E iCacheDateItem) {
		VnoListCacheVo vnoListCachePo = new VnoListCacheVo();
		BeanUtil.copyProperties(iCacheDateItem, vnoListCachePo);
		// 先是默认按表主键更新的
		super.setItem(vnoListCachePo);

	}

	public VnoListCache(String cacheName,String daoName, Class<VnoListCacheVo> clazzCD) {
		super(cacheName, daoName, clazzCD);
	}

}
