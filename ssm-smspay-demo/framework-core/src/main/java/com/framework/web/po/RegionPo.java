/*
 * @ClassName Region
 * @Description 
 * @version 1.0
 * @Date 2017-03-16 09:56:27
 */
package com.framework.web.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class RegionPo {
    /**
     * @Fields regionId null
     */
    private Long regionId;
    /**
     * @Fields parentRegionId null
     */
    private Long parentRegionId;
    /**
     * @Fields regionLevel 省                  10A
市                  10B
区（县）       10C
商圈               10D
街道（商场） 10E
     */
    private String regionLevel;
    /**
     * @Fields regionCode 父子关系用于获取完整的地域序列
如
00_00_00_00_00
     */
    private String regionCode;
    /**
     * @Fields regionName null
     */
    private String regionName;
    /**
     * @Fields status 00A 有效
00X 失效
00U 归档
     */
    private String status;
    /**
     * @Fields createDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
     * @Fields statusDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    /**
     * @Fields regionPath null
     */
    private String regionPath;

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getParentRegionId() {
        return parentRegionId;
    }

    public void setParentRegionId(Long parentRegionId) {
        this.parentRegionId = parentRegionId;
    }

    public String getRegionLevel() {
        return regionLevel;
    }

    public void setRegionLevel(String regionLevel) {
        this.regionLevel = regionLevel;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getRegionPath() {
        return regionPath;
    }

    public void setRegionPath(String regionPath) {
        this.regionPath = regionPath;
    }
}