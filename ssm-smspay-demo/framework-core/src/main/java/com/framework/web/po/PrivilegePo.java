package com.framework.web.po;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
public class PrivilegePo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
    */
    private Long parentPrivId;
    /**
    */
    private String privilegeCode;
    /**
    */
    private Long privilegeId;
    /**
    */
    private String privilegeName;
    /**
    A01 操作权限<br />A02 数据权限<br />A03 菜单权限
    */
    private String privilegeType;
    /**
    00A 有效<br />00X 失效<br />00U 归档
    */
    private String status;
    /**
    */
    private Long showOrder;
    
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    
    private String privIdPath;
    
    public Date getCreateDate(){
        return this.createDate;
    }
    public void  setCreateDate(Date createDate){
        this.createDate=createDate;
    }
    public Long getParentPrivId(){
        return this.parentPrivId;
    }
    public void  setParentPrivId(Long parentPrivId){
        this.parentPrivId=parentPrivId;
    }
    public String getPrivilegeCode(){
        return this.privilegeCode;
    }
    public void  setPrivilegeCode(String privilegeCode){
        this.privilegeCode=privilegeCode;
    }
    public Long getPrivilegeId(){
        return this.privilegeId;
    }
    public void  setPrivilegeId(Long privilegeId){
        this.privilegeId=privilegeId;
    }
    public String getPrivilegeName(){
        return this.privilegeName;
    }
    public void  setPrivilegeName(String privilegeName){
        this.privilegeName=privilegeName;
    }
    public String getPrivilegeType(){
        return this.privilegeType;
    }
    public void  setPrivilegeType(String privilegeType){
        this.privilegeType=privilegeType;
    }
    public String getStatus(){
        return this.status;
    }
    public void  setStatus(String status){
        this.status=status;
    }
    public Date getStatusDate(){
        return this.statusDate;
    }
    public void  setStatusDate(Date statusDate){
        this.statusDate=statusDate;
    }
    
    public String getPrivIdPath() {
        return privIdPath;
    }
    
    public void setPrivIdPath(String privIdPath) {
        this.privIdPath = privIdPath;
    }
    
    public Long getShowOrder() {
        return showOrder;
    }
    
    public void setShowOrder(Long showOrder) {
        this.showOrder = showOrder;
    }
    
}
