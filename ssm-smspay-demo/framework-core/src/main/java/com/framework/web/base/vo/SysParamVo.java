package com.framework.web.base.vo;

import java.io.Serializable;

import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.base.cache.item.VnoListCache;
import com.framework.web.po.SysParamPo;

public class SysParamVo extends SysParamPo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String vnoName;
	private String _status;
	

	
	@Override
	public void setStatus(String status){
		super.setStatus(status);
		this._status = DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_STATUS", status);
	}
	public String get_status() {
		return _status;
	}

	public void set_status(String _status) {
		this._status = _status;
	}

	public String getVnoName() {
		return vnoName;
	}

	public void setVnoName(String vnoName) {
		this.vnoName = vnoName;
	}
	public void setVnoId(Long vnoId){
        super.setVnoId(vnoId);
        this.vnoName = VnoListCache.INSTANCE.getItem(Long.valueOf(vnoId)).getVnoName();
    }

	

}
