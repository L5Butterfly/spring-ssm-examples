/**
 * 
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.base.cache.vo.SysErrDescriptionCacheVo;

/**
 * @author Administrator
 *
 */
public interface ISysErrDescriptionDao {

	/**
	 * 查询缓存使用，方法名固定为getCacheList
	 * @return
	 */
	ArrayList<SysErrDescriptionCacheVo> getCacheList();
	
}
