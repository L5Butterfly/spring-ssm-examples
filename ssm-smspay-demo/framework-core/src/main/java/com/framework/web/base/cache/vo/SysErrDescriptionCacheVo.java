/**
 * 
 */
package com.framework.web.base.cache.vo;

import com.framework.core.cache.ICacheItem;
import com.framework.web.po.SysErrDescriptionPo;

/**
 * @author Administrator
 *
 */
public class SysErrDescriptionCacheVo extends SysErrDescriptionPo implements ICacheItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String key() {
		return this.getLangType() +"_"+super.getErrCode();
	}

}
