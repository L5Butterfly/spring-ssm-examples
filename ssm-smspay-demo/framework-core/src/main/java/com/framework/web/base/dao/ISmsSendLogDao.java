/*
 * @ClassName ISmsSendLogDao
 * @Description 
 * @version 1.0
 * @Date 2017-03-07 10:04:19
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.base.vo.SmsSendVo;
import com.framework.web.po.SmsSendLogPo;

public interface ISmsSendLogDao {

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(SmsSendLogPo record);

    /**
     * 查询号码时间段内发送的次数
     * @param po
     * @return
     */
   ArrayList<SmsSendLogPo> querySendTimes(SmsSendLogPo po);
   
   
   ArrayList<SmsSendVo> selectSendLogListByPage(SmsSendVo vo);
   
   ArrayList<SmsSendVo> selectGroupLogListByPage(SmsSendVo vo);
}