/**
 * 
 */
package com.framework.web;

import com.framework.core.utils.ParamUtil;

/**
 * @author Administrator
 *
 */
public interface WebConstants {

	//登陆用户session名称
	String STAFF_INFO = "STAFF_INFO_BEAN";
	
	//登陆用户信息
	String USER_INFO_BEAN = "USER_INFO_BEAN";
		
	//utf8编码
	String ENCODING_UTF8 = "UTF-8";
	
	 //管理界面copyright的厂商名称
    String COMPANY_NAME = ParamUtil.getParamString("POR_SERV_0002", "政务服务网");
    
    //网站备案号
    String ICP_PREPARED_NO = ParamUtil.getParamString("POR_SERV_0003", "粤ICP备15089625号");
    
	//项目启动正常，用于启动脚本监听
	String WEB_APP_START_OK = "-----------------Project start success-------------";
}
