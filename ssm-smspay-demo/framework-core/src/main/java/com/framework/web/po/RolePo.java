package com.framework.web.po;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
public class RolePo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    private String loginPage;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
    */
    private Long roleId;
    /**
    00A 有效<br />00X 失效<br />00U 归档
    */
    private String status;
    /**
    */
    private Long vnoId;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    /**
    */
    private String roleName;
    /**
    */
    private String vnoShowFlag;
    public String getLoginPage(){
        return this.loginPage;
    }
    public void setLoginPage(String loginPage){
        this.loginPage=loginPage;
    }
    public Date getCreateDate(){
        return this.createDate;
    }
    public void setCreateDate(Date createDate){
        this.createDate=createDate;
    }
    public Long getRoleId(){
        return this.roleId;
    }
    public void setRoleId(Long roleId){
        this.roleId=roleId;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status=status;
    }
    public Long getVnoId(){
        return this.vnoId;
    }
    public void setVnoId(Long vnoId){
        this.vnoId=vnoId;
    }
    public Date getStatusDate(){
        return this.statusDate;
    }
    public void setStatusDate(Date statusDate){
        this.statusDate=statusDate;
    }
    public String getRoleName(){
        return this.roleName;
    }
    public void setRoleName(String roleName){
        this.roleName=roleName;
    }
    public String getVnoShowFlag(){
        return this.vnoShowFlag;
    }
    public void setVnoShowFlag(String vnoShowFlag){
        this.vnoShowFlag=vnoShowFlag;
    }
}
