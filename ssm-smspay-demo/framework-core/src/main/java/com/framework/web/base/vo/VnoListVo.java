package com.framework.web.base.vo;

import java.io.Serializable;

import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.po.VnoListPo;

public class VnoListVo extends VnoListPo implements Serializable {
	private static final long serialVersionUID = -3479355174648887337L;
	private String regionName;
	private String regionPathName;
	private String parentRegionName;
	private String staffCode;
	private String parentVnoName;
	private Long leafNum;
	private String staffName;
	private String _status;

	@Override
	public void setStatus(String status) {
		super.setStatus(status);
		this._status = DataDictionaryCache.INSTANCE.getDataDictionaryValueName(
				"DIC_STATUS", status);
	}

	public String get_status() {
		return _status;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getParentVnoName() {
		return parentVnoName;
	}

	public void setParentVnoName(String parentVnoName) {
		this.parentVnoName = parentVnoName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getParentRegionName() {
		return parentRegionName;
	}

	public void setParentRegionName(String parentRegionName) {
		this.parentRegionName = parentRegionName;
	}

	public String getRegionPathName() {
		return regionPathName;
	}

	public void setRegionPathName(String regionPathName) {
		this.regionPathName = regionPathName;
	}

	public Long getLeafNum() {
		return leafNum;
	}

	public void setLeafNum(Long leafNum) {
		this.leafNum = leafNum;
	}

}
