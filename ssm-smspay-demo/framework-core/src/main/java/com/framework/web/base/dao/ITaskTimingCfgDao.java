/**
 * 
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.base.vo.TaskTimingCfgVo;
import com.framework.web.po.TaskTimingCfgPo;

/**
 * @author Administrator
 *
 */
public interface ITaskTimingCfgDao {

	/**
	 * 
	 * @return
	 */
	ArrayList<TaskTimingCfgPo> getTaskList(String taskType);
	
	ArrayList<TaskTimingCfgVo> qryTaskTimingInfo4JqByPage(TaskTimingCfgVo taskTimingCfgVo);
	
	int insertTask(TaskTimingCfgPo taskTimingCfgPo);
	
	int modifyTask(TaskTimingCfgPo taskTimingCfgPo);
	
	TaskTimingCfgPo queryTaskByName(String taskName);
	
}
