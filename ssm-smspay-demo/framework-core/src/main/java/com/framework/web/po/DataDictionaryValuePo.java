package com.framework.web.po;
import java.io.Serializable;
public class DataDictionaryValuePo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    类型分组<br />如<br />性别类型<br />省份类型等
    */
    private String dicTypeCode;
    /**
    */
    private String dicValueCode;
    /**
    */
    private String dataExt2;
    /**
    */
    private String langType;
    /**
    00A 有效<br />00X 失效<br />00U 归档
    */
    private String status;
    /**
    */
    private String dicValue;
    /**
    */
    private Long priority;
    /**
    */
    private String dataExt1;
    /**
    */
    private String dicValueName;
    
    private String dicDesc;
   
    public String getDicTypeCode() {
		return dicTypeCode;
	}
	public void setDicTypeCode(String dicTypeCode) {
		this.dicTypeCode = dicTypeCode;
	}
	public String getDicValueCode(){
        return this.dicValueCode;
    }
    public void setDicValueCode(String dicValueCode){
        this.dicValueCode=dicValueCode;
    }
    public String getDataExt2(){
        return this.dataExt2;
    }
    public void setDataExt2(String dataExt2){
        this.dataExt2=dataExt2;
    }
    public String getLangType(){
        return this.langType;
    }
    public void setLangType(String langType){
        this.langType=langType;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status=status;
    }
    public String getDicValue(){
        return this.dicValue;
    }
    public void setDicValue(String dicValue){
        this.dicValue=dicValue;
    }
    public Long getPriority(){
        return this.priority;
    }
    public void setPriority(Long priority){
        this.priority=priority;
    }
    public String getDataExt1(){
        return this.dataExt1;
    }
    public void setDataExt1(String dataExt1){
        this.dataExt1=dataExt1;
    }
    public String getDicValueName(){
        return this.dicValueName;
    }
    public void setDicValueName(String dicValueName){
        this.dicValueName=dicValueName;
    }
	public String getDicDesc() {
		return dicDesc;
	}
	public void setDicDesc(String dicDesc) {
		this.dicDesc = dicDesc;
	}
    
}
