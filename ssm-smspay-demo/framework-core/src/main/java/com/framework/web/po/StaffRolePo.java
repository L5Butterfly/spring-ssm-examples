package com.framework.web.po;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
public class StaffRolePo implements Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    private Long orgnizationId;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
    */
    private Long roleId;
    /**
    00A 有效<br />00X 失效<br />00U 归档
    */
    private String status;
    /**
    */
    private Long vnoId;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    /**
    */
    private Long staffId;
    public Long getOrgnizationId(){
        return this.orgnizationId;
    }
    public void setOrgnizationId(Long orgnizationId){
        this.orgnizationId=orgnizationId;
    }
    public Date getCreateDate(){
        return this.createDate;
    }
    public void setCreateDate(Date createDate){
        this.createDate=createDate;
    }
    public Long getRoleId(){
        return this.roleId;
    }
    public void setRoleId(Long roleId){
        this.roleId=roleId;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status=status;
    }
    public Long getVnoId(){
        return this.vnoId;
    }
    public void setVnoId(Long vnoId){
        this.vnoId=vnoId;
    }
    public Date getStatusDate(){
        return this.statusDate;
    }
    public void setStatusDate(Date statusDate){
        this.statusDate=statusDate;
    }
    public Long getStaffId(){
        return this.staffId;
    }
    public void setStaffId(Long staffId){
        this.staffId=staffId;
    }
}
