/**
 * 
 */
package com.framework.web.base.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.StaffRoleVo;
import com.framework.web.base.vo.StaffVo;
import com.framework.web.po.MenuPo;
import com.framework.web.po.PrivilegePo;
import com.framework.web.po.StaffPo;

/**
 * @author Administrator
 *
 */
public interface IStaffDao {

	/**
	 * 根据账号查询操作员信息
	 * @param code
	 * @return
	 */
	StaffPo selectStaffByCode(@Param("code") String code);
	
	/**
	 * 根据ID查询操作员信息
	 * @param id
	 * @return
	 */
	StaffPo selectStaffById(@Param("id") int id);
	
	/**
	 * 查询员工角色信息
	 * @param staffId
	 * @return
	 */
	ArrayList<StaffRoleVo> selectStaffRoleByStaffId(@Param("staffId") long staffId);
	
	/**
	 * 
	 * @param staffId
	 * @param roleId
	 * @return
	 */
	ArrayList<MenuPo> getStaffMenu(@Param("staffId") Long staffId,@Param("roleId") Long roleId);
	
	/**
	 * 
	 * @param staffId
	 * @param roleId
	 * @return
	 */
	ArrayList<PrivilegePo> getStaffPrivilege(@Param("staffId") Long staffId,@Param("roleId") Long roleId);
	
	/**
	 * 
	 * @return
	 */
	ArrayList<StaffVo> getStaffListByPage(StaffVo staffVo);
	
	/**
	 * 查询操作员的权限列表
	 * @param staffVo
	 * @return
	 */
	ArrayList<StaffRoleVo> selectStaffRoleList(StaffVo staffVo);
	
	/**
	 * 操作员重置密码
	 * @param staff
	 * @return
	 * @throws BaseAppException
	 */
	int resetStaffPwd(StaffVo staff) throws BaseAppException;
	
	/**
	 * 新增操作员角色信息
	 * @param staffRole
	 * @throws BaseAppException
	 */
	void insertStaffRole(StaffRoleVo staffRole) throws BaseAppException;
	
	/**
	 * 删除操作员角色信息
	 * @param staffId
	 * @return
	 * @throws BaseAppException
	 */
	int deleteStaffRoleByStaffId(Long staffId) throws BaseAppException;
	
	/**
	 * 新增操作员
	 * @param staff
	 * @return
	 * @throws BaseAppException
	 */
	long insertStaff(StaffVo staff) throws BaseAppException;
	
	/**
	 * 修改操作员
	 * @param staff
	 * @return
	 * @throws BaseAppException
	 */
	int modifyStaff(StaffVo staff) throws BaseAppException;
	
	/**
	 * 修改操作员密码
	 * @param staff
	 * @return
	 * @throws BaseAppException
	 */
	int updateStaffPwd(StaffVo staff) throws BaseAppException;
	
	ArrayList<String> getStaffListByVnoIdAndRoleId(Map<String, Object> params);
	
	ArrayList<StaffPo> selectStaffByStaffCodes(List<String> staffCodes);	
	
	/**
	 * 
	 * @param roleId
	 * @return
	 */
	ArrayList<StaffVo> selectStaffListByRoleId(Long roleId);
	
	/**
	 * 查询所有人员
	 * @param staffVo
	 * @return
	 * @throws BaseAppException
	 */
	ArrayList<StaffVo> selectStaffListByPage(StaffVo staffVo) throws BaseAppException;
}
