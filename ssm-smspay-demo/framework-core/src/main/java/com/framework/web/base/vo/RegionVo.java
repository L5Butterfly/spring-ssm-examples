package com.framework.web.base.vo;

import java.io.Serializable;

import com.framework.web.po.RegionPo;

public class RegionVo extends RegionPo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public Long getLeafNum() {
		return leafNum;
	}
	public void setLeafNum(Long leafNum) {
		this.leafNum = leafNum;
	}
	public String getParentRegionName() {
		return parentRegionName;
	}
	public void setParentRegionName(String parentRegionName) {
		this.parentRegionName = parentRegionName;
	}
	private Long leafNum;
    private String parentRegionName;
    private Long count;

	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}

}
