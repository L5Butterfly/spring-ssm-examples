/**
 * 
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.vo.PrivilegeVo;
import com.framework.web.base.vo.RolePrivilegeVo;
import com.framework.web.base.vo.RoleVo;
import com.framework.web.po.RolePo;


/**
 * @author Administrator
 *
 */
public interface IRoleDao {

	ArrayList<RoleVo> selectRoleListByPage(RoleVo role) throws BaseAppException;

	ArrayList<PrivilegeVo> queryPrivList4jsTreeAll(@Param("roleId") Long roleId) throws BaseAppException;
	
    ArrayList<PrivilegeVo> selectPrivListByRoleId(@Param("roleId") Long roleId) throws BaseAppException;

    int modifyRole(RolePo po) throws BaseAppException;

    int addRole(RolePo po) throws BaseAppException;

    int deleteRolePrivilege(Long roleId) throws BaseAppException;

    int addRolePrivilege(RolePrivilegeVo rolePrivilegeVo) throws BaseAppException;

    ArrayList<PrivilegeVo> selectPrivListByIds(Long[] privilegeIds) throws BaseAppException;

    RoleVo selectRoleByName(String roleName) throws BaseAppException;
}
