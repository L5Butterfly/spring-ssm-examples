package com.framework.web.base.vo;

import com.framework.web.po.PrivilegePo;


public class PrivilegeVo extends PrivilegePo {

    private static final long serialVersionUID = 1L;
    
    private Long roleId;
    
    private boolean leaf;
    
    public Long getRoleId() {
        return roleId;
    }

    
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }


    public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

}
