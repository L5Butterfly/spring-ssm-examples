/*
 * @ClassName UploadFile
 * @Description 
 * @version 1.0
 * @Date 2017-07-24 11:50:02
 */
package com.framework.web.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class UploadFilePo {
    /**
     * @Fields id null
     */
    private Long id;
    /**
     * @Fields fileName null
     */
    private String fileName;
    /**
     * @Fields fileType 00F:文件
00I:图片
00V:视频
     */
    private String fileType;
    /**
     * @Fields fileSize null
     */
    private Long fileSize;
    /**
     * @Fields filePath null
     */
    private String filePath;
    /**
     * @Fields createDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
     * @Fields status null
     */
    private String status;
    /**
     * @Fields domain null
     */
    private String domain;
    /**
     * @Fields vnoId null
     */
    private Long vnoId;
    /**
     * @Fields dataExt1 null
     */
    private String dataExt1;
    /**
     * @Fields statusDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    /**
     * @Fields staffId null
     */
    private Long staffId;
    /**
     * @Fields model 001:AP导入
002:客户资料
003:卡导入
004:对账文件
005:PORTAL广告
     */
    private String model;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Long getVnoId() {
        return vnoId;
    }

    public void setVnoId(Long vnoId) {
        this.vnoId = vnoId;
    }

    public String getDataExt1() {
        return dataExt1;
    }

    public void setDataExt1(String dataExt1) {
        this.dataExt1 = dataExt1;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    
}