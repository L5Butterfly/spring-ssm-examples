/*
 * @ClassName VersionListPo
 * @Description 
 * @version 1.0
 * @Date 2017-03-07 10:05:37
 */
package com.framework.web.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class VersionListPo {
    /**
     * @Fields versionCode 命名规则，如
安装基础版   V1.0.0
布丁              P1.0.0
     */
    private String versionCode;
    /**
     * @Fields type null
     */
    private String type;
    /**
     * @Fields releaseTime null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date releaseTime;
    /**
     * @Fields installTime null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date installTime;
    /**
     * @Fields description null
     */
    private String description;

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Date getInstallTime() {
        return installTime;
    }

    public void setInstallTime(Date installTime) {
        this.installTime = installTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}