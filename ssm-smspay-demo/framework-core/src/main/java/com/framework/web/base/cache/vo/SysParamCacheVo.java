package com.framework.web.base.cache.vo;

import com.framework.core.Constants;
import com.framework.core.cache.ICacheItem;
import com.framework.core.utils.Util;
import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.po.SysParamPo;

public class SysParamCacheVo extends SysParamPo implements ICacheItem {

	@Override
	public String key() {
		return this.getParamCode() + Constants.SPLIT_UNDERLINE + this.getVnoId();
	}

	public SysParamCacheVo() {
	}

	private String _status;

	public String get_status() {
		return _status;
	}

	public void set_status(String _status) {
		this._status = _status;
	}

	@Override
	public void setStatus(String status) {
		super.setStatus(status);
		this._status = Util.isEmpty(status) ? "" : DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_STATUS", status);
	}
}
