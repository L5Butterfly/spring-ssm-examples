/**
 * 
 */
package com.framework.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.framework.core.utils.Log4jUtil;


/**
 * @author Administrator
 *
 */
public class Log4jListener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		Log4jUtil.initLogConfig();
	}
}
