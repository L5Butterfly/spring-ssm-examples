package com.framework.web.base.cache.item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.log4j.Logger;

import com.framework.core.Constants;
import com.framework.core.cache.BaseCache;
import com.framework.core.cache.CacheFactory;
import com.framework.core.cache.ICacheProvider;
import com.framework.core.utils.Util;
import com.framework.web.base.cache.vo.DataDictionaryCacheVo;
import com.framework.web.base.cache.vo.DataDictionaryValueCacheVo;
import com.framework.web.po.DataDictionaryValuePo;
import com.google.inject.Singleton;

/**
 * 数据字典缓存聚合引用类
 *
 * @author xgf
 *
 */
@Singleton
public class DataDictionaryCache extends BaseCache<DataDictionaryCacheVo> {
	public Logger logger = Logger.getLogger(DataDictionaryCache.class);
	/** INSTANCE */
	public static DataDictionaryCache INSTANCE;

	static {
		INSTANCE = new DataDictionaryCache("DataDictionary", "IDataDictionaryDao", DataDictionaryCacheVo.class);
	}

	/** 附加 DataDictionaryValue Provider */
	private ICacheProvider<DataDictionaryValueCacheVo> iDDVProviderValue = CacheFactory.getCacheProvider("DataDictionaryValue","IDataDictionaryValueDao", DataDictionaryValueCacheVo.class, this.getClass());

	@Override
	public void delItem(String key) {
		DataDictionaryCacheVo DicPo = super.getItem(getCacheKey(key));
		for(DataDictionaryValueCacheVo valuePo:iDDVProviderValue.getAll()) {
			if(valuePo.getDicTypeCode().equals(DicPo.getDicTypeCode()))
				iDDVProviderValue.delItem(valuePo);
		}
		super.delItem(key);
	}

	@Override
	public void delItem(Long key) {
		DataDictionaryCacheVo DicPo = super.getItem(key);
		for(DataDictionaryValueCacheVo valuePo:iDDVProviderValue.getAll()) {
			if(valuePo.getDicTypeCode().equals(DicPo.getDicTypeCode()))
				iDDVProviderValue.delItem(valuePo);
		}
		super.delItem(key);
	}

	@Override
	public <E extends DataDictionaryCacheVo> void setItem(E DataDictionaryVo) {
		//先删除
		for(DataDictionaryValueCacheVo valuePo:iDDVProviderValue.getAll()) {
			if(valuePo.getDicTypeCode().equals(DataDictionaryVo.getDicTypeCode()))
				iDDVProviderValue.delItem(valuePo);
		}
		//再修改
		super.setItem(DataDictionaryVo);
		for(DataDictionaryValueCacheVo valuePo:DataDictionaryVo.getDataDictionaryValueAl()) {
				iDDVProviderValue.setItem(valuePo);
		}
	}
/**
 * 陶杰
 */
	
	public void delDic(String key){
		DataDictionaryCacheVo DicPo = super.getItem(getCacheKey(key));
		for(DataDictionaryCacheVo DictionaryPo:INSTANCE.getAll()){
			if(DicPo.getDicTypeCode().equals(DictionaryPo.getDicTypeCode())){
				INSTANCE.delItem(DictionaryPo);
			}
		}
		super.delItem(key);
	}
	public void delDic(Long key){
		DataDictionaryCacheVo DicPo = super.getItem(key);
		for(DataDictionaryCacheVo DictionaryPo:INSTANCE.getAll()){
			if(DicPo.getDicTypeCode().equals(DictionaryPo.getDicTypeCode())){
				INSTANCE.delItem(DictionaryPo);
			}
		}
		super.delItem(key);
	}
	public <E extends DataDictionaryCacheVo> void setDic(E DataDictionaryVo){
		//先删除
				for(DataDictionaryCacheVo DicPo:INSTANCE.getAll()) {
					if(DicPo.getDicTypeCode().equals(DataDictionaryVo.getDicTypeCode()))
						INSTANCE.delItem(DicPo);
				}
				//再修改
				super.setItem(DataDictionaryVo);
				INSTANCE.setItem(DataDictionaryVo);
	}
	
	@Override
	public DataDictionaryCacheVo  getItem(String key) {
        return super.getItem(getCacheKey(key));
    }

	@Override
	public void reload() {
		super.reload();
		iDDVProviderValue.reload();
		assembleDic();
	}

	@Override
	public void cleanData() {
		super.cleanData();
		iDDVProviderValue.cleanData();
	}
	/**
	 * DataDictionaryCache
	 */
	public DataDictionaryCache(String cacheName,String daoName, Class<DataDictionaryCacheVo> clazzCD) {
		super(cacheName,daoName, clazzCD);
		// 装配字典信息
		assembleDic();
	}

	/**
	 * assemble 将DataDictionaryValue 追加到DataDictionary
	 */
	private void assembleDic() {
		ArrayList<DataDictionaryCacheVo> al = this.getAll();
		ArrayList<DataDictionaryValueCacheVo> alv = iDDVProviderValue.getAll();
		//按照优先级排序，数据库查询的时候会导致乱序，页面展示不正常
		Collections.sort(alv,new Comparator<DataDictionaryValuePo>(){
            @Override
			public int compare(DataDictionaryValuePo arg0, DataDictionaryValuePo arg1) {
                return arg0.getPriority().compareTo(arg1.getPriority());
            }
        });

		ArrayList<DataDictionaryCacheVo> filledDataDictionaryVoList = new ArrayList<DataDictionaryCacheVo>();
		for (DataDictionaryCacheVo DicPo : al) {
			for ( DataDictionaryValueCacheVo DicValues : alv) {
				if (DicPo.getDicTypeCode().equals(DicValues.getDicTypeCode())
						&& DicPo.getLangType().equals(DicValues.getLangType())) {
					DicPo.getDataDictionaryValueAl().add(DicValues);
				}
			}
			filledDataDictionaryVoList.add(DicPo);
		}
		for(DataDictionaryCacheVo DicPo : filledDataDictionaryVoList) {
			super.setItem(DicPo);  //不能用this.set，ArrayList ConcurrentModificationException
		}
	}

	/**
	 * 根据字典值编码取值
	 *
	 * @param dicValueCode
	 * @return
	 */
	public DataDictionaryValuePo getDataDictionaryValue(String dicValueCode) {
		return iDDVProviderValue.getItem(getCacheKey(dicValueCode));
	}

	public ArrayList<DataDictionaryValueCacheVo> getDataDictionaryValueAll() {
		return iDDVProviderValue.getAll();
	}

	/**
	 * 根据字典类型编码取该类字典内容
	 *
	 * @param dicTypeCode
	 * @return
	 */
	public ArrayList<DataDictionaryValueCacheVo> getDataDictionaryValueGroup(String dicTypeCode) {
		DataDictionaryCacheVo DataDictionaryVo = this.getItem(dicTypeCode);
		return DataDictionaryVo == null ? new ArrayList<DataDictionaryValueCacheVo>() : DataDictionaryVo.getDataDictionaryValueAl();
	}

	/**
	 * 根据字典类型编码和字典值去别名
	 *
	 * @param dicTypeCode
	 * @param dicValue
	 * @return
	 */
	public String getDataDictionaryValueName(String dicTypeCode, String dicValue) {
		if(Util.isEmpty(dicValue) || Util.isEmpty(dicValue.trim()))
			return dicValue;
		ArrayList<DataDictionaryValueCacheVo> al = getDataDictionaryValueGroup(dicTypeCode);
		for (int i = 0; i < al.size(); i++) {
			if (al.get(i).getDicValue().equals(dicValue)) {
				return al.get(i).getDicValueName();
			}
		}
		return dicValue;
	}

	/**
	 * 根据字典类型编码和字典内容获取编码
	 *
	 * @param dicTypeCode
	 * @param dicValueName
	 * @return
	 */
	public String getDataDicValueCodeByName(String dicTypeCode, String dicValueName) {
		if(Util.isEmpty(dicValueName) || Util.isEmpty(dicValueName.trim()))
			return dicValueName;
		ArrayList<DataDictionaryValueCacheVo> al = getDataDictionaryValueGroup(dicTypeCode);
		for (int i = 0; i < al.size(); i++) {
			if (al.get(i).getDicValueName().equals(dicValueName)) {
				return al.get(i).getDicValueCode();
			}
		}
		return dicValueName;
	}
	
	/**
     * 根据字典类型编码和字典内容获取字典值
     *
     * @param dicTypeCode
     * @param dicValueName
     * @return
     */
    public String getDataDicValueByName(String dicTypeCode, String dicValueName) {
        if(Util.isEmpty(dicValueName) || Util.isEmpty(dicValueName.trim()))
            return dicValueName;
        ArrayList<DataDictionaryValueCacheVo> al = getDataDictionaryValueGroup(dicTypeCode);
        for (int i = 0; i < al.size(); i++) {
            if (al.get(i).getDicValueName().equals(dicValueName)) {
                return al.get(i).getDicValue();
            }
        }
        return dicValueName;
    }
    
	/**
	 * 根据不同的语言类型组装缓存key
	 * @param code code
	 * @return
	 */
	private String getCacheKey(String code){
		String langType = "zh_CN";
		return  code + Constants.SPLIT_UNDERLINE + langType;
	}
}
