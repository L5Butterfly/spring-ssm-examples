/**
 * 
 */
package com.framework.web.base.vo;

import java.io.Serializable;
import java.util.ArrayList;

import com.framework.core.session.UserInfoBean;
import com.framework.web.base.cache.item.VnoListCache;
import com.framework.web.po.MenuPo;
import com.framework.web.po.PrivilegePo;
import com.framework.web.po.StaffPo;

/**
 * @author Administrator
 *
 */
public class StaffLoginVo extends UserInfoBean implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long roleId;
	 
	private Long staffId;

	private Long vnoId;
	
	private String vnoName;
	
	private String roleName;
	    
	private StaffPo staffPo;

	private ArrayList<StaffRoleVo> roleList;
	
	private ArrayList<MenuPo> alMenuPo;

	private ArrayList<PrivilegePo> privilegePo;
	
	private String isManager;

	private String isSaler;
	
	public StaffPo getStaffPo() {
		return staffPo;
	}

	public void setStaffPo(StaffPo staffPo) {
		this.staffPo = staffPo;
		this.staffId=staffPo.getStaffId();
		super.setUserId(staffPo.getStaffId());
		super.setUserCode(staffPo.getStaffCode());
	}

	public ArrayList<MenuPo> getAlMenuPo() {
		return alMenuPo;
	}

	public void setAlMenuPo(ArrayList<MenuPo> alMenuPo) {
		this.alMenuPo = alMenuPo;
	}

	public ArrayList<PrivilegePo> getPrivilegePo() {
		return privilegePo;
	}

	public void setPrivilegePo(ArrayList<PrivilegePo> privilegePo) {
		this.privilegePo = privilegePo;
	}

	public ArrayList<StaffRoleVo> getRoleList() {
		return roleList;
	}

	public void setRoleList(ArrayList<StaffRoleVo> roleList) {
		this.roleList = roleList;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getVnoId() {
		return vnoId;
	}

	public void setVnoId(Long vnoId) {
		this.vnoId = vnoId;
		this.vnoName = VnoListCache.INSTANCE.getItem(vnoId).getVnoName();
	}

	public String getVnoName() {
		return vnoName;
	}

	public void setVnoName(String vnoName) {
		this.vnoName = vnoName;
	}

	public String getIsManager() {
		return isManager;
	}

	public void setIsManager(String isManager) {
		this.isManager = isManager;
	}

	public String getIsSaler() {
		return isSaler;
	}

	public void setIsSaler(String isSaler) {
		this.isSaler = isSaler;
	}
	
}
