package com.framework.web.base.vo;

import java.io.Serializable;

import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.po.TaskTimingCfgPo;

public class TaskTimingCfgVo extends TaskTimingCfgPo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String _status;

	public String get_status() {
		return _status;
	}

	@Override
	public void setStatus(String status){
		super.setStatus(status);
		this._status = DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_STATUS", status);
    }
	
	
}
