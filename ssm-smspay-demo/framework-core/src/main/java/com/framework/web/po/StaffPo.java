package com.framework.web.po;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.framework.core.log.IArgVo;
public class StaffPo implements IArgVo, Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    private Long orgnizationId;
    /**
    00A 有效<br />00X 失效<br />00U 归档
    */
    private String status;
    /**
    */
    private Long staffId;
    /**
    */
    private String wechatId;
    /**
    MD5加密过的
    */
    private String password;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date passExpDate;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
    */
    private String staffName;
    /**
    */
    private String email;
    /**
    */
    private Long vnoId;
    /**
    可从OA系统导入
    */
    private String staffCode;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    /**
    */
    private String mobileNum;
    public Long getOrgnizationId(){
        return this.orgnizationId;
    }
    public void setOrgnizationId(Long orgnizationId){
        this.orgnizationId=orgnizationId;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status=status;
    }
    public Long getStaffId(){
        return this.staffId;
    }
    public void setStaffId(Long staffId){
        this.staffId=staffId;
    }
    public String getWechatId(){
        return this.wechatId;
    }
    public void setWechatId(String wechatId){
        this.wechatId=wechatId;
    }
    public String getPassword(){
        return this.password;
    }
    public void setPassword(String password){
        this.password=password;
    }
    public Date getPassExpDate(){
        return this.passExpDate;
    }
    public void setPassExpDate(Date passExpDate){
        this.passExpDate=passExpDate;
    }
    
    public Date getCreateDate(){
        return this.createDate;
    }
    public void setCreateDate(Date createDate){
        this.createDate=createDate;
    }
    public String getStaffName(){
        return this.staffName;
    }
    public void setStaffName(String staffName){
        this.staffName=staffName;
    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email=email;
    }
    public Long getVnoId(){
        return this.vnoId;
    }
    public void setVnoId(Long vnoId){
        this.vnoId=vnoId;
    }
    public String getStaffCode(){
        return this.staffCode;
    }
    public void setStaffCode(String staffCode){
        this.staffCode=staffCode;
    }
    
    public Date getStatusDate(){
        return this.statusDate;
    }
    public void setStatusDate(Date statusDate){
        this.statusDate=statusDate;
    }
    public String getMobileNum(){
        return this.mobileNum;
    }
    public void setMobileNum(String mobileNum){
        this.mobileNum=mobileNum;
    }
	@Override
	public String getOperateArgs() {
		return "登陆账号:"+staffCode+",\n MD5加密密码:" + password;
	}
}
