/**
 * 
 */
package com.framework.web.interceptor;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.framework.core.exception.BaseAppException;
import com.framework.core.exception.SysErrCode;
import com.framework.core.page.Page;
import com.framework.core.session.UserInfoBean;
import com.framework.core.session.UserSessionInfo;
import com.framework.core.utils.Util;
import com.framework.web.WebConstants;

/**
 * @author Administrator
 *
 */
public class GlobalInterceptor implements HandlerInterceptor {

	private static final Logger logger = Logger.getLogger(GlobalInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
		doPageAndSort(req);
		
		//设置用户session保存的信息
		UserSessionInfo userInfo = new UserSessionInfo(req, res);
		UserInfoBean userBean = (UserInfoBean) req.getSession().getAttribute(WebConstants.USER_INFO_BEAN);
		userInfo.setUserBean(userBean);
		UserInfoBean managerBean = (UserInfoBean) req.getSession().getAttribute(WebConstants.STAFF_INFO);
		userInfo.setManagerBean(managerBean);
		UserSessionInfo.threadLocal.set(userInfo);
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse res, Object handler, ModelAndView model) throws Exception {
		 //移除分页信息
		Page.threadLocal.remove();
		
		UserSessionInfo.threadLocal.remove();
	}

	@Override
	public void afterCompletion(HttpServletRequest req, HttpServletResponse res, Object handler, Exception ex) throws Exception {
		 //移除分页信息
		Page.threadLocal.remove();
		doException(res, ex);
	}

	/**
	 * 处理分页查询的分页信息和排序信息
	 * @param req
	 */
	private void doPageAndSort(HttpServletRequest req) {
		String rows = req.getParameter("rows");
		String pageNo = req.getParameter("page");
		String sidx = req.getParameter("sidx");
		String sord = req.getParameter("sord");
		
		// 是否是分页查询
		if (Util.notEmpty(pageNo) && Util.notEmpty(rows)) {
			Page page = new Page();
			if (NumberUtils.isParsable(rows)) {
				page.setRows(NumberUtils.toInt(rows));
			}
			if (NumberUtils.isParsable(pageNo)) {
				page.setPageNo(NumberUtils.toInt(pageNo));
			}
			
			page.setSidx(sidx);
			page.setSord(sord);
			Page.threadLocal.set(page);
		}
	}
	
	/**
	 * 异常处理
	 * @param res
	 * @param ex
	 */
	private void doException(HttpServletResponse res, Exception ex){
		if (null == ex) {
			return ;	
		}

		// 设置ContentType
		res.setContentType("text/json;charset=UTF-8");
		res.setHeader("Cache-Control", "no-cache, must-revalidate");
		
		// 设置状态码
		res.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		
		// 避免乱码
		res.setCharacterEncoding(WebConstants.ENCODING_UTF8);
		try {
			String result = "{\"errcode\": \"{errcode}\", \"msg\": \"{msg}\"}";

			if (ex instanceof BaseAppException) {
				BaseAppException be = (BaseAppException) ex;
				result = result.replace("{errcode}", be.getCode()).replace("{msg}", be.getDesc());
			} else {
				result = result.replace("{errcode}", SysErrCode.UNKNOW_EXPCEPTION).replace("{msg}", "UnKnown error");
				logger.error(ex.getMessage(), ex);
			}
			
			ServletOutputStream out = res.getOutputStream();
			out.write(result.getBytes(WebConstants.ENCODING_UTF8));
			out.flush();
			out.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
