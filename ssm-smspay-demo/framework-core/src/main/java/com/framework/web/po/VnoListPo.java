/*
 * @ClassName VnoList
 * @Description 
 * @version 1.0
 * @Date 2017-05-11 11:10:35
 */
package com.framework.web.po;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class VnoListPo implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * @Fields vnoId null
     */
    private Long vnoId;
    /**
     * @Fields vnoCode null
     */
    private String vnoCode;
    /**
     * @Fields regionId null
     */
    private Long regionId;
    /**
     * @Fields parentVnoId null
     */
    private Long parentVnoId;
    /**
     * @Fields vnoIdPath VNO_ID PATH
_0_10_
用于保存VNO层级关系的完整路径
     */
    private String vnoIdPath;
    /**
     * @Fields vnoName null
     */
    private String vnoName;
    /**
     * @Fields vnoLevel 预留，目前只支持一级
     */
    private Long vnoLevel;
    /**
     * @Fields vnoDesc 目前只支持两级
     */
    private String vnoDesc;
    /**
     * @Fields status 00A 有效
00X 失效
00U 归档
     */
    private String status;
    /**
     * @Fields createDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
     * @Fields statusDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date statusDate;
    /**
     * @Fields contactPerson null
     */
    private String contactPerson;
    /**
     * @Fields contactPhone null
     */
    private String contactPhone;
    /**
     * @Fields mailAddr null
     */
    private String mailAddr;
    /**
     * @Fields address null
     */
    private String address;
    /**
     * @Fields staffId 每个VNO都有一个负责人，是我们公司负责这个企业的业务员，不是这个企业的法人。 取值为WBOSS中给这个业务员分配的操作员标识STAFF_ID。
     */
    private Long staffId;

    public Long getVnoId() {
        return vnoId;
    }

    public void setVnoId(Long vnoId) {
        this.vnoId = vnoId;
    }

    public String getVnoCode() {
        return vnoCode;
    }

    public void setVnoCode(String vnoCode) {
        this.vnoCode = vnoCode;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getParentVnoId() {
        return parentVnoId;
    }

    public void setParentVnoId(Long parentVnoId) {
        this.parentVnoId = parentVnoId;
    }

    public String getVnoIdPath() {
        return vnoIdPath;
    }

    public void setVnoIdPath(String vnoIdPath) {
        this.vnoIdPath = vnoIdPath;
    }

    public String getVnoName() {
        return vnoName;
    }

    public void setVnoName(String vnoName) {
        this.vnoName = vnoName;
    }

    public Long getVnoLevel() {
        return vnoLevel;
    }

    public void setVnoLevel(Long vnoLevel) {
        this.vnoLevel = vnoLevel;
    }

    public String getVnoDesc() {
        return vnoDesc;
    }

    public void setVnoDesc(String vnoDesc) {
        this.vnoDesc = vnoDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getMailAddr() {
        return mailAddr;
    }

    public void setMailAddr(String mailAddr) {
        this.mailAddr = mailAddr;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

}