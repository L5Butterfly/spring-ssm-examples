package com.framework.web.base.cache.vo;


import com.framework.core.cache.ICacheItem;
import com.framework.web.po.VnoListPo;

public class VnoListCacheVo extends VnoListPo implements ICacheItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String key() {
		return super.getVnoId()+"";
	}
}
