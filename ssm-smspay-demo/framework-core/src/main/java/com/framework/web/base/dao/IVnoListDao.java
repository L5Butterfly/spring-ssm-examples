/*
 * @ClassName IVnoListDao
 * @Description 
 * @version 1.0
 * @Date 2017-05-11 11:10:35
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.framework.core.exception.BaseAppException;
import com.framework.web.base.cache.vo.VnoListCacheVo;
import com.framework.web.base.vo.VnoListVo;
import com.framework.web.po.VnoListPo;

public interface IVnoListDao {
    /**
     * @Title deleteByPrimaryKey
     * @param vnoId
     * @return int
     */
    int deleteByPrimaryKey(Long vnoId);

    /**
     * @Title insert
     * @param record
     * @return int
     */
    int insert(VnoListPo record);

    /**
     * @Title insertSelective
     * @param record
     * @return int
     */
    int insertSelective(VnoListPo record);

    /**
     * @Title selectByPrimaryKey
     * @param vnoId
     * @return VnoList
     */
    VnoListVo selectByPrimaryKey(Long vnoId);

    /**
     * @Title updateByPrimaryKeySelective
     * @param record
     * @return int
     */
    int updateByPrimaryKeySelective(VnoListPo record);

    /**
     * @Title updateByPrimaryKey
     * @param record
     * @return int
     */
    int updateByPrimaryKey(VnoListPo record);
    
    /**
   	 * 查询缓存使用，方法名固定为getCacheList
   	 * @return
   	 */
   	ArrayList<VnoListCacheVo> getCacheList();
   	
   	/**
   	 * 
   	 * @param vo
   	 * @return
   	 */
   	ArrayList<VnoListVo> getVnoList4TreeLazy(VnoListVo vo);
   	
   	ArrayList<VnoListVo> querySubVnoList(Long vnoId);

	ArrayList<VnoListVo> queryVnoByVnoCode(String vnoCode);
	
    ArrayList<VnoListVo> selectVnoList4jsTree(VnoListVo vo) throws BaseAppException;
    
    ArrayList<VnoListVo> selectVnoListByRegionId(@Param("regionId") Long regionId) throws BaseAppException;
}