/*
 * @ClassName SmsSendLogPo
 * @Description 
 * @version 1.0
 * @Date 2017-03-07 10:04:19
 */
package com.framework.web.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class SmsSendLogPo {
    /**
     * @Fields smsId null
     */
    private Long smsId;
    /**
     * @Fields smsContent null
     */
    private String smsContent;
    /**
     * @Fields sendMobile null
     */
    private String sendMobile;
    /**
     * @Fields sendDate null
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date sendDate;
    /**
     * @Fields vnoId null
     */
    private Long vnoId;
    /**
     * @Fields sucFlag null
     */
    private String sucFlag;
    /**
     * @Fields spRet T 成功
F 失败
     */
    private String spRet;
    /**
     * @Fields failTrace null
     */
    private String failTrace;

    public Long getSmsId() {
        return smsId;
    }

    public void setSmsId(Long smsId) {
        this.smsId = smsId;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public String getSendMobile() {
        return sendMobile;
    }

    public void setSendMobile(String sendMobile) {
        this.sendMobile = sendMobile;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Long getVnoId() {
        return vnoId;
    }

    public void setVnoId(Long vnoId) {
        this.vnoId = vnoId;
    }

    public String getSucFlag() {
        return sucFlag;
    }

    public void setSucFlag(String sucFlag) {
        this.sucFlag = sucFlag;
    }

    public String getSpRet() {
        return spRet;
    }

    public void setSpRet(String spRet) {
        this.spRet = spRet;
    }

    public String getFailTrace() {
        return failTrace;
    }

    public void setFailTrace(String failTrace) {
        this.failTrace = failTrace;
    }
}