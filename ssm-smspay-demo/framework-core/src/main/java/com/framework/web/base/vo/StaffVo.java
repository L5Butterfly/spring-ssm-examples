/**
 * 
 */
package com.framework.web.base.vo;

import java.io.Serializable;

import com.framework.web.base.cache.item.DataDictionaryCache;
import com.framework.web.base.cache.item.VnoListCache;
import com.framework.web.po.StaffPo;

/**
 * @author Administrator
 *
 */
public class StaffVo extends StaffPo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String vnoName;
	
	private String _status;
	
	private String oldPass;
	
	private String newPass;
	
	private String roleIds;
	
	private Long[] staffIds;
	
	 
	public Long[] getStaffIds() {
		return staffIds;
	}

	public void setStaffIds(Long[] staffIds) {
		this.staffIds = staffIds;
	}

	@Override
	public void setStatus(String status){
		super.setStatus(status);
		this._status = DataDictionaryCache.INSTANCE.getDataDictionaryValueName("DIC_STATUS", status);
	}
	
    public String get_status() {
		return _status;
	}

	public String getVnoName() {
		return vnoName;
	}

	public void setVnoName(String vnoName) {
		this.vnoName = vnoName;
	}
    
	public void setVnoId(Long vnoId){
        super.setVnoId(vnoId);
        this.vnoName = VnoListCache.INSTANCE.getItem(vnoId).getVnoName();
    }

	public String getOldPass() {
		return oldPass;
	}

	public void setOldPass(String oldPass) {
		this.oldPass = oldPass;
	}

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
}
