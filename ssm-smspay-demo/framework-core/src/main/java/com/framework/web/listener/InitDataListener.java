/**
 * 
 */
package com.framework.web.listener;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.framework.core.Constants;
import com.framework.core.utils.SeqUtil;
import com.framework.web.WebConstants;

/**
 * @author Administrator
 *
 */
public abstract class InitDataListener implements ApplicationListener<ContextRefreshedEvent> {

	public static final Date SYSTEM_START_TIME = new Date();
	
	private static Logger logger = Logger.getLogger(InitDataListener.class);

	private String dbType;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		if ((!Constants.isProduction) || (Constants.IS_MAIN_PROCESS)) {
			logger.info("System load end, start load data");
			loadData();
		}
		
		SeqUtil.dbType = dbType;
		
		logger.fatal(WebConstants.WEB_APP_START_OK);
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	
	protected abstract void loadData();
}
