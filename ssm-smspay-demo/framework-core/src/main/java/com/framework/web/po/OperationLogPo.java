package com.framework.web.po;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.framework.core.log.IArgVo;
public class OperationLogPo implements IArgVo,Serializable { 
    private static final long serialVersionUID = 1L;

    /**
    */
    private String param;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
    S：成功<br />F：失败
    */
    private String operResult;
    /**
    不记录完整的服务方法，记录服务别名
    */
    private String svcName;
    /**
    */
    private String httpClient;
    /**
    */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date operTime;
    /**
    */
    private Long userId;
    /**
    */
    private Long costTime;
    /**
    */
    private String expCode;
    /**
    */
    private String sourIp;
    public String getParam(){
        return this.param;
    }
    public void setParam(String param){
        this.param=param;
    }
    public Date getCreateTime(){
        return this.createTime;
    }
    public void setCreateTime(Date createTime){
        this.createTime=createTime;
    }
    public String getOperResult(){
        return this.operResult;
    }
    public void setOperResult(String operResult){
        this.operResult=operResult;
    }
    public String getSvcName(){
        return this.svcName;
    }
    public void setSvcName(String svcName){
        this.svcName=svcName;
    }
    public String getHttpClient(){
        return this.httpClient;
    }
    public void setHttpClient(String httpClient){
        this.httpClient=httpClient;
    }
    public Date getOperTime(){
        return this.operTime;
    }
    public void setOperTime(Date operTime){
        this.operTime=operTime;
    }
    public Long getUserId(){
        return this.userId;
    }
    public void setUserId(Long userId){
        this.userId=userId;
    }
    public Long getCostTime(){
        return this.costTime;
    }
    public void setCostTime(Long costTime){
        this.costTime=costTime;
    }
    public String getExpCode(){
        return this.expCode;
    }
    public void setExpCode(String expCode){
        this.expCode=expCode;
    }
    public String getSourIp(){
        return this.sourIp;
    }
    public void setSourIp(String sourIp){
        this.sourIp=sourIp;
    }
	@Override
	public String getOperateArgs() {
		// TODO Auto-generated method stub
		return null;
	}
}
