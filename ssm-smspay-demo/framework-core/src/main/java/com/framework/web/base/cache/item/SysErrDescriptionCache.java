package com.framework.web.base.cache.item;

import com.framework.core.cache.BaseCache;
import com.framework.web.base.cache.vo.SysErrDescriptionCacheVo;
import com.google.inject.Singleton;

/**
 * SysErrDescriptionCache
 *
 * @author xgf
 *
 */
@Singleton
public class SysErrDescriptionCache extends BaseCache <SysErrDescriptionCacheVo>{

	/** INSTANCE */
	public static SysErrDescriptionCache INSTANCE;

	static {
		INSTANCE = new SysErrDescriptionCache("SysErrDescription", "ISysErrDescriptionDao", SysErrDescriptionCacheVo.class);
	}

	public SysErrDescriptionCache(String cacheName,String daoName, Class<SysErrDescriptionCacheVo> clazzCD) {
		super(cacheName, daoName, clazzCD);
	}

}
