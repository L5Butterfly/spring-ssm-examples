/**
 * 
 */
package com.framework.web.base.dao;

import java.util.ArrayList;

import com.framework.web.base.vo.OperationLogVo;

/**
 * @author Administrator
 *
 */
public interface IOperationLogDao {

	/**
	 * 保存操作日志
	 * @param logVo
	 */
	void saveOperationLog(OperationLogVo logVo) ;
	
	/**
	 * 查询操作日志
	 * @param logVo
	 * @return
	 */
	ArrayList<OperationLogVo> getLogListByPage(OperationLogVo logVo);
	
	/**
	 * 查询操作日志
	 * @param logVo
	 * @return
	 */
	ArrayList<OperationLogVo> queryStaffOperLogList4JqByPage(OperationLogVo operationLogVo);
}
