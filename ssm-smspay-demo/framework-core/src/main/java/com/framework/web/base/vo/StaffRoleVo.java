package com.framework.web.base.vo;

import java.io.Serializable;

import com.framework.web.po.StaffRolePo;


public class StaffRoleVo extends StaffRolePo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String roleName;
	private String staffName;
	private String staffCode;
	private String vnoName;
	private String loginPage;
    private String roleIds;

	public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }

    public String getLoginPage() {
		return loginPage;
	}

	public void setLoginPage(String loginPage) {
		this.loginPage = loginPage;
	}

	public String getVnoName() {
        return vnoName;
    }

    public void setVnoName(String vnoName) {
        this.vnoName = vnoName;
    }

    public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
